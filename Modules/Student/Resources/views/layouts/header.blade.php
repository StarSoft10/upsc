<header class="block">
    <ul class="header-menu horizontal-list nav">
        <li>
            <a class="header-menu-tab active" id="information-tab" data-toggle="tab" href="#information" role="tab" aria-controls="information" aria-selected="false">
                <span class="icon entypo-user scnd-font-color"></span> {{ __('pages.profile') }}
            </a>
        </li>

        @can('view_borderou_note')
            @if(count($notes) > 0)
                <li>
                    <a class="header-menu-tab" id="notes-tab" data-toggle="tab" href="#notes" role="tab" aria-controls="notes" aria-selected="false">
                        <span class="icon entypo-book-open scnd-font-color"></span> {{ __('pages.notes') }}
                    </a>
                </li>
            @endif
        @endcan

        <li>
            <a class="header-menu-tab" id="settings-tab" data-toggle="tab" href="#settings" role="tab" aria-controls="settings" aria-selected="false">
                <span class="icon entypo-cog scnd-font-color"></span> {{ __('pages.settings') }}
            </a>
        </li>

        {{--<li>--}}
            {{--<a class="header-menu-tab" id="orar-tab" data-toggle="tab" href="#orar" role="tab" aria-controls="orar" aria-selected="false">--}}
                {{--<span class="icon entypo-calendar scnd-font-color"></span> {{ __('pages.orar') }}--}}
            {{--</a>--}}
        {{--</li>--}}
    </ul>
    <div class="profile-menu">
        <div class="profile-picture small-profile-picture">
            @if(Auth::user()->photo == null || Auth::user()->photo == '')
                <img src="{{ asset('images/default_photo.jpg') }}" alt="Photo" id="header_photo">
            @else
                <img src="{{ asset('storage/users-photo/' . Auth::user()->photo) }}" alt="Photo" id="header_photo">
            @endif
        </div>
        <p>
            <a href="{{ route('logout') }}">
                <span class="entypo-logout scnd-font-color" style="font-size: 25px;"></span>
            </a>
        </p>
    </div>
</header>


