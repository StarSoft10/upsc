<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8" />
    <title>
        @yield('title', '')
    </title>
    <meta name="description" content="UPSC">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/student-profile.css') }}">
    {{--<link rel="stylesheet" href="{{ asset('css/full-calendar/main.min.css') }}">--}}
    <link rel="icon" type="image/png" sizes="64x64" href="{{asset('images/icon.png')}}">
    @yield('custom-css')
</head>
<body>
    <div class="container-fluid">
        @include('student::layouts.header')

        @if(session()->has('success') || session()->has('warning') || session()->has('error'))
            <div class="row">
                <div class="col-sm-12">
                    @if(session()->has('success'))
                        <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                            <span class="badge badge-pill badge-success">{{ __('pages.success') }}</span>
                            {{ session()->get('success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif

                    @if(session()->has('warning'))
                        <div class="sufee-alert alert with-close alert-warning alert-dismissible fade show">
                            <span class="badge badge-pill badge-warning">{{ __('pages.warning') }}</span>
                            {{ session()->get('warning') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif

                    @if(session()->has('error'))
                        <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                            <span class="badge badge-pill badge-danger">{{ __('pages.error') }}</span>
                            {{ session()->get('error') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif
                </div>
            </div>
        @endif

        @yield('content')
    </div>

    <script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('vendors/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/notify.min.js') }}"></script>
    {{--<script src="{{ asset('js/full-calendar/main.min.js') }}"></script>--}}
    {{--<script src="{{ asset('js/full-calendar/locales-all.min.js') }}"></script>--}}
    @yield('custom-scripts')
</body>
</html>
