@extends('student::layouts.app')

@section('title')
    {{ __('pages.profile') }} : {{ $student->first_name }} {{ $student->last_name }}
@endsection

@section('content')

    <div class="tab-content">
        <div class="tab-pane fade active show" id="information" role="tabpanel" aria-labelledby="information-tab">
            <div class="row">
                <div class="col-md-3">
                    <div class="profile block">
                        <label for="profile_image" style="display: block;position: absolute;left: 3%;top: 0;">
                            <a class="add-button" style="cursor: pointer;">
                                <span class="icon entypo-plus scnd-font-color"></span>
                            </a>
                        </label>
                        <input type="file" name="profile_image" id="profile_image" accept="image/*" style="display: none;">

                        @if(Auth::user()->photo == null || Auth::user()->photo == '')
                            {{--<img src="{{ asset('images/default_photo.jpg') }}" alt="Photo" id="profile_image_old" style="width: 100%;clip-path: circle();">--}}
                            <img src="{{ asset('images/default_photo.jpg') }}" alt="Photo" id="profile_image_old" style="clip-path: circle();">
                        @else
                            <img src="{{ asset('storage/users-photo/' . Auth::user()->photo) }}" alt="Photo" id="profile_image_old">
                        @endif

                        <img id="image_preview" style="display: none;" src="">

                        <h1 class="user-name">{{ Auth::user()->student->first_name }} {{ Auth::user()->student->last_name }}</h1>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="menu-box block">
                        <h2 class="titular">{{ __('pages.personal_information') }}</h2>
                        <ul class="menu-box-menu">
                            <li>
                                <a class="menu-box-tab" href="javascript:void(0)">
                                    <span class="icon entypo-mail scnd-font-color"></span>
                                    <span>{{ __('pages.email') }}:</span>
                                    <div class="menu-box-number">{{ $student->user->email }}</div>
                                </a>
                            </li>
                            <li>
                                <a class="menu-box-tab" href="javascript:void(0)">
                                    <span class="icon entypo-mail scnd-font-color"></span>
                                    <span>{{ __('pages.personal_email') }}:</span>
                                    <div class="menu-box-number">{{ $student->personal_email }}</div>
                                </a>
                            </li>
                            <li>
                                <a class="menu-box-tab" href="javascript:void(0)">
                                    <span class="icon entypo-mobile scnd-font-color"></span>
                                    <span>{{ __('pages.mobile') }}:</span>
                                    <div class="menu-box-number">{{ $student->mobile }}</div>
                                </a>
                            </li>
                            <li>
                                <a class="menu-box-tab" href="javascript:void(0)">
                                    <span class="icon entypo-user scnd-font-color"></span>
                                    <span>{{ __('pages.patronymic') }}:</span>
                                    <div class="menu-box-number">{{ $student->patronymic }}</div>
                                </a>
                            </li>
                            <li>
                                <a class="menu-box-tab" href="javascript:void(0)">
                                    <span class="icon entypo-vcard scnd-font-color"></span>
                                    <span>{{ __('pages.idnp') }}:</span>
                                    <div class="menu-box-number">{{ $student->idnp }}</div>
                                </a>
                            </li>
                            <li>
                                <a class="menu-box-tab" href="javascript:void(0)">
                                    <span class="icon @if($student->gender == 'M') entypo-tools @else entypo-heart @endif scnd-font-color"></span>
                                    <span>{{ __('pages.gender') }}:</span>
                                    <div class="menu-box-number">{{ $student->gender }}</div>
                                </a>
                            </li>
                            <li>
                                <a class="menu-box-tab" href="javascript:void(0)">
                                    <span class="icon entypo-calendar scnd-font-color"></span>
                                    <span>{{ __('pages.birth_date') }}:</span>
                                    <div class="menu-box-number">{{ $student->birth_date }}</div>
                                </a>
                            </li>
                            <li>
                                <a class="menu-box-tab" href="javascript:void(0)">
                                    <span class="icon entypo-home scnd-font-color"></span>
                                    <span>{{ __('pages.city') }}:</span>
                                    <div class="menu-box-number">{{ $student->city }}</div>
                                </a>
                            </li>
                            <li>
                                <a class="menu-box-tab" href="javascript:void(0)">
                                    <span class="icon entypo-home scnd-font-color"></span>
                                    <span>{{ __('pages.address') }}:</span>
                                    <div class="menu-box-number">{{ $student->address }}</div>
                                </a>
                            </li>
                            <li>
                                <a class="menu-box-tab" href="javascript:void(0)">
                                    <span class="icon entypo-globe scnd-font-color"></span>
                                    <span>{{ __('pages.citizenship') }}:</span>
                                    <div class="menu-box-number">{{ $student->citizenship }}</div>
                                </a>
                            </li>
                            <li>
                                <a class="menu-box-tab" href="javascript:void(0)">
                                    <span class="icon entypo-globe scnd-font-color"></span>
                                    <span>{{ __('pages.nationality') }}:</span>
                                    <div class="menu-box-number">{{ $student->nationality }}</div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-5">
                    <div class="menu-box block">
                        <h2 class="titular">{{ __('pages.studies') }}</h2>
                        <ul class="menu-box-menu">
                            <li>
                                <a class="menu-box-tab" href="javascript:void(0)">
                                    <span class="icon entypo-flow-tree scnd-font-color"></span>
                                    <span>{{ __('pages.cycle') }}:</span>
                                    <div class="menu-box-number">{{ $student->cycle->name }}</div>
                                </a>
                            </li>
                            <li>
                                <a class="menu-box-tab" href="javascript:void(0)">
                                    <span class="icon entypo-suitcase scnd-font-color"></span>
                                    <span>{{ __('pages.faculty') }}:</span>
                                    <div class="menu-box-number">{{ $student->faculty->name }}</div>
                                </a>
                            </li>
                            <li>
                                <a class="menu-box-tab" href="javascript:void(0)">
                                    <span class="icon entypo-keyboard scnd-font-color"></span>
                                    <span>{{ __('pages.speciality') }}:</span>
                                    <div class="menu-box-number">{{ $student->speciality->name }}</div>
                                </a>
                            </li>
                            <li>
                                <a class="menu-box-tab" href="javascript:void(0)">
                                    <span class="icon entypo-users scnd-font-color"></span>
                                    <span>{{ __('pages.group') }}:</span>
                                    <div class="menu-box-number">{{ $student->group->name }}</div>
                                </a>
                            </li>
                            <li>
                                <a class="menu-box-tab" href="javascript:void(0)">
                                    <span class="icon entypo-calendar scnd-font-color"></span>
                                    <span>{{ __('pages.year_of_admission') }}:</span>
                                    <div class="menu-box-number">{{ $student->year_of_admission }}</div>
                                </a>
                            </li>

                            <li>
                                <a class="menu-box-tab" href="javascript:void(0)">
                                    <span class="icon @if(strtolower($student->study) == 'zi') entypo-adjust @else entypo-moon @endif scnd-font-color"></span>
                                    <span>{{ __('pages.study') }}:</span>
                                    <div class="menu-box-number">@if($student->study == 1) {{ __('pages.morning') }} @else {{ __('pages.afternoon') }} @endif</div>
                                </a>
                            </li>
                            <li>
                                <a class="menu-box-tab" href="javascript:void(0)">
                                    <span class="icon entypo-cc-nc scnd-font-color"></span>
                                    <span>{{ __('pages.budget') }}:</span>
                                    <div class="menu-box-number">@if($student->budget == 1) {{ __('pages.yes') }} @else {{ __('pages.no') }} @endif</div>
                                </a>
                            </li>
                            <li>
                                <a class="menu-box-tab" href="javascript:void(0)">
                                    <span class="icon entypo-megaphone scnd-font-color"></span>
                                    <span>{{ __('pages.expelled') }}:</span>
                                    <div class="menu-box-number">@if($student->expelled == 1) {{ __('pages.yes') }} @else {{ __('pages.no') }} @endif</div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        @can('view_borderou_note')
            @if(count($notes) > 0)
                <div class="tab-pane fade" id="notes" role="tabpanel" aria-labelledby="notes-tab">
                    <div class="row">
                        <div class="col-md-12 mb-25px">
                            <ul class="header-menu horizontal-list nav">
                                @php $i = 1;@endphp
                                @foreach($notes as $displayName => $semesters)
                                    <li>
                                        <a class="header-menu-tab @if($i == 1) active @endif" id="info_{{ $i }}-tab" data-toggle="tab"
                                            href="#info_{{ $i }}" role="tab" aria-controls="info_{{ $i }}" aria-selected="false">
                                            {{ $displayName }}
                                        </a>
                                    </li>
                                    @php $i++;@endphp
                                @endforeach
                            </ul>
                        </div>

                        <div class="col-md-12">
                            <div class="tab-content">
                                @php $j = 1;@endphp
                                @foreach($notes as $displayName => $semesters)
                                    <div class="tab-pane fade @if($j == 1) active show @endif" id="info_{{ $j }}" role="tabpanel" aria-labelledby="info_{{ $j }}-tab">

                                        <div class="row">
                                            <div class="col-md-12 mb-25px">
                                                <ul class="header-menu horizontal-list nav">
                                                    @php $i = 1;@endphp
                                                    @foreach($semesters as $semester => $borderouNotes)
                                                        <li>
                                                            <a class="header-menu-tab @if($i == 1) active @endif" id="sem_{{ $j }}_{{ $i }}-tab" data-toggle="tab"
                                                               href="#sem_{{ $j }}_{{ $i }}" role="tab" aria-controls="sem_{{ $j }}_{{ $i }}" aria-selected="false">
                                                                {{ $semester }}
                                                            </a>
                                                        </li>
                                                        @php $i++;@endphp
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tab-content">
                                                    @php $i = 1;@endphp
                                                    @foreach($semesters as $semester => $borderouNotes)
                                                        <div class="tab-pane fade @if($i == 1) active show @endif" id="sem_{{ $j }}_{{ $i }}" role="tabpanel" aria-labelledby="sem_{{ $j }}_{{ $i }}-tab">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <table class="table table-sm">
                                                                        <colgroup>
                                                                            <col span="1" style="width: 40%;">
                                                                            <col span="1" style="width: 20%;">
                                                                            <col span="1" style="width: 20%;">
                                                                            <col span="1" style="width: 20%;">
                                                                        </colgroup>
                                                                        <thead>
                                                                        <tr>
                                                                            <th style="border-top: none;">{{ __('pages.course') }}</th>
                                                                            <th style="border-top: none;">{{ __('pages.semester_note') }}</th>
                                                                            <th style="border-top: none;">{{ __('pages.exam_note') }}</th>
                                                                            <th style="border-top: none;">{{ __('pages.final_note') }}</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        @if(count($borderouNotes) > 0)
                                                                            @foreach($borderouNotes as $key => $borderouNote)
                                                                                <tr>
                                                                                    <th>{{ $borderouNote['courseName'] }}</th>
                                                                                    <td>{{ $borderouNote['semesterNote'] }}</td>
                                                                                    <td>{{ $borderouNote['examNote'] }}</td>
                                                                                    <td>{{ $borderouNote['finalNote'] }}</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        @else
                                                                            <tr class="text-center"><td colspan="4">{{ __('messages.not_found_any_notes') }}</td></tr>
                                                                        @endif
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @php $i++;@endphp
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @php $j++;@endphp
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endcan

        <div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="settings-tab">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="account block">
                        <h2 class="titular">{{ __('messages.change_your_credentials') }}</h2>
                        {!! Form::model($user, ['method' => 'PATCH', 'route' => ['student.update'], 'autocomplete' => 'off']) !!}
                            {!! Form::hidden('user', $user->id) !!}

                            <p class="wrapper-email">
                                {!! Form::email('email', null, ['class' => 'email text-input', 'required' => 'required', 'placeholder' => __('pages.email')]) !!}
                            </p>
                            <p class="wrapper-email">
                                {!! Form::email('personal_email', null, ['class' => 'email text-input', 'placeholder' => __('pages.personal_email')]) !!}
                            </p>
                            <p class="wrapper-password">
                                {!! Form::input('password', 'password', null, ['class' => 'email text-input', 'placeholder' => __('pages.password')]) !!}
                            </p>
                            <p class="wrapper-password">
                                {!! Form::input('password', 'password_confirmation', null, ['class' => 'email text-input', 'placeholder' => __('pages.confirm_password')]) !!}
                            </p>
                            <p>
                                {!! Form::submit(__('pages.update'), ['class' => 'btn btn-success btn-lg mb-30px']) !!}
                            </p>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        {{--<div class="tab-pane fade" id="orar" role="tabpanel" aria-labelledby="orar-tab">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    {{--<div id='calendar'></div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
@endsection

@section('custom-css')
    <style>
        .notifyjs-bootstrap-success {
            background-color: #3fcd05!important;
        }
        .notifyjs-bootstrap-warn {
            background-color: #db2c1a!important;
        }
        #profile_image_old, #image_preview {
            width: 100%;
            border-radius: 50%;
            overflow: hidden;
            border: 5px solid #0350d2;
            margin-left: auto;
            margin-right: auto;
            display: block;
        }
        #header_photo {
            width: 40px;
            height: 40px;
            border-radius: 50%;
            overflow: hidden;
        }
        {{--#calendar {--}}
            {{--color: white;--}}
        {{--}--}}
    </style>
@endsection

@section('custom-scripts')
    {{--<script>--}}
        {{--$(function () {--}}
            {{--let calendar = new FullCalendar.Calendar('#calendar', {--}}
                {{--locale: '{{ LaravelLocalization::getCurrentLocale() }}'--}}
            {{--});--}}

            {{--calendar.render();--}}
        {{--})--}}
        {{--document.addEventListener('DOMContentLoaded', function() {--}}
            {{--var calendarEl = document.getElementById('calendar');--}}
            {{--var calendar = new FullCalendar.Calendar(calendarEl, {--}}
                {{--initialView: 'dayGridMonth',--}}
                {{--locale: '{{ LaravelLocalization::getCurrentLocale() }}',--}}
                {{--defaultView: 'listWeek'--}}
            {{--});--}}
            {{--calendar.render();--}}
        {{--});--}}
    {{--</script>--}}

    <script>
        $('body').on('change', '#profile_image', function () {
            let formData = new FormData();
            let profileImage = $('#profile_image');

            if (profileImage[0].files.length === 0) {
                alert('UPLOAD FILE');
                return false;
            }

            formData.append('photo', profileImage.prop('files')[0]);

            let reader = new FileReader();
            reader.onload = function(){
                let output = document.getElementById('image_preview');
                let output2 = document.getElementById('header_photo');
                output.src = reader.result;
                output2.src = reader.result;
            };
            reader.readAsDataURL(event.target.files[0]);
            $('#image_preview').show();
            $('#profile_image_old').hide();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                async: true,
                url: '{{ route('student.changePhoto')}}',
                type: 'POST',
                cache: false,
                contentType: false,
                processData: false,
                data: formData,
                success: function (response) {
                    profileImage.val('');
                    if (response.type === 'warning') {
                        $.notify(response.message, {
                            autoHide: true,
                            autoHideDelay: 5000,
                            globalPosition: 'top right',
                            style: 'bootstrap',
                            className: 'warn'
                        });
                    } else {
                        $.notify(response.message, {
                            autoHide: true,
                            autoHideDelay: 5000,
                            globalPosition: 'top right',
                            style: 'bootstrap',
                            className: 'success'
                        });
                    }
                }
            });
        })
    </script>
@endsection
