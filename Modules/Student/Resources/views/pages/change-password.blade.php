<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8" />
    <title>
        {{ __('pages.profile') }} : {{ $student->first_name }} {{ $student->last_name }}
    </title>
    <meta name="description" content="UPSC">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/student-profile.css') }}">
    {{--<link rel="stylesheet" href="{{ asset('css/full-calendar/main.min.css') }}">--}}
    <link rel="icon" type="image/png" sizes="64x64" href="{{asset('images/icon.png')}}">
    <style>
        .notifyjs-bootstrap-success {
            background-color: #3fcd05!important;
        }
        .notifyjs-bootstrap-warn {
            background-color: #db2c1a!important;
        }
        #profile_image_old, #image_preview {
            width: 100%;
            border-radius: 50%;
            overflow: hidden;
            border: 5px solid #0350d2;
            margin-left: auto;
            margin-right: auto;
            display: block;
        }
        #header_photo {
            width: 40px;
            height: 40px;
            border-radius: 50%;
            overflow: hidden;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <header class="block">
        <div class="profile-menu">
            <div class="profile-picture small-profile-picture">
                @if(Auth::user()->photo == null || Auth::user()->photo == '')
                    <img src="{{ asset('images/default_photo.jpg') }}" alt="Photo" id="header_photo">
                @else
                    <img src="{{ asset('storage/users-photo/' . Auth::user()->photo) }}" alt="Photo" id="header_photo">
                @endif
            </div>
            <p>
                <a href="{{ route('logout') }}">
                    <span class="entypo-logout scnd-font-color" style="font-size: 25px;"></span>
                </a>
            </p>
        </div>
    </header>

    @if(session()->has('success') || session()->has('warning') || session()->has('error'))
        <div class="row">
            <div class="col-sm-12">
                @if(session()->has('success'))
                    <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                        <span class="badge badge-pill badge-success">{{ __('pages.success') }}</span>
                        {{ session()->get('success') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                @endif

                @if(session()->has('warning'))
                    <div class="sufee-alert alert with-close alert-warning alert-dismissible fade show">
                        <span class="badge badge-pill badge-warning">{{ __('pages.warning') }}</span>
                        {{ session()->get('warning') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                @endif

                @if(session()->has('error'))
                    <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                        <span class="badge badge-pill badge-danger">{{ __('pages.error') }}</span>
                        {{ session()->get('error') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                @endif
            </div>
        </div>
    @endif

    <div class="tab-content">
        <div class="tab-pane fade active show" id="settings" role="tabpanel" aria-labelledby="settings-tab">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <div class="account block">
                        <h2 class="titular">{{ __('messages.change_your_password') }}</h2>
                        {!! Form::open(['route' => 'student.changePassword', 'method' => 'POST', 'autocomplete' => 'off']) !!}
                            {!! Form::hidden('user', $user->id) !!}
                            <p class="wrapper-password">
                                {!! Form::input('password', 'password', null, ['class' => 'email text-input', 'placeholder' => __('pages.password'), 'required' => 'required']) !!}
                            </p>
                            <p class="wrapper-password">
                                {!! Form::input('password', 'password_confirmation', null, ['class' => 'email text-input', 'placeholder' => __('pages.confirm_password'), 'required' => 'required']) !!}
                            </p>
                            <p>
                                {!! Form::submit(__('pages.update'), ['class' => 'btn btn-success btn-lg mb-30px']) !!}
                            </p>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('vendors/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ asset('vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/notify.min.js') }}"></script>
</body>
</html>

