<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

Route::group([
    'prefix' => LaravelLocalization::setLocale() . '/student',
    'middleware' => ['auth', 'role:student', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'localize']
], function () {
    Route::get('/', 'StudentController@index')->name('student');
    Route::patch('update', 'StudentController@update')->name('student.update');
    Route::post('changePhoto', 'StudentController@changePhoto')->name('student.changePhoto');
    Route::post('changePassword', 'StudentController@changePassword')->name('student.changePassword');
});