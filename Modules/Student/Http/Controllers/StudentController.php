<?php

namespace Modules\Student\Http\Controllers;

use App\Entities\Borderou;
use App\Entities\BorderouNote;
use App\Entities\Semester;
use App\Entities\Student;
use App\Entities\User;
use App\Helpers\AppHelper;
use App\Helpers\StudentHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\File;
use Validator;

class StudentController extends Controller
{
    public $helper;
    public $studentHelper;

    public function __construct()
    {
        $this->middleware(['auth', 'role:student']);
        $this->middleware('check_if_blocked');
        $this->helper = new AppHelper();
        $this->studentHelper = new StudentHelper();
    }

    public function index()
    {
        $student = Auth::user()->student;
        $user = Auth::user();

        if($student->password_changed == 0){
            return view('student::pages.change-password', compact('user', 'student'));
        }

        $notes = [];

        if (Auth::user()->hasPermissionTo('view_borderou_note')) {
            $notes = $this->studentHelper->getStudentNotes($student);
        }

//        dd($notes);

        return view('student::index', compact('user', 'student', 'notes'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => [
                'required',
                'email',
                'regex:/^[a-z0-9\@\.\_\-]+$/u',
                Rule::unique('users')->ignore($request->user, 'id')
            ],
            'personal_email' => 'email',
            'password' => 'confirmed'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        if (trim($request->password) !== '') {
            Student::where('id', Auth::user()->student->id)->update([
                'password_changed' => 1,
                'personal_email' => $request->personal_email,
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);

            User::where('id', $request->user)->update([
                'email' => mb_strtolower(trim($request->email), 'UTF-8'),
                'password' => Hash::make($request->password),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        } else {
            Student::where('id', Auth::user()->student->id)->update([
                'personal_email' => $request->personal_email,
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);

            User::where('id', $request->user)->update([
                'email' => mb_strtolower(trim($request->email), 'UTF-8'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }

        return back()->with('success', __('messages.information_update_successfully'));
    }

    public function changePhoto(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'photo' => 'required|mimes:jpeg,png,jpg,gif,svg|max:10000'
        ]);


        if ($validator->fails()) {
            return response()->json(['type' => 'warning', 'message' => $validator->errors()->first()]);
        }

        if (!$request->hasFile('photo')) {
            return response()->json(['type' => 'warning', 'message' => __('messages.photo_is_required')]);
        }

        $authUser = Auth::user();
        $photoName = $authUser->photo;
        if (Storage::disk('public')->exists('users-photo/' . $photoName)) {
            Storage::disk('public')->delete('users-photo/' . $photoName);
        }

        $photo = $request->file('photo');
        $fileName = time() . $authUser->id . '.' . $photo->getClientOriginalExtension();
        Storage::disk('public')->put('users-photo/' . $fileName, File::get($photo));
        $photoName = $fileName;

        User::where('id', $authUser->id)->update([
            'photo' => $photoName,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return response()->json(['type' => 'success', 'message' => __('messages.information_update_successfully')]);
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'confirmed|required'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        if (trim($request->password) == '' || trim($request->password) == null) {
            return back()->with('error', __('messages.something_wrong_try_again_after_time'));
        }

        Student::where('id', Auth::user()->student->id)->update([
            'password_changed' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        User::where('id', $request->user)->update([
            'password' => Hash::make($request->password),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return redirect()->route('student')->with('success', __('messages.information_update_successfully'));
    }
}
