<?php

namespace Modules\Rector\Http\Controllers;

use App\Entities\AllowedFacultiesByModel;
use App\Entities\Chair;
use App\Entities\Faculty;
use App\Entities\User;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;
use Illuminate\Database\Eloquent\Builder;

class ChairController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->middleware(['auth', 'role:rector']);
        $this->middleware('check_if_blocked');
        $this->helper = new AppHelper();
    }

    public function index(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_chair')) {
            return redirect()->route('rector.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $email = !empty($request->email) ? trim($request->email) : '';
        $phone = !empty($request->phone) ? trim($request->phone) : '';

        $chairs = Chair::where('deleted', 0)->orderBy('last_name')->orderBy('first_name')->paginate(10);
        $chairs->appends([
            'name' => $name,
            'email' => $email,
            'phone' => $phone
        ]);

        $permissions = $this->helper->getAllowedPermissionsByModel('chair');
        $faculties = Faculty::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        return view('rector::chairs.index', compact('chairs', 'name', 'email', 'phone', 'permissions',
            'faculties'));
    }

    public function search(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_chair')) {
            return redirect()->route('rector.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $email = !empty($request->email) ? trim($request->email) : '';
        $phone = !empty($request->phone) ? trim($request->phone) : '';

        $chairs = Chair::where('deleted', 0)->with('user')
            ->whereHas('user', function (Builder $query) use ($email) {
                if ($email !== '') {
                    $query->where('email', 'LIKE', '%' . trim($email) . '%');
                }
            })->where(function ($query) use ($name, $phone) {
                if ($name !== '') {
                    $query->where('first_name', 'LIKE', '%' . trim($name) . '%');
                    $query->orWhere('last_name', 'LIKE', '%' . trim($name) . '%');
                }
                if ($phone !== '') {
                    $query->where('phone', 'LIKE', '%' . trim($phone) . '%');
                }
            })->orderBy('last_name')->orderBy('first_name')->paginate(10);
        $chairs->appends([
            'name' => $name,
            'email' => $email,
            'phone' => $phone
        ]);

        $permissions = $this->helper->getAllowedPermissionsByModel('chair');
        $faculties = Faculty::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        return view('rector::chairs.index', compact('chairs', 'name', 'email', 'phone', 'permissions',
            'faculties'));
    }

    public function store(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('create_chair')) {
            return redirect()->route('rector.not-found');
        }

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users|regex:/^[a-z0-9\@\.\_\-]+$/u',
            'password' => 'required|confirmed',
            'permissions' => 'required|array|min:1',
            'faculties' => 'required|array|min:1'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $user = User::create([
            'name' => $request->first_name . ' ' . $request->last_name,
            'email' => mb_strtolower(trim($request->email), 'UTF-8'),
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make($request->password)
        ]);
        $user->assignRole('chair');
        foreach ($request->permissions as $permission) {
            $user->givePermissionTo($permission);
        }

        $chair = Chair::create([
            'user_id' => $user->id,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone
        ]);

        AllowedFacultiesByModel::create([
            'model_id' => $user->id,
            'faculties' => json_encode($request->faculties)
        ]);

        $this->helper->addLog('rector', Auth::user()->rector->id, 'chairs', 1, 'DB id: [' . $chair->id . ']');

        return back()->with('success', __('messages.chair_created_successfully'));
    }

    public function show($id)
    {
        if (!Auth::user()->hasPermissionTo('view_chair')) {
            return redirect()->route('rector.not-found');
        }

        $chair = Chair::where('id', $id)->where('deleted', 0)->first();

        if (!$chair) {
            return back()->with('warning', __('messages.chair_not_found'));
        }
        $chairPermissions = [];
        foreach ($chair->user->getDirectPermissions() as $permission) {
            $chairPermissions[] = $permission->id;
        }
        $chair->permissions = $chairPermissions;

        $permissions = $this->helper->getAllowedPermissionsByModel('chair');
        $faculties = Faculty::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $allowedFaculties = AllowedFacultiesByModel::where('model_id', $chair->user->id)->first();
        $allowedFacultiesArray = [];
        if($allowedFaculties) {
            $allowedFacultiesArray = json_decode($allowedFaculties->faculties);
        }

        return view('rector::chairs.show', compact('chair', 'permissions', 'chairPermissions',
            'faculties', 'allowedFacultiesArray'));
    }

    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermissionTo('edit_chair')) {
            return redirect()->route('rector.not-found');
        }

        $chair = Chair::where('id', $id)->where('deleted', 0)->first();
        if (!$chair) {
            return back()->with('warning', __('messages.chair_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => [
                'required',
                'email',
                'regex:/^[a-z0-9\@\.\_\-]+$/u',
                Rule::unique('users')->ignore(User::findOrFail($chair->user->id)),
            ],
            'password' => 'confirmed',
            'permissions' => 'required|array|min:1',
            'faculties' => 'required|array|min:1'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        User::where('id', $chair->user->id)->update([
            'name' => $request->first_name . ' ' . $request->last_name,
            'email' => mb_strtolower(trim($request->email), 'UTF-8'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $user = User::find($chair->user->id);
        $user->syncPermissions($request->permissions);

        if (trim($request->password) !== '') {
            User::where('id', $chair->user->id)->update([
                'password' => Hash::make($request->password),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }

        Chair::where('id', $id)->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        if(AllowedFacultiesByModel::where('model_id', $chair->user->id)->first()){
            AllowedFacultiesByModel::where('model_id', $chair->user->id)->update([
                'faculties' => json_encode($request->faculties),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        } else {
            AllowedFacultiesByModel::create([
                'model_id' => $user->id,
                'faculties' => json_encode($request->faculties)
            ]);
        }

        $this->helper->addLog('rector', Auth::user()->rector->id, 'chairs', 2, 'DB id: [' . $id . ']');

        return redirect()->route('rector.chairs.index')->with('success', __('messages.chair_updated_successfully'));
    }

    public function block(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_chair')) {
            return redirect()->route('rector.not-found');
        }

        $validator = Validator::make($request->all(), [
            'chair_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $chair = Chair::where('id', $request->chair_id)->where('deleted', 0)->first();
        if (!$chair) {
            return back()->with('warning', __('messages.chair_not_found'));
        }

        Chair::where('id', $request->chair_id)->update([
            'blocked' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('rector', Auth::user()->rector->id, 'chairs', 4, 'DB id: [' . $request->chair_id . ']');

        return back()->with('success', __('messages.chair_blocked_successfully'));
    }

    public function unblock(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_chair')) {
            return redirect()->route('rector.not-found');
        }

        $validator = Validator::make($request->all(), [
            'chair_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $chair = Chair::where('id', $request->chair_id)->where('deleted', 0)->first();
        if (!$chair) {
            return back()->with('warning', __('messages.chair_not_found'));
        }

        Chair::where('id', $request->chair_id)->update([
            'blocked' => 0,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('rector', Auth::user()->rector->id, 'chairs', 5, 'DB id: [' . $request->chair_id . ']');

        return back()->with('success', __('messages.chair_unblocked_successfully'));
    }

    public function delete(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_chair')) {
            return redirect()->route('rector.not-found');
        }

        $validator = Validator::make($request->all(), [
            'chair_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $chair = Chair::where('id', $request->chair_id)->where('deleted', 0)->first();
        if (!$chair) {
            return back()->with('warning', __('messages.chair_not_found'));
        }

        Chair::where('id', $request->chair_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('rector', Auth::user()->rector->id, 'chairs', 3, 'DB id: [' . $request->chair_id . ']');

        return back()->with('success', __('messages.chair_deleted_successfully'));
    }
}
