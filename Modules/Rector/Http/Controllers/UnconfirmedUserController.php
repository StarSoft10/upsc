<?php

namespace Modules\Rector\Http\Controllers;

use App\Entities\Chair;
use App\Entities\Dean;
use App\Entities\HumanResource;
use App\Entities\Rector;
use App\Entities\Secretary;
use App\Entities\Student;
use App\Entities\Teacher;
use App\Entities\TeacherRank;
use App\Entities\UnconfirmedUser;
use App\Entities\User;
use App\Mail\SendMail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Validator;
use Illuminate\Routing\Controller;
use App\Helpers\AppHelper;

class UnconfirmedUserController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->middleware(['auth', 'role:rector']);
        $this->middleware('check_if_blocked');
        $this->helper = new AppHelper();
    }

    public function index(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_unconfirmed_users')) {
            return redirect()->route('rector.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $email = !empty($request->email) ? trim($request->email) : '';

        $unconfirmedUsers = UnconfirmedUser::where('deleted', 0)->orderBy('last_name')->orderBy('first_name')->paginate(10);
        $unconfirmedUsers->appends([
            'name' => $name,
            'email' => $email
        ]);

        $permissions = Permission::all();
        $roles = Role::where('name', '!=', 'superadmin')->where('name', '!=', 'admin')->where('name', '!=', 'rector')->get();
        $teacherRanks = TeacherRank::pluck('name', 'id');

        return view('rector::unconfirmed-users.index', compact('unconfirmedUsers', 'name', 'email',
            'permissions', 'roles', 'teacherRanks'));
    }

    public function search(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_unconfirmed_users')) {
            return redirect()->route('rector.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $email = !empty($request->email) ? trim($request->email) : '';

        $unconfirmedUsers = UnconfirmedUser::where('deleted', 0)->where(function ($query) use ($name, $email) {
            if ($name !== '') {
                $query->where('first_name', 'LIKE', '%' . trim($name) . '%');
                $query->orWhere('last_name', 'LIKE', '%' . trim($name) . '%');
            }
            if ($email !== '') {
                $query->where('email', 'LIKE', '%' . trim($email) . '%');
            }
        })->orderBy('last_name')->orderBy('first_name')->paginate(10);
        $unconfirmedUsers->appends([
            'name' => $name,
            'email' => $email
        ]);

        $permissions = Permission::all();
        $roles = Role::where('name', '!=', 'superadmin')->where('name', '!=', 'admin')->where('name', '!=', 'rector')->get();
        $teacherRanks = TeacherRank::pluck('name', 'id');

        return view('rector::unconfirmed-users.index', compact('unconfirmedUsers', 'name', 'email',
            'permissions', 'roles', 'teacherRanks'));
    }

    public function show($id)
    {
        if (!Auth::user()->hasPermissionTo('view_unconfirmed_users')) {
            return redirect()->route('rector.not-found');
        }

        $unconfirmedUser = UnconfirmedUser::where('id', $id)->where('deleted', 0)->first();

        if (!$unconfirmedUser) {
            return back()->with('warning', __('messages.unconfirmed_user_not_found'));
        }

        return view('rector::unconfirmed-users.show', compact('unconfirmedUser'));
    }

    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermissionTo('edit_unconfirmed_users')) {
            return redirect()->route('rector.not-found');
        }

        $unconfirmedUser = UnconfirmedUser::where('id', $id)->where('deleted', 0)->first();
        if (!$unconfirmedUser) {
            return back()->with('warning', __('messages.unconfirmed_user_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => [
                'required',
                'email',
                'regex:/^[a-z0-9\@\.\_\-]+$/u',
                Rule::unique('unconfirmed_users')->ignore(UnconfirmedUser::findOrFail($unconfirmedUser->id)),
            ],
            'password' => 'confirmed'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        UnconfirmedUser::where('id', $id)->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => str_replace(['ă', 'â', 'î', 'ș', 'ț'], ['a', 'a', 'i', 's', 't'], mb_strtolower(trim($request->email), 'UTF-8')),
            'phone' => $request->phone,
            'plain_password' => $request->plain_password,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('rector', Auth::user()->rector->id, 'unconfirmed_users', 2, 'DB id: [' . $id . ']');

        return redirect()->route('rector.unconfirmed-users.index')->with('success', __('messages.unconfirmed_user_updated_successfully'));
    }

    public function confirm(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('confirm_unconfirmed_users')) {
            return redirect()->route('rector.not-found');
        }

        $validator = Validator::make($request->all(), [
            'unconfirmed_user_id' => 'required|numeric',
            'role' => 'required|numeric',
            'permissions' => 'required|array|min:1'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $unconfirmedUser = UnconfirmedUser::where('id', $request->unconfirmed_user_id)
            ->where('deleted', 0)
            ->where('confirmed', 0)
            ->first();
        if (!$unconfirmedUser) {
            return back()->with('warning', __('messages.unconfirmed_user_not_found'));
        }

        $role = Role::find($request->role);
        if (!$role) {
            return back()->with('warning', __('messages.role_not_found'));
        }

        if($request->role == 8 && empty($request->teacher_rank_id)){
            return back()->with('warning', __('messages.teacher_rank_is_required'));
        }

        $teacherRank = TeacherRank::find($request->teacher_rank_id);
        if (!$teacherRank) {
            return back()->with('warning', __('messages.teacher_rank_not_found'));
        }

        $user = User::create([
            'name' => $unconfirmedUser->first_name . ' ' . $unconfirmedUser->last_name,
            'email' => str_replace(['ă', 'â', 'î', 'ș', 'ț'], ['a', 'a', 'i', 's', 't'], mb_strtolower(trim($unconfirmedUser->email), 'UTF-8')),
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make($unconfirmedUser->plain_password)
        ]);

        $user->assignRole($role->name);
        foreach ($request->permissions as $permission) {
            $user->givePermissionTo($permission);
        }

        if ($role == 'rector') {
            Rector::create([
                'user_id' => $user->id,
                'first_name' => $unconfirmedUser->first_name,
                'last_name' => $unconfirmedUser->last_name,
                'phone' => $unconfirmedUser->phone
            ]);
        } elseif ($role->name == 'secretary') {
            Secretary::create([
                'user_id' => $user->id,
                'first_name' => $unconfirmedUser->first_name,
                'last_name' => $unconfirmedUser->last_name,
                'phone' => $unconfirmedUser->phone
            ]);
        } elseif ($role->name == 'dean') {
            Dean::create([
                'user_id' => $user->id,
                'first_name' => $unconfirmedUser->first_name,
                'last_name' => $unconfirmedUser->last_name,
                'phone' => $unconfirmedUser->phone
            ]);
        } elseif ($role->name == 'chair') {
            Chair::create([
                'user_id' => $user->id,
                'first_name' => $unconfirmedUser->first_name,
                'last_name' => $unconfirmedUser->last_name,
                'phone' => $unconfirmedUser->phone
            ]);
        } elseif ($role->name == 'human-resource') {
            HumanResource::create([
                'user_id' => $user->id,
                'first_name' => $unconfirmedUser->first_name,
                'last_name' => $unconfirmedUser->last_name,
                'phone' => $unconfirmedUser->phone
            ]);
        } elseif ($role->name == 'teacher') {
            Teacher::create([
                'user_id' => $user->id,
                'teacher_rank_id' => $teacherRank->id,
                'first_name' => $unconfirmedUser->first_name,
                'last_name' => $unconfirmedUser->last_name,
                'phone' => $unconfirmedUser->phone
            ]);
        } elseif ($role->name == 'student') {
            Student::create([
                'user_id' => $user->id,
                'first_name' => $unconfirmedUser->first_name,
                'last_name' => $unconfirmedUser->last_name,
                'phone' => $unconfirmedUser->phone
            ]);
        }

        UnconfirmedUser::where('id', $unconfirmedUser->id)->update([
            'confirmed' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('rector', Auth::user()->rector->id, 'unconfirmed_users', 6, 'DB id: [' . $unconfirmedUser->id . '] Given role: ' . $role->name);

        $email = $unconfirmedUser->email;
        $data = [
            'from' => 'whatsapp@safebit.co.il',
            'to' => $email,
            'bcc' => ['mihai.c@x-cloud.biz'],
            'subject' => 'UPSC Activare Profil',
            'message' => 'Profilul dvs este activat cu success'
        ];
        Mail::send(new SendMail($data));

        if (count(Mail::failures()) > 0) {
            Log::info('Failed send notification to email: ' . $email);
        } else {
            Log::info('Success send notification to email: ' . $email);
        }

        return back()->with('success', __('messages.unconfirmed_user_confirmed_successfully'));
    }

    public function delete(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_unconfirmed_users')) {
            return redirect()->route('rector.not-found');
        }

        $validator = Validator::make($request->all(), [
            'unconfirmed_user_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $unconfirmedUser = UnconfirmedUser::where('id', $request->unconfirmed_user_id)->where('deleted', 0)->first();
        if (!$unconfirmedUser) {
            return back()->with('warning', __('messages.unconfirmed_user_not_found'));
        }

        UnconfirmedUser::where('id', $request->unconfirmed_user_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('rector', Auth::user()->rector->id, 'unconfirmed_users', 3, 'DB id: [' . $request->unconfirmed_user_id . ']');

        return back()->with('success', __('messages.unconfirmed_user_deleted_successfully'));
    }
}
