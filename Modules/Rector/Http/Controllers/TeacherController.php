<?php

namespace Modules\Rector\Http\Controllers;

use App\Entities\Teacher;
use App\Entities\TeacherRank;
use App\Entities\User;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;
use Illuminate\Database\Eloquent\Builder;

class TeacherController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->middleware(['auth', 'role:rector']);
        $this->middleware('check_if_blocked');
        $this->helper = new AppHelper();
    }

    public function index(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_teacher')) {
            return redirect()->route('rector.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $email = !empty($request->email) ? trim($request->email) : '';
        $phone = !empty($request->phone) ? trim($request->phone) : '';
        $teacherRanksIds = !empty($request->teacherRanksIds) ? $request->teacherRanksIds : [];

        $teachers = Teacher::where('deleted', 0)->orderBy('last_name')->orderBy('first_name')->paginate(10);
        $teachers->appends([
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
            'teacherRanksIds' => $teacherRanksIds
        ]);

        $teacherRanks = TeacherRank::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $permissions = $this->helper->getAllowedPermissionsByModel('teacher');

        return view('rector::teachers.index', compact('teachers', 'teacherRanks', 'permissions',
            'name', 'email', 'phone', 'teacherRanksIds'));
    }

    public function search(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_teacher')) {
            return redirect()->route('rector.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $email = !empty($request->email) ? trim($request->email) : '';
        $phone = !empty($request->phone) ? trim($request->phone) : '';
        $teacherRanksIds = !empty($request->teacherRanksIds) ? $request->teacherRanksIds : [];

        $teachers = Teacher::where('deleted', 0)->with('user')
            ->whereHas('user', function (Builder $query) use ($email) {
                if ($email !== '') {
                    $query->where('email', 'LIKE', '%' . trim($email) . '%');
                }
            })->where(function ($query) use ($name, $phone, $teacherRanksIds) {
                if ($name !== '') {
                    $query->where('first_name', 'LIKE', '%' . trim($name) . '%');
                    $query->orWhere('last_name', 'LIKE', '%' . trim($name) . '%');
                }
                if ($phone !== '') {
                    $query->where('phone', 'LIKE', '%' . trim($phone) . '%');
                }
                if (count($teacherRanksIds) > 0) {
                    $query->whereIn('teacher_rank_id', $teacherRanksIds);
                }
            })->orderBy('last_name')->orderBy('first_name')->paginate(10);
        $teachers->appends([
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
            'teacherRanksIds' => $teacherRanksIds
        ]);

        $teacherRanks = TeacherRank::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $permissions = $this->helper->getAllowedPermissionsByModel('teacher');

        return view('rector::teachers.index', compact('teachers', 'teacherRanks', 'permissions',
            'name', 'email', 'phone', 'teacherRanksIds'));
    }

    public function store(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('create_teacher')) {
            return redirect()->route('rector.not-found');
        }

        $validator = Validator::make($request->all(), [
            'teacher_rank_id' => 'required|numeric|gt:0',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users|regex:/^[a-z0-9\@\.\_\-]+$/u',
            'password' => 'required|confirmed',
            'permissions' => 'required|array|min:1'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $user = User::create([
            'name' => $request->first_name . ' ' . $request->last_name,
            'email' => mb_strtolower(trim($request->email), 'UTF-8'),
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make($request->password)
        ]);
        $user->assignRole('teacher');
        foreach ($request->permissions as $permission) {
            $user->givePermissionTo($permission);
        }

        $teacher = Teacher::create([
            'user_id' => $user->id,
            'teacher_rank_id' => $request->teacher_rank_id,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'mobile' => $request->mobile
        ]);

        $this->helper->addLog('rector', Auth::user()->rector->id, 'teachers', 1, 'DB id: [' . $teacher->id . ']');

        return back()->with('success', __('messages.teacher_created_successfully'));
    }

    public function show($id)
    {
        if (!Auth::user()->hasPermissionTo('view_teacher')) {
            return redirect()->route('rector.not-found');
        }

        $teacher = Teacher::where('id', $id)->where('deleted', 0)->first();

        if (!$teacher) {
            return back()->with('warning', __('messages.teacher_not_found'));
        }
        $teacherPermissions = [];
        foreach ($teacher->user->getDirectPermissions() as $permission) {
            $teacherPermissions[] = $permission->id;
        }
        $teacher->permissions = $teacherPermissions;

        $teacherRanks = TeacherRank::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $permissions = $this->helper->getAllowedPermissionsByModel('teacher');

        return view('rector::teachers.show', compact('teacher', 'teacherRanks', 'permissions', 'teacherPermissions'));
    }

    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermissionTo('edit_teacher')) {
            return redirect()->route('rector.not-found');
        }

        $teacher = Teacher::where('id', $id)->where('deleted', 0)->first();
        if (!$teacher) {
            return back()->with('warning', __('messages.teacher_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'teacher_rank_id' => 'required|numeric|gt:0',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => [
                'required',
                'email',
                'regex:/^[a-z0-9\@\.\_\-]+$/u',
                Rule::unique('users')->ignore(User::findOrFail($teacher->user->id)),
            ],
            'password' => 'confirmed',
            'permissions' => 'required|array|min:1'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        User::where('id', $teacher->user->id)->update([
            'name' => $request->first_name . ' ' . $request->last_name,
            'email' => mb_strtolower(trim($request->email), 'UTF-8'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $user = User::find($teacher->user->id);
        $user->syncPermissions($request->permissions);

        if (trim($request->password) !== '') {
            User::where('id', $teacher->user->id)->update([
                'password' => Hash::make($request->password),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }

        Teacher::where('id', $id)->update([
            'teacher_rank_id' => $request->teacher_rank_id,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'mobile' => $request->mobile,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('rector', Auth::user()->rector->id, 'teachers', 2, 'DB id: [' . $id . ']');

        return redirect()->route('rector.teachers.index')->with('success', __('messages.teacher_updated_successfully'));
    }

    public function block(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_teacher')) {
            return redirect()->route('rector.not-found');
        }

        $validator = Validator::make($request->all(), [
            'teacher_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $teacher = Teacher::where('id', $request->teacher_id)->where('deleted', 0)->first();
        if (!$teacher) {
            return back()->with('warning', __('messages.teacher_not_found'));
        }

        Teacher::where('id', $request->teacher_id)->update([
            'blocked' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('rector', Auth::user()->rector->id, 'teachers', 4, 'DB id: [' . $request->teacher_id . ']');

        return back()->with('success', __('messages.teacher_blocked_successfully'));
    }

    public function unblock(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_teacher')) {
            return redirect()->route('rector.not-found');
        }

        $validator = Validator::make($request->all(), [
            'teacher_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $teacher = Teacher::where('id', $request->teacher_id)->where('deleted', 0)->first();
        if (!$teacher) {
            return back()->with('warning', __('messages.teacher_not_found'));
        }

        Teacher::where('id', $request->teacher_id)->update([
            'blocked' => 0,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('rector', Auth::user()->rector->id, 'teachers', 5, 'DB id: [' . $request->teacher_id . ']');

        return back()->with('success', __('messages.teacher_unblocked_successfully'));
    }

    public function delete(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_teacher')) {
            return redirect()->route('rector.not-found');
        }

        $validator = Validator::make($request->all(), [
            'teacher_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $teacher = Teacher::where('id', $request->teacher_id)->where('deleted', 0)->first();
        if (!$teacher) {
            return back()->with('warning', __('messages.teacher_not_found'));
        }

        Teacher::where('id', $request->teacher_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('rector', Auth::user()->rector->id, 'teachers', 3, 'DB id: [' . $request->teacher_id . ']');

        return back()->with('success', __('messages.teacher_deleted_successfully'));
    }
}
