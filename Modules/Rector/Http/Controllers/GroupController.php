<?php

namespace Modules\Rector\Http\Controllers;

use App\Entities\Cycle;
use App\Entities\Faculty;
use App\Entities\Speciality;
use App\Entities\Group;
use App\Helpers\StudyHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\AppHelper;

class GroupController extends Controller
{
    public $helper;

    public $studyHelper;

    public function __construct()
    {
        $this->middleware(['auth', 'role:rector']);
        $this->middleware('check_if_blocked');
        $this->helper = new AppHelper();
        $this->studyHelper = new StudyHelper();
    }

    public function index(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_group')) {
            return redirect()->route('rector.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';
        $cycleIds = !empty($request->cycleIds) ? $this->helper->removeValueFromArray($request->cycleIds, 0) : [];
        $facultyIds = !empty($request->facultyIds) ? $this->helper->removeValueFromArray($request->facultyIds, 0) : [];
        $specialityIds = !empty($request->specialityIds) ? $this->helper->removeValueFromArray($request->specialityIds, 0) : [];

        $groups = Group::where('deleted', 0)->orderBy('id', 'ASC')->paginate(10);
        $groups->appends([
            'name' => $name,
            'shortName' => $shortName,
            'cycleIds' => $cycleIds,
            'facultyIds' => $facultyIds,
            'specialityIds' => $specialityIds
        ]);

        $cycles = Cycle::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $faculties = Faculty::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $specialities = Speciality::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        return view('rector::groups.index', compact('groups', 'cycles', 'faculties',
            'specialities', 'name', 'shortName', 'cycleIds', 'facultyIds', 'specialityIds'));
    }

    public function search(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_group')) {
            return redirect()->route('rector.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';
        $cycleIds = !empty($request->cycleIds) ? $this->helper->removeValueFromArray($request->cycleIds, 0) : [];
        $facultyIds = !empty($request->facultyIds) ? $this->helper->removeValueFromArray($request->facultyIds, 0) : [];
        $specialityIds = !empty($request->specialityIds) ? $this->helper->removeValueFromArray($request->specialityIds, 0) : [];

        $groups = Group::where('deleted', 0)->where(function ($query) use ($name, $shortName, $cycleIds, $facultyIds, $specialityIds) {
            if ($name !== '') {
                $query->where('name', 'LIKE', '%' . trim($name) . '%');
            }
            if ($shortName !== '') {
                $query->where('short_name', 'LIKE', '%' . trim($shortName) . '%');
            }
            if (count($cycleIds) > 0) {
                $query->whereIn('cycle_id', $cycleIds);
            }
            if (count($facultyIds) > 0) {
                $query->whereIn('faculty_id', $facultyIds);
            }
            if (count($specialityIds) > 0) {
                $query->whereIn('speciality_id', $specialityIds);
            }
        })->orderBy('id', 'ASC')->paginate(10);
        $groups->appends([
            'name' => $name,
            'shortName' => $shortName,
            'cycleIds' => $cycleIds,
            'facultyIds' => $facultyIds,
            'specialityIds' => $specialityIds
        ]);

        $cycles = Cycle::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $faculties = Faculty::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $specialities = Speciality::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        return view('rector::groups.index', compact('groups', 'cycles', 'faculties', 'specialities',
            'name', 'shortName', 'cycleIds', 'facultyIds', 'specialityIds'));
    }

    public function store(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('create_group')) {
            return redirect()->route('rector.not-found');
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('groups')->where(function ($query) use ($request) {
                    $query->where('deleted', 0);
                    $query->where('cycle_id', $request->cycle_id);
                    $query->where('faculty_id', $request->faculty_id);
                    $query->where('speciality_id', $request->speciality_id);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('groups')->where(function ($query) use ($request) {
                    $query->where('deleted', 0);
                    $query->where('cycle_id', $request->cycle_id);
                    $query->where('faculty_id', $request->faculty_id);
                    $query->where('speciality_id', $request->speciality_id);
                })
            ],
            'cycle_id' => 'required|numeric|gt:0',
            'faculty_id' => 'required|numeric|gt:0',
            'speciality_id' => 'required|numeric|gt:0'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $group = Group::create([
            'cycle_id' => $request->cycle_id,
            'faculty_id' => $request->faculty_id,
            'speciality_id' => $request->speciality_id,
            'name' => trim($request->name),
            'short_name' => trim($request->short_name)
        ]);

        $this->helper->addLog('rector', Auth::user()->rector->id, 'groups', 1, 'DB id: [' . $group->id . ']');

        return back()->with('success', __('messages.group_created_successfully'));
    }

    public function show($id)
    {
        if (!Auth::user()->hasPermissionTo('view_group')) {
            return redirect()->route('rector.not-found');
        }

        $group = Group::where('id', $id)->where('deleted', 0)->first();

        if (!$group) {
            return back()->with('warning', __('messages.group_not_found'));
        }

        $cycles = Cycle::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $faculties = Faculty::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $specialities = Speciality::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $faculty = $group->faculty->name;
        $speciality = $group->speciality->name;

        return view('rector::groups.show', compact('group', 'cycles', 'faculties', 'specialities',
            'faculty', 'speciality'));
    }

    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermissionTo('edit_group')) {
            return redirect()->route('rector.not-found');
        }

        $group = Group::where('id', $id)->where('deleted', 0)->first();
        if (!$group) {
            return back()->with('warning', __('messages.group_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('groups')->where(function ($query) use ($id, $request) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                    $query->where('cycle_id', $request->cycle_id);
                    $query->where('faculty_id', $request->faculty_id);
                    $query->where('speciality_id', $request->speciality_id);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('groups')->where(function ($query) use ($id, $request) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                    $query->where('cycle_id', $request->cycle_id);
                    $query->where('faculty_id', $request->faculty_id);
                    $query->where('speciality_id', $request->speciality_id);
                })
            ],
            'cycle_id' => 'required|numeric|gt:0',
            'faculty_id' => 'required|numeric|gt:0',
            'speciality_id' => 'required|numeric|gt:0'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        Group::where('id', $id)->update([
            'cycle_id' => $request->cycle_id,
            'faculty_id' => $request->faculty_id,
            'speciality_id' => $request->speciality_id,
            'name' => trim($request->name),
            'short_name' => trim($request->short_name),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('rector', Auth::user()->rector->id, 'groups', 2, 'DB id: [' . $id . ']');

        return redirect()->route('rector.groups.index')->with('success', __('messages.group_updated_successfully'));
    }

    public function delete(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_group')) {
            return redirect()->route('rector.not-found');
        }

        $validator = Validator::make($request->all(), [
            'group_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $group = Group::where('id', $request->group_id)->where('deleted', 0)->first();
        if (!$group) {
            return back()->with('warning', __('messages.group_not_found'));
        }

        Group::where('id', $request->group_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('rector', Auth::user()->rector->id, 'groups', 3, 'DB id: [' . $request->group_id . ']');

        return back()->with('success', __('messages.group_deleted_successfully'));
    }

    public function getGroupsBySpecialityForSelectSearch(Request $request)
    {
        return $this->studyHelper->getGroupsBySpecialityForSelectSearch($request);
    }

    public function getGroupsBySpecialityForSelectAddEdit(Request $request)
    {
        return $this->studyHelper->getGroupsBySpecialityForSelectAddEdit($request);
    }
}
