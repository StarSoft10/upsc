<?php

namespace Modules\Rector\Http\Controllers;

use App\Entities\DurationType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\AppHelper;

class DurationTypeController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->middleware(['auth', 'role:rector']);
        $this->middleware('check_if_blocked');
        $this->helper = new AppHelper();
    }

    public function index(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_duration_type')) {
            return redirect()->route('rector.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';

        $durationTypes = DurationType::where('deleted', 0)->orderBy('id', 'ASC')->paginate(10);
        $durationTypes->appends([
            'name' => $name,
            'shortName' => $shortName
        ]);

        return view('rector::duration-types.index', compact('durationTypes', 'name', 'shortName'));
    }

    public function search(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_duration_type')) {
            return redirect()->route('rector.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';

        $durationTypes = DurationType::where('deleted', 0)->where(function ($query) use ($name, $shortName) {
            if ($name !== '') {
                $query->where('name', 'LIKE', '%' . trim($name) . '%');
            }
            if ($shortName !== '') {
                $query->where('short_name', 'LIKE', '%' . trim($shortName) . '%');
            }
        })->orderBy('id', 'ASC')->paginate(10);
        $durationTypes->appends([
            'name' => $name,
            'shortName' => $shortName
        ]);

        return view('rector::duration-types.index', compact('durationTypes', 'name', 'shortName'));
    }

    public function store(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('create_duration_type')) {
            return redirect()->route('rector.not-found');
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('duration_types')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('duration_types')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $durationType = DurationType::create(['name' => trim($request->name), 'short_name' => trim($request->short_name)]);

        $this->helper->addLog('rector', Auth::user()->rector->id, 'duration_types', 1, 'DB id: [' . $durationType->id . ']');

        return back()->with('success', __('messages.duration_type_created_successfully'));
    }

    public function show($id)
    {
        if (!Auth::user()->hasPermissionTo('view_duration_type')) {
            return redirect()->route('rector.not-found');
        }

        $durationType = DurationType::where('id', $id)->where('deleted', 0)->first();

        if (!$durationType) {
            return back()->with('warning', __('messages.duration_type_not_found'));
        }

        return view('rector::duration-types.show', compact('durationType'));
    }

    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermissionTo('edit_duration_type')) {
            return redirect()->route('rector.not-found');
        }

        $durationType = DurationType::where('id', $id)->where('deleted', 0)->first();
        if (!$durationType) {
            return back()->with('warning', __('messages.duration_type_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('duration_types')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('duration_types')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        DurationType::where('id', $id)->update([
            'name' => trim($request->name),
            'short_name' => trim($request->short_name),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('rector', Auth::user()->rector->id, 'duration_types', 2, 'DB id: [' . $id . ']');

        return redirect()->route('rector.duration-types.index')->with('success', __('messages.duration_type_updated_successfully'));
    }

    public function delete(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_duration_type')) {
            return redirect()->route('rector.not-found');
        }

        $validator = Validator::make($request->all(), [
            'duration_type_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $durationType = DurationType::where('id', $request->duration_type_id)->where('deleted', 0)->first();
        if (!$durationType) {
            return back()->with('warning', __('messages.duration_type_not_found'));
        }

        DurationType::where('id', $request->duration_type_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('rector', Auth::user()->rector->id, 'duration_types', 3, 'DB id: [' . $request->duration_type_id . ']');

        return back()->with('success', __('messages.duration_type_deleted_successfully'));
    }
}
