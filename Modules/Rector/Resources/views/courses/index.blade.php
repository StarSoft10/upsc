@extends('rector::layouts.app')

@section('title')
    {{ __('pages.courses') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            {!! Form::open(['route' => 'rector.courses.search', 'method' => 'GET']) !!}
                                <div class="row">
                                    <div class="col-md-3">
                                        {!! Form::text('name', $name, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.name')]) !!}
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::text('short_name', $shortName, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.short_name')]) !!}
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::text('code', $code, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.code')]) !!}
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::submit(__('pages.search'), ['class' => 'btn btn-primary btn-sm']) !!}
                                        <a href="{{ route('rector.courses.index') }}" class="btn btn-info btn-sm" title="{{ __('pages.reset_filter') }}">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-2 text-right">
                            @can('create_course')
                                <button type="button" class="btn btn-success mb-1 btn-sm" data-toggle="modal" data-target="#addCourse">
                                    {{ __('pages.add_course') }}
                                </button>
                            @endcan
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th>{{ __('pages.name') }}</th>
                                <th>{{ __('pages.short_name') }}</th>
                                <th>{{ __('pages.code') }}</th>
                                <th>{{ __('pages.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($courses) > 0)
                                @foreach($courses as $course)
                                    <tr>
                                        <td class="text-center"><span>{{ $course->name }}</span></td>
                                        <td class="text-center"><span>{{ $course->short_name }}</span></td>
                                        <td class="text-center"><span>{{ $course->code }}</span></td>
                                        <td class="text-center">
                                            @can('view_course')
                                                <a href="{{ route('rector.courses.show', $course->id) }}" class="btn-no-bg" title="{{ __('pages.show_edit') }}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            @endcan

                                            @can('delete_course')
                                                <a href="javascript:void(0)" data-courseid="{{ $course->id }}" class="btn-no-bg delete-course" title="{{ __('pages.delete') }}">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="4">{{ __('messages.not_found_any_courses') }}</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="table-footer">
                        {!! $courses->render() !!}
                        <div class="total-found">{{ __('pages.total_found') }}: {{ $courses->total() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @can('create_course')
        <div class="modal fade" id="addCourse" role="dialog" aria-labelledby="addCourse" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    {!! Form::open(['url' => route('rector.courses.store'), 'method' => 'post', 'autocomplete' => 'off']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.add_course') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('name', __('pages.name'), ['class' => 'control-label']) !!}
                                        {!! Form::text('name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('short_name', __('pages.short_name'), ['class' => 'control-label']) !!}
                                        {!! Form::text('short_name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('code', __('pages.code'), ['class' => 'control-label']) !!}
                                        {!! Form::text('code', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            {!! Form::submit(__('pages.add'), ['class' => 'btn btn-success btn-sm']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan

    @can('delete_course')
        <div class="modal fade" id="deleteCourse" role="dialog" aria-labelledby="deleteCourse" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['route' => 'rector.courses.delete']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.delete_course') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-info" role="alert">
                                <div class="alert-text">
                                    {{ __('pages.sure_to_delete_course?') }}
                                </div>
                            </div>
                            <input type="hidden" name="course_id">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            <button type="submit" class="btn btn-danger btn-sm">{{ __('pages.delete') }}</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan
@endsection

@section('custom-css')
@endsection

@section('custom-scripts')
    <script>
        let body = $('body');

        body.on('click', '.delete-course', function () {
            let courseId = $(this).attr('data-courseid');
            let modal = $('#deleteCourse');
            modal.find('input[name="course_id"]').val(courseId);
            modal.modal('show');
        });

        $('#deleteCourse').on('hidden.bs.modal', function () {
            $(this).find('input[name="course_id"]').val('');
        });
    </script>
@endsection