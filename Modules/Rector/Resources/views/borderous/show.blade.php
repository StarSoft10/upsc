@extends('rector::layouts.app')

@section('title')
    {{ __('pages.borderou') }} {{ $borderou->name }}
@endsection

@section('content')
    <div class="custom-loader" style="display: none"></div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>{{ $borderou->name }}</h4>
                </div>
                <div class="card-body">
                    <div class="custom-tab">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active show" id="edit-tab" data-toggle="tab" href="#edit" role="tab" aria-controls="edit" aria-selected="false">
                                    <i class="fa fa-edit"></i>
                                    {{ __('pages.edit_borderou') }}
                                </a>
                            </div>
                        </nav>

                        <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                            <div class="tab-pane fade active show" id="edit" role="tabpanel" aria-labelledby="edit-tab">
                                {!! Form::model($borderou, ['method' => 'PATCH', 'route' => ['rector.borderous.update', $borderou->id], 'autocomplete' => 'off']) !!}
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                {!! Form::label('name', __('pages.borderou_name'), ['class' => 'control-label']) !!}
                                                {!! Form::text('name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group" style="height: 63px;">
                                                {!! Form::label('cycle_id', __('pages.cycle'), ['class' => 'control-label']) !!}
                                                <select data-placeholder="{{ __('pages.chose_cycle') }}" class="standardSelect" name="cycle_id" id="cycle_id" required title="{{ __('pages.cycle') }}" disabled>
                                                    @foreach($cycles as $key => $cycle)
                                                        <option value="{{ $key }}" @if($key == $borderou->cycle_id) selected @endif>{{ $cycle }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group" style="height: 63px;">
                                                {!! Form::label('faculty_id', __('pages.faculty'), ['class' => 'control-label']) !!}
                                                <select data-placeholder="{{ __('pages.chose_cycle') }}" class="standardSelect" name="faculty_id" id="faculty_id" required title="{{ __('pages.faculty') }}" disabled>
                                                    <option value="{{ $borderou->faculty_id }}" selected>{{ $faculty }}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group" style="height: 63px;">
                                                {!! Form::label('speciality_id', __('pages.speciality'), ['class' => 'control-label']) !!}
                                                <select data-placeholder="{{ __('pages.chose_faculty') }}" class="standardSelect" name="speciality_id" id="speciality_id" required title="{{ __('pages.speciality') }}" disabled>
                                                    <option value="{{ $borderou->speciality_id }}" selected>{{ $speciality }}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group" style="height: 63px;">
                                                {!! Form::label('group_id', __('pages.group'), ['class' => 'control-label']) !!}
                                                <select data-placeholder="{{ __('pages.chose_speciality') }}" class="standardSelect" name="group_id" id="group_id" required title="{{ __('pages.group') }}" disabled>
                                                    @foreach($groups as $key => $group)
                                                        <option value="{{ $key }}" @if($key == $borderou->group_id) selected @endif>{{ $group }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group" style="height: 63px;">
                                                {!! Form::label('year_of_study_id', __('pages.year_of_study'), ['class' => 'control-label']) !!}
                                                <select data-placeholder="{{ __('pages.year_of_study') }}" class="standardSelect" name="year_of_study_id" id="year_of_study_id" required title="{{ __('pages.year_of_study') }}">
                                                    @foreach($yearOfStudies as $key => $yearOfStudy)
                                                        <option value="{{ $key }}" @if($key == $borderou->year_of_study_id) selected @endif>{{ $yearOfStudy }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group" style="height: 63px;">
                                                {!! Form::label('semester_id', __('pages.semester'), ['class' => 'control-label']) !!}
                                                <select data-placeholder="{{ __('pages.semester') }}" class="standardSelect" name="semester_id" id="semester_id" required title="{{ __('pages.semester') }}">
                                                    @foreach($semesters as $key => $semester)
                                                        <option value="{{ $key }}" @if($key == $borderou->semester_id) selected @endif>{{ $semester }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group" style="height: 63px;">
                                                {!! Form::label('course_id', __('pages.course'), ['class' => 'control-label']) !!}
                                                <select data-placeholder="{{ __('pages.course') }}" class="standardSelect" name="course_id" id="course_id" required title="{{ __('pages.course') }}">
                                                    @foreach($courses as $key => $course)
                                                        <option value="{{ $key }}" @if($key == $borderou->course_id) selected @endif>{{ $course }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group" style="height: 63px;">
                                                {!! Form::label('teacher_id', __('pages.teacher'), ['class' => 'control-label']) !!}
                                                <select data-placeholder="{{ __('pages.teacher') }}" class="standardSelect" name="teacher_id" id="teacher_id" required title="{{ __('pages.teacher') }}">
                                                    @foreach($teachers as $key => $teacher)
                                                        <option value="{{ $key }}" @if($key == $borderou->teacher_id) selected @endif>{{ $teacher }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                {!! Form::label('year_of_admission', __('pages.borderou_year'), ['class' => 'control-label']) !!}
                                                {!! Form::text('year_of_admission', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                {!! Form::label('total_hours', __('pages.total_hours'), ['class' => 'control-label']) !!}
                                                {!! Form::text('total_hours', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                {!! Form::label('credits', __('pages.credits'), ['class' => 'control-label']) !!}
                                                {!! Form::text('credits', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                {!! Form::label('course_title', __('pages.course_title'), ['class' => 'control-label']) !!}
                                                {!! Form::text('course_title', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group" style="height: 63px;">
                                                {!! Form::label('borderou_type_id', __('pages.borderou_type'), ['class' => 'control-label']) !!}
                                                <select data-placeholder="{{ __('pages.borderou_type') }}" class="standardSelect" name="borderou_type_id" id="borderou_type_id" required title="{{ __('pages.borderou_type') }}" disabled>
                                                    @foreach($borderouTypes as $key => $borderouType)
                                                        <option value="{{ $key }}" @if($key == $borderou->borderou_type_id) selected @endif>{{ $borderouType }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                {!! Form::label('exam_date', __('pages.exam_date'), ['class' => 'control-label']) !!}
                                                {!! Form::date('exam_date', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                {!! Form::label('date_of_liquidation', __('pages.date_of_liquidation'), ['class' => 'control-label']) !!}
                                                {!! Form::date('date_of_liquidation', null, ['class' => 'input-sm form-control-sm form-control']) !!}
                                            </div>
                                        </div>
                                    </div>

                                    {!! Form::submit(__('pages.edit'), ['class' => 'btn btn-primary btn-sm']) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('vendors/chosen/chosen.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
@endsection

@section('custom-scripts')
    <script src="{{ asset('vendors/chosen/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>

    @if (LaravelLocalization::getCurrentLocale() == 'ro')
        <script src="{{ asset('js/bootstrap-datepicker.ro.min.js') }}"></script>
    @endif
    @if(LaravelLocalization::getCurrentLocale() == 'ru')
        <script src="{{ asset('js/bootstrap-datepicker.ru.min.js') }}"></script>
    @endif

    <script>
        $(function() {
            $('.standardSelect').chosen({
                disable_search_threshold: 10,
                no_results_text: '{{ __('pages.nothing_found') }}',
                width: '100%'
            });

            $('.datepicker').datepicker({
                format: 'yyyy/mm/dd',
                language: '{{ LaravelLocalization::getCurrentLocale() }}'
            });

            // getFacultiesByCycleForSelectAddEdit('#cycle_id', $('#hidden_faculty').val());
            // getSpecialitiesByFacultyForSelectAddEdit('#faculty_id', $('#hidden_speciality').val());
            // getGroupsBySpecialityForSelectAddEdit('#speciality_id', $('#hidden_group').val());
        });
    </script>
    <script>
        let body = $('body');

        body.on('change', '#cycle_id', function () {
            getFacultiesByCycleForSelectAddEdit('#cycle_id');
        });
        body.on('change', '#faculty_id', function () {
            getSpecialitiesByFacultyForSelectAddEdit('#faculty_id');
        });
        body.on('change', '#speciality_id', function () {
            getGroupsBySpecialityForSelectAddEdit('#speciality_id');
        });



        function getFacultiesByCycleForSelectAddEdit(element, hidden = null) {
            let loader = $('.custom-loader');
            let cycleId = $.trim($(element).val());
            let facultySelect = $('#faculty_id');
            let specialitySelect = $('#speciality_id');
            let groupSelect = $('#group_id');

            if(cycleId === '' || cycleId === undefined || cycleId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('rector.faculties.getFacultiesByCycleForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    cycleId: cycleId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    facultySelect.empty();
                    facultySelect.html(response.html);
                    facultySelect.trigger('chosen:updated');

                    specialitySelect.empty();
                    specialitySelect.trigger('chosen:updated');

                    groupSelect.empty();
                    groupSelect.trigger('chosen:updated');

                    let totalFaculties = $('#faculty_id > option').length;
                    if(totalFaculties === 1){
                        getSpecialitiesByFacultyForSelectAddEdit('#faculty_id');
                    }

                    let totalSpecialities = $('#speciality_id > option').length;
                    if(totalSpecialities === 1){
                        getGroupsBySpecialityForSelectAddEdit('#speciality_id');
                    }
                }
            });
        }
        function getSpecialitiesByFacultyForSelectAddEdit(element, hidden = null) {
            let loader = $('.custom-loader');
            let facultyId = $.trim($(element).val());
            let specialitySelect = $('#speciality_id');

            if(facultyId === '' || facultyId === undefined || facultyId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('rector.specialities.getSpecialitiesByFacultyForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    facultyId: facultyId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    specialitySelect.empty();
                    specialitySelect.html(response.html);
                    specialitySelect.trigger('chosen:updated');

                    let totalSpecialities = $('#speciality_id > option').length;
                    if(totalSpecialities === 1){
                        getGroupsBySpecialityForSelectAddEdit('#speciality_id');
                    }
                }
            });
        }
        function getGroupsBySpecialityForSelectAddEdit(element, hidden = null) {
            let loader = $('.custom-loader');
            let specialityId = $.trim($(element).val());
            let groupSelect = $('#group_id');

            if(specialityId === '' || specialityId === undefined || specialityId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('rector.groups.getGroupsBySpecialityForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    specialityId: specialityId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    groupSelect.empty();
                    groupSelect.html(response.html);
                    groupSelect.trigger('chosen:updated');
                }
            });
        }
    </script>
@endsection