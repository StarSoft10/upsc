@extends('rector::layouts.app')

@section('title')
    {{ __('pages.student') }} {{ $student->first_name }} {{ $student->last_name }}
@endsection

@section('content')
    <div class="custom-loader" style="display: none"></div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>{{ $student->first_name }} {{ $student->last_name }}</h4>
                </div>
                <div class="card-body">
                    <div class="custom-tab">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active show" id="information-tab" data-toggle="tab" href="#information" role="tab" aria-controls="information" aria-selected="true">
                                    <i class="fa fa-info-circle"></i>
                                    {{ __('pages.student_information') }}
                                </a>
                                @can('edit_student')
                                    <a class="nav-item nav-link" id="edit-tab" data-toggle="tab" href="#edit" role="tab" aria-controls="edit" aria-selected="false">
                                        <i class="fa fa-edit"></i>
                                        {{ __('pages.edit_student') }}
                                    </a>
                                @endcan
                                <a class="nav-item nav-link" id="changes-tab" data-toggle="tab" href="#changes" role="tab" aria-controls="changes" aria-selected="false">
                                    <i class="fa fa-exchange"></i>
                                    {{ __('pages.changes_in_studies') }}
                                </a>
                                @can('view_borderou_note')
                                    <a class="nav-item nav-link" id="notes-tab" data-toggle="tab" href="#notes" role="tab" aria-controls="notes" aria-selected="false">
                                        <i class="fa fa-sort-numeric-asc"></i>
                                        {{ __('pages.student_notes') }}
                                    </a>
                                @endcan
                            </div>
                        </nav>

                        <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                            <div class="tab-pane fade active show" id="information" role="tabpanel" aria-labelledby="information-tab">
                                <div class="mx-auto d-block">
                                    <div class="row">
                                        <div class="col-md-4">
                                            @if($student->user->photo == null || $student->user->photo == '')
                                                <img class="rounded-circle mx-auto d-block" src="{{ asset('images/default_photo.jpg') }}" alt="Photo">
                                            @else
                                                <img class="rounded-circle mx-auto d-block" src="{{ asset('storage/users-photo/' . $student->user->photo) }}" alt="Photo">
                                            @endif
                                            <h5 class="text-sm-center mt-2 mb-1">{{ $student->first_name }} {{ $student->last_name }}</h5>
                                        </div>

                                        <div class="col-md-4">
                                            <ul style="list-style: none;">
                                                <li>
                                                    <i class="fa fa-envelope"></i>
                                                    <span>{{ __('pages.email') }}:</span>
                                                    <a href="mailto:{{ $student->user->email }}">{{ $student->user->email }}</a>
                                                </li>
                                                <li>
                                                    <i class="fa fa-envelope"></i>
                                                    <span>{{ __('pages.personal_email') }}:</span>
                                                    <a href="mailto:{{ $student->personal_email }}">{{ $student->personal_email }}</a>
                                                </li>
                                                <li>
                                                    <i class="fa fa-mobile"></i>
                                                    <span>{{ __('pages.mobile') }}:</span>
                                                    <a href="tel:{{ $student->mobile }}">{{ $student->mobile }}</a>
                                                </li>
                                                <li>
                                                    <i class="fa fa-user"></i>
                                                    <span>{{ __('pages.patronymic') }}:</span>
                                                    {{ $student->patronymic }}
                                                </li>
                                                <li>
                                                    <i class="fa fa-sort-numeric-asc"></i>
                                                    <span>{{ __('pages.idnp') }}:</span>
                                                    {{ $student->idnp }}
                                                </li>
                                                <li>
                                                    @if($student->gender == 'M') <i class="fa fa-male"></i> @else <i class="fa fa-female"></i> @endif
                                                    <span>{{ __('pages.gender') }}:</span>
                                                    @if(strtolower($student->gender) == 'm') {{ __('pages.male') }} @else {{ __('pages.female') }}@endif
                                                </li>
                                                <li>
                                                    <i class="fa fa-calendar"></i>
                                                    <span>{{ __('pages.birth_date') }}:</span>
                                                    {{ $student->birth_date }}
                                                </li>
                                                <li>
                                                    <i class="fa fa-building-o"></i>
                                                    <span>{{ __('pages.city') }}:</span>
                                                    {{ $student->city }}
                                                </li>
                                                <li>
                                                    <i class="fa fa-road"></i>
                                                    <span>{{ __('pages.address') }}:</span>
                                                    {{ $student->address }}
                                                </li>
                                                <li>
                                                    <i class="fa fa-globe"></i>
                                                    <span>{{ __('pages.citizenship') }}:</span>
                                                    {{ $student->citizenship }}
                                                </li>
                                                <li>
                                                    <i class="fa fa-globe"></i>
                                                    <span>{{ __('pages.nationality') }}:</span>
                                                    {{ $student->nationality }}
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="col-md-4">
                                            <ul style="list-style: none;">
                                                <li>
                                                    <i class="fa fa-desktop"></i>
                                                    <span>{{ __('pages.cycle') }}:</span>
                                                    {{ $student->cycle->name }}
                                                </li>
                                                <li>
                                                    <i class="fa fa-building-o"></i>
                                                    <span>{{ __('pages.faculty') }}:</span>
                                                    {{ $student->faculty->name }}
                                                </li>
                                                <li>
                                                    <i class="fa fa-gavel"></i>
                                                    <span>{{ __('pages.speciality') }}:</span>
                                                    {{ $student->speciality->name }}
                                                </li>
                                                <li>
                                                    <i class="fa fa-users"></i>
                                                    <span>{{ __('pages.group') }}:</span>
                                                    {{ $student->group->name }}
                                                </li>
                                                <li>
                                                    <i class="fa fa-calendar"></i>
                                                    <span>{{ __('pages.year_of_admission') }}:</span>
                                                    {{ $student->year_of_admission }}
                                                </li>
                                                <li>
                                                    @if($student->study == 1) <i class="fa fa-sun-o"></i> @else <i class="fa fa-moon-o"></i> @endif
                                                    <span>{{ __('pages.study') }}:</span>
                                                    @if($student->study == 1) {{ __('pages.morning') }} @else {{ __('pages.afternoon') }} @endif
                                                </li>
                                                <li>
                                                    <i class="fa fa-money"></i>
                                                    <span>{{ __('pages.budget') }}:</span>
                                                    @if($student->budget == 1) {{ __('pages.yes') }} @else {{ __('pages.no') }} @endif
                                                </li>
                                                <li>
                                                    <i class="fa fa-times-circle"></i>
                                                    <span>{{ __('pages.expelled') }}:</span>
                                                    @if($student->expelled == 1) {{ __('pages.yes') }} @else {{ __('pages.no') }} @endif
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5 class="text-center">{{ __('pages.additional_info') }}</h5>
                                            <div>{{ $student->additional_info }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @can('edit_student')
                                <div class="tab-pane fade" id="edit" role="tabpanel" aria-labelledby="edit-tab">
                                    {!! Form::model($student, ['method' => 'PATCH', 'route' => ['rector.students.update', $student->id], 'autocomplete' => 'off']) !!}

                                        <div class="custom-tab">
                                            <nav>
                                                <div class="nav nav-tabs" id="nav-tab-additional" role="tablist">
                                                    <a class="nav-item nav-link active show" id="base-info-tab" data-toggle="tab" href="#base-info" role="tab" aria-controls="base-info" aria-selected="true">
                                                        <i class="fa fa-info-circle"></i>
                                                        {{ __('pages.base_info') }}
                                                    </a>
                                                    @if(count($additionalStudiesArray) > 0)
                                                        @foreach($additionalStudiesArray as $key => $additionalStudy)
                                                            <a class="nav-item nav-link additional" id="base-info-tab-{{ $additionalStudy['id'] }}" data-toggle="tab" data-id="{{ $additionalStudy['id'] }}"
                                                               href="#base-info{{ $additionalStudy['id'] }}" role="tab" aria-controls="base-info{{ $additionalStudy['id'] }}" aria-selected="true">
                                                                <i class="fa fa-info-circle"></i>
                                                                {{ __('pages.additional_study') }}
                                                            </a>
                                                        @endforeach
                                                    @endif
                                                    <a class="nav-link" id="new-tab" href="javascript:void(0)" role="tab">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                </div>
                                            </nav>

                                            <div class="tab-content pl-3 pt-2" id="nav-tabContent2">
                                                <div class="tab-pane fade active show" id="base-info" role="tabpanel" aria-labelledby="base-info-tab">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                {!! Form::label('first_name', __('pages.first_name'), ['class' => 'control-label']) !!}
                                                                {!! Form::text('first_name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                {!! Form::label('last_name', __('pages.last_name'), ['class' => 'control-label']) !!}
                                                                {!! Form::text('last_name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                {!! Form::label('patronymic', __('pages.patronymic'), ['class' => 'control-label']) !!}
                                                                {!! Form::text('patronymic', null, ['class' => 'input-sm form-control-sm form-control']) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                {!! Form::label('idnp', __('pages.idnp'), ['class' => 'control-label']) !!}
                                                                {!! Form::text('idnp', null, ['class' => 'input-sm form-control-sm form-control']) !!}
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group" style="height: 63px;">
                                                                {!! Form::label('cycle_id', __('pages.cycle'), ['class' => 'control-label']) !!}
                                                                <select data-placeholder="{{ __('pages.chose_cycle') }}" class="standardSelect" name="cycle_id" id="cycle_id" required title="{{ __('pages.cycle') }}">
                                                                    @foreach($cycles as $key => $cycle)
                                                                        <option value="{{ $key }}" @if($key == $student->cycle_id) selected @endif>{{ $cycle }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group" style="height: 63px;">
                                                                {!! Form::label('faculty_id', __('pages.faculty'), ['class' => 'control-label']) !!}
                                                                <select data-placeholder="{{ __('pages.chose_cycle') }}" class="standardSelect" name="faculty_id" id="faculty_id" required title="{{ __('pages.faculty') }}">
                                                                    @foreach($faculties as $key => $faculty)
                                                                        <option value="{{ $key }}" @if($key == $student->faculty_id) selected @endif>{{ $faculty }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group" style="height: 63px;">
                                                                {!! Form::label('speciality_id', __('pages.speciality'), ['class' => 'control-label']) !!}
                                                                <select data-placeholder="{{ __('pages.chose_faculty') }}" class="standardSelect" name="speciality_id" id="speciality_id" required title="{{ __('pages.speciality') }}">
                                                                    @foreach($specialities as $key => $speciality)
                                                                        <option value="{{ $key }}" @if($key == $student->speciality_id) selected @endif>{{ $speciality }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group" style="height: 63px;">
                                                                {!! Form::label('group_id', __('pages.group'), ['class' => 'control-label']) !!}
                                                                <select data-placeholder="{{ __('pages.chose_speciality') }}" class="standardSelect" name="group_id" id="group_id" required title="{{ __('pages.group') }}">
                                                                    @foreach($groups as $key => $group)
                                                                        <option value="{{ $key }}" @if($key == $student->group_id) selected @endif>{{ $group }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <div class="form-group" style="height: 63px;">
                                                                {!! Form::label('year_of_study_id', __('pages.year_of_study'), ['class' => 'control-label']) !!}
                                                                <select data-placeholder="{{ __('pages.year_of_study') }}" class="standardSelect" name="year_of_study_id" id="year_of_study_id" required title="{{ __('pages.year_of_study') }}">
                                                                    @foreach($yearOfStudies as $key => $yearOfStudy)
                                                                        <option value="{{ $key }}" @if($key == $student->year_of_study_id) selected @endif>{{ $yearOfStudy }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group" style="height: 63px;">
                                                                {!! Form::label('gender', __('pages.gender'), ['class' => 'control-label']) !!}
                                                                <select data-placeholder="{{ __('pages.chose_gender') }}" class="standardSelect"
                                                                        name="gender" id="gender" title="{{ __('pages.gender') }}">
                                                                    <option value="M" @if($student->gender == 'M') selected @endif>{{ __('pages.male') }}</option>
                                                                    <option value="F" @if($student->gender == 'F') selected @endif>{{ __('pages.female') }}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                {!! Form::label('personal_email', __('pages.personal_email'), ['class' => 'control-label']) !!}
                                                                {!! Form::email('personal_email', null, ['class' => 'input-sm form-control-sm form-control']) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                {!! Form::label('mobile', __('pages.mobile'), ['class' => 'control-label']) !!}
                                                                {!! Form::text('mobile', null, ['class' => 'input-sm form-control-sm form-control']) !!}
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                {!! Form::label('birth_date', __('pages.birth_date'), ['class' => 'control-label']) !!}
                                                                {!! Form::text('birth_date', null, ['class' => 'input-sm form-control-sm form-control datepicker', 'required' => 'required']) !!}
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                {!! Form::label('city', __('pages.city'), ['class' => 'control-label']) !!}
                                                                {!! Form::text('city', null, ['class' => 'input-sm form-control-sm form-control']) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                {!! Form::label('address', __('pages.address'), ['class' => 'control-label']) !!}
                                                                {!! Form::text('address', null, ['class' => 'input-sm form-control-sm form-control']) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                {!! Form::label('citizenship', __('pages.citizenship'), ['class' => 'control-label']) !!}
                                                                {!! Form::text('citizenship', null, ['class' => 'input-sm form-control-sm form-control']) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                {!! Form::label('nationality', __('pages.nationality'), ['class' => 'control-label']) !!}
                                                                {!! Form::text('nationality', null, ['class' => 'input-sm form-control-sm form-control']) !!}
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                {!! Form::label('year_of_admission', __('pages.year_of_admission'), ['class' => 'control-label']) !!}
                                                                {!! Form::text('year_of_admission', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                {!! Form::label('study', __('pages.study'), ['class' => 'control-label']) !!}
                                                                <select data-placeholder="{{ __('pages.study') }}" class="standardSelect"
                                                                        name="study" id="study" title="{{ __('pages.study') }}">
                                                                    <option value="1" @if($student->study == '1') selected @endif>{{ __('pages.morning') }}</option>
                                                                    <option value="2" @if($student->study == '2') selected @endif>{{ __('pages.afternoon') }}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group" style="height: 63px;">
                                                                {!! Form::label('budget', __('pages.budget'), ['class' => 'control-label']) !!}
                                                                <select data-placeholder="{{ __('pages.budget') }}" class="standardSelect"
                                                                        name="budget" id="budget" title="{{ __('pages.budget') }}">
                                                                    <option value="1" @if($student->budget == '1') selected @endif>{{ __('pages.yes') }}</option>
                                                                    <option value="0" @if($student->budget == '0') selected @endif>{{ __('pages.no') }}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group" style="height: 63px;">
                                                                {!! Form::label('expelled', __('pages.expelled'), ['class' => 'control-label']) !!}
                                                                <select data-placeholder="{{ __('pages.expelled') }}" class="standardSelect"
                                                                        name="expelled" id="expelled" title="{{ __('pages.expelled') }}">
                                                                    <option value="1" @if($student->expelled == '1') selected @endif>{{ __('pages.yes') }}</option>
                                                                    <option value="0" @if($student->expelled == '0') selected @endif>{{ __('pages.no') }}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                {!! Form::label('email', __('pages.email'), ['class' => 'control-label']) !!}
                                                                {!! Form::email('email', $student->user->email, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                {!! Form::label('password', __('pages.password'), ['class' => 'control-label']) !!}
                                                                {!! Form::input('password', 'password', null, ['class' => 'input-sm form-control-sm form-control', 'autocomplete' => 'off']) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                {!! Form::label('password_confirmation', __('pages.confirm_password'), ['class' => 'control-label']) !!}
                                                                {!! Form::input('password', 'password_confirmation', null, ['class' => 'input-sm form-control-sm form-control',
                                                                    'autocomplete' => 'off']) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group" style="height: 63px;">
                                                                {!! Form::label('permissions', __('pages.permissions'), ['class' => 'control-label']) !!}
                                                                <select data-placeholder="{{ __('pages.chose_permissions') }}" multiple class="standardSelect" name="permissions[]" id="permissions" required>
                                                                    @foreach($permissions as $permission)
                                                                        <option value="{{ $permission->id }}" data-permission="{{ $permission->name }}" @if(in_array($permission->id, $studentPermissions)) selected @endif>
                                                                            {{ __('permissions.' . $permission->name) }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                {!! Form::label('additional_info', __('pages.additional_info'), ['class' => 'control-label']) !!}
                                                                {!! Form::textarea('additional_info', null, ['class' => 'input-sm form-control-sm form-control', 'rows' => 4, 'style' => 'resize: none']) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                @if(count($additionalStudiesArray) > 0)
                                                    @foreach($additionalStudiesArray as $key => $additionalStudy)
                                                        <div class="tab-pane fade parent" id="base-info{{ $additionalStudy['id'] }}" role="tabpanel" aria-labelledby="base-info-tab">
                                                            <input type="hidden" name="ids[]" value="{{ $additionalStudy['id'] }}">
                                                            <div class="row">
                                                                <div class="col-md-12 text-center">
                                                                    <button type="button" class="btn btn-danger mb-1 btn-sm remove-tab" title="{{ __('pages.delete') }}"
                                                                            data-tab="base-info{{ $additionalStudy['id'] }}" data-tab-counter="{{ $additionalStudy['id'] }}">
                                                                        <i class="fa fa-remove"></i>
                                                                    </button>
                                                                </div>
                                                            </div>


                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                        <label class="control-label">{{ __('pages.cycle') }}</label>
                                                                        <select data-placeholder="{{ __('pages.chose_cycle') }}" class="standardSelect" name="additional_cycles[]" title="{{ __('pages.cycle') }}">
                                                                            <option value="0">{{ __('pages.chose_cycle') }}</option>
                                                                            @foreach($cycles as $key2 => $cycle)
                                                                                <option value="{{ $key2 }}" @if($key2 == $additionalStudy['cycle_id']) selected @endif>{{ $cycle }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                        <label class="control-label">{{ __('pages.faculty') }}</label>
                                                                        <select data-placeholder="{{ __('pages.chose_faculty') }}" class="standardSelect" name="additional_faculties[]" title="{{ __('pages.faculty') }}">
                                                                            @foreach($additionalStudy['faculties'] as $key2 => $faculty)
                                                                                <option value="{{ $key2 }}" @if($key2 == $additionalStudy['faculty_id']) selected @endif>{{ $faculty }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                        <label class="control-label">{{ __('pages.speciality') }}</label>
                                                                        <select data-placeholder="{{ __('pages.chose_speciality') }}" class="standardSelect" name="additional_specialities[]" title="{{ __('pages.speciality') }}">
                                                                            @foreach($additionalStudy['specialities'] as $key2 => $speciality)
                                                                                <option value="{{ $key2 }}" @if($key2 == $additionalStudy['speciality_id']) selected @endif>{{ $speciality }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-3">
                                                                    <div class="form-group">
                                                                        <label class="control-label">{{ __('pages.group') }}</label>
                                                                        <select data-placeholder="{{ __('pages.chose_group') }}" class="standardSelect" name="additional_groups[]" title="{{ __('pages.group') }}">
                                                                            @foreach($additionalStudy['groups'] as $key2 => $group)
                                                                                <option value="{{ $key2 }}" @if($key2 == $additionalStudy['group_id']) selected @endif>{{ $group }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="row">
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label  class="control-label">{{ __('pages.year_of_study') }}</label>
                                                                        <select data-placeholder="{{ __('pages.year_of_study') }}" class="standardSelect"
                                                                                name="additional_year_of_studies[]" title="{{ __('pages.year_of_study') }}">
                                                                            @foreach($yearOfStudies as $key2 => $yearOfStudy)
                                                                                <option value="{{ $key2 }}" @if($key2 == $additionalStudy['year_of_study_id']) selected @endif>{{ $yearOfStudy }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label  class="control-label">{{ __('pages.year_of_admission') }}</label>
                                                                        <input class="input-sm form-control-sm form-control" name="year_of_admissions[]" type="text" value="{{ $additionalStudy['year_of_admission'] }}" title="">
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label class="control-label">{{ __('pages.study') }}</label>
                                                                        <select data-placeholder="{{ __('pages.study') }}" class="standardSelect" name="studies[]" title="{{ __('pages.study') }}">
                                                                            <option value="1" @if($additionalStudy['study'] == '1') selected @endif>{{ __('pages.morning') }}</option>
                                                                            <option value="2" @if($additionalStudy['study'] == '2') selected @endif>{{ __('pages.afternoon') }}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label  class="control-label">{{ __('pages.budget') }}</label>
                                                                        <select data-placeholder="{{ __('pages.budget') }}" class="standardSelect" name="budgets[]" title="{{ __('pages.budget') }}">
                                                                            <option value="1" @if($additionalStudy['budget'] == '1') selected @endif>{{ __('pages.yes') }}</option>
                                                                            <option value="0" @if($additionalStudy['budget'] == '0') selected @endif>{{ __('pages.no') }}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label class="control-label">{{ __('pages.expelled') }}</label>
                                                                        <select data-placeholder="{{ __('pages.expelled') }}" class="standardSelect" name="expelleds[]" title="{{ __('pages.expelled') }}">
                                                                            <option value="1" @if($additionalStudy['expelled'] == '1') selected @endif>{{ __('pages.yes') }}</option>
                                                                            <option value="0" @if($additionalStudy['expelled'] == '0') selected @endif>{{ __('pages.no') }}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">{{ __('pages.additional_info') }}</label>
                                                                        <textarea class="input-sm form-control-sm form-control" rows="4" style="resize: none" name="additional_infos[]" cols="50" title="">{{ $additionalStudy['additional_info'] }}</textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>

                                        {!! Form::submit(__('pages.edit'), ['class' => 'btn btn-primary btn-sm']) !!}
                                    {!! Form::close() !!}
                                </div>
                            @endcan

                            <div class="tab-pane fade" id="changes" role="tabpanel" aria-labelledby="changes-tab">
                                <table class="table">
                                    <thead class="thead-dark">
                                    <tr class="text-center">
                                        <th>{{ __('pages.year_of_admission') }}</th>
                                        <th>{{ __('pages.cycle') }}</th>
                                        <th>{{ __('pages.faculty') }}</th>
                                        <th>{{ __('pages.speciality') }}</th>
                                        <th>{{ __('pages.group') }}</th>
                                        <th>{{ __('pages.date') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($studentChanges) > 0)
                                        @foreach($studentChanges as $studentChange)
                                            <tr>
                                                <td class="text-center"><span>{{ $studentChange->year_of_admission }}</span></td>
                                                <td class="text-center"><span>{{ $studentChange->cycle }}</span></td>
                                                <td class="text-center"><span>{{ $studentChange->speciality }}</span></td>
                                                <td class="text-center"><span>{{ $studentChange->faculty }}</span></td>
                                                <td class="text-center"><span>{{ $studentChange->group }}</span></td>
                                                <td class="text-center"><span>{{ Carbon\Carbon::parse($studentChange->created_at)->format('d/m/Y H:i:s') }}</span></td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr class="text-center">
                                            <td colspan="6">{{ __('messages.not_found_any_student_changes') }}</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>

                            @can('view_borderou_note')
                                <div class="tab-pane fade" id="notes" role="tabpanel" aria-labelledby="notes-tab">
                                    @if(count($notes) > 0)
                                        <nav>
                                            <div class="nav nav-tabs" id="nav-tab-2" role="tablist">

                                                @php $i = 1;@endphp
                                                @foreach($notes as $displayName => $semesters)
                                                    <a class="nav-item nav-link @if($i == 1) active show @endif" id="info_{{ $i }}-tab"
                                                       data-toggle="tab" href="#info_{{ $i }}" role="tab" aria-controls="info_{{ $i }}"
                                                       aria-selected="true" @if($i == 1) style="padding: .5rem 0rem;" @endif >
                                                        <i class="fa fa-sort-numeric-asc"></i>
                                                        {{ $displayName }}
                                                    </a>
                                                    @php $i++;@endphp
                                                @endforeach
                                            </div>
                                        </nav>

                                        <div class="tab-content">
                                            @php $j = 1;@endphp
                                            @foreach($notes as $displayName => $semesters)
                                                <div class="tab-pane fade @if($j == 1) active show @endif" id="info_{{ $j }}" role="tabpanel" aria-labelledby="info_{{ $j }}-tab">
                                                    <nav>
                                                        <div class="nav nav-tabs" id="nav-tab-2-{{ $j }}" role="tablist">
                                                            @php $i = 1;@endphp
                                                            @foreach($semesters as $semester => $borderouNotes)
                                                                <a class="nav-item nav-link @if($i == 1) active show @endif" id="sem_{{ $j }}_{{ $i }}-tab"
                                                                   data-toggle="tab" href="#sem_{{ $j }}_{{ $i }}" role="tab" aria-controls="sem_{{ $j }}_{{ $i }}"
                                                                   aria-selected="true" @if($i == 1) style="padding: .5rem 0rem;" @endif >
                                                                    <i class="fa fa-sort-numeric-asc"></i>
                                                                    {{ $semester }}
                                                                </a>
                                                                @php $i++;@endphp
                                                            @endforeach
                                                        </div>
                                                    </nav>
                                                    <div class="tab-content">
                                                        @php $i = 1;@endphp
                                                        @foreach($semesters as $semester => $borderouNotes)
                                                            <div class="tab-pane fade @if($i == 1) active show @endif" id="sem_{{ $j }}_{{ $i }}" role="tabpanel" aria-labelledby="sem_{{ $j }}_{{ $i }}-tab">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <table class="table table-sm">
                                                                            <colgroup>
                                                                                <col span="1" style="width: 40%;">
                                                                                <col span="1" style="width: 20%;">
                                                                                <col span="1" style="width: 20%;">
                                                                                <col span="1" style="width: 20%;">
                                                                            </colgroup>
                                                                            <thead>
                                                                            <tr>
                                                                                <th style="border-top: none;">{{ __('pages.course') }}</th>
                                                                                <th style="border-top: none;">{{ __('pages.semester_note') }}</th>
                                                                                <th style="border-top: none;">{{ __('pages.exam_note') }}</th>
                                                                                <th style="border-top: none;">{{ __('pages.final_note') }}</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            @if(count($borderouNotes) > 0)
                                                                                @foreach($borderouNotes as $key => $borderouNote)
                                                                                    <tr>
                                                                                        <th>{{ $borderouNote['courseName'] }}</th>
                                                                                        <td>{{ $borderouNote['semesterNote'] }}</td>
                                                                                        <td>{{ $borderouNote['examNote'] }}</td>
                                                                                        <td>{{ $borderouNote['finalNote'] }}</td>
                                                                                    </tr>
                                                                                @endforeach
                                                                            @else
                                                                                <tr class="text-center"><td colspan="4">{{ __('messages.not_found_any_notes') }}</td></tr>
                                                                            @endif
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @php $i++;@endphp
                                                        @endforeach
                                                    </div>
                                                </div>
                                                @php $j++;@endphp
                                            @endforeach
                                        </div>
                                    @else
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                {{ __('messages.not_found_any_notes') }}
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endcan
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('vendors/chosen/chosen.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
    <style>
        .info-student-body ul li {
            border-bottom: 1px solid;
            border-left: 1px solid;
            border-right: 1px solid;
            padding: 5px;
            margin-bottom: 10px;
        }
    </style>
@endsection

@section('custom-scripts')
    <script src="{{ asset('vendors/chosen/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>

    @if (LaravelLocalization::getCurrentLocale() == 'ro')
        <script src="{{ asset('js/bootstrap-datepicker.ro.min.js') }}"></script>
    @endif
    @if(LaravelLocalization::getCurrentLocale() == 'ru')
        <script src="{{ asset('js/bootstrap-datepicker.ru.min.js') }}"></script>
    @endif

    <script>
        $(function() {
            $('.standardSelect').chosen({
                disable_search_threshold: 10,
                no_results_text: '{{ __('pages.nothing_found') }}',
                width: '100%'
            });

            $('.datepicker').datepicker({
                format: 'yyyy/mm/dd',
                language: '{{ LaravelLocalization::getCurrentLocale() }}'
            });

            // getFacultiesByCycleForSelectAddEdit('#cycle_id', $('#hidden_faculty').val());
            // getSpecialitiesByFacultyForSelectAddEdit('#faculty_id', $('#hidden_speciality').val());
            // getGroupsBySpecialityForSelectAddEdit('#speciality_id', $('#hidden_group').val());
        });
    </script>
    <script>
        let body = $('body');

        let additionalTabs = 1000000;
        body.on('click', '#new-tab', function () {
            let navTabs = $('#nav-tab-additional');
            let tabContents = $('#nav-tabContent2');
            additionalTabs ++;

            let buttonHtml = '' +
                '<a class="nav-item nav-link active show" id="base-info-tab-'+ additionalTabs +'" data-toggle="tab" ' +
                '    href="#base-info'+ additionalTabs +'" role="tab" aria-controls="base-info'+ additionalTabs +'" aria-selected="true">' +
                '    <i class="fa fa-info-circle"></i>' +
                '    {{ __('pages.additional_study') }}' +
                '</a>';

            let tabHtml = '' +
                '<div class="tab-pane fade active show parent" id="base-info'+ additionalTabs +'" role="tabpanel" aria-labelledby="base-info-tab">' +
                '    <div class="row">' +
                '        <div class="col-md-12 text-center">' +
                '            <button type="button" class="btn btn-danger mb-1 btn-sm remove-tab" title="{{ __('pages.delete') }}"' +
                '                data-tab="base-info'+ additionalTabs +'" data-tab-counter="'+ additionalTabs +'">' +
                '                <i class="fa fa-remove"></i>' +
                '            </button>' +
                '        </div>' +
                '    </div>' +


                '    <div class="row">' +
                '        <div class="col-md-3">' +
                '            <div class="form-group">' +
                '                <label class="control-label">{{ __('pages.cycle') }}</label>' +
                '                <select data-placeholder="{{ __('pages.chose_cycle') }}" class="standardSelect"' +
                '                    name="additional_cycles[]" title="{{ __('pages.cycle') }}">' +
                '                    <option value="0">{{ __('pages.chose_cycle') }}</option>' +
                '                        @foreach($cycles as $key => $cycle)' +
                '                            <option value="{{ $key }}">{{ $cycle }}</option>' +
                '                        @endforeach' +
                '                </select>' +
                '            </div>' +
                '        </div>' +

                '        <div class="col-md-3">' +
                '            <div class="form-group">' +
                '                <label class="control-label">{{ __('pages.faculty') }}</label>' +
                '                <select data-placeholder="{{ __('pages.chose_faculty') }}" class="standardSelect"' +
                '                    name="additional_faculties[]" title="{{ __('pages.faculty') }}">' +
                '                    <option value="0">{{ __('pages.chose_cycle') }}</option>' +
                '                </select>' +
                '            </div>' +
                '        </div>' +

                '        <div class="col-md-3">' +
                '            <div class="form-group">' +
                '                <label class="control-label">{{ __('pages.speciality') }}</label>' +
                '                <select data-placeholder="{{ __('pages.chose_speciality') }}" class="standardSelect"' +
                '                    name="additional_specialities[]" title="{{ __('pages.speciality') }}">' +
                '                    <option value="0">{{ __('pages.chose_faculty') }}</option>' +
                '                </select>' +
                '            </div>' +
                '        </div>' +

                '        <div class="col-md-3">' +
                '            <div class="form-group">' +
                '                <label class="control-label">{{ __('pages.group') }}</label>' +
                '                <select data-placeholder="{{ __('pages.chose_group') }}" class="standardSelect"' +
                '                    name="additional_groups[]" title="{{ __('pages.group') }}">' +
                '                    <option value="0">{{ __('pages.chose_speciality') }}</option>' +
                '                </select>' +
                '            </div>' +
                '        </div>' +
                '    </div>' +


                '    <div class="row">' +
                '        <div class="col-md-2">' +
                '            <div class="form-group">' +
                '                <label class="control-label">{{ __('pages.year_of_study') }}</label>' +
                '                <select data-placeholder="{{ __('pages.year_of_study') }}" class="standardSelect"' +
                '                    name="additional_year_of_studies[]" title="{{ __('pages.year_of_study') }}">' +
                '                    @foreach($yearOfStudies as $key => $yearOfStudy)' +
                '                        <option value="{{ $key }}">{{ $yearOfStudy }}</option>' +
                '                    @endforeach' +
                '                </select>' +
                '            </div>' +
                '        </div>' +

                '        <div class="col-md-2">' +
                '            <div class="form-group">' +
                '                <label class="control-label">{{ __('pages.year_of_admission') }}</label>' +
                '                <input class="input-sm form-control-sm form-control" name="year_of_admissions[]" type="text">' +
                '            </div>' +
                '        </div>' +

                '        <div class="col-md-2">' +
                '            <div class="form-group">' +
                '                <label class="control-label">{{ __('pages.study') }}</label>' +
                '                <select data-placeholder="{{ __('pages.study') }}" class="standardSelect" name="studies[]" title="{{ __('pages.study') }}">' +
                '                    <option value="1">{{ __('pages.morning') }}</option>' +
                '                    <option value="2">{{ __('pages.afternoon') }}</option>' +
                '                </select>' +
                '            </div>' +
                '        </div>' +

                '        <div class="col-md-2">' +
                '            <div class="form-group">' +
                '                <label class="control-label">{{ __('pages.budget') }}</label>' +
                '                <select data-placeholder="{{ __('pages.budget') }}" class="standardSelect" name="budgets[]" title="{{ __('pages.budget') }}">' +
                '                    <option value="1">{{ __('pages.yes') }}</option>' +
                '                    <option value="0">{{ __('pages.no') }}</option>' +
                '                </select>' +
                '            </div>' +
                '        </div>' +

                '        <div class="col-md-2">' +
                '            <div class="form-group">' +
                '                <label class="control-label">{{ __('pages.expelled') }}</label>' +
                '                <select data-placeholder="{{ __('pages.expelled') }}" class="standardSelect" name="expelleds[]" title="{{ __('pages.expelled') }}">' +
                '                    <option value="1">{{ __('pages.yes') }}</option>' +
                '                    <option value="0">{{ __('pages.no') }}</option>' +
                '                </select>' +
                '           </div>' +
                '        </div>' +
                '    </div>' +


                '    <div class="row">' +
                '        <div class="col-md-6">' +
                '            <div class="form-group">' +
                '                <label class="control-label">{{ __('pages.additional_info') }}</label>' +
                '                <textarea class="input-sm form-control-sm form-control" rows="4" style="resize: none" name="additional_infos[]" cols="50"></textarea>' +
                '            </div>' +
                '        </div>' +
                '    </div>' +
                '</div>';

            tabContents.find('div').removeClass('active show');
            navTabs.find('a').removeClass('active show');

            $(buttonHtml).insertBefore($(this));
            tabContents.append(tabHtml);

            $('#base-info'+ additionalTabs +' .standardSelect').chosen({
                disable_search_threshold: 10,
                no_results_text: '{{ __('pages.nothing_found') }}',
                width: '100%'
            });

            console.log('ADD: ' + additionalTabs);
        });

        body.on('click', '.remove-tab', function () {
            let navTabs = $('#nav-tab-additional');
            let tabContents = $('#nav-tabContent2');

            let tab = $(this).attr('data-tab');
            let tabCounter = $(this).attr('data-tab-counter');
            let deletedTabButton = $('#base-info-tab-' + tabCounter);
            let deletedTabContent = $('#' + tab);

            if(tabCounter > 0){
                additionalTabs --;
            }

            tabContents.find('div').removeClass('active show');
            navTabs.find('a').removeClass('active show');

            deletedTabButton.prev().addClass('active show');
            deletedTabButton.remove();

            deletedTabContent.prev().addClass('active show');
            deletedTabContent.remove();

            let sd = tab.replace(/[^0-9]/gi, '');
            let id = parseInt(sd, 10);

            if(id < 1000000){
                removeAdditionalStudy(id);
            }

            console.log('REMOVE: ' + additionalTabs);
        });

        body.on('change', '#cycle_id', function () {
            getFacultiesByCycleForSelectAddEdit('#cycle_id');
        });
        body.on('change', '#faculty_id', function () {
            getSpecialitiesByFacultyForSelectAddEdit('#faculty_id');
        });
        body.on('change', '#speciality_id', function () {
            getGroupsBySpecialityForSelectAddEdit('#speciality_id');
        });



        body.on('change', 'select[name="additional_cycles[]"]', function () {
            let id = $(this);
            getFacultiesByCycleForSelectAdditionalInfo(id);
        });
        body.on('change', 'select[name="additional_faculties[]"]', function () {
            let id = $(this);
            getSpecialitiesByFacultyForSelectAdditionalInfo(id)
        });
        body.on('change', 'select[name="additional_specialities[]"]', function () {
            let id = $(this);
            getGroupsBySpecialityForSelectAdditionalInfo(id)
        });



        function getFacultiesByCycleForSelectAddEdit(element, hidden = null) {
            let loader = $('.custom-loader');
            let cycleId = $.trim($(element).val());
            let facultySelect = $('#faculty_id');
            let specialitySelect = $('#speciality_id');
            let groupSelect = $('#group_id');

            if(cycleId === '' || cycleId === undefined || cycleId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });
            
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('rector.faculties.getFacultiesByCycleForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    cycleId: cycleId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });
                    
                    facultySelect.empty();
                    facultySelect.html(response.html);
                    facultySelect.trigger('chosen:updated');

                    specialitySelect.empty();
                    specialitySelect.trigger('chosen:updated');

                    groupSelect.empty();
                    groupSelect.trigger('chosen:updated');

                    let totalFaculties = $('#faculty_id > option').length;
                    if(totalFaculties === 1){
                        getSpecialitiesByFacultyForSelectAddEdit('#faculty_id');
                    }

                    let totalSpecialities = $('#speciality_id > option').length;
                    if(totalSpecialities === 1){
                        getGroupsBySpecialityForSelectAddEdit('#speciality_id');
                    }
                }
            });
        }
        function getSpecialitiesByFacultyForSelectAddEdit(element, hidden = null) {
            let loader = $('.custom-loader');
            let facultyId = $.trim($(element).val());
            let specialitySelect = $('#speciality_id');

            if(facultyId === '' || facultyId === undefined || facultyId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });
            
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('rector.specialities.getSpecialitiesByFacultyForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    facultyId: facultyId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });
                    
                    specialitySelect.empty();
                    specialitySelect.html(response.html);
                    specialitySelect.trigger('chosen:updated');

                    let totalSpecialities = $('#speciality_id > option').length;
                    if(totalSpecialities === 1){
                        getGroupsBySpecialityForSelectAddEdit('#speciality_id');
                    }
                }
            });
        }
        function getGroupsBySpecialityForSelectAddEdit(element, hidden = null) {
            let loader = $('.custom-loader');
            let specialityId = $.trim($(element).val());
            let groupSelect = $('#group_id');

            if(specialityId === '' || specialityId === undefined || specialityId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });
            
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('rector.groups.getGroupsBySpecialityForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    specialityId: specialityId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });
                    
                    groupSelect.empty();
                    groupSelect.html(response.html);
                    groupSelect.trigger('chosen:updated');
                }
            });
        }


        function getFacultiesByCycleForSelectAdditionalInfo(element) {
            let loader = $('.custom-loader');
            let cycleId = element.val();
            let parent = element.parents('.parent');


            let facultySelect = parent.find('select[name="additional_faculties[]"]');
            let specialitySelect = parent.find('select[name="additional_specialities[]"]');
            let groupSelect = parent.find('select[name="additional_groups[]"]');

            if(cycleId === '' || cycleId === undefined || cycleId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('rector.faculties.getFacultiesByCycleForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    cycleId: cycleId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    facultySelect.empty();
                    facultySelect.html(response.html);
                    facultySelect.trigger('chosen:updated');

                    specialitySelect.empty();
                    specialitySelect.trigger('chosen:updated');

                    groupSelect.empty();
                    groupSelect.trigger('chosen:updated');
                }
            });
        }
        function getSpecialitiesByFacultyForSelectAdditionalInfo(element) {
            let loader = $('.custom-loader');
            let facultyId = element.val();
            let parent = element.parents('.parent');
            let specialitySelect = parent.find('select[name="additional_specialities[]"]');

            if(facultyId === '' || facultyId === undefined || facultyId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('rector.specialities.getSpecialitiesByFacultyForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    facultyId: facultyId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    specialitySelect.empty();
                    specialitySelect.html(response.html);
                    specialitySelect.trigger('chosen:updated');
                }
            });
        }
        function getGroupsBySpecialityForSelectAdditionalInfo(element) {
            let loader = $('.custom-loader');
            let specialityId = element.val();
            let parent = element.parents('.parent');
            let groupSelect = parent.find('select[name="additional_groups[]"]');

            if(specialityId === '' || specialityId === undefined || specialityId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('rector.groups.getGroupsBySpecialityForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    specialityId: specialityId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    groupSelect.empty();
                    groupSelect.html(response.html);
                    groupSelect.trigger('chosen:updated');
                }
            });
        }
        function removeAdditionalStudy(id) {
            let loader = $('.custom-loader');

            if(id === '' || id === undefined || id == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('rector.students.deleteAdditionalStudy')}}',
                type: 'POST',
                async: true,
                data: {
                    id: id
                },
                success: function(){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });
                }
            });
        }
    </script>
@endsection