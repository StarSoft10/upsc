@extends('rector::layouts.app')

@section('title')
    {{ __('pages.year_of_studies') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            {!! Form::open(['route' => 'rector.year-of-studies.search', 'method' => 'GET']) !!}
                                <div class="row">
                                    <div class="col-md-4">
                                        {!! Form::text('name', $name, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.name')]) !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::submit(__('pages.search'), ['class' => 'btn btn-primary btn-sm']) !!}
                                        <a href="{{ route('rector.year-of-studies.index') }}" class="btn btn-info btn-sm" title="{{ __('pages.reset_filter') }}">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-2 text-right">
                            @can('create_year_of_study')
                                <button type="button" class="btn btn-success mb-1 btn-sm" data-toggle="modal" data-target="#addYearOfStudy">
                                    {{ __('pages.add_year_of_study') }}
                                </button>
                            @endcan
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th>{{ __('pages.name') }}</th>
                                <th>{{ __('pages.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($yearOfStudies) > 0)
                                @foreach($yearOfStudies as $yearOfStudy)
                                    <tr>
                                        <td class="text-center"><span>{{ $yearOfStudy->name }}</span></td>
                                        <td class="text-center">
                                            @can('view_year_of_study')
                                                <a href="{{ route('rector.year-of-studies.show', $yearOfStudy->id) }}" class="btn-no-bg" title="{{ __('pages.show_edit') }}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            @endcan

                                            @can('delete_year_of_study')
                                                <a href="javascript:void(0)" data-yearofstudyid="{{ $yearOfStudy->id }}" class="btn-no-bg delete-year-of-study" title="{{ __('pages.delete') }}">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="2">{{ __('messages.not_found_any_year_of_studies') }}</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="table-footer">
                        {!! $yearOfStudies->render() !!}
                        <div class="total-found">{{ __('pages.total_found') }}: {{ $yearOfStudies->total() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @can('create_year_of_study')
        <div class="modal fade" id="addYearOfStudy" role="dialog" aria-labelledby="addYearOfStudy" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    {!! Form::open(['url' => route('rector.year-of-studies.store'), 'method' => 'post', 'autocomplete' => 'off']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.add_year_of_study') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('name', __('pages.name'), ['class' => 'control-label']) !!}
                                        {!! Form::text('name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            {!! Form::submit(__('pages.add'), ['class' => 'btn btn-success btn-sm']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan

    @can('delete_year_of_study')
        <div class="modal fade" id="deleteYearOfStudy" role="dialog" aria-labelledby="deleteYearOfStudy" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['route' => 'rector.year-of-studies.delete']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.delete_year_of_study') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-info" role="alert">
                                <div class="alert-text">
                                    {{ __('pages.sure_to_delete_year_of_study?') }}
                                </div>
                            </div>
                            <input type="hidden" name="year_of_study_id">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            <button type="submit" class="btn btn-danger btn-sm">{{ __('pages.delete') }}</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan
@endsection

@section('custom-css')
@endsection

@section('custom-scripts')
    <script>
        let body = $('body');

        body.on('click', '.delete-year-of-study', function () {
            let yearOfStudyId = $(this).attr('data-yearofstudyid');
            let modal = $('#deleteYearOfStudy');
            modal.find('input[name="year_of_study_id"]').val(yearOfStudyId);
            modal.modal('show');
        });

        $('#deleteYearOfStudy').on('hidden.bs.modal', function () {
            $(this).find('input[name="year_of_study_id"]').val('');
        });
    </script>
@endsection