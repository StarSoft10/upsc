@extends('rector::layouts.app')

@section('title')
    {{ __('pages.year_of_study') }} {{ $yearOfStudy->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-info-circle"></i>
                    <strong class="card-title">{{ __('pages.year_of_study_information') }}</strong>
                </div>
                <div class="card-body">
                    <ul style="list-style: none;">
                        <li><span>{{ __('pages.name') }}:</span><b>&nbsp;{{ $yearOfStudy->name }}</b></li>
                    </ul>
                </div>
            </div>
        </div>
        @can('edit_year_of_study')
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-edit"></i>
                        <strong class="card-title">{{ __('pages.edit_year_of_study') }}</strong>
                    </div>
                    <div class="card-body">
                        {!! Form::model($yearOfStudy, ['method' => 'PATCH', 'route' => ['rector.year-of-studies.update', $yearOfStudy->id], 'autocomplete' => 'off']) !!}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('name', __('pages.name'), ['class' => 'control-label']) !!}
                                        {!! Form::text('name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                </div>
                            </div>
                            {!! Form::submit(__('pages.edit'), ['class' => 'btn btn-primary btn-sm']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        @endcan
    </div>
@endsection

@section('custom-css')
@endsection

@section('custom-scripts')
@endsection
