@extends('rector::layouts.app')

@section('title')
    {{ __('pages.unconfirmed_user') }} {{ $unconfirmedUser->first_name }} {{ $unconfirmedUser->last_name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>{{ $unconfirmedUser->first_name }} {{ $unconfirmedUser->last_name }}</h4>
                </div>
                <div class="card-body">
                    <div class="custom-tab">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active show" id="information-tab" data-toggle="tab" href="#information" role="tab" aria-controls="information" aria-selected="true">
                                    <i class="fa fa-info-circle"></i>
                                    {{ __('pages.unconfirmed_user_information') }}
                                </a>
                                @can('edit_unconfirmed_users')
                                    <a class="nav-item nav-link" id="edit-tab" data-toggle="tab" href="#edit" role="tab" aria-controls="edit" aria-selected="false">
                                        <i class="fa fa-edit"></i>
                                        {{ __('pages.edit_unconfirmed_user') }}
                                    </a>
                                @endcan
                            </div>
                        </nav>

                        <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                            <div class="tab-pane fade active show" id="information" role="tabpanel" aria-labelledby="information-tab">
                                <div class="mx-auto d-block">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <img class="rounded-circle mx-auto d-block" src="{{ asset('images/default_photo.jpg') }}" alt="Photo">
                                            <h5 class="text-sm-center mt-2 mb-1">{{ $unconfirmedUser->first_name }} {{ $unconfirmedUser->last_name }}</h5>
                                        </div>
                                        <div class="col-md-4">
                                            <ul style="list-style: none;">
                                                <li>
                                                    <i class="fa fa-envelope"></i>
                                                    <span>{{ __('pages.email') }}:</span>
                                                    <a href="mailto:{{ $unconfirmedUser->email }}">{{ $unconfirmedUser->email }}</a>
                                                </li>
                                                <li>
                                                    <i class="fa fa-phone"></i>
                                                    <span>{{ __('pages.phone') }}:</span>
                                                    <a href="tel:{{ $unconfirmedUser->phone }}">{{ $unconfirmedUser->phone }}</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @can('edit_unconfirmed_users')
                                <div class="tab-pane fade" id="edit" role="tabpanel" aria-labelledby="edit-tab">
                                    {!! Form::model($unconfirmedUser, ['method' => 'PATCH', 'route' => ['rector.unconfirmed-users.update', $unconfirmedUser->id], 'autocomplete' => 'off']) !!}
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {!! Form::label('first_name', __('pages.first_name'), ['class' => 'control-label']) !!}
                                                    {!! Form::text('first_name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('email', __('pages.email'), ['class' => 'control-label']) !!}
                                                    {!! Form::email('email', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('password', __('pages.password'), ['class' => 'control-label']) !!}
                                                    {!! Form::input('password', 'password', null, ['class' => 'input-sm form-control-sm form-control', 'autocomplete' => 'off']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {!! Form::label('last_name', __('pages.last_name'), ['class' => 'control-label']) !!}
                                                    {!! Form::text('last_name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('phone', __('pages.phone'), ['class' => 'control-label']) !!}
                                                    {!! Form::text('phone', null, ['class' => 'input-sm form-control-sm form-control']) !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::label('password_confirmation', __('pages.confirm_password'), ['class' => 'control-label']) !!}
                                                    {!! Form::input('password', 'password_confirmation', null, ['class' => 'input-sm form-control-sm form-control',
                                                        'autocomplete' => 'off']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        {!! Form::submit(__('pages.edit'), ['class' => 'btn btn-primary btn-sm']) !!}
                                    {!! Form::close() !!}
                                </div>
                            @endcan
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
@endsection

@section('custom-scripts')
@endsection
