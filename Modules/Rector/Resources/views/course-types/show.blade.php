@extends('rector::layouts.app')

@section('title')
    {{ __('pages.course_type') }} {{ $courseType->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-info-circle"></i>
                    <strong class="card-title">{{ __('pages.course_type_information') }}</strong>
                </div>
                <div class="card-body">
                    <ul style="list-style: none;">
                        <li><span>{{ __('pages.name') }}:</span><b>&nbsp;{{ $courseType->name }}</b></li>
                        <li><span>{{ __('pages.short_name') }}:</span><b>&nbsp;{{ $courseType->short_name }}</b></li>
                        <li><span>{{ __('pages.color') }}:</span><b>&nbsp;{{ $courseType->color }}</b></li>
                    </ul>
                </div>
            </div>
        </div>

        @can('edit_course_type')
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-edit"></i>
                        <strong class="card-title">{{ __('pages.edit_course_type') }}</strong>
                    </div>
                    <div class="card-body">
                        {!! Form::model($courseType, ['method' => 'PATCH', 'route' => ['rector.course-types.update', $courseType->id], 'autocomplete' => 'off']) !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('name', __('pages.name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('short_name', __('pages.short_name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('short_name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('color', __('pages.color'), ['class' => 'control-label']) !!}
                                    {!! Form::color('color', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                        </div>
                        {!! Form::submit(__('pages.edit'), ['class' => 'btn btn-primary btn-sm']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        @endcan
    </div>
@endsection

@section('custom-css')
@endsection

@section('custom-scripts')
@endsection
