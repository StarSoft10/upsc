@extends('rector::layouts.app')

@section('title')
    {{ __('pages.duration') }} {{ $duration->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-info-circle"></i>
                    <strong class="card-title">{{ __('pages.duration_information') }}</strong>
                </div>
                <div class="card-body">
                    <ul style="list-style: none;">
                        <li><span>{{ __('pages.name') }}:</span><b>&nbsp;{{ $duration->name }}</b></li>
                        <li><span>{{ __('pages.start') }}:</span><b>&nbsp;{{ $duration->start }}</b></li>
                        <li><span>{{ __('pages.end') }}:</span><b>&nbsp;{{ $duration->end }}</b></li>
                        <li><span>{{ __('pages.order') }}:</span><b>&nbsp;{{ $duration->order }}</b></li>
                        <li><span>{{ __('pages.duration_type') }}:</span><b>&nbsp;{{ $duration->durationType->name }}</b></li>
                    </ul>
                </div>
            </div>
        </div>

        @can('edit_duration')
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-edit"></i>
                        <strong class="card-title">{{ __('pages.edit_duration') }}</strong>
                    </div>
                    <div class="card-body">
                        {!! Form::model($duration, ['method' => 'PATCH', 'route' => ['rector.durations.update', $duration->id], 'autocomplete' => 'off']) !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('name', __('pages.name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('start', __('pages.start'), ['class' => 'control-label']) !!}
                                    {!! Form::date('start', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('end', __('pages.end'), ['class' => 'control-label']) !!}
                                    {!! Form::date('end', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('order', __('pages.order'), ['class' => 'control-label']) !!}
                                    {!! Form::number('order', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('duration_type_id', __('pages.duration_type'), ['class' => 'control-label']) !!}
                                    <select data-placeholder="{{ __('pages.chose_duration_type') }}" class="standardSelect" name="duration_type_id" id="duration_type_id">
                                        @foreach($durationTypes as $key => $durationType)
                                            <option value="{{ $key }}" @if($key == $duration->duration_type_id) selected @endif>{{ $durationType }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        {!! Form::submit(__('pages.edit'), ['class' => 'btn btn-primary btn-sm']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        @endcan
    </div>
@endsection

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('vendors/chosen/chosen.min.css') }}">
@endsection

@section('custom-scripts')
    <script src="{{ asset('vendors/chosen/chosen.jquery.min.js') }}"></script>
    <script>
        $(function() {
            $('.standardSelect').chosen({
                disable_search_threshold: 10,
                no_results_text: '{{ __('pages.nothing_found') }}',
                width: '100%'
            });
        });
    </script>
@endsection