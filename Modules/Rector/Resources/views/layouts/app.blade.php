<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8" />
    <title>
        @yield('title', '')
    </title>
    <meta name="description" content="UPSC">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/themify-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/selectFX/css/cs-skin-elastic.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/jqvmap/dist/jqvmap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/aside.css') }}">
    <link rel="stylesheet" href="{{ asset('css/users.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome/css/all.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <link rel="icon" type="image/png" sizes="64x64" href="{{asset('images/icon.png')}}">
    @yield('custom-css')
</head>

<body>

@include('rector::layouts.aside')

<div id="right-panel" class="right-panel">
    @include('rector::layouts.header')

    <div class="content mt-3">
        @if(session()->has('success') || session()->has('warning') || session()->has('error'))
            <div class="row">
                <div class="col-sm-12">
                    @if(session()->has('success'))
                        <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                            <span class="badge badge-pill badge-success">{{ __('pages.success') }}</span>
                            {{ session()->get('success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif

                    @if(session()->has('warning'))
                        <div class="sufee-alert alert with-close alert-warning alert-dismissible fade show">
                            <span class="badge badge-pill badge-warning">{{ __('pages.warning') }}</span>
                            {{ session()->get('warning') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif

                    @if(session()->has('error'))
                        <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                            <span class="badge badge-pill badge-danger">{{ __('pages.error') }}</span>
                            {{ session()->get('error') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif
                </div>
            </div>
        @endif

        @yield('content')

    </div>
</div>


{{--@stack('css')--}}

{{--@stack('js')--}}
{{--<script src="{{ asset('js/jquery3.5.1.min.js') }}"></script>--}}
{{--<script src="{{ asset('js/bootstrap.js') }}"></script>--}}

<script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('vendors/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ asset('vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script src="{{ asset('js/permissions.js') }}"></script>
<script src="{{ asset('js/notify.min.js') }}"></script>


{{--<script src="{{ asset('vendors/chart.js/dist/Chart.bundle.min.js') }}"></script>--}}
{{--<script src="{{ asset('assets/js/dashboard.js') }}"></script>--}}
{{--<script src="{{ asset('assets/js/widgets.js') }}"></script>--}}
{{--<script src="{{ asset('vendors/jqvmap/dist/jquery.vmap.min.js') }}"></script>--}}
{{--<script src="{{ asset('vendors/jqvmap/examples/js/jquery.vmap.sampledata.js') }}"></script>--}}
{{--<script src="{{ asset('vendors/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>--}}

@yield('custom-scripts')
</body>
</html>
