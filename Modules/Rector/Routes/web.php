<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

Route::group([
    'prefix' => LaravelLocalization::setLocale() . '/rector',
    'middleware' => ['auth', 'role:rector', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'localize']
], function () {
    Route::get('/', 'RectorController@index')->name('rector');
    Route::get('profile', 'RectorController@show')->name('rector.profile');
    Route::patch('update', 'RectorController@update')->name('rector.update');
    Route::patch('changePhoto', 'RectorController@changePhoto')->name('rector.changePhoto');
    Route::get('not-found', 'RectorController@notFound')->name('rector.not-found');


    /*
     * User routes
     */
    Route::group(['prefix' => 'unconfirmed-users'], function () {
        Route::post('delete', 'UnconfirmedUserController@delete')->name('rector.unconfirmed-users.delete');
        Route::get('search', 'UnconfirmedUserController@search')->name('rector.unconfirmed-users.search');
        Route::post('confirm', 'UnconfirmedUserController@confirm')->name('rector.unconfirmed-users.confirm');
    });
    Route::resource('unconfirmed-users', 'UnconfirmedUserController', ['names' => [
        'index' => 'rector.unconfirmed-users.index',
        'show' => 'rector.unconfirmed-users.show',
        'update' => 'rector.unconfirmed-users.update'
    ]]);


    Route::group(['prefix' => 'secretaries'], function () {
        Route::post('delete', 'SecretaryController@delete')->name('rector.secretaries.delete');
        Route::get('search', 'SecretaryController@search')->name('rector.secretaries.search');
        Route::post('block', 'SecretaryController@block')->name('rector.secretaries.block');
        Route::post('unblock', 'SecretaryController@unblock')->name('rector.secretaries.unblock');
    });
    Route::resource('secretaries', 'SecretaryController', ['names' => [
        'index' => 'rector.secretaries.index',
        'store' => 'rector.secretaries.store',
        'show' => 'rector.secretaries.show',
        'update' => 'rector.secretaries.update'
    ]]);


    Route::group(['prefix' => 'deans'], function () {
        Route::post('delete', 'DeanController@delete')->name('rector.deans.delete');
        Route::get('search', 'DeanController@search')->name('rector.deans.search');
        Route::post('block', 'DeanController@block')->name('rector.deans.block');
        Route::post('unblock', 'DeanController@unblock')->name('rector.deans.unblock');
    });
    Route::resource('deans', 'DeanController', ['names' => [
        'index' => 'rector.deans.index',
        'store' => 'rector.deans.store',
        'show' => 'rector.deans.show',
        'update' => 'rector.deans.update'
    ]]);


    Route::group(['prefix' => 'chairs'], function () {
        Route::post('delete', 'ChairController@delete')->name('rector.chairs.delete');
        Route::get('search', 'ChairController@search')->name('rector.chairs.search');
        Route::post('block', 'ChairController@block')->name('rector.chairs.block');
        Route::post('unblock', 'ChairController@unblock')->name('rector.chairs.unblock');
    });
    Route::resource('chairs', 'ChairController', ['names' => [
        'index' => 'rector.chairs.index',
        'store' => 'rector.chairs.store',
        'show' => 'rector.chairs.show',
        'update' => 'rector.chairs.update'
    ]]);


    Route::group(['prefix' => 'human-resources'], function () {
        Route::post('delete', 'HumanResourceController@delete')->name('rector.human-resources.delete');
        Route::get('search', 'HumanResourceController@search')->name('rector.human-resources.search');
        Route::post('block', 'HumanResourceController@block')->name('rector.human-resources.block');
        Route::post('unblock', 'HumanResourceController@unblock')->name('rector.human-resources.unblock');
    });
    Route::resource('human-resources', 'HumanResourceController', ['names' => [
        'index' => 'rector.human-resources.index',
        'store' => 'rector.human-resources.store',
        'show' => 'rector.human-resources.show',
        'update' => 'rector.human-resources.update'
    ]]);


    Route::group(['prefix' => 'teachers'], function () {
        Route::post('delete', 'TeacherController@delete')->name('rector.teachers.delete');
        Route::get('search', 'TeacherController@search')->name('rector.teachers.search');
        Route::post('block', 'TeacherController@block')->name('rector.teachers.block');
        Route::post('unblock', 'TeacherController@unblock')->name('rector.teachers.unblock');
    });
    Route::resource('teachers', 'TeacherController', ['names' => [
        'index' => 'rector.teachers.index',
        'store' => 'rector.teachers.store',
        'show' => 'rector.teachers.show',
        'update' => 'rector.teachers.update'
    ]]);


    Route::group(['prefix' => 'students'], function () {
        Route::post('delete', 'StudentController@delete')->name('rector.students.delete');
        Route::get('search', 'StudentController@search')->name('rector.students.search');
        Route::post('block', 'StudentController@block')->name('rector.students.block');
        Route::post('unblock', 'StudentController@unblock')->name('rector.students.unblock');
        Route::post('exportStudentsPdf', 'StudentController@exportStudentsPdf')->name('rector.students.exportStudentsPdf');
        Route::post('exportStudentsExcel', 'StudentController@exportStudentsExcel')->name('rector.students.exportStudentsExcel');
        Route::post('exportStudentPdf', 'StudentController@exportStudentPdf')->name('rector.students.exportStudentPdf');
        Route::post('exportStudentExcel', 'StudentController@exportStudentExcel')->name('rector.students.exportStudentExcel');
        Route::post('deleteAdditionalStudy', 'StudentController@deleteAdditionalStudy')->name('rector.students.deleteAdditionalStudy');
    });
    Route::resource('students', 'StudentController', ['names' => [
        'index' => 'rector.students.index',
        'store' => 'rector.students.store',
        'show' => 'rector.students.show',
        'update' => 'rector.students.update'
    ]]);


    /*
     * Studies routes
     */
    Route::group(['prefix' => 'year-of-studies'], function () {
        Route::post('delete', 'YearOfStudyController@delete')->name('rector.year-of-studies.delete');
        Route::get('search', 'YearOfStudyController@search')->name('rector.year-of-studies.search');
    });
    Route::resource('year-of-studies', 'YearOfStudyController', ['names' => [
        'index' => 'rector.year-of-studies.index',
        'store' => 'rector.year-of-studies.store',
        'show' => 'rector.year-of-studies.show',
        'update' => 'rector.year-of-studies.update'
    ]]);


    Route::group(['prefix' => 'cycles'], function () {
        Route::post('delete', 'CycleController@delete')->name('rector.cycles.delete');
        Route::get('search', 'CycleController@search')->name('rector.cycles.search');
    });
    Route::resource('cycles', 'CycleController', ['names' => [
        'index' => 'rector.cycles.index',
        'store' => 'rector.cycles.store',
        'show' => 'rector.cycles.show',
        'update' => 'rector.cycles.update'
    ]]);


    Route::group(['prefix' => 'faculties'], function () {
        Route::post('delete', 'FacultyController@delete')->name('rector.faculties.delete');
        Route::get('search', 'FacultyController@search')->name('rector.faculties.search');
        Route::post('getFacultiesByCycleForSelectSearch', 'FacultyController@getFacultiesByCycleForSelectSearch')->name('rector.faculties.getFacultiesByCycleForSelectSearch');
        Route::post('getFacultiesByCycleForSelectAddEdit', 'FacultyController@getFacultiesByCycleForSelectAddEdit')->name('rector.faculties.getFacultiesByCycleForSelectAddEdit');
    });
    Route::resource('faculties', 'FacultyController', ['names' => [
        'index' => 'rector.faculties.index',
        'store' => 'rector.faculties.store',
        'show' => 'rector.faculties.show',
        'update' => 'rector.faculties.update'
    ]]);


    Route::group(['prefix' => 'specialities'], function () {
        Route::post('delete', 'SpecialityController@delete')->name('rector.specialities.delete');
        Route::get('search', 'SpecialityController@search')->name('rector.specialities.search');
        Route::post('getSpecialitiesByFacultyForSelectSearch', 'SpecialityController@getSpecialitiesByFacultyForSelectSearch')->name('rector.specialities.getSpecialitiesByFacultyForSelectSearch');
        Route::post('getSpecialitiesByFacultyForSelectAddEdit', 'SpecialityController@getSpecialitiesByFacultyForSelectAddEdit')->name('rector.specialities.getSpecialitiesByFacultyForSelectAddEdit');
    });
    Route::resource('specialities', 'SpecialityController', ['names' => [
        'index' => 'rector.specialities.index',
        'store' => 'rector.specialities.store',
        'show' => 'rector.specialities.show',
        'update' => 'rector.specialities.update'
    ]]);


    Route::group(['prefix' => 'groups'], function () {
        Route::post('delete', 'GroupController@delete')->name('rector.groups.delete');
        Route::get('search', 'GroupController@search')->name('rector.groups.search');
        Route::post('getGroupsBySpecialityForSelectSearch', 'GroupController@getGroupsBySpecialityForSelectSearch')->name('rector.groups.getGroupsBySpecialityForSelectSearch');
        Route::post('getGroupsBySpecialityForSelectAddEdit', 'GroupController@getGroupsBySpecialityForSelectAddEdit')->name('rector.groups.getGroupsBySpecialityForSelectAddEdit');
    });
    Route::resource('groups', 'GroupController', ['names' => [
        'index' => 'rector.groups.index',
        'store' => 'rector.groups.store',
        'show' => 'rector.groups.show',
        'update' => 'rector.groups.update'
    ]]);


    Route::group(['prefix' => 'courses'], function () {
        Route::post('delete', 'CourseController@delete')->name('rector.courses.delete');
        Route::get('search', 'CourseController@search')->name('rector.courses.search');
    });
    Route::resource('courses', 'CourseController', ['names' => [
        'index' => 'rector.courses.index',
        'store' => 'rector.courses.store',
        'show' => 'rector.courses.show',
        'update' => 'rector.courses.update'
    ]]);


    Route::group(['prefix' => 'course-types'], function () {
        Route::post('delete', 'CourseTypeController@delete')->name('rector.course-types.delete');
        Route::get('search', 'CourseTypeController@search')->name('rector.course-types.search');
    });
    Route::resource('course-types', 'CourseTypeController', ['names' => [
        'index' => 'rector.course-types.index',
        'store' => 'rector.course-types.store',
        'show' => 'rector.course-types.show',
        'update' => 'rector.course-types.update'
    ]]);


    Route::group(['prefix' => 'days'], function () {
        Route::post('delete', 'DayController@delete')->name('rector.days.delete');
    });
    Route::resource('days', 'DayController', ['names' => [
        'index' => 'rector.days.index',
        'store' => 'rector.days.store',
        'show' => 'rector.days.show',
        'update' => 'rector.days.update'
    ]]);


    Route::group(['prefix' => 'duration-types'], function () {
        Route::post('delete', 'DurationTypeController@delete')->name('rector.duration-types.delete');
        Route::get('search', 'DurationTypeController@search')->name('rector.duration-types.search');
    });
    Route::resource('duration-types', 'DurationTypeController', ['names' => [
        'index' => 'rector.duration-types.index',
        'store' => 'rector.duration-types.store',
        'show' => 'rector.duration-types.show',
        'update' => 'rector.duration-types.update'
    ]]);


    Route::group(['prefix' => 'durations'], function () {
        Route::post('delete', 'DurationController@delete')->name('rector.durations.delete');
        Route::get('search', 'DurationController@search')->name('rector.durations.search');
    });
    Route::resource('durations', 'DurationController', ['names' => [
        'index' => 'rector.durations.index',
        'store' => 'rector.durations.store',
        'show' => 'rector.durations.show',
        'update' => 'rector.durations.update'
    ]]);


    Route::group(['prefix' => 'semesters'], function () {
        Route::post('delete', 'SemesterController@delete')->name('rector.semesters.delete');
    });
    Route::resource('semesters', 'SemesterController', ['names' => [
        'index' => 'rector.semesters.index',
        'store' => 'rector.semesters.store',
        'show' => 'rector.semesters.show',
        'update' => 'rector.semesters.update'
    ]]);


    Route::group(['prefix' => 'week-types'], function () {
        Route::post('delete', 'WeekTypeController@delete')->name('rector.week-types.delete');
        Route::get('search', 'WeekTypeController@search')->name('rector.week-types.search');
    });
    Route::resource('week-types', 'WeekTypeController', ['names' => [
        'index' => 'rector.week-types.index',
        'store' => 'rector.week-types.store',
        'show' => 'rector.week-types.show',
        'update' => 'rector.week-types.update'
    ]]);


    Route::group(['prefix' => 'weeks'], function () {
        Route::post('delete', 'WeekController@delete')->name('rector.weeks.delete');
        Route::get('search', 'WeekController@search')->name('rector.weeks.search');
    });
    Route::resource('weeks', 'WeekController', ['names' => [
        'index' => 'rector.weeks.index',
        'store' => 'rector.weeks.store',
        'show' => 'rector.weeks.show',
        'update' => 'rector.weeks.update'
    ]]);


    Route::group(['prefix' => 'teacher-ranks'], function () {
        Route::post('delete', 'TeacherRankController@delete')->name('rector.teacher-ranks.delete');
        Route::get('search', 'TeacherRankController@search')->name('rector.teacher-ranks.search');
    });
    Route::resource('teacher-ranks', 'TeacherRankController', ['names' => [
        'index' => 'rector.teacher-ranks.index',
        'store' => 'rector.teacher-ranks.store',
        'show' => 'rector.teacher-ranks.show',
        'update' => 'rector.teacher-ranks.update'
    ]]);


    /*
   * Borderous routes
   */
    Route::group(['prefix' => 'borderous'], function () {
        Route::post('delete', 'BorderouController@delete')->name('rector.borderous.delete');
        Route::get('search', 'BorderouController@search')->name('rector.borderous.search');
        Route::post('getBorderouForView', 'BorderouController@getBorderouForView')->name('rector.borderous.getBorderouForView');
        Route::post('getBorderouNote', 'BorderouController@getBorderouNote')->name('rector.borderous.getBorderouNote');
        Route::post('updateBorderouNote', 'BorderouController@updateBorderouNote')->name('rector.borderous.updateBorderouNote');
        Route::post('exportBorderouNotePdf', 'BorderouController@exportBorderouNotePdf')->name('rector.borderous.exportBorderouNotePdf');
        Route::post('exportBorderouNoteExcel', 'BorderouController@exportBorderouNoteExcel')->name('rector.borderous.exportBorderouNoteExcel');
        Route::post('deleteStudentBorderouNote', 'BorderouController@deleteStudentBorderouNote')->name('rector.borderous.deleteStudentBorderouNote');
        Route::post('addStudentBorderouNote', 'BorderouController@addStudentBorderouNote')->name('rector.borderous.addStudentBorderouNote');
    });
    Route::resource('borderous', 'BorderouController', ['names' => [
        'index' => 'rector.borderous.index',
        'store' => 'rector.borderous.store',
        'show' => 'rector.borderous.show',
        'update' => 'rector.borderous.update'
    ]]);
});