<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

Route::group([
    'prefix' => LaravelLocalization::setLocale() . '/dean',
    'middleware' => ['auth', 'role:dean', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'localize']
], function () {
    Route::get('/', 'DeanController@index')->name('dean');
    Route::get('profile', 'DeanController@show')->name('dean.profile');
    Route::patch('update', 'DeanController@update')->name('dean.update');
    Route::patch('changePhoto', 'DeanController@changePhoto')->name('dean.changePhoto');
    Route::get('not-found', 'DeanController@notFound')->name('dean.not-found');


    /*
     * User routes
     */
    Route::group(['prefix' => 'students'], function () {
        Route::post('delete', 'StudentController@delete')->name('dean.students.delete');
        Route::get('search', 'StudentController@search')->name('dean.students.search');
        Route::post('block', 'StudentController@block')->name('dean.students.block');
        Route::post('unblock', 'StudentController@unblock')->name('dean.students.unblock');
        Route::post('exportStudentsPdf', 'StudentController@exportStudentsPdf')->name('dean.students.exportStudentsPdf');
        Route::post('exportStudentsExcel', 'StudentController@exportStudentsExcel')->name('dean.students.exportStudentsExcel');
        Route::post('exportStudentPdf', 'StudentController@exportStudentPdf')->name('dean.students.exportStudentPdf');
        Route::post('exportStudentExcel', 'StudentController@exportStudentExcel')->name('dean.students.exportStudentExcel');
        Route::post('deleteAdditionalStudy', 'StudentController@deleteAdditionalStudy')->name('dean.students.deleteAdditionalStudy');
    });
    Route::resource('students', 'StudentController', ['names' => [
        'index' => 'dean.students.index',
        'store' => 'dean.students.store',
        'show' => 'dean.students.show',
        'update' => 'dean.students.update'
    ]]);


    /*
     * Studies routes
     */
    Route::group(['prefix' => 'faculties'], function () {
        Route::post('getFacultiesByCycleForSelectSearch', 'FacultyController@getFacultiesByCycleForSelectSearch')->name('dean.faculties.getFacultiesByCycleForSelectSearch');
        Route::post('getFacultiesByCycleForSelectAddEdit', 'FacultyController@getFacultiesByCycleForSelectAddEdit')->name('dean.faculties.getFacultiesByCycleForSelectAddEdit');
    });

    Route::group(['prefix' => 'specialities'], function () {
        Route::post('getSpecialitiesByFacultyForSelectSearch', 'SpecialityController@getSpecialitiesByFacultyForSelectSearch')->name('dean.specialities.getSpecialitiesByFacultyForSelectSearch');
        Route::post('getSpecialitiesByFacultyForSelectAddEdit', 'SpecialityController@getSpecialitiesByFacultyForSelectAddEdit')->name('dean.specialities.getSpecialitiesByFacultyForSelectAddEdit');
    });

    Route::group(['prefix' => 'groups'], function () {
        Route::post('getGroupsBySpecialityForSelectSearch', 'GroupController@getGroupsBySpecialityForSelectSearch')->name('dean.groups.getGroupsBySpecialityForSelectSearch');
        Route::post('getGroupsBySpecialityForSelectAddEdit', 'GroupController@getGroupsBySpecialityForSelectAddEdit')->name('dean.groups.getGroupsBySpecialityForSelectAddEdit');
    });


    /*
   * Borderous routes
   */
    Route::group(['prefix' => 'borderous'], function () {
        Route::post('delete', 'BorderouController@delete')->name('dean.borderous.delete');
        Route::get('search', 'BorderouController@search')->name('dean.borderous.search');
        Route::post('getBorderouForView', 'BorderouController@getBorderouForView')->name('dean.borderous.getBorderouForView');
        Route::post('getBorderouNote', 'BorderouController@getBorderouNote')->name('dean.borderous.getBorderouNote');
        Route::post('updateBorderouNote', 'BorderouController@updateBorderouNote')->name('dean.borderous.updateBorderouNote');
        Route::post('exportBorderouNotePdf', 'BorderouController@exportBorderouNotePdf')->name('dean.borderous.exportBorderouNotePdf');
        Route::post('exportBorderouNoteExcel', 'BorderouController@exportBorderouNoteExcel')->name('dean.borderous.exportBorderouNoteExcel');
        Route::post('deleteStudentBorderouNote', 'BorderouController@deleteStudentBorderouNote')->name('dean.borderous.deleteStudentBorderouNote');
        Route::post('addStudentBorderouNote', 'BorderouController@addStudentBorderouNote')->name('dean.borderous.addStudentBorderouNote');
    });
    Route::resource('borderous', 'BorderouController', ['names' => [
        'index' => 'dean.borderous.index',
        'store' => 'dean.borderous.store',
        'show' => 'dean.borderous.show',
        'update' => 'dean.borderous.update'
    ]]);
});