<?php

namespace Modules\Dean\Http\Controllers;

use App\Helpers\AppHelper;
use App\Helpers\StudyHelper;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Routing\Controller;

class GroupController extends Controller
{
    public $helper;

    public $studyHelper;

    public function __construct()
    {
        $this->helper = new AppHelper();
        $this->studyHelper = new StudyHelper();
    }

    public function getGroupsBySpecialityForSelectSearch(Request $request)
    {
        return $this->studyHelper->getGroupsBySpecialityForSelectSearch($request);
    }

    public function getGroupsBySpecialityForSelectAddEdit(Request $request)
    {
        return $this->studyHelper->getGroupsBySpecialityForSelectAddEdit($request);
    }
}
