@extends('dean::layouts.app')

@section('title')
    {{ __('pages.dashboard') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-info-circle"></i>
                    <strong class="card-title">{{ __('pages.information_of_users') }}</strong>
                </div>

                <div class="card-body">
                    <div id="chart_1" style="width:100%; height:400px;"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
@endsection

@section('custom-scripts')
<!--[if lt IE 9]>
<script src="https://code.highcharts.com/modules/oldie-polyfills.js"></script>
<![endif]-->

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>
    {{--<script src="https://code.highcharts.com/modules/exporting.js"></script>--}}
{{--<script src="https://code.highcharts.com/modules/export-data.js"></script>--}}
{{--<script src="https://code.highcharts.com/modules/accessibility.js"></script>--}}

<!--[if lt IE 9]>
    <script src="https://code.highcharts.com/modules/oldie.js"></script>
    <![endif]-->

    <script>
        let categories = [];
        let data = [];

        @foreach($userWithRoles as $name => $total)
        categories.push('{{ __('pages.' . $name) }}');
        data.push(parseInt('{{ $total }}'));
                @endforeach

        const chart = Highcharts.chart('chart_1', {
                chart: {
                    type: 'column',
                    options3d: {
                        enabled: true,
                        alpha: 10,
                        beta: 25,
                        depth: 70
                    }
                },
                title: {
                    text: '{{ __('pages.users') }}'
                },
                subtitle: {
                    text: '{{ __('messages.count_all_user_types') }}'
                },
                plotOptions: {
                    column: {
                        depth: 25
                    }
                },
                xAxis: {
                    categories: categories,
                    labels: {
                        skew3d: true,
                        style: {
                            fontSize: '16px'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: null
                    }
                },
                series: [{
                    name: '{{ __('pages.users') }}',
                    data: data
                }]
            });

        $('body').find('.highcharts-credits').hide();
    </script>
@endsection
