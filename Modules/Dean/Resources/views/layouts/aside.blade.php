<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="{{ route('dean') }}"><img src="{{ asset('images/logo.png') }}" alt="Logo" style="width: 29%;"></a>
            <a class="navbar-brand hidden" href="{{ route('dean') }}"><img src="{{ asset('images/logo.png') }}" alt="Logo"></a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="{{ route('dean') }}">
                        <i class="menu-icon fa fa-dashboard"></i>
                        {{ __('aside.dashboard') }}
                    </a>
                </li>

                @php
                    $userRoutes = false;
                    if(\Request::is('*/students*')){
                       $userRoutes = true;
                    }
                @endphp

                <h3 class="menu-title">{{ __('aside.all_user_types') }}</h3>
                <li class="menu-item-has-children dropdown @if($userRoutes) show @endif">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-users"></i>{{ __('aside.users') }}
                    </a>
                    <ul class="sub-menu children dropdown-menu @if($userRoutes) show @endif">
                        @can('view_student')
                            <li>
                                <i class="fa fa-user"></i>
                                <a href="{{ route('dean.students.index') }}" class="@if(Request::is('*students*')) current @endif">
                                    {{ __('aside.students') }}
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>

                <h3 class="menu-title">{{ __('aside.borderous') }}</h3>
                <li>
                    <a href="{{ route('dean.borderous.index') }}" class="@if(Request::is('*borderous*')) current @endif">
                        <i class="menu-icon ti-folder"></i>
                        {{ __('aside.borderous') }}
                    </a>
                </li>

                {{--<li>--}}
                    {{--<a href="javascript:void(0)"> <i class="menu-icon ti-email"></i>Some page</a>--}}
                {{--</li>--}}
            </ul>
        </div>
    </nav>
</aside>