<?php

namespace Modules\SuperAdmin\Http\Controllers;

use App\Entities\Course;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Routing\Controller;
use Illuminate\Validation\Rule;

class CourseController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    public function index(Request $request)
    {
        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';
        $code = !empty($request->code) ? trim($request->code) : '';

        $courses = Course::where('deleted', 0)->orderBy('id', 'ASC')->paginate(10);
        $courses->appends([
            'name' => $name,
            'shortName' => $shortName,
            'code' => $code
        ]);

        return view('superadmin::courses.index', compact('courses', 'name', 'shortName', 'code'));
    }

    public function search(Request $request)
    {
        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';
        $code = !empty($request->code) ? trim($request->code) : '';

        $courses = Course::where('deleted', 0)->where(function ($query) use ($name, $shortName, $code) {
            if ($name !== '') {
                $query->where('name', 'LIKE', '%' . trim($name) . '%');
            }
            if ($shortName !== '') {
                $query->where('short_name', 'LIKE', '%' . trim($shortName) . '%');
            }
            if ($code !== '') {
                $query->where('code', 'LIKE', '%' . trim($code) . '%');
            }
        })->orderBy('id', 'ASC')->paginate(10);
        $courses->appends([
            'name' => $name,
            'shortName' => $shortName,
            'code' => $code
        ]);

        return view('superadmin::courses.index', compact('courses', 'name', 'shortName', 'code'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('courses')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('courses')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ],
            'code' => [
                'required',
                Rule::unique('courses')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $course = Course::create([
            'name' => trim($request->name),
            'short_name' => trim($request->short_name),
            'code' => trim($request->code)
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'courses', 1, 'DB id: [' . $course->id . ']');

        return back()->with('success', __('messages.course_created_successfully'));
    }

    public function show($id)
    {
        $course = Course::where('id', $id)->where('deleted', 0)->first();

        if (!$course) {
            return back()->with('warning', __('messages.course_not_found'));
        }

        return view('superadmin::courses.show', compact('course'));
    }

    public function update(Request $request, $id)
    {
        $course = Course::where('id', $id)->where('deleted', 0)->first();
        if (!$course) {
            return back()->with('warning', __('messages.course_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('courses')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('courses')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ],
            'code' => [
                'required',
                Rule::unique('courses')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        Course::where('id', $id)->update([
            'name' => trim($request->name),
            'short_name' => trim($request->short_name),
            'code' => trim($request->code),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'courses', 2, 'DB id: [' . $id . ']');

        return redirect()->route('superadmin.courses.index')->with('success', __('messages.course_updated_successfully'));
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'course_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $course = Course::where('id', $request->course_id)->where('deleted', 0)->first();
        if (!$course) {
            return back()->with('warning', __('messages.course_not_found'));
        }

        Course::where('id', $request->course_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'courses', 3, 'DB id: [' . $request->course_id . ']');

        return back()->with('success', __('messages.course_deleted_successfully'));
    }
}
