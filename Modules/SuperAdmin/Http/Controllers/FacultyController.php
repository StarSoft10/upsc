<?php

namespace Modules\SuperAdmin\Http\Controllers;

use App\Entities\Cycle;
use App\Entities\Faculty;
use App\Helpers\AppHelper;
use App\Helpers\StudyHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;

class FacultyController extends Controller
{
    public $helper;

    public $studyHelper;

    public function __construct()
    {
        $this->helper = new AppHelper();
        $this->studyHelper = new StudyHelper();
    }

    public function index(Request $request)
    {
        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';
        $cycleIds = !empty($request->cycleIds) ? $this->helper->removeValueFromArray($request->cycleIds, 0) : [];

        $faculties = Faculty::where('deleted', 0)->orderBy('id', 'ASC')->paginate(10);
        $faculties->appends([
            'name' => $name,
            'shortName' => $shortName,
            'cycleIds' => $cycleIds
        ]);

        $cycles = Cycle::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        return view('superadmin::faculties.index', compact('faculties', 'cycles', 'name', 'shortName', 'cycleIds'));
    }

    public function search(Request $request)
    {
        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';
        $cycleIds = !empty($request->cycleIds) ? $this->helper->removeValueFromArray($request->cycleIds, 0) : [];

        $faculties = Faculty::where('deleted', 0)->where(function ($query) use ($name, $shortName, $cycleIds) {
            if ($name !== '') {
                $query->where('name', 'LIKE', '%' . trim($name) . '%');
            }
            if ($shortName !== '') {
                $query->where('short_name', 'LIKE', '%' . trim($shortName) . '%');
            }
            if (count($cycleIds) > 0) {
                $query->whereIn('cycle_id', $cycleIds);
            }
        })->orderBy('id', 'ASC')->paginate(10);
        $faculties->appends([
            'name' => $name,
            'shortName' => $shortName,
            'cycleIds' => $cycleIds
        ]);

        $cycles = Cycle::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        return view('superadmin::faculties.index', compact('faculties', 'cycles', 'name', 'shortName', 'cycleIds'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('faculties')->where(function ($query) use ($request) {
                    $query->where('deleted', 0);
                    $query->where('cycle_id', $request->cycle_id);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('faculties')->where(function ($query) use ($request) {
                    $query->where('deleted', 0);
                    $query->where('cycle_id', $request->cycle_id);
                })
            ],
            'cycle_id' => 'required|numeric|gt:0'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $faculty = Faculty::create([
            'cycle_id' => $request->cycle_id,
            'name' => trim($request->name),
            'short_name' => trim($request->short_name)
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'faculties', 1, 'DB id: [' . $faculty->id . ']');

        return back()->with('success', __('messages.faculty_created_successfully'));
    }

    public function show($id)
    {
        $faculty = Faculty::where('id', $id)->where('deleted', 0)->first();

        if (!$faculty) {
            return back()->with('warning', __('messages.faculty_not_found'));
        }

        $cycles = Cycle::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        return view('superadmin::faculties.show', compact('faculty', 'cycles'));
    }

    public function update(Request $request, $id)
    {
        $faculty = Faculty::where('id', $id)->where('deleted', 0)->first();
        if (!$faculty) {
            return back()->with('warning', __('messages.faculty_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('faculties')->where(function ($query) use ($id, $request) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                    $query->where('cycle_id', $request->cycle_id);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('faculties')->where(function ($query) use ($id, $request) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                    $query->where('cycle_id', $request->cycle_id);
                })
            ],
            'cycle_id' => 'required|numeric|gt:0'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        Faculty::where('id', $id)->update([
            'cycle_id' => $request->cycle_id,
            'name' => trim($request->name),
            'short_name' => trim($request->short_name),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'faculties', 2, 'DB id: [' . $id . ']');

        return redirect()->route('superadmin.faculties.index')->with('success', __('messages.faculty_updated_successfully'));
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'faculty_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $faculty = Faculty::where('id', $request->faculty_id)->where('deleted', 0)->first();
        if (!$faculty) {
            return back()->with('warning', __('messages.faculty_not_found'));
        }

        Faculty::where('id', $request->faculty_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'faculties', 3, 'DB id: [' . $request->faculty_id . ']');

        return back()->with('success', __('messages.faculty_deleted_successfully'));
    }

    public function getFacultiesByCycleForSelectSearch(Request $request)
    {
        return $this->studyHelper->getFacultiesByCycleForSelectSearch($request);
    }

    public function getFacultiesByCycleForSelectAddEdit(Request $request)
    {
        return $this->studyHelper->getFacultiesByCycleForSelectAddEdit($request);
    }
}
