<?php

namespace Modules\SuperAdmin\Http\Controllers;

use App\Entities\RegisterNotification;
use App\Entities\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\File;
use Validator;

class SuperAdminController extends Controller
{
    public function index()
    {
        $userWithRoles = [];
        $roles = Role::where('name', '!=', 'superadmin')->pluck('name');

        foreach ($roles as $roleName){
            $roleNameInitial = $roleName;
            $roleName = str_replace('-r', 'R', $roleName);
            $model = "App\Entities" . '\\'. ucfirst($roleName);

            $userWithRoles[$roleNameInitial] = $model::with('users')->where('deleted', 0)->count();
        }

        return view('superadmin::index', compact('userWithRoles'));
    }

    public function show()
    {
        $user = Auth::user();
        return view('superadmin::superadmin.show', compact('user'));
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => [
                'required',
                'email',
                'regex:/^[a-z0-9\@\.\_\-]+$/u',
                Rule::unique('users')->ignore($request->user, 'id')
            ],
            'name' => 'required',
            'password' => 'confirmed'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        if (trim($request->password) !== '') {
            User::where('id', $request->user)->update([
                'name' => $request->name,
                'email' => mb_strtolower(trim($request->email), 'UTF-8'),
                'password' => Hash::make($request->password),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        } else {
            User::where('id', $request->user)->update([
                'name' => $request->name,
                'email' => mb_strtolower(trim($request->email), 'UTF-8'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }

        return back()->with('success', __('messages.information_update_successfully'));
    }

    public function changePhoto(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'photo' => 'required|mimes:jpeg,png,jpg,gif,svg|max:10000'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        if (!$request->hasFile('photo')) {
            return back()->with('warning', __('messages.photo_is_required'));
        }

        $updatedUser = User::find($request->user);
        $photoName = $updatedUser->photo;
        if (Storage::disk('public')->exists('users-photo/' . $photoName)) {
            Storage::disk('public')->delete('users-photo/' . $photoName);
        }
        $photo = $request->file('photo');
        $fileName = time() . $request->user . '.' . $photo->getClientOriginalExtension();
        Storage::disk('public')->put('users-photo/' . $fileName, File::get($photo));
        $photoName = $fileName;

        User::where('id', $request->user)->update([
            'photo' => $photoName,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return back()->with('success', __('messages.information_update_successfully'));
    }

    public function getRegisterNotifications()
    {
        $registerNotifications = RegisterNotification::where('read', 0)->get();

        $total = count($registerNotifications);

        $html = '';
        if (count($registerNotifications) > 0) {
            foreach ($registerNotifications as $registerNotification) {

                $cssClass = 'bg-flat-color-3';
                $message = __('messages.new_unconfirmed_user_register');
                $link = route('superadmin.unconfirmed-users.show', $registerNotification->model_id);

                if ($registerNotification->type === 'unconfirmed-user') {
                    $cssClass = 'bg-flat-color-3';
                    $message = __('messages.new_unconfirmed_user_register');
                    $link = route('superadmin.unconfirmed-users.show', $registerNotification->model_id);
                } else if ($registerNotification->type === 'student') {
                    $cssClass = 'bg-flat-color-1';
                    $message = __('messages.new_student_register');
                    $link = route('superadmin.students.show', $registerNotification->model_id);
                } else if ($registerNotification->type === 'teacher') {
                    $cssClass = 'bg-flat-color-5';
                    $message = __('messages.new_teacher_register');
                    $link = route('superadmin.teachers.show', $registerNotification->model_id);
                }

                $html .= '<a class="dropdown-item media ' . $cssClass . '" href="' . $link . '" data-notification-id="' . $registerNotification->id . '">
                            <span class="photo media-left">
                                <img alt="avatar" src="' . asset('images/default_photo.jpg') . '">
                            </span>
                            <span class="message media-body">
                                <span class="name float-left">' . $registerNotification->model_info . '</span>
                                <span class="time float-right">' . $registerNotification->created_at->diffForHumans() . '</span>
                                <span class="clearfix"></span>
                                <span>' . $message . '</span>
                            </span>
                        </a>';
            }
        } else {
            $html = '<div class="not_found_notifications">' . __('messages.not_found_any_register_notifications') . '</div>';
        }


        return response()->json(['total' => $total, 'html' => $html]);
    }
}
