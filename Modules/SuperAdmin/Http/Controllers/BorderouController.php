<?php

namespace Modules\SuperAdmin\Http\Controllers;

use App\Entities\Borderou;
use App\Entities\BorderouNote;
use App\Entities\BorderouNoteCode;
use App\Entities\BorderouType;
use App\Entities\Course;
use App\Entities\Semester;
use App\Entities\StudentAdditionalStudy;
use App\Entities\Teacher;
use App\Entities\Cycle;
use App\Entities\Faculty;
use App\Entities\Speciality;
use App\Entities\Group;
use App\Entities\Student;
use App\Entities\YearOfStudy;
use App\Helpers\AppHelper;
use App\Helpers\BorderouHelper;
use App\Helpers\StudyHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;
use PDF;

class BorderouController extends Controller
{
    public $helper;
    public $borderouHelper;
    public $studyHelper;

    public function __construct()
    {
        $this->helper = new AppHelper();
        $this->borderouHelper = new BorderouHelper();
        $this->studyHelper = new StudyHelper();
    }

    public function index(Request $request)
    {

//        $path = public_path('images/logo_full.png');
//        $base64 = base64_encode(file_get_contents($path));
//        $logo = 'data:' . mime_content_type($path) . ';base64,' . $base64;
//
//        $pathBackground = public_path('images/4.png');
//        $base64Background = base64_encode(file_get_contents($pathBackground));
//        $logoBackground = 'data:' . mime_content_type($pathBackground) . ';base64,' . $base64Background;
//
//        $borderou = Borderou::where('id', 1)->first();
//        $borderouNotes = BorderouNote::where('borderou_id', 1)->get();
//        $html = '<div style="font-weight: bold; font-size: 12px;margin-bottom: 0;float: left;width: 50%">
//                     '. __('pages.cycle') .': '. $borderou->cycle->name .' <br>
//                     '. __('pages.faculty') .': '. $borderou->faculty->name .' <br>
//                     '. __('pages.speciality') .': '. $borderou->speciality->name .' <br>
//                     '. __('pages.group') .': '. $borderou->group->name .'
//                 </div>
//                 <div style="font-weight: bold; font-size: 12px;margin-bottom: 0;float: right;width: 50%">
//                     '. __('pages.year_of_study') .': '. $borderou->yearOfStudy->name .' <br>
//                     '. __('pages.semester') .': '. $borderou->semester->name .' <br>
//                     '. __('pages.course') .': '. $borderou->course->name .' <br>
//                     '. __('pages.course_title') .': '. $borderou->course_title .' <br>
//                     '. __('pages.teacher') .': '. $borderou->teacher->first_name .' '. $borderou->teacher->last_name .'
//                 </div>
//                 <div style="clear: both"></div>
//                 <table class="table table-bordered" style="margin-top: 10px">
//                     <thead>
//                         <tr>
//                             <th>#</th>
//                             <th>'. __('pages.full_name') .'</th>
//                             <th>'. __('pages.semester_note') .'</th>
//                             <th>'. __('pages.exam_note') .'</th>
//                         </tr>
//                     </thead>
//                     <tbody>';
//
//        for ($i = 0; $i < 20; $i++){
//            foreach ($borderouNotes as $key => $borderouNote){
//                $studentFullName = $borderouNote->student->first_name .' '. $borderouNote->student->last_name;
//                $semesterNote = $borderouNote->semester_note;
//                $examNote = $borderouNote->exam_note;
//
//                $html .= '<tr>
//                          <th>'. ($key + 1) .'</th>
//                          <th>'. $studentFullName .'</th>
//                          <td>'. $semesterNote .'</td>
//                          <td>'. $examNote .'</td>
//                      </tr>';
//            }
//        }
//
//        $html .= '</tbody></table>';
//
//        $pdf = PDF::loadView('exports.borderou-notes-pdf', compact('html', 'logo', 'logoBackground'));
//        return $pdf->stream('document.pdf');


        $cycleIds = !empty($request->cycleIds) ? $this->helper->removeValueFromArray($request->cycleIds, 0) : [];
        $borderouTypeIds = !empty($request->borderouTypeIds) ? $this->helper->removeValueFromArray($request->borderouTypeIds, 0) : [];
        $facultyIds = !empty($request->facultyIds) ? $this->helper->removeValueFromArray($request->facultyIds, 0) : [];
        $specialityIds = !empty($request->specialityIds) ? $this->helper->removeValueFromArray($request->specialityIds, 0) : [];
        $groupIds = !empty($request->groupIds) ? $this->helper->removeValueFromArray($request->groupIds, 0) : [];
        $yearOfStudyIds = !empty($request->yearOfStudyIds) ? $this->helper->removeValueFromArray($request->yearOfStudyIds, 0) : [];
        $name = !empty($request->name) ? trim($request->name) : '';
        $yearOfAdmission = !empty($request->yearOfAdmission) ? trim($request->yearOfAdmission) : '';
        $semesterIds = !empty($request->semesterIds) ? $request->semesterIds : [];
        $courseIds = !empty($request->courseIds) ? $request->courseIds : [];
        $teacherIds = !empty($request->teacherIds) ? $request->teacherIds : [];

        $borderous = Borderou::where('deleted', 0)->orderBy('id', 'ASC')->paginate(10);
        $borderous->appends([
            'cycleIds' => $cycleIds,
            'borderouTypeIds' => $borderouTypeIds,
            'facultyIds' => $facultyIds,
            'specialityIds' => $specialityIds,
            'groupIds' => $groupIds,
            'yearOfStudyIds' => $yearOfStudyIds,
            'name' => $name,
            'yearOfAdmission' => $yearOfAdmission,
            'semesterIds' => $semesterIds,
            'courseIds' => $courseIds,
            'teacherIds' => $teacherIds,
        ]);

        $cycles = Cycle::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $faculties = Faculty::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $specialities = Speciality::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $groups = Group::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $yearOfStudies = YearOfStudy::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $semesters = Semester::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $courses = Course::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $teachersObj = Teacher::select('id', 'first_name', 'last_name')->where('deleted', 0)->orderBy('last_name')->orderBy('first_name')->get();
        $teachers = [];
        foreach ($teachersObj as $teacher) {
            $teachers[$teacher->id] = $teacher->first_name . ' ' . $teacher->last_name;
        }
        $borderouTypes = BorderouType::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        $from = Carbon::now()->format('Y-m-d 00:00:00');
        $to = Carbon::now()->format('Y-m-d 23:59:59');

        $borderouNoteCode = BorderouNoteCode::whereBetween('created_at', [$from, $to])->orderBy('id', 'DESC')->first();
        $todayCode = __('messages.code_not_generated');
        $defaultCode = '10101991CM';

        if ($borderouNoteCode) {
            $todayCode = $borderouNoteCode->code;
            $defaultCode = $borderouNoteCode->default_code;
        }

        return view('superadmin::borderous.index', compact('borderous', 'cycles', 'faculties', 'specialities',
            'groups', 'yearOfStudies', 'semesters', 'courses', 'teachers', 'cycleIds', 'borderouTypeIds', 'facultyIds',
            'specialityIds', 'groupIds', 'yearOfStudyIds', 'name', 'yearOfAdmission', 'semesterIds', 'courseIds', 'teacherIds',
            'todayCode', 'defaultCode', 'borderouTypes'));
    }

    public function search(Request $request)
    {
        $cycleIds = !empty($request->cycleIds) ? $this->helper->removeValueFromArray($request->cycleIds, 0) : [];
        $borderouTypeIds = !empty($request->borderouTypeIds) ? $this->helper->removeValueFromArray($request->borderouTypeIds, 0) : [];
        $facultyIds = !empty($request->facultyIds) ? $this->helper->removeValueFromArray($request->facultyIds, 0) : [];
        $specialityIds = !empty($request->specialityIds) ? $this->helper->removeValueFromArray($request->specialityIds, 0) : [];
        $groupIds = !empty($request->groupIds) ? $this->helper->removeValueFromArray($request->groupIds, 0) : [];
        $yearOfStudyIds = !empty($request->yearOfStudyIds) ? $this->helper->removeValueFromArray($request->yearOfStudyIds, 0) : [];
        $name = !empty($request->name) ? trim($request->name) : '';
        $yearOfAdmission = !empty($request->yearOfAdmission) ? trim($request->yearOfAdmission) : '';
        $semesterIds = !empty($request->semesterIds) ? $request->semesterIds : [];
        $courseIds = !empty($request->courseIds) ? $request->courseIds : [];
        $teacherIds = !empty($request->teacherIds) ? $request->teacherIds : [];

        $borderous = Borderou::where('deleted', 0)->where(function ($query)
        use (
            $cycleIds, $borderouTypeIds, $facultyIds, $specialityIds, $groupIds, $yearOfStudyIds, $name, $yearOfAdmission, $semesterIds,
            $courseIds, $teacherIds
        ) {
            if (count($cycleIds) > 0) {
                $query->whereIn('cycle_id', $cycleIds);
            }
            if (count($borderouTypeIds) > 0) {
                $query->whereIn('borderou_type_id', $borderouTypeIds);
            }
            if (count($facultyIds) > 0) {
                $query->whereIn('faculty_id', $facultyIds);
            }
            if (count($specialityIds) > 0) {
                $query->whereIn('speciality_id', $specialityIds);
            }
            if (count($groupIds) > 0) {
                $query->whereIn('group_id', $groupIds);
            }
            if (count($yearOfStudyIds) > 0) {
                $query->whereIn('year_of_study_id', $yearOfStudyIds);
            }
            if ($name !== '') {
                $query->where('name', 'LIKE', '%' . $name . '%');
            }
            if ($yearOfAdmission !== '') {
                $query->where('year_of_admission', '=', $yearOfAdmission);
            }
            if (count($semesterIds) > 0) {
                $query->whereIn('semester_id', $semesterIds);
            }
            if (count($courseIds) > 0) {
                $query->whereIn('course_id', $courseIds);
            }
            if (count($teacherIds) > 0) {
                $query->whereIn('teacher_id', $teacherIds);
            }
        })->orderBy('id', 'ASC')->paginate(10);
        $borderous->appends([
            'cycleIds' => $cycleIds,
            'borderouTypeIds' => $borderouTypeIds,
            'facultyIds' => $facultyIds,
            'specialityIds' => $specialityIds,
            'groupIds' => $groupIds,
            'yearOfStudyIds' => $yearOfStudyIds,
            'name' => $name,
            'yearOfAdmission' => $yearOfAdmission,
            'semesterIds' => $semesterIds,
            'courseIds' => $courseIds,
            'teacherIds' => $teacherIds,
        ]);

        $cycles = Cycle::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $faculties = Faculty::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $specialities = Speciality::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $groups = Group::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $yearOfStudies = YearOfStudy::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $semesters = Semester::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $courses = Course::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $teachersObj = Teacher::select('id', 'first_name', 'last_name')->where('deleted', 0)->orderBy('last_name')->orderBy('first_name')->get();
        $teachers = [];
        foreach ($teachersObj as $teacher) {
            $teachers[$teacher->id] = $teacher->first_name . ' ' . $teacher->last_name;
        }
        $borderouTypes = BorderouType::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        $from = Carbon::now()->format('Y-m-d 00:00:00');
        $to = Carbon::now()->format('Y-m-d 23:59:59');

        $borderouNoteCode = BorderouNoteCode::whereBetween('created_at', [$from, $to])->orderBy('id', 'DESC')->first();
        $todayCode = __('messages.code_not_generated');
        $defaultCode = '10101991CM';

        if ($borderouNoteCode) {
            $todayCode = $borderouNoteCode->code;
            $defaultCode = $borderouNoteCode->default_code;
        }

        return view('superadmin::borderous.index', compact('borderous', 'cycles', 'faculties', 'specialities',
            'groups', 'yearOfStudies', 'semesters', 'courses', 'teachers', 'cycleIds', 'borderouTypeIds', 'facultyIds',
            'specialityIds', 'groupIds', 'yearOfStudyIds', 'name', 'yearOfAdmission', 'semesterIds', 'courseIds', 'teacherIds',
            'todayCode', 'defaultCode', 'borderouTypes'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:borderous',
            'borderou_type_id' => 'required|numeric|gt:0',
            'cycle_id' => 'required|numeric|gt:0',
            'faculty_id' => 'required|numeric|gt:0',
            'speciality_id' => 'required|numeric|gt:0',
            'group_id' => 'required|numeric|gt:0',
            'year_of_study_id' => 'required|numeric|gt:0',
            'semester_id' => 'required|numeric|gt:0',
            'course_id' => 'required|numeric|gt:0',
            'teacher_id' => 'required|numeric|gt:0',
            'year_of_admission' => 'required|numeric',
            'total_hours' => 'required|numeric',
            'credits' => 'required|numeric',
            'course_title' => 'required',
            'exam_date' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        if (Borderou::where('cycle_id', $request->cycle_id)->where('faculty_id', $request->faculty_id)
            ->where('speciality_id', $request->speciality_id)->where('group_id', $request->group_id)
            ->where('year_of_study_id', $request->year_of_study_id)->where('semester_id', $request->semester_id)
            ->where('course_id', $request->course_id)->where('teacher_id', $request->teacher_id)
            ->where('borderou_type_id', $request->borderou_type_id)->where('deleted', 0)->exists()) {

            return back()->with('warning', __('messages.borderou_already_exist_with_this_data'));
        }

        if (Student::where('group_id', $request->group_id)->where('deleted', 0)->where('expelled', 0)->count() == 0 &&
            StudentAdditionalStudy::where('group_id', $request->group_id)->count() == 0) {
            return back()->with('warning', __('messages.not_found_any_students_in_this_group'));
        }

        $borderou = Borderou::create([
            'name' => trim($request->name),
            'borderou_type_id' => $request->borderou_type_id,
            'cycle_id' => $request->cycle_id,
            'faculty_id' => $request->faculty_id,
            'speciality_id' => $request->speciality_id,
            'group_id' => $request->group_id,
            'year_of_study_id' => $request->year_of_study_id,
            'semester_id' => $request->semester_id,
            'course_id' => $request->course_id,
            'teacher_id' => $request->teacher_id,
            'year_of_admission' => trim($request->year_of_admission),
            'total_hours' => trim($request->total_hours),
            'credits' => trim($request->credits),
            'course_title' => trim($request->course_title),
            'exam_date' => trim($request->exam_date),
            'date_of_liquidation' => trim($request->date_of_liquidation),
            'created_by_user' => Auth::user()->id
        ]);

        $students1 = [];
        $students2 = [];

        if(Student::where('group_id', $request->group_id)->where('deleted', 0)->where('expelled', 0)->count() > 0){
            $students1 = collect(Student::select('id')
                ->where('group_id', $request->group_id)
                ->where('deleted', 0)
                ->where('expelled', 0)
                ->orderBy('last_name')
                ->orderBy('first_name')
                ->get());
        }
        if(StudentAdditionalStudy::where('group_id', $request->group_id)->count() > 0) {
            $studentIds = StudentAdditionalStudy::select('student_id')->where('group_id', $request->group_id)->get()->toArray();
            $studentIdsArray = [];

            foreach($studentIds as $studentId) {
                $studentIdsArray[] = $studentId['student_id'];
            }

            $students2 = collect(Student::select('id')
                ->whereIn('id', $studentIdsArray)
                ->where('deleted', 0)
                ->where('expelled', 0)
                ->orderBy('last_name')
                ->orderBy('first_name')
                ->get());
        }

        $collectionOfStudents = new Collection;
        $collectionOfStudents->push($students1);
        $collectionOfStudents->push($students2);

        foreach ($collectionOfStudents as $students){
            foreach ($students as $student) {
                BorderouNote::create([
                    'borderou_id' => $borderou->id,
                    'student_id' => $student->id,
                    'semester_note' => null,
                    'semester_score' => null,
                    'exam_note' => null,
                    'exam_score' => null,
                    'final_note' => null,
                    'ects' => null
                ]);
            }
        }

        $this->helper->addLog('superadmin', Auth::user()->name, 'borderous', 1, 'DB id: [' . $borderou->id . ']');

        return back()->with('success', __('messages.borderou_created_successfully'));
    }

    public function show($id)
    {
        $borderou = Borderou::where('id', $id)->where('deleted', 0)->first();

        if (!$borderou) {
            return back()->with('warning', __('messages.borderou_not_found'));
        }

        $cycles = Cycle::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $faculties = Faculty::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $specialities = Speciality::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $groups = Group::where('deleted', 0)->where('speciality_id', $borderou->speciality_id)->orderBy('id', 'ASC')->pluck('name', 'id');
        $yearOfStudies = YearOfStudy::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $semesters = Semester::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $courses = Course::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $teachersObj = Teacher::select('id', 'first_name', 'last_name')->where('deleted', 0)->orderBy('last_name')->orderBy('first_name')->get();
        $teachers = [];
        foreach ($teachersObj as $teacher) {
            $teachers[$teacher->id] = $teacher->first_name . ' ' . $teacher->last_name;
        }
        $borderouTypes = BorderouType::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        $faculty = $borderou->faculty->name;
        $speciality = $borderou->speciality->name;
        $group = $borderou->group->name;

        return view('superadmin::borderous.show', compact('borderou', 'cycles', 'faculties', 'specialities',
            'groups', 'yearOfStudies', 'semesters', 'courses', 'teachers', 'faculty', 'speciality', 'group', 'borderouTypes'));
    }

    public function update(Request $request, $id)
    {
        $borderou = Borderou::where('id', $id)->where('deleted', 0)->first();
        if (!$borderou) {
            return back()->with('warning', __('messages.borderou_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('borderous')->ignore(Borderou::findOrFail($borderou->id)),
            ],
            'year_of_study_id' => 'required|numeric',
            'semester_id' => 'required|numeric',
            'course_id' => 'required|numeric',
            'teacher_id' => 'required|numeric',
            'year_of_admission' => 'required|numeric',
            'total_hours' => 'required|numeric',
            'credits' => 'required|numeric',
            'course_title' => 'required',
            'exam_date' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        Borderou::where('id', $id)->update([
            'name' => trim($request->name),
            'year_of_study_id' => trim($request->year_of_study_id),
            'semester_id' => trim($request->semester_id),
            'course_id' => trim($request->course_id),
            'teacher_id' => trim($request->teacher_id),
            'year_of_admission' => trim($request->year_of_admission),
            'total_hours' => trim($request->total_hours),
            'credits' => trim($request->credits),
            'course_title' => trim($request->course_title),
            'exam_date' => trim($request->exam_date),
            'date_of_liquidation' => trim($request->date_of_liquidation),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'borderous', 2, 'DB id: [' . $id . ']');

        return redirect()->route('superadmin.borderous.index')->with('success', __('messages.borderou_updated_successfully'));
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'borderou_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $borderou = Borderou::where('id', $request->borderou_id)->where('deleted', 0)->first();
        if (!$borderou) {
            return back()->with('warning', __('messages.borderou_not_found'));
        }

        Borderou::where('id', $request->borderou_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'borderous', 3, 'DB id: [' . $request->borderou_id . ']');

        return back()->with('success', __('messages.borderou_deleted_successfully'));
    }

    public function exportBorderouNotePdf(Request $request)
    {
        return $this->borderouHelper->exportBorderouNotePdf($request);
    }

    public function exportBorderouNoteExcel(Request $request)
    {
        return $this->borderouHelper->exportBorderouNoteExcel($request);
    }

    public function getBorderouForView(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'borderou_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['type' => 'warning', $validator->errors()->first()]);
        }

        $borderou = Borderou::where('id', $request->borderou_id)->where('deleted', 0)->first();
        if (!$borderou) {
            return response()->json(['type' => 'warning', 'message' => __('messages.borderou_not_found')]);
        }

        $borderouInfo = [
            'name' => $borderou->name,
            'borderouType' => $this->studyHelper->getBorderouTypeById($borderou->borderou_type_id),
            'cycleName' => $this->studyHelper->getCycleNameById($borderou->cycle_id),
            'faculty' => $this->studyHelper->getFacultyNameById($borderou->faculty_id),
            'speciality' => $this->studyHelper->getSpecialityNameById($borderou->speciality_id),
            'group' => $this->studyHelper->getGroupNameById($borderou->group_id),
            'yearOfStudy' => $this->studyHelper->getYearOfStudyNameById($borderou->year_of_study_id),
            'semester' => $this->studyHelper->getSemesterNameById($borderou->semester_id),
            'course' => $this->studyHelper->getCourseNameById($borderou->course_id),
            'teacher' => $this->studyHelper->getTeacherNameById($borderou->teacher_id),
            'yearOfAdmission' => $borderou->year_of_admission,
            'totalHours' => $borderou->total_hours,
            'credits' => $borderou->credits,
            'courseTitle' => $borderou->course_title,
            'examDate' => !empty($borderou->exam_date) ? Carbon::createFromFormat('Y-m-d', $borderou->exam_date)->format('d/m/Y') : '',
            'dateOfLiquidation' => !empty($borderou->date_of_liquidation) ? Carbon::createFromFormat('Y-m-d', $borderou->date_of_liquidation)->format('d/m/Y') : '',
            'finished' => $borderou->finished == 0 ? __('pages.no') : __('pages.yes'),
            'owner' => $this->borderouHelper->getBorderouOwner($borderou->created_by_user),
        ];

        $borderouNotes = BorderouNote::where('borderou_id', $request->borderou_id)->get();

        $html = '<table class="table table-sm">
                 <thead>
                     <tr>
                         <th style="border-top: none;">' . __('pages.full_name') . '</th>
                         <th style="border-top: none;">' . __('pages.semester_note') . '</th>
                         <th style="border-top: none;">' . __('pages.semester_score') . '</th>
                         <th style="border-top: none;">' . __('pages.exam_note') . '</th>
                         <th style="border-top: none;">' . __('pages.exam_score') . '</th>
                         <th style="border-top: none;">' . __('pages.final_note') . '</th>
                         <th style="border-top: none;">' . __('pages.ects') . '</th>
                     </tr>
                 </thead>
                 <tbody>';
        foreach ($borderouNotes as $borderouNote) {
            $studentFullName = $borderouNote->student->last_name . ' ' . $borderouNote->student->first_name;
            $finalNote = '';
            $ects = $borderouNote->ects;

            $semNote = $this->borderouHelper->getSemesterNote($borderouNote);
            $semScore = '';
            if(!ctype_alpha($semNote)){
                $semScore = $this->borderouHelper->getSemesterScore($borderouNote);
            }

            $examNote = $this->borderouHelper->getExamNote($borderouNote);
            $examScore = '';
            if(!ctype_alpha($examNote)){
                $examScore = $this->borderouHelper->getExamScore($borderouNote);
            }

            if((is_numeric($semNote) && $semNote >= 5) && $examNote == 0){
                $examScore = '';
                $finalNote = $this->borderouHelper->getExamScore($borderouNote);
                $ects = '';
            } elseif ((is_numeric($semNote) && $semNote >= 5) && $examNote == -2) {
                $examScore = '';
                $finalNote = '';
                $ects = '';
            } elseif ((is_numeric($semNote) && $semNote < 5) && $examNote == -2) {
                $semScore = '';
                $examScore = '';
                $finalNote = '';
                $ects = '';
            }

//              1. Cind Nota semestrială este > 5  si Nota la examen este Admis     atunci la Punctaj la examen nu aratam nimik
//              data la Nota finală punem Admis si ECTS tot nu punem nimik ....

//              2. Cind Nota semestrială este > 5  si Nota la examen este NeAdmis      atunci la Punctaj la examen
//              si Nota finală`si `ECTS nu aratam nimik

//              3. Cind Nota semestrială este < 5                      atunci la Punctaj pe semestru,  Nota la examen ,
//              Punctaj la examen  si Nota finală`si `ECTS nu aratam nimik



            $html .= '<tr>
                          <th>' . $studentFullName . '</th>
                          <td>' . $semNote . '</td>
                          <td>' . $semScore . '</td>
                          <td>' . $examNote . '</td>
                          <td>' . $examScore . '</td>
                          <td>' . $finalNote . '</td>
                          <td>' . $ects . '</td>
                      </tr>';
        }

        $html .= '</tbody></table>';

        return response()->json(['type' => 'success', 'borderouInfo' => $borderouInfo, 'borderouInfoNote' => $html]);
    }

    public function getBorderouNote(Request $request)
    {
        return $this->borderouHelper->getBorderouNote($request);
    }

    public function updateBorderouNote(Request $request)
    {
        if (count($request->all()) <= 2) {
            return back()->with('error', __('messages.something_wrong'));
        }

        $validator = Validator::make($request->all(), [
            'borderou_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $borderou = Borderou::where('id', $request->borderou_id)->where('deleted', 0)->first();
        if (!$borderou) {
            return back()->with('warning', __('messages.borderou_not_found'));
        }

        $borderouType = BorderouType::where('id', $borderou->borderou_type_id)->where('deleted', 0)->first();
        if (!$borderouType) {
            return back()->with('warning', __('messages.borderou_type_not_found'));
        }

        $code = !empty($request->code) ? trim($request->code) : '';
        if ($borderou->finished == 1 && $code == '') {
            return back()->with('warning', __('messages.borderou_already_finished'));
        }

        if ($borderou->finished == 1 && $code !== '') {
            $from = Carbon::now()->format('Y-m-d 00:00:00');
            $to = Carbon::now()->format('Y-m-d 23:59:59');

            $borderouNoteCode = BorderouNoteCode::where('code', $code)->whereBetween('created_at', [$from, $to])->orderBy('id', 'DESC')->first();
            if (!$borderouNoteCode) {
                $borderouNoteDefaultCode = BorderouNoteCode::where('default_code', '!=', '')->orderBy('id', 'DESC')->first();
                if ($code == $borderouNoteDefaultCode->default_code) {
                    $this->updateNotes($borderou, $request);

                    $this->helper->addLog('superadmin', Auth::user()->name, 'borderou_notes',
                        2, 'DB id: [' . $borderou->id . '] ' . __('messages.using_code') . ' ' . $code);

                    return back()->with('success', __('messages.borderou_notes_added_successfully'));
                }

                return back()->with('error', __('messages.invalid_code'));
            } else {
                $this->updateNotes($borderou, $request);

                $this->helper->addLog('superadmin', Auth::user()->name, 'borderou_notes',
                    2, 'DB id: [' . $borderou->id . '] ' . __('messages.using_code') . ' ' . $code);

                return back()->with('success', __('messages.borderou_notes_added_successfully'));
            }
        }

        $this->updateNotes($borderou, $request);

        Borderou::where('id', $borderou->id)->update([
            'finished' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return back()->with('success', __('messages.borderou_notes_added_successfully'));
    }

    public function updateNotes($borderou, $request)
    {
        $semesterNoteId = array_filter($request->all(), function ($key) {
            return strpos($key, 'semester_note_') === 0;
        }, ARRAY_FILTER_USE_KEY);

        $borderouType = BorderouType::where('id', $borderou->borderou_type_id)->where('deleted', 0)->first();

        foreach ($semesterNoteId as $fullId => $semNote) {
            $explode = explode('_', $fullId);
            $id = $explode[2];

            if ($semNote > 0) {
                $semesterNote = number_format($semNote, 2);
                $semesterScore = number_format(($semNote * $borderouType->semester_index), 2);
            } else {
                $semesterNote = $semNote;
                $semesterScore = $semNote;
            }

            $examenNote = $request->input('exam_note_' . $id);

            if ($examenNote > 0) {
                $examNote = number_format($examenNote, 2);
                $examScore = number_format(($examNote * $borderouType->exam_index), 2);
            } else {
                $examNote = $examenNote;
                $examScore = $examenNote;
            }

            if ($semNote > 0 && $examenNote > 0) {
                $finalNote = number_format(($semesterScore + $examScore), 2);
                $ects = $this->borderouHelper->getEctsByFinalNote($finalNote);
            } else {
                $finalNote = null;
                $ects = null;
            }

            BorderouNote::where('borderou_id', $borderou->id)->where('id', $id)->update([
                'semester_note' => $semesterNote,
                'semester_score' => $semesterScore,
                'exam_note' => $examNote,
                'exam_score' => $examScore,
                'final_note' => $finalNote,
                'ects' => $ects,
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }

        return true;
    }

    public function addStudentBorderouNote(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'borderou_id' => 'required|numeric',
            'addedStudentsIds' => 'required|array|min:1',
        ]);

        if ($validator->fails()) {
            return response()->json(['type' => 'warning', $validator->errors()->first()]);
        }

        $borderou = Borderou::where('id', $request->borderou_id)->where('deleted', 0)->first();
        if (!$borderou) {
            return response()->json(['type' => 'warning', 'message' => __('messages.borderou_not_found')]);
        }

        foreach ($request->addedStudentsIds as $studentId) {
            BorderouNote::create([
                'borderou_id' => $request->borderou_id,
                'student_id' => $studentId,
                'semester_note' => null,
                'semester_score' => null,
                'exam_note' => null,
                'exam_score' => null,
                'final_note' => null,
                'ects' => null
            ]);
        }

        return response()->json(['type' => 'success', 'message' => __('messages.student_added_successfully')]);
    }

    public function deleteStudentBorderouNote(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'borderou_id' => 'required|numeric',
            'borderouNoteId' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json(['type' => 'warning', $validator->errors()->first()]);
        }

        $borderou = Borderou::where('id', $request->borderou_id)->where('deleted', 0)->first();
        if (!$borderou) {
            return response()->json(['type' => 'warning', 'message' => __('messages.borderou_not_found')]);
        }

        $borderouNote = BorderouNote::where('id', $request->borderouNoteId)->where('borderou_id', $request->borderou_id)->first();
        if (!$borderouNote) {
            return response()->json(['type' => 'warning', 'message' => __('messages.borderou_not_found')]);
        }

        BorderouNote::where('id', $request->borderouNoteId)->where('borderou_id', $request->borderou_id)->delete();

        $this->helper->addLog('superadmin', Auth::user()->name, 'borderou_notes', 3,
            'DB id: [' . $request->borderouNoteId . ']' . ' Borderou DB id: [' . $request->borderou_id . ']');

        return response()->json(['type' => 'success', 'message' => __('messages.student_deleted_successfully')]);
    }
}
