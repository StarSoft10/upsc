<?php

namespace Modules\SuperAdmin\Http\Controllers;

use App\Entities\Database;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Routing\Controller;
use Illuminate\Validation\Rule;

class DatabaseController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    public function index()
    {
        $databases = Database::where('deleted', 0)->paginate(10);

        return view('superadmin::databases.index', compact('databases'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('databases')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ],
            'year' => 'required',
            'server_ip' => 'required',
            'database_name' => [
                'required',
                Rule::unique('databases')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ],
            'connection_name' => 'required|unique:databases',
            'username' => 'required',
            'password' => 'required',
            'port' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $database = Database::create([
            'name' => trim($request->name),
            'year' => trim($request->year),
            'server_ip' => trim($request->server_ip),
            'database_name' => trim($request->database_name),
            'connection_name' => trim($request->connection_name),
            'username' => trim($request->username),
            'password' => trim($request->password),
            'port' => trim($request->port)
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'databases', 1, 'DB id: [' . $database->id . ']');

        return back()->with('success', __('messages.database_created_successfully'));
    }

    public function show($id)
    {
        $database = Database::where('id', $id)->where('deleted', 0)->first();

        if (!$database) {
            return back()->with('warning', __('messages.database_not_found'));
        }

        return view('superadmin::databases.show', compact('database'));
    }

    public function update(Request $request, $id)
    {
        $database = Database::where('id', $id)->where('deleted', 0)->first();
        if (!$database) {
            return back()->with('warning', __('messages.database_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('databases')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ],
            'year' => 'required',
            'server_ip' => 'required',
            'database_name' => [
                'required',
                Rule::unique('databases')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ],
            'connection_name' => [
                'required',
                Rule::unique('databases')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ],
            'username' => 'required',
            'password' => 'required',
            'port' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        Database::where('id', $id)->update([
            'name' => trim($request->name),
            'year' => trim($request->year),
            'server_ip' => trim($request->server_ip),
            'database_name' => trim($request->database_name),
            'connection_name' => trim($request->connection_name),
            'username' => trim($request->username),
            'password' => trim($request->password),
            'port' => trim($request->port),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'databases', 2, 'DB id: [' . $id . ']');

        return redirect()->route('superadmin.databases.index')->with('success', __('messages.database_updated_successfully'));
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'database_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $database = Database::where('id', $request->database_id)->where('deleted', 0)->first();
        if (!$database) {
            return back()->with('warning', __('messages.database_not_found'));
        }

        Database::where('id', $request->database_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'databases', 3, 'DB id: [' . $request->database_id . ']');

        return back()->with('success', __('messages.database_deleted_successfully'));
    }
}
