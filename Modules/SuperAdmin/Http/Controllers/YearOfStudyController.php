<?php

namespace Modules\SuperAdmin\Http\Controllers;

use App\Entities\YearOfStudy;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;

class YearOfStudyController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    public function index(Request $request)
    {
        $name = !empty($request->name) ? trim($request->name) : '';

        $yearOfStudies = YearOfStudy::where('deleted', 0)->orderBy('id', 'ASC')->paginate(10);
        $yearOfStudies->appends(['name' => $name]);

        return view('superadmin::year-of-studies.index', compact('yearOfStudies', 'name'));
    }

    public function search(Request $request)
    {
        $name = !empty($request->name) ? trim($request->name) : '';

        $yearOfStudies = YearOfStudy::where('deleted', 0)->where(function ($query) use ($name) {
            if ($name !== '') {
                $query->where('name', 'LIKE', '%' . trim($name) . '%');
            }
        })->orderBy('id', 'ASC')->paginate(10);
        $yearOfStudies->appends(['name' => $name]);

        return view('superadmin::year-of-studies.index', compact('yearOfStudies', 'name'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('year_of_studies')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $yearOfStudy = YearOfStudy::create(['name' => trim($request->name)]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'year_of_studies', 1, 'DB id: [' . $yearOfStudy->id . ']');

        return back()->with('success', __('messages.year_of_study_created_successfully'));
    }

    public function show($id)
    {
        $yearOfStudy = YearOfStudy::where('id', $id)->where('deleted', 0)->first();

        if (!$yearOfStudy) {
            return back()->with('warning', __('messages.year_of_study_not_found'));
        }

        return view('superadmin::year-of-studies.show', compact('yearOfStudy'));
    }

    public function update(Request $request, $id)
    {
        $yearOfStudy = YearOfStudy::where('id', $id)->where('deleted', 0)->first();
        if (!$yearOfStudy) {
            return back()->with('warning', __('messages.year_of_study_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('year_of_studies')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        YearOfStudy::where('id', $id)->update([
            'name' => trim($request->name),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'year_of_studies', 2, 'DB id: [' . $id . ']');

        return redirect()->route('superadmin.year-of-studies.index')->with('success', __('messages.year_of_study_updated_successfully'));
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'year_of_study_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $yearOfStudy = YearOfStudy::where('id', $request->year_of_study_id)->where('deleted', 0)->first();
        if (!$yearOfStudy) {
            return back()->with('warning', __('messages.year_of_study_not_found'));
        }

        YearOfStudy::where('id', $request->year_of_study_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'year_of_studies', 3, 'DB id: [' . $request->year_of_study_id . ']');

        return back()->with('success', __('messages.year_of_study_deleted_successfully'));
    }
}
