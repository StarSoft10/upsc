<?php

namespace Modules\SuperAdmin\Http\Controllers;

use App\Entities\DurationType;
use App\Entities\Duration;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;

class DurationController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    public function index(Request $request)
    {
        $name = !empty($request->name) ? trim($request->name) : '';
        $durationTypeIds = !empty($request->durationTypeIds) ? $request->durationTypeIds : [];

        $durations = Duration::where('deleted', 0)->orderBy('id', 'ASC')->paginate(10);
        $durations->appends([
            'name' => $name,
            'durationTypeIds' => $durationTypeIds
        ]);

        $durationTypes = DurationType::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        return view('superadmin::durations.index', compact('durations', 'durationTypes', 'name', 'durationTypeIds'));
    }

    public function search(Request $request)
    {
        $name = !empty($request->name) ? trim($request->name) : '';
        $durationTypeIds = !empty($request->durationTypeIds) ? $request->durationTypeIds : [];

        $durations = Duration::where('deleted', 0)->where(function ($query) use ($name, $durationTypeIds) {
            if ($name !== '') {
                $query->where('name', 'LIKE', '%' . trim($name) . '%');
            }
            if (count($durationTypeIds) > 0) {
                $query->whereIn('duration_type_id', $durationTypeIds);
            }
        })->orderBy('id', 'ASC')->paginate(10);
        $durations->appends([
            'name' => $name,
            'durationTypeIds' => $durationTypeIds
        ]);

        $durationTypes = DurationType::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        return view('superadmin::durations.index', compact('durations', 'durationTypes', 'name', 'durationTypeIds'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'duration_type_id' => 'required|numeric',
            'name' => [
                'required',
                Rule::unique('durations')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ],
            'start' => 'required',
            'end' => 'required',
            'order' => [
                'required',
                'numeric',
                Rule::unique('durations')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $duration = Duration::create([
            'duration_type_id' => $request->duration_type_id,
            'name' => trim($request->name),
            'start' => trim($request->start),
            'end' => trim($request->end),
            'order' => trim($request->order)
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'durations', 1, 'DB id: [' . $duration->id . ']');

        return back()->with('success', __('messages.duration_created_successfully'));
    }

    public function show($id)
    {
        $duration = Duration::where('id', $id)->where('deleted', 0)->first();

        if (!$duration) {
            return back()->with('warning', __('messages.duration_not_found'));
        }

        $durationTypes = DurationType::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        return view('superadmin::durations.show', compact('duration', 'durationTypes'));
    }

    public function update(Request $request, $id)
    {
        $duration = Duration::where('id', $id)->where('deleted', 0)->first();
        if (!$duration) {
            return back()->with('warning', __('messages.duration_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'duration_type_id' => 'required|numeric',
            'name' => [
                'required',
                Rule::unique('durations')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ],
            'start' => 'required',
            'end' => 'required',
            'order' => [
                'required',
                'numeric',
                Rule::unique('durations')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        Duration::where('id', $id)->update([
            'duration_type_id' => $request->duration_type_id,
            'name' => trim($request->name),
            'start' => trim($request->start),
            'end' => trim($request->end),
            'order' => trim($request->order),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'durations', 2, 'DB id: [' . $id . ']');

        return redirect()->route('superadmin.durations.index')->with('success', __('messages.duration_updated_successfully'));
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'duration_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $duration = Duration::where('id', $request->duration_id)->where('deleted', 0)->first();
        if (!$duration) {
            return back()->with('warning', __('messages.duration_not_found'));
        }

        Duration::where('id', $request->duration_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'durations', 3, 'DB id: [' . $request->duration_id . ']');

        return back()->with('success', __('messages.duration_deleted_successfully'));
    }
}
