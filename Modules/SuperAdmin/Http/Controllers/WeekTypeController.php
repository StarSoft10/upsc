<?php

namespace Modules\SuperAdmin\Http\Controllers;

use App\Entities\WeekType;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;

class WeekTypeController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    public function index(Request $request)
    {
        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';

        $weekTypes = WeekType::where('deleted', 0)->orderBy('id', 'ASC')->paginate(10);
        $weekTypes->appends([
            'name' => $name,
            'shortName' => $shortName
        ]);

        return view('superadmin::week-types.index', compact('weekTypes', 'name', 'shortName'));
    }

    public function search(Request $request)
    {
        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';

        $weekTypes = WeekType::where('deleted', 0)->where(function ($query) use ($name, $shortName) {
            if ($name !== '') {
                $query->where('name', 'LIKE', '%' . trim($name) . '%');
            }
            if ($shortName !== '') {
                $query->where('short_name', 'LIKE', '%' . trim($shortName) . '%');
            }
        })->orderBy('id', 'ASC')->paginate(10);
        $weekTypes->appends([
            'name' => $name,
            'shortName' => $shortName
        ]);

        return view('superadmin::week-types.index', compact('weekTypes', 'name', 'shortName'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('week_types')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('week_types')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $weekType = WeekType::create(['name' => trim($request->name), 'short_name' => trim($request->short_name)]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'week_types', 1, 'DB id: [' . $weekType->id . ']');

        return back()->with('success', __('messages.week_type_created_successfully'));
    }

    public function show($id)
    {
        $weekType = WeekType::where('id', $id)->where('deleted', 0)->first();

        if (!$weekType) {
            return back()->with('warning', __('messages.week_type_not_found'));
        }

        return view('superadmin::week-types.show', compact('weekType'));
    }

    public function update(Request $request, $id)
    {
        $weekType = WeekType::where('id', $id)->where('deleted', 0)->first();
        if (!$weekType) {
            return back()->with('warning', __('messages.week_type_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('week_types')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('week_types')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        WeekType::where('id', $id)->update([
            'name' => trim($request->name),
            'short_name' => trim($request->short_name),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'week_types', 2, 'DB id: [' . $id . ']');

        return redirect()->route('superadmin.week-types.index')->with('success', __('messages.week_type_updated_successfully'));
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'week_type_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $weekType = WeekType::where('id', $request->week_type_id)->where('deleted', 0)->first();
        if (!$weekType) {
            return back()->with('warning', __('messages.week_type_not_found'));
        }

        WeekType::where('id', $request->week_type_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'week_types', 3, 'DB id: [' . $request->week_type_id . ']');

        return back()->with('success', __('messages.week_type_deleted_successfully'));
    }
}
