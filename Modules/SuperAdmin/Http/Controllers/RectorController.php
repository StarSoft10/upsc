<?php

namespace Modules\SuperAdmin\Http\Controllers;

use App\Entities\Rector;
use App\Entities\User;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;
use Illuminate\Database\Eloquent\Builder;

class RectorController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    public function index(Request $request)
    {
        $name = !empty($request->name) ? trim($request->name) : '';
        $email = !empty($request->email) ? trim($request->email) : '';
        $phone = !empty($request->phone) ? trim($request->phone) : '';

        $rectors = Rector::where('deleted', 0)->orderBy('last_name')->orderBy('first_name')->paginate(10);
        $rectors->appends([
            'name' => $name,
            'email' => $email,
            'phone' => $phone
        ]);

        $permissions = $this->helper->getAllowedPermissionsByModel('rector');

        return view('superadmin::rectors.index', compact('rectors', 'name', 'email', 'phone', 'permissions'));
    }

    public function search(Request $request)
    {
        $name = !empty($request->name) ? trim($request->name) : '';
        $email = !empty($request->email) ? trim($request->email) : '';
        $phone = !empty($request->phone) ? trim($request->phone) : '';

        $rectors = Rector::where('deleted', 0)->with('user')
            ->whereHas('user', function (Builder $query) use ($email) {
                if ($email !== '') {
                    $query->where('email', 'LIKE', '%' . trim($email) . '%');
                }
            })->where(function ($query) use ($name, $phone) {
                if ($name !== '') {
                    $query->where('first_name', 'LIKE', '%' . trim($name) . '%');
                    $query->orWhere('last_name', 'LIKE', '%' . trim($name) . '%');
                }
                if ($phone !== '') {
                    $query->where('phone', 'LIKE', '%' . trim($phone) . '%');
                }
            })->orderBy('last_name')->orderBy('first_name')->paginate(10);
        $rectors->appends([
            'name' => $name,
            'email' => $email,
            'phone' => $phone
        ]);

        $permissions = $this->helper->getAllowedPermissionsByModel('rector');

        return view('superadmin::rectors.index', compact('rectors', 'name', 'email', 'phone', 'permissions'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users|regex:/^[a-z0-9\@\.\_\-]+$/u',
            'password' => 'required|confirmed',
            'permissions' => 'required|array|min:1'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $user = User::create([
            'name' => $request->first_name . ' ' . $request->last_name,
            'email' => mb_strtolower(trim($request->email), 'UTF-8'),
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make($request->password)
        ]);
        $user->assignRole('rector');
        foreach ($request->permissions as $permission) {
            $user->givePermissionTo($permission);
        }

        $rector = Rector::create([
            'user_id' => $user->id,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'rectors', 1, 'DB id: [' . $rector->id . ']');

        return back()->with('success', __('messages.rector_created_successfully'));
    }

    public function show($id)
    {
        $rector = Rector::where('id', $id)->where('deleted', 0)->first();

        if (!$rector) {
            return back()->with('warning', __('messages.rector_not_found'));
        }
        $rectorPermissions = [];
        foreach ($rector->user->getDirectPermissions() as $permission) {
            $rectorPermissions[] = $permission->id;
        }
        $rector->permissions = $rectorPermissions;

        $permissions = $this->helper->getAllowedPermissionsByModel('rector');

        return view('superadmin::rectors.show', compact('rector', 'permissions', 'rectorPermissions'));
    }

    public function update(Request $request, $id)
    {
        $rector = Rector::where('id', $id)->where('deleted', 0)->first();
        if (!$rector) {
            return back()->with('warning', __('messages.rector_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => [
                'required',
                'email',
                'regex:/^[a-z0-9\@\.\_\-]+$/u',
                Rule::unique('users')->ignore(User::findOrFail($rector->user->id)),
            ],
            'password' => 'confirmed',
            'permissions' => 'required|array|min:1'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        User::where('id', $rector->user->id)->update([
            'name' => $request->first_name . ' ' . $request->last_name,
            'email' => mb_strtolower(trim($request->email), 'UTF-8'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $user = User::find($rector->user->id);
        $user->syncPermissions($request->permissions);

        if (trim($request->password) !== '') {
            User::where('id', $rector->user->id)->update([
                'password' => Hash::make($request->password),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }

        Rector::where('id', $id)->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'rectors', 2, 'DB id: [' . $id . ']');

        return redirect()->route('superadmin.rectors.index')->with('success', __('messages.rector_updated_successfully'));
    }

    public function block(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'rector_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $rector = Rector::where('id', $request->rector_id)->where('deleted', 0)->first();
        if (!$rector) {
            return back()->with('warning', __('messages.rector_not_found'));
        }

        Rector::where('id', $request->rector_id)->update([
            'blocked' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'rectors', 4, 'DB id: [' . $request->rector_id . ']');

        return back()->with('success', __('messages.rector_blocked_successfully'));
    }

    public function unblock(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'rector_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $rector = Rector::where('id', $request->rector_id)->where('deleted', 0)->first();
        if (!$rector) {
            return back()->with('warning', __('messages.rector_not_found'));
        }

        Rector::where('id', $request->rector_id)->update([
            'blocked' => 0,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'rectors', 5, 'DB id: [' . $request->rector_id . ']');

        return back()->with('success', __('messages.rector_unblocked_successfully'));
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'rector_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $rector = Rector::where('id', $request->rector_id)->where('deleted', 0)->first();
        if (!$rector) {
            return back()->with('warning', __('messages.rector_not_found'));
        }

        Rector::where('id', $request->rector_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'rectors', 3, 'DB id: [' . $request->rector_id . ']');

        return back()->with('success', __('messages.rector_deleted_successfully'));
    }
}
