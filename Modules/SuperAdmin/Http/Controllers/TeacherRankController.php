<?php

namespace Modules\SuperAdmin\Http\Controllers;

use App\Entities\TeacherRank;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;

class TeacherRankController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    public function index(Request $request)
    {
        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';

        $teacherRanks = TeacherRank::where('deleted', 0)->orderBy('id', 'ASC')->paginate(10);
        $teacherRanks->appends([
            'name' => $name,
            'shortName' => $shortName
        ]);

        return view('superadmin::teacher-ranks.index', compact('teacherRanks', 'name', 'shortName'));
    }

    public function search(Request $request)
    {
        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';

        $teacherRanks = TeacherRank::where('deleted', 0)->where(function ($query) use ($name, $shortName) {
            if ($name !== '') {
                $query->where('name', 'LIKE', '%' . trim($name) . '%');
            }
            if ($shortName !== '') {
                $query->where('short_name', 'LIKE', '%' . trim($shortName) . '%');
            }
        })->orderBy('id', 'ASC')->paginate(10);
        $teacherRanks->appends([
            'name' => $name,
            'shortName' => $shortName
        ]);

        return view('superadmin::teacher-ranks.index', compact('teacherRanks', 'name', 'shortName'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('teacher_ranks')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('teacher_ranks')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $teacherRank = TeacherRank::create([
            'name' => trim($request->name),
            'short_name' => trim($request->short_name)
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'teacher_ranks', 1, 'DB id: [' . $teacherRank->id . ']');

        return back()->with('success', __('messages.teacher_rank_created_successfully'));
    }

    public function show($id)
    {
        $teacherRank = TeacherRank::where('id', $id)->where('deleted', 0)->first();

        if (!$teacherRank) {
            return back()->with('warning', __('messages.teacher_rank_not_found'));
        }

        return view('superadmin::teacher-ranks.show', compact('teacherRank'));
    }

    public function update(Request $request, $id)
    {
        $teacherRank = TeacherRank::where('id', $id)->where('deleted', 0)->first();
        if (!$teacherRank) {
            return back()->with('warning', __('messages.teacher_rank_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('teacher_ranks')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('teacher_ranks')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        TeacherRank::where('id', $id)->update([
            'name' => trim($request->name),
            'short_name' => trim($request->short_name),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'teacher_ranks', 2, 'DB id: [' . $id . ']');

        return redirect()->route('superadmin.teacher-ranks.index')->with('success', __('messages.teacher_rank_updated_successfully'));
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'teacher_rank_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $teacherRank = TeacherRank::where('id', $request->teacher_rank_id)->where('deleted', 0)->first();
        if (!$teacherRank) {
            return back()->with('warning', __('messages.teacher_rank_not_found'));
        }

        TeacherRank::where('id', $request->teacher_rank_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'teacher_ranks', 3, 'DB id: [' . $request->teacher_rank_id . ']');

        return back()->with('success', __('messages.teacher_rank_deleted_successfully'));
    }
}
