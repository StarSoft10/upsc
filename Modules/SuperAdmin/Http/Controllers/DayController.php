<?php

namespace Modules\SuperAdmin\Http\Controllers;

use App\Entities\Day;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Routing\Controller;
use Illuminate\Validation\Rule;

class DayController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    public function index()
    {
        $days = Day::where('deleted', 0)->orderBy('order')->paginate(10);

        return view('superadmin::days.index', compact('days'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('days')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ],
            'order' => [
                'required',
                'numeric',
                Rule::unique('days')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $day = Day::create(['name' => trim($request->name), 'order' => trim($request->order)]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'days', 1, 'DB id: [' . $day->id . ']');

        return back()->with('success', __('messages.day_created_successfully'));
    }

    public function show($id)
    {
        $day = Day::where('id', $id)->where('deleted', 0)->first();

        if (!$day) {
            return back()->with('warning', __('messages.day_not_found'));
        }

        return view('superadmin::days.show', compact('day'));
    }

    public function update(Request $request, $id)
    {
        $day = Day::where('id', $id)->where('deleted', 0)->first();
        if (!$day) {
            return back()->with('warning', __('messages.day_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('days')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ],
            'order' => [
                'required',
                'numeric',
                Rule::unique('days')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        Day::where('id', $id)->update([
            'name' => trim($request->name),
            'order' => trim($request->order),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'days', 2, 'DB id: [' . $id . ']');

        return redirect()->route('superadmin.days.index')->with('success', __('messages.day_updated_successfully'));
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'day_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $day = Day::where('id', $request->day_id)->where('deleted', 0)->first();
        if (!$day) {
            return back()->with('warning', __('messages.day_not_found'));
        }

        Day::where('id', $request->day_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'days', 3, 'DB id: [' . $request->day_id . ']');

        return back()->with('success', __('messages.day_deleted_successfully'));
    }
}
