<?php

namespace Modules\SuperAdmin\Http\Controllers;

use App\Entities\Admin;
use App\Entities\User;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;
use Illuminate\Database\Eloquent\Builder;

class AdminController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    public function index(Request $request)
    {
        $name = !empty($request->name) ? trim($request->name) : '';
        $email = !empty($request->email) ? trim($request->email) : '';
        $phone = !empty($request->phone) ? trim($request->phone) : '';

        $admins = Admin::where('deleted', 0)->orderBy('last_name')->orderBy('first_name')->paginate(10);
        $admins->appends([
            'name' => $name,
            'email' => $email,
            'phone' => $phone
        ]);

        $permissions = $this->helper->getAllowedPermissionsByModel('admin');

        return view('superadmin::admins.index', compact('admins', 'name', 'email', 'phone', 'permissions'));
    }

    public function search(Request $request)
    {
        $name = !empty($request->name) ? trim($request->name) : '';
        $email = !empty($request->email) ? trim($request->email) : '';
        $phone = !empty($request->phone) ? trim($request->phone) : '';

        $admins = Admin::where('deleted', 0)->with('user')
            ->whereHas('user', function (Builder $query) use ($email) {
                if ($email !== '') {
                    $query->where('email', 'LIKE', '%' . trim($email) . '%');
                }
            })->where(function ($query) use ($name, $phone) {
                if ($name !== '') {
                    $query->where('first_name', 'LIKE', '%' . trim($name) . '%');
                    $query->orWhere('last_name', 'LIKE', '%' . trim($name) . '%');
                }
                if ($phone !== '') {
                    $query->where('phone', 'LIKE', '%' . trim($phone) . '%');
                }
            })->orderBy('last_name')->orderBy('first_name')->paginate(10);
        $admins->appends([
            'name' => $name,
            'email' => $email,
            'phone' => $phone
        ]);

        $permissions = $this->helper->getAllowedPermissionsByModel('admin');

        return view('superadmin::admins.index', compact('admins', 'name', 'email', 'phone', 'permissions'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users|regex:/^[a-z0-9\@\.\_\-]+$/u',
            'password' => 'required|confirmed',
            'permissions' => 'required|array|min:1'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $user = User::create([
            'name' => $request->first_name . ' ' . $request->last_name,
            'email' => mb_strtolower(trim($request->email), 'UTF-8'),
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make($request->password)
        ]);
        $user->assignRole('admin');
        foreach ($request->permissions as $permission) {
            $user->givePermissionTo($permission);
        }

        $admin = Admin::create([
            'user_id' => $user->id,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'admins', 1, 'DB id: [' . $admin->id . ']');

        return back()->with('success', __('messages.admin_created_successfully'));
    }

    public function show($id)
    {
        $admin = Admin::where('id', $id)->where('deleted', 0)->first();

        if (!$admin) {
            return back()->with('warning', __('messages.admin_not_found'));
        }
        $adminPermissions = [];
        foreach ($admin->user->getDirectPermissions() as $permission) {
            $adminPermissions[] = $permission->id;
        }
        $admin->permissions = $adminPermissions;

//        $permissions = Permission::all();
        $permissions = $this->helper->getAllowedPermissionsByModel('admin');

        return view('superadmin::admins.show', compact('admin', 'permissions', 'adminPermissions'));
    }

    public function update(Request $request, $id)
    {
        $admin = Admin::where('id', $id)->where('deleted', 0)->first();
        if (!$admin) {
            return back()->with('warning', __('messages.admin_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => [
                'required',
                'email',
                'regex:/^[a-z0-9\@\.\_\-]+$/u',
                Rule::unique('users')->ignore(User::findOrFail($admin->user->id)),
            ],
            'password' => 'confirmed',
            'permissions' => 'required|array|min:1'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        User::where('id', $admin->user->id)->update([
            'name' => $request->first_name . ' ' . $request->last_name,
            'email' => mb_strtolower(trim($request->email), 'UTF-8'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $user = User::find($admin->user->id);
        $user->syncPermissions($request->permissions);

        if (trim($request->password) !== '') {
            User::where('id', $admin->user->id)->update([
                'password' => Hash::make($request->password),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }

        Admin::where('id', $id)->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'admins', 2, 'DB id: [' . $id . ']');

        return redirect()->route('superadmin.admins.index')->with('success', __('messages.admin_updated_successfully'));
    }

    public function block(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'admin_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $admin = Admin::where('id', $request->admin_id)->where('deleted', 0)->first();
        if (!$admin) {
            return back()->with('warning', __('messages.admin_not_found'));
        }

        Admin::where('id', $request->admin_id)->update([
            'blocked' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'admins', 4, 'DB id: [' . $request->admin_id . ']');

        return back()->with('success', __('messages.admin_blocked_successfully'));
    }

    public function unblock(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'admin_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $admin = Admin::where('id', $request->admin_id)->where('deleted', 0)->first();
        if (!$admin) {
            return back()->with('warning', __('messages.admin_not_found'));
        }

        Admin::where('id', $request->admin_id)->update([
            'blocked' => 0,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'admins', 5, 'DB id: [' . $request->admin_id . ']');

        return back()->with('success', __('messages.admin_unblocked_successfully'));
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'admin_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $admin = Admin::where('id', $request->admin_id)->where('deleted', 0)->first();
        if (!$admin) {
            return back()->with('warning', __('messages.admin_not_found'));
        }

        Admin::where('id', $request->admin_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'admins', 3, 'DB id: [' . $request->admin_id . ']');

        return back()->with('success', __('messages.admin_deleted_successfully'));
    }
}
