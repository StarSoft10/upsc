<?php

namespace Modules\SuperAdmin\Http\Controllers;

use App\Entities\Semester;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Routing\Controller;
use Illuminate\Validation\Rule;

class SemesterController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    public function index()
    {
        $semesters = Semester::where('deleted', 0)->orderBy('order')->paginate(10);

        return view('superadmin::semesters.index', compact('semesters'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('semesters')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ],
            'order' => [
                'required',
                'numeric',
                Rule::unique('semesters')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $semester = Semester::create(['name' => trim($request->name), 'order' => trim($request->order)]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'semesters', 1, 'DB id: [' . $semester->id . ']');

        return back()->with('success', __('messages.semester_created_successfully'));
    }

    public function show($id)
    {
        $semester = Semester::where('id', $id)->where('deleted', 0)->first();

        if (!$semester) {
            return back()->with('warning', __('messages.semester_not_found'));
        }

        return view('superadmin::semesters.show', compact('semester'));
    }

    public function update(Request $request, $id)
    {
        $semester = Semester::where('id', $id)->where('deleted', 0)->first();
        if (!$semester) {
            return back()->with('warning', __('messages.semester_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('semesters')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ],
            'order' => [
                'required',
                'numeric',
                Rule::unique('semesters')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        Semester::where('id', $id)->update([
            'name' => trim($request->name),
            'order' => trim($request->order),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'semesters', 2, 'DB id: [' . $id . ']');

        return redirect()->route('superadmin.semesters.index')->with('success', __('messages.semester_updated_successfully'));
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'semester_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $semester = Semester::where('id', $request->semester_id)->where('deleted', 0)->first();
        if (!$semester) {
            return back()->with('warning', __('messages.semester_not_found'));
        }

        Semester::where('id', $request->semester_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'semesters', 3, 'DB id: [' . $request->semester_id . ']');

        return back()->with('success', __('messages.semester_deleted_successfully'));
    }
}
