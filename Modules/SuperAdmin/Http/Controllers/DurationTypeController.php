<?php

namespace Modules\SuperAdmin\Http\Controllers;

use App\Entities\DurationType;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;

class DurationTypeController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    public function index(Request $request)
    {
        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';

        $durationTypes = DurationType::where('deleted', 0)->orderBy('id', 'ASC')->paginate(10);
        $durationTypes->appends([
            'name' => $name,
            'shortName' => $shortName
        ]);

        return view('superadmin::duration-types.index', compact('durationTypes', 'name', 'shortName'));
    }

    public function search(Request $request)
    {
        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';

        $durationTypes = DurationType::where('deleted', 0)->where(function ($query) use ($name, $shortName) {
            if ($name !== '') {
                $query->where('name', 'LIKE', '%' . trim($name) . '%');
            }
            if ($shortName !== '') {
                $query->where('short_name', 'LIKE', '%' . trim($shortName) . '%');
            }
        })->orderBy('id', 'ASC')->paginate(10);
        $durationTypes->appends([
            'name' => $name,
            'shortName' => $shortName
        ]);

        return view('superadmin::duration-types.index', compact('durationTypes', 'name', 'shortName'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('duration_types')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('duration_types')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $durationType = DurationType::create([
            'name' => trim($request->name),
            'short_name' => trim($request->short_name)
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'duration_types', 1, 'DB id: [' . $durationType->id . ']');

        return back()->with('success', __('messages.duration_type_created_successfully'));
    }

    public function show($id)
    {
        $durationType = DurationType::where('id', $id)->where('deleted', 0)->first();

        if (!$durationType) {
            return back()->with('warning', __('messages.duration_type_not_found'));
        }

        return view('superadmin::duration-types.show', compact('durationType'));
    }

    public function update(Request $request, $id)
    {
        $durationType = DurationType::where('id', $id)->where('deleted', 0)->first();
        if (!$durationType) {
            return back()->with('warning', __('messages.duration_type_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('duration_types')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('duration_types')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        DurationType::where('id', $id)->update([
            'name' => trim($request->name),
            'short_name' => trim($request->short_name),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'duration_types', 2, 'DB id: [' . $id . ']');

        return redirect()->route('superadmin.duration-types.index')->with('success', __('messages.duration_type_updated_successfully'));
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'duration_type_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $durationType = DurationType::where('id', $request->duration_type_id)->where('deleted', 0)->first();
        if (!$durationType) {
            return back()->with('warning', __('messages.duration_type_not_found'));
        }

        DurationType::where('id', $request->duration_type_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'duration_types', 3, 'DB id: [' . $request->duration_type_id . ']');

        return back()->with('success', __('messages.duration_type_deleted_successfully'));
    }
}
