<?php

namespace Modules\SuperAdmin\Http\Controllers;

use App\Entities\BorderouType;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;

class BorderouTypeController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    public function index(Request $request)
    {
        $name = !empty($request->name) ? trim($request->name) : '';

        $borderouTypes = BorderouType::where('deleted', 0)->orderBy('id', 'ASC')->paginate(10);
        $borderouTypes->appends(['name' => $name ]);

        return view('superadmin::borderou-types.index', compact('borderouTypes', 'name'));
    }

    public function search(Request $request)
    {
        $name = !empty($request->name) ? trim($request->name) : '';

        $borderouTypes = BorderouType::where('deleted', 0)->where(function ($query) use ($name) {
            if ($name !== '') {
                $query->where('name', 'LIKE', '%' . trim($name) . '%');
            }
        })->orderBy('id', 'ASC')->paginate(10);
        $borderouTypes->appends(['name' => $name ]);

        return view('superadmin::borderou-types.index', compact('borderouTypes', 'name'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('borderou_types')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ],
            'semester_index' => 'required|numeric',
            'exam_index' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $borderouType = BorderouType::create([
            'name' => trim($request->name),
            'semester_index' => trim($request->semester_index),
            'exam_index' => trim($request->exam_index)
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'borderou_types', 1, 'DB id: [' . $borderouType->id . ']');

        return back()->with('success', __('messages.borderou_type_created_successfully'));
    }

    public function show($id)
    {
        $borderouType = BorderouType::where('id', $id)->where('deleted', 0)->first();

        if (!$borderouType) {
            return back()->with('warning', __('messages.borderou_type_not_found'));
        }

        return view('superadmin::borderou-types.show', compact('borderouType'));
    }

    public function update(Request $request, $id)
    {
        $borderouType = BorderouType::where('id', $id)->where('deleted', 0)->first();
        if (!$borderouType) {
            return back()->with('warning', __('messages.borderou_type_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('borderou_types')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ],
            'semester_index' => 'required|numeric',
            'exam_index' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        BorderouType::where('id', $id)->update([
            'name' => trim($request->name),
            'semester_index' => trim($request->semester_index),
            'exam_index' => trim($request->exam_index),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'borderou_types', 2, 'DB id: [' . $id . ']');

        return redirect()->route('superadmin.borderou-types.index')->with('success', __('messages.borderou_type_updated_successfully'));
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'borderou_type_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $borderouType = BorderouType::where('id', $request->borderou_type_id)->where('deleted', 0)->first();
        if (!$borderouType) {
            return back()->with('warning', __('messages.borderou_type_not_found'));
        }

        BorderouType::where('id', $request->borderou_type_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'borderou_types', 3, 'DB id: [' . $request->borderou_type_id . ']');

        return back()->with('success', __('messages.borderou_type_deleted_successfully'));
    }
}
