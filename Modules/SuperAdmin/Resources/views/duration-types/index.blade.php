@extends('superadmin::layouts.app')

@section('title')
    {{ __('pages.duration_types') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            {!! Form::open(['route' => 'superadmin.duration-types.search', 'method' => 'GET']) !!}
                                <div class="row">
                                    <div class="col-md-4">
                                        {!! Form::text('name', $name, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.name')]) !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::text('short_name', $shortName, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.short_name')]) !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::submit(__('pages.search'), ['class' => 'btn btn-primary btn-sm']) !!}
                                        <a href="{{ route('superadmin.duration-types.index') }}" class="btn btn-info btn-sm" title="{{ __('pages.reset_filter') }}">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-2 text-right">
                            <button type="button" class="btn btn-success mb-1 btn-sm" data-toggle="modal" data-target="#addDurationType">
                                {{ __('pages.add_duration_type') }}
                            </button>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th>{{ __('pages.name') }}</th>
                                <th>{{ __('pages.short_name') }}</th>
                                <th>{{ __('pages.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($durationTypes) > 0)
                                @foreach($durationTypes as $durationType)
                                    <tr>
                                        <td class="text-center"><span>{{ $durationType->name }}</span></td>
                                        <td class="text-center"><span>{{ $durationType->short_name }}</span></td>
                                        <td class="text-center">
                                            <a href="{{ route('superadmin.duration-types.show', $durationType->id) }}" class="btn-no-bg" title="{{ __('pages.show_edit') }}">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="javascript:void(0)" data-durationtypeid="{{ $durationType->id }}" class="btn-no-bg delete-duration-type" title="{{ __('pages.delete') }}">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="3">{{ __('messages.not_found_any_duration_types') }}</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="table-footer">
                        {!! $durationTypes->render() !!}
                        <div class="total-found">{{ __('pages.total_found') }}: {{ $durationTypes->total() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addDurationType" role="dialog" aria-labelledby="addDurationType" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                {!! Form::open(['url' => route('superadmin.duration-types.store'), 'method' => 'post', 'autocomplete' => 'off']) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.add_duration_type') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('name', __('pages.name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('short_name', __('pages.short_name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('short_name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        {!! Form::submit(__('pages.add'), ['class' => 'btn btn-success btn-sm']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteDurationType" role="dialog" aria-labelledby="deleteDurationType" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'superadmin.duration-types.delete']) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.delete_duration_type') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info" role="alert">
                            <div class="alert-text">
                                {{ __('pages.sure_to_delete_duration_type?') }}
                            </div>
                        </div>
                        <input type="hidden" name="duration_type_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        <button type="submit" class="btn btn-danger btn-sm">{{ __('pages.delete') }}</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
@endsection

@section('custom-scripts')
    <script>
        let body = $('body');

        body.on('click', '.delete-duration-type', function () {
            let durationTypeId = $(this).attr('data-durationtypeid');
            let modal = $('#deleteDurationType');
            modal.find('input[name="duration_type_id"]').val(durationTypeId);
            modal.modal('show');
        });

        $('#deleteDurationType').on('hidden.bs.modal', function () {
            $(this).find('input[name="duration_type_id"]').val('');
        });
    </script>
@endsection