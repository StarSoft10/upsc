@extends('superadmin::layouts.app')

@section('title')
    {{ __('pages.borderou_types') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-9">
                            {!! Form::open(['route' => 'superadmin.borderou-types.search', 'method' => 'GET']) !!}
                                <div class="row">
                                    <div class="col-md-4">
                                        {!! Form::text('name', $name, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.name')]) !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::submit(__('pages.search'), ['class' => 'btn btn-primary btn-sm']) !!}
                                        <a href="{{ route('superadmin.borderou-types.index') }}" class="btn btn-info btn-sm" title="{{ __('pages.reset_filter') }}">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-3 text-right">
                            <button type="button" class="btn btn-success mb-1 btn-sm" data-toggle="modal" data-target="#addBorderouType">
                                {{ __('pages.add_borderou_type') }}
                            </button>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th>{{ __('pages.name') }}</th>
                                <th>{{ __('pages.semester_index') }}</th>
                                <th>{{ __('pages.exam_index') }}</th>
                                <th>{{ __('pages.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($borderouTypes) > 0)
                                @foreach($borderouTypes as $borderouType)
                                    <tr>
                                        <td class="text-center"><span>{{ $borderouType->name }}</span></td>
                                        <td class="text-center"><span>{{ $borderouType->semester_index }}</span></td>
                                        <td class="text-center"><span>{{ $borderouType->exam_index }}</span></td>
                                        <td class="text-center">
                                            <a href="{{ route('superadmin.borderou-types.show', $borderouType->id) }}" class="btn-no-bg" title="{{ __('pages.show_edit') }}">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="javascript:void(0)" data-borderoutypeid="{{ $borderouType->id }}" class="btn-no-bg delete-borderou-type" title="{{ __('pages.delete') }}">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="4">{{ __('messages.not_found_any_borderou_types') }}</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="table-footer">
                        {!! $borderouTypes->render() !!}
                        <div class="total-found">{{ __('pages.total_found') }}: {{ $borderouTypes->total() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addBorderouType" role="dialog" aria-labelledby="addBorderouType" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                {!! Form::open(['url' => route('superadmin.borderou-types.store'), 'method' => 'post', 'autocomplete' => 'off']) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.add_borderou_type') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('name', __('pages.name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('semester_index', __('pages.semester_index'), ['class' => 'control-label']) !!}
                                    {!! Form::text('semester_index', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('exam_index', __('pages.exam_index'), ['class' => 'control-label']) !!}
                                    {!! Form::text('exam_index', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        {!! Form::submit(__('pages.add'), ['class' => 'btn btn-success btn-sm']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteBorderouType" role="dialog" aria-labelledby="deleteBorderouType" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'superadmin.borderou-types.delete']) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.delete_borderou_type') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info" role="alert">
                            <div class="alert-text">
                                {{ __('pages.sure_to_delete_borderou_type?') }}
                            </div>
                        </div>
                        <input type="hidden" name="borderou_type_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        <button type="submit" class="btn btn-danger btn-sm">{{ __('pages.delete') }}</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
@endsection

@section('custom-scripts')
    <script>
        let body = $('body');

        body.on('click', '.delete-borderou-type', function () {
            let borderouTypeId = $(this).attr('data-borderoutypeid');
            let modal = $('#deleteBorderouType');
            modal.find('input[name="borderou_type_id"]').val(borderouTypeId);
            modal.modal('show');
        });

        $('#deleteBorderouType').on('hidden.bs.modal', function () {
            $(this).find('input[name="borderou_type_id"]').val('');
        });
    </script>
@endsection