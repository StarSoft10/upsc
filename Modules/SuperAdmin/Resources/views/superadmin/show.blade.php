@extends('superadmin::layouts.app')

@section('title')
    {{ __('pages.profile') }} : {{ $user->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-info-circle"></i>
                    <strong class="card-title mb-3">{{ __('pages.profile_card') }}</strong>
                </div>
                <div class="card-body">
                    <div class="mx-auto d-block">
                        @if($user->photo == null || $user->photo == '')
                            <img class="rounded-circle mx-auto" src="{{ asset('images/default_photo.jpg') }}" alt="Photo" style="display: block">
                        @else
                            <img class="rounded-circle mx-auto" src="{{ asset('storage/users-photo/' . $user->photo) }}" alt="Photo" style="display: block">
                        @endif
                        <h5 class="text-sm-center mt-2 mb-1">{{ $user->name }}</h5>

                        {!! Form::model($user, ['method' => 'PATCH', 'route' => ['superadmin.changePhoto'], 'enctype' => 'multipart/form-data']) !!}
                            {!! Form::hidden('user', $user->id) !!}
                                <div class="form-group">
                                    <div class="text-center">
                                        <img id="image_preview" style="width:100%; display: none; margin-bottom: 10px;" src="">
                                    </div>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input i_attach_photo input-sm form-control-sm form-control"
                                               name="photo" accept="image/*" required>
                                        <label class="custom-file-label l_attach_photo">
                                            @if($user->photo == null) {{ __('pages.choose_photo') }} @else {{ __('pages.change_photo') }} @endif
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group text-right">
                                    @if($user->photo == null || $user->photo == '')
                                        {!! Form::submit(__('pages.add'), ['class' => 'btn btn-success']) !!}
                                    @else
                                        {!! Form::submit(__('pages.change'), ['class' => 'btn btn-primary']) !!}
                                    @endif
                                </div>
                        {!! Form::close() !!}
                    </div>
                    <hr>
                    <div class="card-text text-sm-center">
                        <a href="javascript:void(0)"><i class="fa fa-facebook pr-1"></i></a>
                        <a href="javascript:void(0)"><i class="fa fa-twitter pr-1"></i></a>
                        <a href="javascript:void(0)"><i class="fa fa-linkedin pr-1"></i></a>
                        <a href="javascript:void(0)"><i class="fa fa-pinterest pr-1"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-edit"></i>
                    <strong class="card-title mb-3">{{ __('pages.edit') }}</strong>
                </div>
                <div class="card-body card-block">
                    {!! Form::model($user, ['method' => 'PATCH', 'route' => ['superadmin.update'], 'autocomplete' => 'off']) !!}
                        {!! Form::hidden('user', $user->id) !!}
                        <div class="form-group">
                            <label class="form-control-label">{{ __('pages.name') }}</label>
                            <div class="input-group">
                                <div class="input-group-addon" style="padding: 0.3rem 0.55rem;"><i class="fa fa-user"></i></div>
                                {!! Form::text('name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">{{ __('pages.email') }}</label>
                            <div class="input-group">
                                <div class="input-group-addon" style="padding: 0.3rem 0.55rem;"><i class="fa fa-envelope"></i></div>
                                {!! Form::email('email', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">{{ __('pages.password') }}</label>
                            <div class="input-group">
                                <div class="input-group-addon" style="padding: 0.3rem 0.55rem;"><i class="fa fa-asterisk"></i></div>
                                {!! Form::input('password', 'password', null, ['class' => 'finput-sm form-control-sm form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">{{ __('pages.confirm_password') }}</label>
                            <div class="input-group">
                                <div class="input-group-addon" style="padding: 0.3rem 0.55rem;"><i class="fa fa-asterisk"></i></div>
                                {!! Form::input('password', 'password_confirmation', null, ['class' => 'input-sm form-control-sm form-control']) !!}
                            </div>
                        </div>
                        <div class="form-actions form-group">
                            {!! Form::submit(__('pages.update'), ['class' => 'btn btn-success btn-sm']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-scripts')
    <script>
        $(function () {
            $('.i_attach_photo').on('change', function() {
                let files = $(this).prop('files');
                let countFiles = $(this).prop('files').length;
                let fileNames = '';

                let reader = new FileReader();
                reader.onload = function(){
                    let output = document.getElementById('image_preview');
                    output.src = reader.result;
                };
                reader.readAsDataURL(event.target.files[0]);
                $('#image_preview').show();
                $('.rounded-circle').hide();

                $.each(files, function(i, file) {
                    if(i + 1 < countFiles){
                        fileNames += file.name + ', ';
                    } else {
                        fileNames += file.name;
                    }
                });

                $(this).siblings('.l_attach_photo').html(fileNames);
            });
        });
    </script>
@endsection