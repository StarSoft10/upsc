<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8" />
    <title>
        @yield('title', '')
    </title>
    <meta name="description" content="UPSC">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/themify-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/selectFX/css/cs-skin-elastic.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/jqvmap/dist/jqvmap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/aside.css') }}">
    <link rel="stylesheet" href="{{ asset('css/users.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome/css/all.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

    <link rel="icon" type="image/png" sizes="64x64" href="{{asset('images/icon.png')}}">
    @yield('custom-css')
    <style>
        #header .dropdown-menu .dropdown-item {
            color: #f4f5f4!important;
        }
        #header .dropdown-menu .dropdown-item:hover {
            color: #000000!important;
        }
    </style>
</head>

<body>

@include('superadmin::layouts.aside')

<div id="right-panel" class="right-panel">
    @include('superadmin::layouts.header')

    <div class="content mt-3">
        @if(session()->has('success') || session()->has('warning') || session()->has('error'))
            <div class="row">
                <div class="col-sm-12">
                    @if(session()->has('success'))
                        <div class="sufee-alert alert with-close alert-success alert-dismissible fade show">
                            <span class="badge badge-pill badge-success">{{ __('pages.success') }}</span>
                            {{ session()->get('success') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif

                    @if(session()->has('warning'))
                        <div class="sufee-alert alert with-close alert-warning alert-dismissible fade show">
                            <span class="badge badge-pill badge-warning">{{ __('pages.warning') }}</span>
                            {{ session()->get('warning') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif

                    @if(session()->has('error'))
                        <div class="sufee-alert alert with-close alert-danger alert-dismissible fade show">
                            <span class="badge badge-pill badge-danger">{{ __('pages.error') }}</span>
                            {{ session()->get('error') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif
                </div>
            </div>
        @endif

        @yield('content')

    </div>
</div>


<script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('vendors/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ asset('vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script src="{{ asset('js/permissions.js') }}"></script>
<script src="{{ asset('js/notify.min.js') }}"></script>

<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
<script>
    Pusher.logToConsole = true;

    let pusher = new Pusher('b8c16e92910698ae0d41', {cluster: 'eu'});

    let channel = pusher.subscribe('new-registration-notification');
    let channel2 = pusher.subscribe('read-new-registration-notification');

    channel.bind('App\\Events\\NewRegistrationNotification', function(data) {
        if (data.userObj && data.type) {
            let userObj = data.userObj;
            let type = data.type;
            let createdAt = data.createdAt;
            let registerNotification = data.registerNotification;
            let notificationBlock = $('#header #message .count');
            let totalNotifications = parseInt($.trim(notificationBlock.text()));

            if(isNaN(totalNotifications) || totalNotifications === undefined || totalNotifications == null || totalNotifications === ''){
                totalNotifications = 0;
            }

            $('.not_found_notifications').remove();

            let cssClass = 'bg-flat-color-3';
            let message = '{{ __('messages.new_unconfirmed_user_register') }}';
            let link = '{{ route('superadmin.unconfirmed-users.show', ':id') }}';

            if(type === 'unconfirmed-user'){
                cssClass = 'bg-flat-color-3';
                message = '{{ __('messages.new_unconfirmed_user_register') }}';
                link = '{{ route('superadmin.unconfirmed-users.show', ':id') }}';
            } else if(type === 'student') {
                cssClass = 'bg-flat-color-1';
                message = '{{ __('messages.new_student_register') }}';
                link = '{{ route('superadmin.students.show', ':id') }}';
            } else if(type === 'teacher') {
                cssClass = 'bg-flat-color-5';
                message = '{{ __('messages.new_teacher_register') }}';
                link = '{{ route('superadmin.teachers.show', ':id') }}';
            }

            link = link.replace(':id', userObj.id);

            let html = '' +
                '<a class="dropdown-item media '+ cssClass +'" href="'+ link +'" data-notification-id="'+  registerNotification.id +'">' +
                '    <span class="photo media-left">' +
                '        <img alt="avatar" src="{{ asset('images/default_photo.jpg') }}">' +
                '    </span>' +
                '    <span class="message media-body">' +
                '        <span class="name float-left">'+ userObj.first_name + ' ' + userObj.last_name + '</span>' +
                '        <span class="time float-right">'+ createdAt +'</span>' +
                '        <span class="clearfix"></span>' +
                '        <span>'+ message +'</span>' +
                '    </span>' +
                '</a>';

            notificationBlock.html(parseInt(totalNotifications + 1));

            $(html).prependTo($('.dropdown.for-message .dropdown-menu'));
        }
    });

    channel2.bind('App\\Events\\ReadNewRegistrationNotification', function(data) {
        if (data.registerNotification ) {
            let registerNotification = data.registerNotification;
            let notificationBlock = $('#header #message .count');
            let totalNotifications = parseInt($.trim(notificationBlock.text()));

            if(isNaN(totalNotifications) || totalNotifications === undefined || totalNotifications == null || totalNotifications === ''){
                totalNotifications = 0;
            }

            $('*[data-notification-id="' + registerNotification.id + '"]').remove();
            notificationBlock.html(parseInt(totalNotifications - 1));
        }
    });
</script>

<script>
    function getRegisterNotifications(){
        let notificationCountBlock = $('#header #message .count');
        let notificationBlock = $('.dropdown.for-message .dropdown-menu');
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $( 'meta[name="csrf-token"]' ).attr('content')
            },
            async: true,
            url: '{{ route('superadmin.getRegisterNotifications')}}',
            type: 'POST',
            data: {},
            success: function (response) {
                notificationCountBlock.html(response.total);
                notificationBlock.html(response.html);
            }
        });
    }

    getRegisterNotifications();
</script>


@yield('custom-scripts')
</body>
</html>
