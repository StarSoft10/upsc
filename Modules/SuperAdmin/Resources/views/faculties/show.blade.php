@extends('superadmin::layouts.app')

@section('title')
    {{ __('pages.faculty') }} {{ $faculty->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-info-circle"></i>
                    <strong class="card-title">{{ __('pages.faculty_information') }}</strong>
                </div>
                <div class="card-body">
                    <ul style="list-style: none;">
                        <li><span>{{ __('pages.name') }}:</span><b>&nbsp;{{ $faculty->name }}</b></li>
                        <li><span>{{ __('pages.short_name') }}:</span><b>&nbsp;{{ $faculty->short_name }}</b></li>
                        <li><span>{{ __('pages.cycle') }}:</span><b>&nbsp;{{ $faculty->cycle->name }}</b></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-edit"></i>
                    <strong class="card-title">{{ __('pages.edit_faculty') }}</strong>
                </div>
                <div class="card-body">
                    {!! Form::model($faculty, ['method' => 'PATCH', 'route' => ['superadmin.faculties.update', $faculty->id], 'autocomplete' => 'off']) !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('name', __('pages.name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('short_name', __('pages.short_name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('short_name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('cycle_id', __('pages.cycle'), ['class' => 'control-label']) !!}
                                    <select data-placeholder="{{ __('pages.chose_cycle') }}" class="standardSelect" name="cycle_id" id="cycle_id">
                                        @foreach($cycles as $key => $cycle)
                                            <option value="{{ $key }}" @if($key == $faculty->cycle_id) selected @endif>{{ $cycle }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        {!! Form::submit(__('pages.edit'), ['class' => 'btn btn-primary btn-sm']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('vendors/chosen/chosen.min.css') }}">
@endsection

@section('custom-scripts')
    <script src="{{ asset('vendors/chosen/chosen.jquery.min.js') }}"></script>
    <script>
        $(function() {
            $('.standardSelect').chosen({
                disable_search_threshold: 10,
                no_results_text: '{{ __('pages.nothing_found') }}',
                width: '100%'
            });
        });
    </script>
@endsection