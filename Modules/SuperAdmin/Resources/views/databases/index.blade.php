@extends('superadmin::layouts.app')

@section('title')
    {{ __('pages.databases') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="button" class="btn btn-success mb-1 btn-sm" data-toggle="modal" data-target="#addDatabase">
                                {{ __('pages.add_database') }}
                            </button>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th>{{ __('pages.name') }}</th>
                                <th>{{ __('pages.year') }}</th>
                                <th>{{ __('pages.server_ip') }}</th>
                                <th>{{ __('pages.database_name') }}</th>
                                <th>{{ __('pages.connection_name') }}</th>
                                <th>{{ __('pages.username') }}</th>
                                <th>{{ __('pages.password') }}</th>
                                <th>{{ __('pages.port') }}</th>
                                <th>{{ __('pages.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($databases) > 0)
                                @foreach($databases as $database)
                                    <tr>
                                        <td class="text-center"><span>{{ $database->name }}</span></td>
                                        <td class="text-center"><span>{{ $database->year }}</span></td>
                                        <td class="text-center"><span>{{ $database->server_ip }}</span></td>
                                        <td class="text-center"><span>{{ $database->database_name }}</span></td>
                                        <td class="text-center"><span>{{ $database->connection_name }}</span></td>
                                        <td class="text-center"><span>{{ $database->username }}</span></td>
                                        <td class="text-center"><span>{{ $database->password }}</span></td>
                                        <td class="text-center"><span>{{ $database->port }}</span></td>
                                        <td class="text-center">
                                            <a href="{{ route('superadmin.databases.show', $database->id) }}" class="btn-no-bg" title="{{ __('pages.show_edit') }}">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="javascript:void(0)" data-databaseid="{{ $database->id }}" class="btn-no-bg delete-database" title="{{ __('pages.delete') }}">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="9">{{ __('messages.not_found_any_databases') }}</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    {!! $databases->render() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addDatabase" role="dialog" aria-labelledby="addDatabase" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                {!! Form::open(['url' => route('superadmin.databases.store'), 'method' => 'post', 'autocomplete' => 'off']) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.add_database') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('name', __('pages.name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('year', __('pages.year'), ['class' => 'control-label']) !!}
                                    {!! Form::text('year', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('server_ip', __('pages.server_ip'), ['class' => 'control-label']) !!}
                                    {!! Form::text('server_ip', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('database_name', __('pages.database_name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('database_name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('connection_name', __('pages.connection_name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('connection_name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('username', __('pages.username'), ['class' => 'control-label']) !!}
                                    {!! Form::text('username', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('password', __('pages.password'), ['class' => 'control-label']) !!}
                                    {!! Form::text('password', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('port', __('pages.port'), ['class' => 'control-label']) !!}
                                    {!! Form::text('port', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        {!! Form::submit(__('pages.add'), ['class' => 'btn btn-success btn-sm']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteDatabase" role="dialog" aria-labelledby="deleteDatabase" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'superadmin.databases.delete']) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.delete_database') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info" role="alert">
                            <div class="alert-text">
                                {{ __('pages.sure_to_delete_database?') }}
                            </div>
                        </div>
                        <input type="hidden" name="database_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        <button type="submit" class="btn btn-danger btn-sm">{{ __('pages.delete') }}</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
@endsection

@section('custom-scripts')
    <script>
        let body = $('body');

        body.on('click', '.delete-database', function () {
            let databaseId = $(this).attr('data-databaseid');
            let modal = $('#deleteDatabase');
            modal.find('input[name="database_id"]').val(databaseId);
            modal.modal('show');
        });

        $('#deleteDatabase').on('hidden.bs.modal', function () {
            $(this).find('input[name="database_id"]').val('');
        });
    </script>
@endsection