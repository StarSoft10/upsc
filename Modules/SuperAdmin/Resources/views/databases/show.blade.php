@extends('superadmin::layouts.app')

@section('title')
    {{ __('pages.database') }} {{ $database->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-info-circle"></i>
                    <strong class="card-title">{{ __('pages.database_information') }}</strong>
                </div>
                <div class="card-body">
                    <ul style="list-style: none;">
                        <li><span>{{ __('pages.name') }}:</span><b>&nbsp;{{ $database->name }}</b></li>
                        <li><span>{{ __('pages.year') }}:</span><b>&nbsp;{{ $database->year }}</b></li>
                        <li><span>{{ __('pages.server_ip') }}:</span><b>&nbsp;{{ $database->server_ip }}</b></li>
                        <li><span>{{ __('pages.database_name') }}:</span><b>&nbsp;{{ $database->database_name }}</b></li>
                        <li><span>{{ __('pages.connection_name') }}:</span><b>&nbsp;{{ $database->connection_name }}</b></li>
                        <li><span>{{ __('pages.username') }}:</span><b>&nbsp;{{ $database->username }}</b></li>
                        <li><span>{{ __('pages.password') }}:</span><b>&nbsp;{{ $database->password }}</b></li>
                        <li><span>{{ __('pages.port') }}:</span><b>&nbsp;{{ $database->port }}</b></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-edit"></i>
                    <strong class="card-title">{{ __('pages.edit_database') }}</strong>
                </div>
                <div class="card-body">
                    {!! Form::model($database, ['method' => 'PATCH', 'route' => ['superadmin.databases.update', $database->id], 'autocomplete' => 'off']) !!}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('name', __('pages.name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('year', __('pages.year'), ['class' => 'control-label']) !!}
                                    {!! Form::text('year', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('server_ip', __('pages.server_ip'), ['class' => 'control-label']) !!}
                                    {!! Form::text('server_ip', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('database_name', __('pages.database_name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('database_name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('connection_name', __('pages.connection_name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('connection_name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    {!! Form::label('username', __('pages.username'), ['class' => 'control-label']) !!}
                                    {!! Form::text('username', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    {!! Form::label('password', __('pages.password'), ['class' => 'control-label']) !!}
                                    {!! Form::text('password', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    {!! Form::label('port', __('pages.port'), ['class' => 'control-label']) !!}
                                    {!! Form::text('port', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                        </div>
                        {!! Form::submit(__('pages.edit'), ['class' => 'btn btn-primary btn-sm']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
@endsection

@section('custom-scripts')
@endsection
