@extends('superadmin::layouts.app')

@section('title')
    {{ __('pages.teacher') }} {{ $teacher->first_name }} {{ $teacher->last_name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>{{ $teacher->first_name }} {{ $teacher->last_name }}</h4>
                </div>
                <div class="card-body">
                    <div class="custom-tab">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active show" id="information-tab" data-toggle="tab" href="#information" role="tab" aria-controls="information" aria-selected="true">
                                    <i class="fa fa-info-circle"></i>
                                    {{ __('pages.teacher_information') }}
                                </a>
                                <a class="nav-item nav-link" id="edit-tab" data-toggle="tab" href="#edit" role="tab" aria-controls="edit" aria-selected="false">
                                    <i class="fa fa-edit"></i>
                                    {{ __('pages.edit_teacher') }}
                                </a>
                            </div>
                        </nav>

                        <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                            <div class="tab-pane fade active show" id="information" role="tabpanel" aria-labelledby="information-tab">
                                <div class="mx-auto d-block">
                                    <div class="row">
                                        <div class="col-md-4">
                                            @if($teacher->user->photo == null || $teacher->user->photo == '')
                                                <img class="rounded-circle mx-auto d-block" src="{{ asset('images/default_photo.jpg') }}" alt="Photo">
                                            @else
                                                <img class="rounded-circle mx-auto d-block" src="{{ asset('storage/users-photo/' . $teacher->user->photo) }}" alt="Photo">
                                            @endif
                                            <h5 class="text-sm-center mt-2 mb-1">{{ $teacher->first_name }} {{ $teacher->last_name }}</h5>
                                        </div>
                                        <div class="col-md-4">
                                            <ul style="list-style: none;">
                                                <li>
                                                    <i class="fa fa-envelope"></i>
                                                    <span>{{ __('pages.email') }}:</span>
                                                    <a href="mailto:{{ $teacher->user->email }}">{{ $teacher->user->email }}</a>
                                                </li>
                                                <li>
                                                    <i class="fa fa-phone"></i>
                                                    <span>{{ __('pages.phone') }}:</span>
                                                    <a href="tel:{{ $teacher->phone }}">{{ $teacher->phone }}</a>
                                                </li>
                                                <li>
                                                    <i class="fa fa-mobile"></i>
                                                    <span>{{ __('pages.mobile') }}:</span>
                                                    <a href="tel:{{ $teacher->mobile }}">{{ $teacher->mobile }}</a>
                                                </li>
                                                <li>
                                                    <i class="fa fa-superscript"></i>
                                                    <span>{{ __('pages.phone') }}:</span>
                                                    {{ $teacher->teacherRank->name }}
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="edit" role="tabpanel" aria-labelledby="edit-tab">
                                {!! Form::model($teacher, ['method' => 'PATCH', 'route' => ['superadmin.teachers.update', $teacher->id], 'autocomplete' => 'off']) !!}
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                {!! Form::label('first_name', __('pages.first_name'), ['class' => 'control-label']) !!}
                                                {!! Form::text('first_name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('phone', __('pages.phone'), ['class' => 'control-label']) !!}
                                                {!! Form::text('phone', null, ['class' => 'input-sm form-control-sm form-control']) !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('password', __('pages.password'), ['class' => 'control-label']) !!}
                                                {!! Form::input('password', 'password', null, ['class' => 'input-sm form-control-sm form-control', 'autocomplete' => 'off']) !!}
                                            </div>
                                            <div class="form-group" style="margin-top: 48px;">
                                                <button type="button" class="btn btn-success btn-sm select-all-js mb-5px">{{ __('pages.select_all') }}</button>
                                                <button type="button" class="btn btn-danger btn-sm select-none-js mb-5px">{{ __('pages.select_none') }}</button>
                                                <button type="button" class="btn btn-primary btn-sm select-view-js mb-5px">{{ __('pages.select_view') }}</button>
                                                <button type="button" class="btn btn-success btn-sm select-create-js mb-5px">{{ __('pages.select_create') }}</button>
                                                <button type="button" class="btn btn-info btn-sm select-edit-js mb-5px">{{ __('pages.select_edit') }}</button>
                                                <button type="button" class="btn btn-danger btn-sm select-delete-js mb-5px">{{ __('pages.select_delete') }}</button>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                {!! Form::label('last_name', __('pages.last_name'), ['class' => 'control-label']) !!}
                                                {!! Form::text('last_name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('mobile', __('pages.mobile'), ['class' => 'control-label']) !!}
                                                {!! Form::text('mobile', null, ['class' => 'input-sm form-control-sm form-control']) !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('password_confirmation', __('pages.confirm_password'), ['class' => 'control-label']) !!}
                                                {!! Form::input('password', 'password_confirmation', null, ['class' => 'input-sm form-control-sm form-control',
                                                    'autocomplete' => 'off']) !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('permissions', __('pages.permissions'), ['class' => 'control-label']) !!}
                                                <select data-placeholder="{{ __('pages.chose_permissions') }}" multiple class="standardSelect" name="permissions[]" id="permissions">
                                                    @foreach($permissions as $permission)
                                                        <option value="{{ $permission->id }}" data-permission="{{ $permission->name }}" @if(in_array($permission->id, $teacherPermissions)) selected @endif>
                                                            {{ __('permissions.' . $permission->name) }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                {!! Form::label('email', __('pages.email'), ['class' => 'control-label']) !!}
                                                {!! Form::email('email', $teacher->user->email, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::label('teacher_rank_id', __('pages.teacher_rank'), ['class' => 'control-label']) !!}
                                                <select data-placeholder="{{ __('pages.chose_teacher_rank') }}" class="standardSelect" name="teacher_rank_id" id="teacher_rank_id">
                                                    @foreach($teacherRanks as $key => $rank)
                                                        <option value="{{ $key }}" @if($teacher->teacher_rank_id == $key) selected @endif>{{ $rank }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    {!! Form::submit(__('pages.edit'), ['class' => 'btn btn-primary btn-sm']) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('vendors/chosen/chosen.min.css') }}">
@endsection

@section('custom-scripts')
    <script src="{{ asset('vendors/chosen/chosen.jquery.min.js') }}"></script>
    <script>
        $(function() {
            $('.standardSelect').chosen({
                disable_search_threshold: 10,
                no_results_text: '{{ __('pages.nothing_found') }}',
                width: '100%'
            });
        });
    </script>
@endsection
