@extends('superadmin::layouts.app')

@section('title')
    {{ __('pages.teachers') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            {!! Form::open(['route' => 'superadmin.teachers.search', 'method' => 'GET']) !!}
                                <div class="row">
                                    <div class="col-md-3">
                                        {!! Form::text('name', $name, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.name')]) !!}
                                    </div>
                                    <div class="col-md-2">
                                        {!! Form::text('email', $email, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.email')]) !!}
                                    </div>
                                    <div class="col-md-2">
                                        {!! Form::text('phone', $phone, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.phone')]) !!}
                                    </div>
                                    <div class="col-md-3">
                                        <select data-placeholder="{{ __('pages.chose_teacher_ranks') }}" multiple class="standardSelect" name="teacherRanksIds[]" id="teacherRanksIds">
                                            @foreach($teacherRanks as $key => $teacherRank)
                                                <option value="{{ $key }}" @if(in_array($key, $teacherRanksIds)) selected @endif>{{ $teacherRank }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        {!! Form::submit(__('pages.search'), ['class' => 'btn btn-primary btn-sm']) !!}
                                        <a href="{{ route('superadmin.teachers.index') }}" class="btn btn-info btn-sm" title="{{ __('pages.reset_filter') }}">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-2 text-right">
                            <button type="button" class="btn btn-success mb-1 btn-sm" data-toggle="modal" data-target="#addTeacher">
                                {{ __('pages.add_teacher') }}
                            </button>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th scope="row">#</th>
                                <th>{{ __('pages.first_name') }}</th>
                                <th>{{ __('pages.last_name') }}</th>
                                <th>{{ __('pages.email') }}</th>
                                <th>{{ __('pages.phone') }}</th>
                                <th>{{ __('pages.mobile') }}</th>
                                <th>{{ __('pages.rank') }}</th>
                                <th>{{ __('pages.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($teachers) > 0)
                                @foreach($teachers as $teacher)
                                    @php
                                        if($teacher->user->photo == null || $teacher->user->photo == ''){
                                            $photo = asset('images/default_photo.jpg');
                                        } else {
                                            $photo = asset('storage/users-photo/' . $teacher->user->photo);
                                        }
                                    @endphp
                                    <tr @if($teacher->blocked) class="blocked" @endif>
                                        <td class="text-center"><img class="user-avatar rounded-circle" src="{{ $photo }}" alt="Photo" style="width: 32px;"></td>
                                        <td class="text-center"><span>{{ $teacher->first_name }}</span></td>
                                        <td class="text-center"><span>{{ $teacher->last_name }}</span></td>
                                        <td class="text-center"><span>{{ $teacher->user->email }}</span></td>
                                        <td class="text-center"><span>{{ $teacher->phone }}</span></td>
                                        <td class="text-center"><span>{{ $teacher->mobile }}</span></td>
                                        <td class="text-center"><span>{{ $teacher->teacherRank->name }}</span></td>
                                        <td class="text-center">
                                            <a href="{{ route('superadmin.teachers.show', $teacher->id) }}" class="btn-no-bg" title="{{ __('pages.show_edit') }}">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="javascript:void(0)" data-teacherid="{{ $teacher->id }}" class="btn-no-bg delete-teacher" title="{{ __('pages.delete') }}">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                            @if($teacher->blocked)
                                                <a href="javascript:void(0)" data-teacherid="{{ $teacher->id }}" class="btn-no-bg unblock-teacher" title="{{ __('pages.unblock') }}">
                                                    <i class="fa fa-unlock"></i>
                                                </a>
                                            @else
                                                <a href="javascript:void(0)" data-teacherid="{{ $teacher->id }}" class="btn-no-bg block-teacher" title="{{ __('pages.block') }}">
                                                    <i class="fa fa-lock"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="8">{{ __('messages.not_found_any_teachers') }}</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="table-footer">
                        {!! $teachers->render() !!}
                        <div class="total-found">{{ __('pages.total_found') }}: {{ $teachers->total() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addTeacher" role="dialog" aria-labelledby="addTeacher" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                {!! Form::open(['url' => route('superadmin.teachers.store'), 'method' => 'post', 'autocomplete' => 'off']) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.add_teacher') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('first_name', __('pages.first_name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('first_name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('phone', __('pages.phone'), ['class' => 'control-label']) !!}
                                    {!! Form::text('phone', '', ['class' => 'input-sm form-control-sm form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('email', __('pages.email'), ['class' => 'control-label']) !!}
                                    {!! Form::email('email', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('password', __('pages.password'), ['class' => 'control-label']) !!}
                                    {!! Form::input('password', 'password', '', ['class' => 'input-sm form-control-sm form-control',
                                        'autocomplete' => 'off', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('last_name', __('pages.last_name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('last_name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('mobile', __('pages.mobile'), ['class' => 'control-label']) !!}
                                    {!! Form::text('mobile', '', ['class' => 'input-sm form-control-sm form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('teacher_rank_id', __('pages.teacher_rank'), ['class' => 'control-label']) !!}
                                    <select data-placeholder="{{ __('pages.chose_teacher_rank') }}" class="standardSelect" name="teacher_rank_id" id="teacher_rank_id">
                                        @foreach($teacherRanks as $key => $rank)
                                            <option value="{{ $key }}">{{ $rank }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('password_confirmation', __('pages.confirm_password'), ['class' => 'control-label']) !!}
                                    {!! Form::input('password', 'password_confirmation', null, ['class' => 'input-sm form-control-sm form-control',
                                        'autocomplete' => 'off', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('permissions', __('pages.permissions'), ['class' => 'control-label']) !!}
                                    <select data-placeholder="{{ __('pages.chose_permissions') }}" multiple class="standardSelect" name="permissions[]" id="permissions">
                                        @foreach($permissions as $permission)
                                            <option value="{{ $permission->id }}" data-permission="{{ $permission->name }}">{{ __('permissions.' . $permission->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-success btn-sm select-all-js mb-5px">{{ __('pages.select_all') }}</button>
                                    <button type="button" class="btn btn-danger btn-sm select-none-js mb-5px">{{ __('pages.select_none') }}</button>
                                    <button type="button" class="btn btn-primary btn-sm select-view-js mb-5px">{{ __('pages.select_view') }}</button>
                                    <button type="button" class="btn btn-success btn-sm select-create-js mb-5px">{{ __('pages.select_create') }}</button>
                                    <button type="button" class="btn btn-info btn-sm select-edit-js mb-5px">{{ __('pages.select_edit') }}</button>
                                    <button type="button" class="btn btn-danger btn-sm select-delete-js mb-5px">{{ __('pages.select_delete') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        {!! Form::submit(__('pages.add'), ['class' => 'btn btn-success btn-sm']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteTeacher" role="dialog" aria-labelledby="deleteTeacher" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'superadmin.teachers.delete']) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.delete_teacher') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info" role="alert">
                            <div class="alert-text">
                                {{ __('pages.sure_to_delete_teacher?') }}
                            </div>
                        </div>
                        <input type="hidden" name="teacher_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        <button type="submit" class="btn btn-danger btn-sm">{{ __('pages.delete') }}</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="unblockTeacher" role="dialog" aria-labelledby="unblockTeacher" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'superadmin.teachers.unblock']) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.unblock_teacher') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info" role="alert">
                            <div class="alert-text">
                                {{ __('pages.sure_to_unblock_teacher?') }}
                            </div>
                        </div>
                        <input type="hidden" name="teacher_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        <button type="submit" class="btn btn-danger btn-sm">{{ __('pages.unblock') }}</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="blockTeacher" role="dialog" aria-labelledby="blockTeacher" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'superadmin.teachers.block']) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.block_teacher') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info" role="alert">
                            <div class="alert-text">
                                {{ __('pages.sure_to_block_teacher?') }}
                            </div>
                        </div>
                        <input type="hidden" name="teacher_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        <button type="submit" class="btn btn-warning btn-sm">{{ __('pages.block') }}</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('vendors/chosen/chosen.min.css') }}">
@endsection

@section('custom-scripts')
    <script src="{{ asset('vendors/chosen/chosen.jquery.min.js') }}"></script>
    <script>
        $(function() {
            $('.standardSelect').chosen({
                disable_search_threshold: 10,
                no_results_text: '{{ __('pages.nothing_found') }}',
                width: '100%'
            });
        });
    </script>
    <script>
        let body = $('body');

        body.on('click', '.delete-teacher', function () {
            let teacherId = $(this).attr('data-teacherid');
            let modal = $('#deleteTeacher');
            modal.find('input[name="teacher_id"]').val(teacherId);
            modal.modal('show');
        });

        body.on('click', '.unblock-teacher', function () {
            let teacherId = $(this).attr('data-teacherid');
            let modal = $('#unblockTeacher');
            modal.find('input[name="teacher_id"]').val(teacherId);
            modal.modal('show');
        });

        body.on('click', '.block-teacher', function () {
            let teacherId = $(this).attr('data-teacherid');
            let modal = $('#blockTeacher');
            modal.find('input[name="teacher_id"]').val(teacherId);
            modal.modal('show');
        });

        $('#deleteTeacher').on('hidden.bs.modal', function () {
            $(this).find('input[name="teacher_id"]').val('');
        });
        $('#unblockTeacher').on('hidden.bs.modal', function () {
            $(this).find('input[name="teacher_id"]').val('');
        });
        $('#blockTeacher').on('hidden.bs.modal', function () {
            $(this).find('input[name="teacher_id"]').val('');
        });
    </script>
@endsection