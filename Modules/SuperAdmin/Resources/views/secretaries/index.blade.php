@extends('superadmin::layouts.app')

@section('title')
    {{ __('pages.secretaries') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            {!! Form::open(['route' => 'superadmin.secretaries.search', 'method' => 'GET']) !!}
                                <div class="row">
                                    <div class="col-md-3">
                                        {!! Form::text('name', $name, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.name')]) !!}
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::text('email', $email, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.email')]) !!}
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::text('phone', $phone, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.phone')]) !!}
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::submit(__('pages.search'), ['class' => 'btn btn-primary btn-sm']) !!}
                                        <a href="{{ route('superadmin.secretaries.index') }}" class="btn btn-info btn-sm" title="{{ __('pages.reset_filter') }}">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-2 text-right">
                            <button type="button" class="btn btn-success mb-1 btn-sm" data-toggle="modal" data-target="#addSecretary">
                                {{ __('pages.add_secretary') }}
                            </button>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th scope="row">#</th>
                                <th>{{ __('pages.first_name') }}</th>
                                <th>{{ __('pages.last_name') }}</th>
                                <th>{{ __('pages.email') }}</th>
                                <th>{{ __('pages.phone') }}</th>
                                <th>{{ __('pages.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($secretaries) > 0)
                                @foreach($secretaries as $secretary)
                                    @php
                                        if($secretary->user->photo == null || $secretary->user->photo == ''){
                                            $photo = asset('images/default_photo.jpg');
                                        } else {
                                            $photo = asset('storage/users-photo/' . $secretary->user->photo);
                                        }
                                    @endphp
                                    <tr @if($secretary->blocked) class="blocked" @endif>
                                        <td class="text-center"><img class="user-avatar rounded-circle" src="{{ $photo }}" alt="Photo" style="width: 32px;"></td>
                                        <td class="text-center"><span>{{ $secretary->first_name }}</span></td>
                                        <td class="text-center"><span>{{ $secretary->last_name }}</span></td>
                                        <td class="text-center"><span>{{ $secretary->user->email }}</span></td>
                                        <td class="text-center"><span>{{ $secretary->phone }}</span></td>
                                        <td class="text-center">
                                            <a href="{{ route('superadmin.secretaries.show', $secretary->id) }}" class="btn-no-bg" title="{{ __('pages.show_edit') }}">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="javascript:void(0)" data-secretaryid="{{ $secretary->id }}" class="btn-no-bg delete-secretary" title="{{ __('pages.delete') }}">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                            @if($secretary->blocked)
                                                <a href="javascript:void(0)" data-secretaryid="{{ $secretary->id }}" class="btn-no-bg unblock-secretary" title="{{ __('pages.unblock') }}">
                                                    <i class="fa fa-unlock"></i>
                                                </a>
                                            @else
                                                <a href="javascript:void(0)" data-secretaryid="{{ $secretary->id }}" class="btn-no-bg block-secretary" title="{{ __('pages.block') }}">
                                                    <i class="fa fa-lock"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="6">{{ __('messages.not_found_any_secretaries') }}</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="table-footer">
                        {!! $secretaries->render() !!}
                        <div class="total-found">{{ __('pages.total_found') }}: {{ $secretaries->total() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addSecretary" role="dialog" aria-labelledby="addSecretary" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                {!! Form::open(['url' => route('superadmin.secretaries.store'), 'method' => 'post', 'autocomplete' => 'off']) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.add_secretary') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('first_name', __('pages.first_name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('first_name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('email', __('pages.email'), ['class' => 'control-label']) !!}
                                    {!! Form::email('email', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('password', __('pages.password'), ['class' => 'control-label']) !!}
                                    {!! Form::input('password', 'password', '', ['class' => 'input-sm form-control-sm form-control',
                                        'autocomplete' => 'off', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('last_name', __('pages.last_name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('last_name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('phone', __('pages.phone'), ['class' => 'control-label']) !!}
                                    {!! Form::text('phone', '', ['class' => 'input-sm form-control-sm form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('password_confirmation', __('pages.confirm_password'), ['class' => 'control-label']) !!}
                                    {!! Form::input('password', 'password_confirmation', null, ['class' => 'input-sm form-control-sm form-control',
                                        'autocomplete' => 'off', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('permissions', __('pages.permissions'), ['class' => 'control-label']) !!}
                                    <select data-placeholder="{{ __('pages.chose_permissions') }}" multiple class="standardSelect" name="permissions[]" id="permissions">
                                        @foreach($permissions as $permission)
                                            <option value="{{ $permission->id }}" data-permission="{{ $permission->name }}">{{ __('permissions.' . $permission->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-success btn-sm select-all-js mb-5px">{{ __('pages.select_all') }}</button>
                                    <button type="button" class="btn btn-danger btn-sm select-none-js mb-5px">{{ __('pages.select_none') }}</button>
                                    <button type="button" class="btn btn-primary btn-sm select-view-js mb-5px">{{ __('pages.select_view') }}</button>
                                    <button type="button" class="btn btn-success btn-sm select-create-js mb-5px">{{ __('pages.select_create') }}</button>
                                    <button type="button" class="btn btn-info btn-sm select-edit-js mb-5px">{{ __('pages.select_edit') }}</button>
                                    <button type="button" class="btn btn-danger btn-sm select-delete-js mb-5px">{{ __('pages.select_delete') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        {!! Form::submit(__('pages.add'), ['class' => 'btn btn-success btn-sm']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteSecretary" role="dialog" aria-labelledby="deleteSecretary" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'superadmin.secretaries.delete']) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.delete_secretary') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info" role="alert">
                            <div class="alert-text">
                                {{ __('pages.sure_to_delete_secretary?') }}
                            </div>
                        </div>
                        <input type="hidden" name="secretary_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        <button type="submit" class="btn btn-danger btn-sm">{{ __('pages.delete') }}</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="unblockSecretary" role="dialog" aria-labelledby="unblockSecretary" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'superadmin.secretaries.unblock']) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.unblock_secretary') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info" role="alert">
                            <div class="alert-text">
                                {{ __('pages.sure_to_unblock_secretary?') }}
                            </div>
                        </div>
                        <input type="hidden" name="secretary_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        <button type="submit" class="btn btn-danger btn-sm">{{ __('pages.unblock') }}</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="blockSecretary" role="dialog" aria-labelledby="blockSecretary" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'superadmin.secretaries.block']) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.block_secretary') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info" role="alert">
                            <div class="alert-text">
                                {{ __('pages.sure_to_block_secretary?') }}
                            </div>
                        </div>
                        <input type="hidden" name="secretary_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        <button type="submit" class="btn btn-warning btn-sm">{{ __('pages.block') }}</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('vendors/chosen/chosen.min.css') }}">
@endsection

@section('custom-scripts')
    <script src="{{ asset('vendors/chosen/chosen.jquery.min.js') }}"></script>
    <script>
        $(function() {
            $('.standardSelect').chosen({
                disable_search_threshold: 10,
                no_results_text: '{{ __('pages.nothing_found') }}',
                width: '100%'
            });
        });
    </script>
    <script>
        let body = $('body');

        body.on('click', '.delete-secretary', function () {
            let secretaryId = $(this).attr('data-secretaryid');
            let modal = $('#deleteSecretary');
            modal.find('input[name="secretary_id"]').val(secretaryId);
            modal.modal('show');
        });

        body.on('click', '.unblock-secretary', function () {
            let secretaryId = $(this).attr('data-secretaryid');
            let modal = $('#unblockSecretary');
            modal.find('input[name="secretary_id"]').val(secretaryId);
            modal.modal('show');
        });

        body.on('click', '.block-secretary', function () {
            let secretaryId = $(this).attr('data-secretaryid');
            let modal = $('#blockSecretary');
            modal.find('input[name="secretary_id"]').val(secretaryId);
            modal.modal('show');
        });

        $('#deleteSecretary').on('hidden.bs.modal', function () {
            $(this).find('input[name="secretary_id"]').val('');
        });
        $('#unblockSecretary').on('hidden.bs.modal', function () {
            $(this).find('input[name="secretary_id"]').val('');
        });
        $('#blockSecretary').on('hidden.bs.modal', function () {
            $(this).find('input[name="secretary_id"]').val('');
        });
    </script>
@endsection