@extends('superadmin::layouts.app')

@section('title')
    {{ __('pages.logs') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::open(['route' => 'superadmin.logs.search', 'method' => 'GET']) !!}
                                <div class="row">
                                    <div class="col-md-2">
                                        <select data-placeholder="{{ __('pages.chose_roles') }}" multiple class="standardSelect" name="roleIds[]" id="roleIds">
                                            @foreach($roles as $role)
                                                <option value="{{ $role }}" @if(in_array($role, $roleIds)) selected @endif>{{ $role }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <select data-placeholder="{{ __('pages.chose_types') }}" multiple class="standardSelect" name="typeIds[]" id="typeIds">
                                            @foreach($types as $key => $type)
                                                <option value="{{ $key }}" @if(in_array($key, $typeIds)) selected @endif>{{ $type }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <select data-placeholder="{{ __('pages.chose_tables') }}" multiple class="standardSelect" name="tableNames[]" id="tableNames">
                                            @foreach($tables as $table)
                                                <option value="{{ $table }}" @if(in_array($table, $tableNames)) selected @endif>{{ $table }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="datetime-local" name="from" class="input-sm form-control-sm form-control" placeholder="{{ __('pages.from') }}" value="{{ $from }}">
                                    </div>
                                    <div class="col-md-3">
                                        <input type="datetime-local" name="to" class="input-sm form-control-sm form-control" placeholder="{{ __('pages.to') }}" value="{{ $to }}">
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-12 text-right">
                                        {!! Form::submit(__('pages.search'), ['class' => 'btn btn-primary btn-sm']) !!}
                                        <a href="{{ route('superadmin.logs.index') }}" class="btn btn-info btn-sm" title="{{ __('pages.reset_filter') }}">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                        <tr class="text-center">
                            <th>{{ __('pages.role') }}</th>
                            <th>{{ __('pages.full_name') }}</th>
                            <th>{{ __('pages.table') }}</th>
                            <th>{{ __('pages.type') }}</th>
                            <th>{{ __('pages.action') }}</th>
                            <th>{{ __('pages.date') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($logs) > 0)
                            @foreach($logs as $log)
                                <tr>
                                    <td class="text-center"><span>{{ $log->entity_role }}</span></td>
                                    <td class="text-center"><span>{{ $log->user }}</span></td>
                                    <td class="text-center"><span>{{ $log->action_table }}</span></td>
                                    <td class="text-center"><span>{{ $log->log }}</span></td>
                                    <td class="text-center"><span>{{ $log->action }}</span></td>
                                    <td class="text-center"><span>{{ $log->date }}</span></td>

                                </tr>
                            @endforeach
                        @else
                            <tr class="text-center">
                                <td colspan="6">{{ __('messages.not_found_any_logs') }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    <div class="table-footer">
                        {!! $logs->render() !!}
                        <div class="total-found">{{ __('pages.total_found') }}: {{ $logs->total() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('vendors/chosen/chosen.min.css') }}">
@endsection

@section('custom-scripts')
    <script src="{{ asset('vendors/chosen/chosen.jquery.min.js') }}"></script>
    <script>
        $(function() {
            $('.standardSelect').chosen({
                disable_search_threshold: 10,
                no_results_text: '{{ __('pages.nothing_found') }}',
                width: '100%'
            });
        });
    </script>
@endsection