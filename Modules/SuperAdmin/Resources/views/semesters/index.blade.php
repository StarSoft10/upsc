@extends('superadmin::layouts.app')

@section('title')
    {{ __('pages.semesters') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="button" class="btn btn-success mb-1 btn-sm" data-toggle="modal" data-target="#addSemester">
                                {{ __('pages.add_semester') }}
                            </button>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th>{{ __('pages.name') }}</th>
                                <th>{{ __('pages.order') }}</th>
                                <th>{{ __('pages.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($semesters) > 0)
                                @foreach($semesters as $semester)
                                    <tr>
                                        <td class="text-center"><span>{{ __($semester->name) }}</span></td>
                                        <td class="text-center"><span>{{ $semester->order }}</span></td>
                                        <td class="text-center">
                                            <a href="{{ route('superadmin.semesters.show', $semester->id) }}" class="btn-no-bg" title="{{ __('pages.show_edit') }}">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="javascript:void(0)" data-semesterid="{{ $semester->id }}" class="btn-no-bg delete-semester" title="{{ __('pages.delete') }}">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="3">{{ __('messages.not_found_any_semesters') }}</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    {!! $semesters->render() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addSemester" role="dialog" aria-labelledby="addSemester" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                {!! Form::open(['url' => route('superadmin.semesters.store'), 'method' => 'post', 'autocomplete' => 'off']) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.add_semester') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('name', __('pages.name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('order', __('pages.order'), ['class' => 'control-label']) !!}
                                    {!! Form::number('order', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        {!! Form::submit(__('pages.add'), ['class' => 'btn btn-success btn-sm']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteSemester" role="dialog" aria-labelledby="deleteSemester" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'superadmin.semesters.delete']) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.delete_semester') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info" role="alert">
                            <div class="alert-text">
                                {{ __('pages.sure_to_delete_semester?') }}
                            </div>
                        </div>
                        <input type="hidden" name="semester_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        <button type="submit" class="btn btn-danger btn-sm">{{ __('pages.delete') }}</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
@endsection

@section('custom-scripts')
    <script>
        let body = $('body');

        body.on('click', '.delete-semester', function () {
            let semesterId = $(this).attr('data-semesterid');
            let modal = $('#deleteSemester');
            modal.find('input[name="semester_id"]').val(semesterId);
            modal.modal('show');
        });

        $('#deleteSemester').on('hidden.bs.modal', function () {
            $(this).find('input[name="semester_id"]').val('');
        });
    </script>
@endsection