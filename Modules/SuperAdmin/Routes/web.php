<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

Route::group([
    'prefix' => LaravelLocalization::setLocale() . '/superadmin',
    'middleware' => ['auth', 'role:superadmin', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'localize']
], function () {
    Route::get('/', 'SuperAdminController@index')->name('superadmin');
    Route::get('/profile', 'SuperAdminController@show')->name('superadmin.profile');
    Route::patch('update', 'SuperAdminController@update')->name('superadmin.update');
    Route::patch('changePhoto', 'SuperAdminController@changePhoto')->name('superadmin.changePhoto');
    Route::post('getRegisterNotifications', 'SuperAdminController@getRegisterNotifications')->name('superadmin.getRegisterNotifications');


    /*
    * Log routes
    */
    Route::group(['prefix' => 'logs'], function () {
        Route::get('search', 'LogController@search')->name('superadmin.logs.search');
    });
    Route::resource('logs', 'LogController', ['names' => [
        'index' => 'superadmin.logs.index'
    ]]);


    /*
   * Databases routes
   */
    Route::group(['prefix' => 'databases'], function () {
        Route::post('delete', 'DatabaseController@delete')->name('superadmin.databases.delete');
    });
    Route::resource('databases', 'DatabaseController', ['names' => [
        'index' => 'superadmin.databases.index',
        'store' => 'superadmin.databases.store',
        'show' => 'superadmin.databases.show',
        'update' => 'superadmin.databases.update'
    ]]);


    /*
     * User routes
     */
    Route::group(['prefix' => 'unconfirmed-users'], function () {
        Route::post('delete', 'UnconfirmedUserController@delete')->name('superadmin.unconfirmed-users.delete');
        Route::get('search', 'UnconfirmedUserController@search')->name('superadmin.unconfirmed-users.search');
        Route::post('confirm', 'UnconfirmedUserController@confirm')->name('superadmin.unconfirmed-users.confirm');
    });
    Route::resource('unconfirmed-users', 'UnconfirmedUserController', ['names' => [
        'index' => 'superadmin.unconfirmed-users.index',
        'show' => 'superadmin.unconfirmed-users.show',
        'update' => 'superadmin.unconfirmed-users.update'
    ]]);


    Route::group(['prefix' => 'admins'], function () {
        Route::post('delete', 'AdminController@delete')->name('superadmin.admins.delete');
        Route::get('search', 'AdminController@search')->name('superadmin.admins.search');
        Route::post('block', 'AdminController@block')->name('superadmin.admins.block');
        Route::post('unblock', 'AdminController@unblock')->name('superadmin.admins.unblock');
    });
    Route::resource('admins', 'AdminController', ['names' => [
        'index' => 'superadmin.admins.index',
        'store' => 'superadmin.admins.store',
        'show' => 'superadmin.admins.show',
        'update' => 'superadmin.admins.update'
    ]]);


    Route::group(['prefix' => 'rectors'], function () {
        Route::post('delete', 'RectorController@delete')->name('superadmin.rectors.delete');
        Route::get('search', 'RectorController@search')->name('superadmin.rectors.search');
        Route::post('block', 'RectorController@block')->name('superadmin.rectors.block');
        Route::post('unblock', 'RectorController@unblock')->name('superadmin.rectors.unblock');
    });
    Route::resource('rectors', 'RectorController', ['names' => [
        'index' => 'superadmin.rectors.index',
        'store' => 'superadmin.rectors.store',
        'show' => 'superadmin.rectors.show',
        'update' => 'superadmin.rectors.update'
    ]]);


    Route::group(['prefix' => 'secretaries'], function () {
        Route::post('delete', 'SecretaryController@delete')->name('superadmin.secretaries.delete');
        Route::get('search', 'SecretaryController@search')->name('superadmin.secretaries.search');
        Route::post('block', 'SecretaryController@block')->name('superadmin.secretaries.block');
        Route::post('unblock', 'SecretaryController@unblock')->name('superadmin.secretaries.unblock');
    });
    Route::resource('secretaries', 'SecretaryController', ['names' => [
        'index' => 'superadmin.secretaries.index',
        'store' => 'superadmin.secretaries.store',
        'show' => 'superadmin.secretaries.show',
        'update' => 'superadmin.secretaries.update'
    ]]);


    Route::group(['prefix' => 'deans'], function () {
        Route::post('delete', 'DeanController@delete')->name('superadmin.deans.delete');
        Route::get('search', 'DeanController@search')->name('superadmin.deans.search');
        Route::post('block', 'DeanController@block')->name('superadmin.deans.block');
        Route::post('unblock', 'DeanController@unblock')->name('superadmin.deans.unblock');
    });
    Route::resource('deans', 'DeanController', ['names' => [
        'index' => 'superadmin.deans.index',
        'store' => 'superadmin.deans.store',
        'show' => 'superadmin.deans.show',
        'update' => 'superadmin.deans.update'
    ]]);


    Route::group(['prefix' => 'chairs'], function () {
        Route::post('delete', 'ChairController@delete')->name('superadmin.chairs.delete');
        Route::get('search', 'ChairController@search')->name('superadmin.chairs.search');
        Route::post('block', 'ChairController@block')->name('superadmin.chairs.block');
        Route::post('unblock', 'ChairController@unblock')->name('superadmin.chairs.unblock');
    });
    Route::resource('chairs', 'ChairController', ['names' => [
        'index' => 'superadmin.chairs.index',
        'store' => 'superadmin.chairs.store',
        'show' => 'superadmin.chairs.show',
        'update' => 'superadmin.chairs.update'
    ]]);


    Route::group(['prefix' => 'human-resources'], function () {
        Route::post('delete', 'HumanResourceController@delete')->name('superadmin.human-resources.delete');
        Route::get('search', 'HumanResourceController@search')->name('superadmin.human-resources.search');
        Route::post('block', 'HumanResourceController@block')->name('superadmin.human-resources.block');
        Route::post('unblock', 'HumanResourceController@unblock')->name('superadmin.human-resources.unblock');
    });
    Route::resource('human-resources', 'HumanResourceController', ['names' => [
        'index' => 'superadmin.human-resources.index',
        'store' => 'superadmin.human-resources.store',
        'show' => 'superadmin.human-resources.show',
        'update' => 'superadmin.human-resources.update'
    ]]);


    Route::group(['prefix' => 'teachers'], function () {
        Route::post('delete', 'TeacherController@delete')->name('superadmin.teachers.delete');
        Route::get('search', 'TeacherController@search')->name('superadmin.teachers.search');
        Route::post('block', 'TeacherController@block')->name('superadmin.teachers.block');
        Route::post('unblock', 'TeacherController@unblock')->name('superadmin.teachers.unblock');
    });
    Route::resource('teachers', 'TeacherController', ['names' => [
        'index' => 'superadmin.teachers.index',
        'store' => 'superadmin.teachers.store',
        'show' => 'superadmin.teachers.show',
        'update' => 'superadmin.teachers.update'
    ]]);


    Route::group(['prefix' => 'students'], function () {
        Route::post('delete', 'StudentController@delete')->name('superadmin.students.delete');
        Route::get('search', 'StudentController@search')->name('superadmin.students.search');
        Route::post('block', 'StudentController@block')->name('superadmin.students.block');
        Route::post('unblock', 'StudentController@unblock')->name('superadmin.students.unblock');
        Route::post('exportStudentsPdf', 'StudentController@exportStudentsPdf')->name('superadmin.students.exportStudentsPdf');
        Route::post('exportStudentsExcel', 'StudentController@exportStudentsExcel')->name('superadmin.students.exportStudentsExcel');
        Route::post('exportStudentPdf', 'StudentController@exportStudentPdf')->name('superadmin.students.exportStudentPdf');
        Route::post('exportStudentExcel', 'StudentController@exportStudentExcel')->name('superadmin.students.exportStudentExcel');
        Route::post('deleteAdditionalStudy', 'StudentController@deleteAdditionalStudy')->name('superadmin.students.deleteAdditionalStudy');
    });
    Route::resource('students', 'StudentController', ['names' => [
        'index' => 'superadmin.students.index',
        'store' => 'superadmin.students.store',
        'show' => 'superadmin.students.show',
        'update' => 'superadmin.students.update'
    ]]);


    /*
     * Studies routes
     */
    Route::group(['prefix' => 'year-of-studies'], function () {
        Route::post('delete', 'YearOfStudyController@delete')->name('superadmin.year-of-studies.delete');
        Route::get('search', 'YearOfStudyController@search')->name('superadmin.year-of-studies.search');
    });
    Route::resource('year-of-studies', 'YearOfStudyController', ['names' => [
        'index' => 'superadmin.year-of-studies.index',
        'store' => 'superadmin.year-of-studies.store',
        'show' => 'superadmin.year-of-studies.show',
        'update' => 'superadmin.year-of-studies.update'
    ]]);


    Route::group(['prefix' => 'cycles'], function () {
        Route::post('delete', 'CycleController@delete')->name('superadmin.cycles.delete');
        Route::get('search', 'CycleController@search')->name('superadmin.cycles.search');
    });
    Route::resource('cycles', 'CycleController', ['names' => [
        'index' => 'superadmin.cycles.index',
        'store' => 'superadmin.cycles.store',
        'show' => 'superadmin.cycles.show',
        'update' => 'superadmin.cycles.update'
    ]]);


    Route::group(['prefix' => 'faculties'], function () {
        Route::post('delete', 'FacultyController@delete')->name('superadmin.faculties.delete');
        Route::get('search', 'FacultyController@search')->name('superadmin.faculties.search');
        Route::post('getFacultiesByCycleForSelectSearch', 'FacultyController@getFacultiesByCycleForSelectSearch')->name('superadmin.faculties.getFacultiesByCycleForSelectSearch');
        Route::post('getFacultiesByCycleForSelectAddEdit', 'FacultyController@getFacultiesByCycleForSelectAddEdit')->name('superadmin.faculties.getFacultiesByCycleForSelectAddEdit');
    });
    Route::resource('faculties', 'FacultyController', ['names' => [
        'index' => 'superadmin.faculties.index',
        'store' => 'superadmin.faculties.store',
        'show' => 'superadmin.faculties.show',
        'update' => 'superadmin.faculties.update'
    ]]);


    Route::group(['prefix' => 'specialities'], function () {
        Route::post('delete', 'SpecialityController@delete')->name('superadmin.specialities.delete');
        Route::get('search', 'SpecialityController@search')->name('superadmin.specialities.search');
        Route::post('getSpecialitiesByFacultyForSelectSearch', 'SpecialityController@getSpecialitiesByFacultyForSelectSearch')->name('superadmin.specialities.getSpecialitiesByFacultyForSelectSearch');
        Route::post('getSpecialitiesByFacultyForSelectAddEdit', 'SpecialityController@getSpecialitiesByFacultyForSelectAddEdit')->name('superadmin.specialities.getSpecialitiesByFacultyForSelectAddEdit');
    });
    Route::resource('specialities', 'SpecialityController', ['names' => [
        'index' => 'superadmin.specialities.index',
        'store' => 'superadmin.specialities.store',
        'show' => 'superadmin.specialities.show',
        'update' => 'superadmin.specialities.update'
    ]]);


    Route::group(['prefix' => 'groups'], function () {
        Route::post('delete', 'GroupController@delete')->name('superadmin.groups.delete');
        Route::get('search', 'GroupController@search')->name('superadmin.groups.search');
        Route::post('getGroupsBySpecialityForSelectSearch', 'GroupController@getGroupsBySpecialityForSelectSearch')->name('superadmin.groups.getGroupsBySpecialityForSelectSearch');
        Route::post('getGroupsBySpecialityForSelectAddEdit', 'GroupController@getGroupsBySpecialityForSelectAddEdit')->name('superadmin.groups.getGroupsBySpecialityForSelectAddEdit');
    });
    Route::resource('groups', 'GroupController', ['names' => [
        'index' => 'superadmin.groups.index',
        'store' => 'superadmin.groups.store',
        'show' => 'superadmin.groups.show',
        'update' => 'superadmin.groups.update'
    ]]);


    Route::group(['prefix' => 'courses'], function () {
        Route::post('delete', 'CourseController@delete')->name('superadmin.courses.delete');
        Route::get('search', 'CourseController@search')->name('superadmin.courses.search');
    });
    Route::resource('courses', 'CourseController', ['names' => [
        'index' => 'superadmin.courses.index',
        'store' => 'superadmin.courses.store',
        'show' => 'superadmin.courses.show',
        'update' => 'superadmin.courses.update'
    ]]);


    Route::group(['prefix' => 'course-types'], function () {
        Route::post('delete', 'CourseTypeController@delete')->name('superadmin.course-types.delete');
        Route::get('search', 'CourseTypeController@search')->name('superadmin.course-types.search');
    });
    Route::resource('course-types', 'CourseTypeController', ['names' => [
        'index' => 'superadmin.course-types.index',
        'store' => 'superadmin.course-types.store',
        'show' => 'superadmin.course-types.show',
        'update' => 'superadmin.course-types.update'
    ]]);


    Route::group(['prefix' => 'days'], function () {
        Route::post('delete', 'DayController@delete')->name('superadmin.days.delete');
    });
    Route::resource('days', 'DayController', ['names' => [
        'index' => 'superadmin.days.index',
        'store' => 'superadmin.days.store',
        'show' => 'superadmin.days.show',
        'update' => 'superadmin.days.update'
    ]]);


    Route::group(['prefix' => 'duration-types'], function () {
        Route::post('delete', 'DurationTypeController@delete')->name('superadmin.duration-types.delete');
        Route::get('search', 'DurationTypeController@search')->name('superadmin.duration-types.search');
    });
    Route::resource('duration-types', 'DurationTypeController', ['names' => [
        'index' => 'superadmin.duration-types.index',
        'store' => 'superadmin.duration-types.store',
        'show' => 'superadmin.duration-types.show',
        'update' => 'superadmin.duration-types.update'
    ]]);


    Route::group(['prefix' => 'durations'], function () {
        Route::post('delete', 'DurationController@delete')->name('superadmin.durations.delete');
        Route::get('search', 'DurationController@search')->name('superadmin.durations.search');
    });
    Route::resource('durations', 'DurationController', ['names' => [
        'index' => 'superadmin.durations.index',
        'store' => 'superadmin.durations.store',
        'show' => 'superadmin.durations.show',
        'update' => 'superadmin.durations.update'
    ]]);


    Route::group(['prefix' => 'semesters'], function () {
        Route::post('delete', 'SemesterController@delete')->name('superadmin.semesters.delete');
    });
    Route::resource('semesters', 'SemesterController', ['names' => [
        'index' => 'superadmin.semesters.index',
        'store' => 'superadmin.semesters.store',
        'show' => 'superadmin.semesters.show',
        'update' => 'superadmin.semesters.update'
    ]]);


    Route::group(['prefix' => 'week-types'], function () {
        Route::post('delete', 'WeekTypeController@delete')->name('superadmin.week-types.delete');
        Route::get('search', 'WeekTypeController@search')->name('superadmin.week-types.search');
    });
    Route::resource('week-types', 'WeekTypeController', ['names' => [
        'index' => 'superadmin.week-types.index',
        'store' => 'superadmin.week-types.store',
        'show' => 'superadmin.week-types.show',
        'update' => 'superadmin.week-types.update'
    ]]);


    Route::group(['prefix' => 'weeks'], function () {
        Route::post('delete', 'WeekController@delete')->name('superadmin.weeks.delete');
        Route::get('search', 'WeekController@search')->name('superadmin.weeks.search');
    });
    Route::resource('weeks', 'WeekController', ['names' => [
        'index' => 'superadmin.weeks.index',
        'store' => 'superadmin.weeks.store',
        'show' => 'superadmin.weeks.show',
        'update' => 'superadmin.weeks.update'
    ]]);


    Route::group(['prefix' => 'teacher-ranks'], function () {
        Route::post('delete', 'TeacherRankController@delete')->name('superadmin.teacher-ranks.delete');
        Route::get('search', 'TeacherRankController@search')->name('superadmin.teacher-ranks.search');
    });
    Route::resource('teacher-ranks', 'TeacherRankController', ['names' => [
        'index' => 'superadmin.teacher-ranks.index',
        'store' => 'superadmin.teacher-ranks.store',
        'show' => 'superadmin.teacher-ranks.show',
        'update' => 'superadmin.teacher-ranks.update'
    ]]);


    /*
    * Borderous routes
    */
    Route::group(['prefix' => 'borderou-types'], function () {
        Route::post('delete', 'BorderouTypeController@delete')->name('superadmin.borderou-types.delete');
        Route::get('search', 'BorderouTypeController@search')->name('superadmin.borderou-types.search');
    });
    Route::resource('borderou-types', 'BorderouTypeController', ['names' => [
        'index' => 'superadmin.borderou-types.index',
        'store' => 'superadmin.borderou-types.store',
        'show' => 'superadmin.borderou-types.show',
        'update' => 'superadmin.borderou-types.update'
    ]]);


    Route::group(['prefix' => 'borderous'], function () {
        Route::post('delete', 'BorderouController@delete')->name('superadmin.borderous.delete');
        Route::get('search', 'BorderouController@search')->name('superadmin.borderous.search');
        Route::post('getBorderouForView', 'BorderouController@getBorderouForView')->name('superadmin.borderous.getBorderouForView');
        Route::post('getBorderouNote', 'BorderouController@getBorderouNote')->name('superadmin.borderous.getBorderouNote');
        Route::post('updateBorderouNote', 'BorderouController@updateBorderouNote')->name('superadmin.borderous.updateBorderouNote');
        Route::post('exportBorderouNotePdf', 'BorderouController@exportBorderouNotePdf')->name('superadmin.borderous.exportBorderouNotePdf');
        Route::post('exportBorderouNoteExcel', 'BorderouController@exportBorderouNoteExcel')->name('superadmin.borderous.exportBorderouNoteExcel');
        Route::post('deleteStudentBorderouNote', 'BorderouController@deleteStudentBorderouNote')->name('superadmin.borderous.deleteStudentBorderouNote');
        Route::post('addStudentBorderouNote', 'BorderouController@addStudentBorderouNote')->name('superadmin.borderous.addStudentBorderouNote');
    });
    Route::resource('borderous', 'BorderouController', ['names' => [
        'index' => 'superadmin.borderous.index',
        'store' => 'superadmin.borderous.store',
        'show' => 'superadmin.borderous.show',
        'update' => 'superadmin.borderous.update'
    ]]);
});