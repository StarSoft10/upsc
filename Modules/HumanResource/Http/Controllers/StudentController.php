<?php

namespace Modules\HumanResource\Http\Controllers;

use App\Entities\StudentChangesInStudy;
use App\Entities\StudentAdditionalStudy;
use App\Entities\User;
use App\Entities\Cycle;
use App\Entities\Faculty;
use App\Entities\Speciality;
use App\Entities\Group;
use App\Entities\Student;
use App\Entities\YearOfStudy;
use App\Helpers\AppHelper;
use App\Helpers\StudentHelper;
use App\Helpers\StudyHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;
use PDF;
use Illuminate\Database\Eloquent\Builder;
use App\Entities\Database;

class StudentController extends Controller
{
    public $helper;
    public $studentHelper;
    public $studyHelper;

    public function __construct()
    {
        $this->middleware(['auth', 'role:human-resource']);
        $this->middleware('check_if_blocked');
        $this->helper = new AppHelper();
        $this->studentHelper = new StudentHelper();
        $this->studyHelper = new StudyHelper();
    }

    public function index(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_student')) {
            return redirect()->route('human-resource.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $idnp = !empty($request->idnp) ? trim($request->idnp) : '';
        $phone = !empty($request->phone) ? trim($request->phone) : '';
        $email = !empty($request->email) ? trim($request->email) : '';
        $city = !empty($request->city) ? trim($request->city) : '';

        $cycleIds = !empty($request->cycleIds) ? $this->helper->removeValueFromArray($request->cycleIds, 0) : [];
        $facultyIds = !empty($request->facultyIds) ? $this->helper->removeValueFromArray($request->facultyIds, 0) : [];
        $specialityIds = !empty($request->specialityIds) ? $this->helper->removeValueFromArray($request->specialityIds, 0) : [];
        $groupIds = !empty($request->groupIds) ? $this->helper->removeValueFromArray($request->groupIds, 0) : [];

        $yearOfStudyIds = !empty($request->yearOfStudyIds) ? $this->helper->removeValueFromArray($request->yearOfStudyIds, 0) : [];

        $budget = isset($request->budget) ? trim($request->budget) : '';
        $expelled = isset($request->expelled) ? trim($request->expelled) : '';
        $study = isset($request->study) ? trim($request->study) : '';

        $students = Student::where('deleted', 0)->orderBy('last_name')->orderBy('first_name')->paginate(10);
        $students->appends([
            'name' => $name,
            'idnp' => $idnp,
            'phone' => $phone,
            'email' => $email,
            'city' => $city,
            'cycleIds' => $cycleIds,
            'facultyIds' => $facultyIds,
            'specialityIds' => $specialityIds,
            'groupIds' => $groupIds,
            'yearOfStudyIds' => $yearOfStudyIds,
            'budget' => $budget,
            'expelled' => $expelled,
            'study' => $study
        ]);

        $cycles = Cycle::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $faculties = Faculty::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $specialities = Speciality::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $groups = Group::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $yearOfStudies = YearOfStudy::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        $permissions = $this->helper->getAllowedPermissionsByModel('student');
        $searchMode = $this->helper->checkIfSearchMode($request);
        $existAdmission = $this->helper->checkIfExistAdmission();
        $databases = Database::where('deleted', 0)->orderBy('id', 'DESC')->pluck('name', 'id');

        return view('humanresource::students.index', compact('students', 'cycles', 'faculties', 'specialities',
            'groups', 'yearOfStudies', 'name', 'idnp', 'phone', 'email', 'city', 'cycleIds', 'facultyIds', 'specialityIds',
            'groupIds', 'yearOfStudyIds', 'budget', 'expelled', 'study', 'permissions', 'searchMode', 'existAdmission',
            'databases'));
    }

    public function search(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_student')) {
            return redirect()->route('human-resource.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $idnp = !empty($request->idnp) ? trim($request->idnp) : '';
        $phone = !empty($request->phone) ? trim($request->phone) : '';
        $email = !empty($request->email) ? trim($request->email) : '';
        $city = !empty($request->city) ? trim($request->city) : '';

        $cycleIds = !empty($request->cycleIds) ? $this->helper->removeValueFromArray($request->cycleIds, 0) : [];
        $facultyIds = !empty($request->facultyIds) ? $this->helper->removeValueFromArray($request->facultyIds, 0) : [];
        $specialityIds = !empty($request->specialityIds) ? $this->helper->removeValueFromArray($request->specialityIds, 0) : [];
        $groupIds = !empty($request->groupIds) ? $this->helper->removeValueFromArray($request->groupIds, 0) : [];

        $yearOfStudyIds = !empty($request->yearOfStudyIds) ? $this->helper->removeValueFromArray($request->yearOfStudyIds, 0) : [];
        $budget = isset($request->budget) ? trim($request->budget) : '';
        $expelled = isset($request->expelled) ? trim($request->expelled) : '';
        $study = isset($request->study) ? trim($request->study) : '';

        $students = Student::where('deleted', 0)->with('user')
            ->whereHas('user', function (Builder $query) use ($email) {
                if ($email !== '') {
                    $query->where('email', 'LIKE', '%' . trim($email) . '%');
                }
            })->where(function ($query)
            use (
                $name, $idnp, $phone, $city, $cycleIds, $facultyIds, $specialityIds, $groupIds, $yearOfStudyIds,
                $budget, $expelled, $study
            ) {
                if ($name !== '') {
                    $query->where(
                        function ($query) use ($name) {
                            return $query
                                ->where('first_name', 'LIKE', '%' . trim($name) . '%')
                                ->orWhere('last_name', 'LIKE', '%' . trim($name) . '%');
                        });
                }
                if ($idnp !== '') {
                    $query->where('idnp', 'LIKE', '%' . trim($idnp) . '%');
                }
                if ($phone !== '') {
                    $query->where(
                        function ($query) use ($phone) {
                            return $query
                                ->where('phone', 'LIKE', '%' . trim($phone) . '%')
                                ->orWhere('mobile', 'LIKE', '%' . trim($phone) . '%');
                        });
                }
                if ($city !== '') {
                    $query->where('city', 'LIKE', '%' . trim($city) . '%');
                }

                if (count($cycleIds) > 0) {
                    $query->whereIn('cycle_id', $cycleIds);

//                    $query->orWhere(
//                        function ($query2) use ($cycleIds) {
//                            $implode = implode(',', $cycleIds);
//                            return $query2
//                                ->orWhereRaw('exists(SELECT * FROM students_additional_study WHERE students_additional_study.student_id = students.id and cycle_id IN ('. $implode .')) = 1');
//                        });
                }
                if (count($facultyIds) > 0) {
                    $query->whereIn('faculty_id', $facultyIds);

//                    $query->orWhere(
//                        function ($query2) use ($facultyIds) {
//                            $implode = implode(',', $facultyIds);
//                            return $query2
//                                ->orWhereRaw('exists(SELECT * FROM students_additional_study WHERE students_additional_study.student_id = students.id and faculty_id IN ('. $implode .')) = 1');
//                        });
                }
                if (count($specialityIds) > 0) {
                    $query->whereIn('speciality_id', $specialityIds);

//                    $query->orWhere(
//                        function ($query2) use ($specialityIds) {
//                            $implode = implode(',', $specialityIds);
//                            return $query2
//                                ->orWhereRaw('exists(SELECT * FROM students_additional_study WHERE students_additional_study.student_id = students.id and speciality_id IN ('. $implode .')) = 1');
//                        });
                }
                if (count($groupIds) > 0) {
                    $query->whereIn('group_id', $groupIds);

//                    $query->orWhere(
//                        function ($query2) use ($groupIds) {
//                            $implode = implode(',', $groupIds);
//                            return $query2
//                                ->orWhereRaw('exists(SELECT * FROM students_additional_study WHERE students_additional_study.student_id = students.id and group_id IN ('. $implode .')) = 1');
//                        });
                }

                if (count($yearOfStudyIds) > 0) {
                    $query->whereIn('year_of_study_id', $yearOfStudyIds);

//                    $query->orWhere(
//                        function ($query2) use ($yearOfStudyIds) {
//                            $implode = implode(',', $yearOfStudyIds);
//                            return $query2
//                                ->orWhereRaw('exists(SELECT * FROM students_additional_study WHERE students_additional_study.student_id = students.id and year_of_study_id IN ('. $implode .')) = 1');
//                        });
                }
                if ($budget !== '') {
                    $query->where('budget', '=', trim($budget));
                }
                if ($expelled !== '') {
                    $query->where('expelled', '=', trim($expelled));
                }
                if ($study !== '') {
                    $query->where('study', '=', trim($study));
                }

                $query->orWhere(function ($query2) use ($cycleIds, $facultyIds, $specialityIds, $groupIds, $yearOfStudyIds) {
                    if (count($cycleIds) > 0) {
                        $implode = implode(',', $cycleIds);
                        $query2->whereRaw('students.id IN (SELECT students_additional_study.student_id FROM students_additional_study WHERE cycle_id IN ('. $implode .')) ');
                    }

                    if (count($facultyIds) > 0) {
                        $implode = implode(',', $facultyIds);
                        $query2->whereRaw('students.id IN (SELECT students_additional_study.student_id FROM students_additional_study WHERE faculty_id IN ('. $implode .')) ');
                    }

                    if (count($specialityIds) > 0) {
                        $implode = implode(',', $specialityIds);
                        $query2->whereRaw('students.id IN (SELECT students_additional_study.student_id FROM students_additional_study WHERE speciality_id IN ('. $implode .')) ');
                    }

                    if (count($groupIds) > 0) {
                        $implode = implode(',', $groupIds);
                        $query2->whereRaw('students.id IN (SELECT students_additional_study.student_id FROM students_additional_study WHERE group_id IN ('. $implode .')) ');
                    }

                    if (count($yearOfStudyIds) > 0) {
                        $implode = implode(',', $yearOfStudyIds);
                        $query2->whereRaw('students.id IN (SELECT students_additional_study.student_id FROM students_additional_study WHERE year_of_study_id IN ('. $implode .')) ');
                    }
                });
            })->orderBy('last_name')->orderBy('first_name')->paginate(10);
        $students->appends([
            'name' => $name,
            'idnp' => $idnp,
            'phone' => $phone,
            'email' => $email,
            'city' => $city,
            'cycleIds' => $cycleIds,
            'facultyIds' => $facultyIds,
            'specialityIds' => $specialityIds,
            'groupIds' => $groupIds,
            'yearOfStudyIds' => $yearOfStudyIds,
            'budget' => $budget,
            'expelled' => $expelled,
            'study' => $study
        ]);

        $cycles = Cycle::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $faculties = Faculty::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $specialities = Speciality::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $groups = Group::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $yearOfStudies = YearOfStudy::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        $permissions = $this->helper->getAllowedPermissionsByModel('student');
        $searchMode = $this->helper->checkIfSearchMode($request);
        $existAdmission = $this->helper->checkIfExistAdmission();
        $databases = Database::where('deleted', 0)->orderBy('id', 'DESC')->pluck('name', 'id');

        return view('humanresource::students.index', compact('students', 'cycles', 'faculties', 'specialities',
            'groups', 'yearOfStudies', 'name', 'idnp', 'phone', 'email', 'city', 'cycleIds', 'facultyIds', 'specialityIds',
            'groupIds', 'yearOfStudyIds', 'budget', 'expelled', 'study', 'permissions', 'searchMode', 'existAdmission',
            'databases'));
    }

    public function store(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('create_student')) {
            return redirect()->route('human-resource.not-found');
        }

        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users|regex:/^[a-z0-9\@\.\_\-]+$/u',
            'password' => 'required|confirmed',
            'permissions' => 'required|array|min:1',
            'cycle_id' => 'required|numeric|gt:0',
            'faculty_id' => 'required|numeric|gt:0',
            'speciality_id' => 'required|numeric|gt:0',
            'group_id' => 'required|numeric|gt:0',
            'year_of_study_id' => 'required|numeric|gt:0',
            'first_name' => 'required',
            'last_name' => 'required',
            'birth_date' => 'required',
            'year_of_admission' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $user = User::create([
            'name' => $request->first_name . ' ' . $request->last_name,
            'email' => $this->studentHelper->formatEmail($request->email),
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make($request->password)
        ]);
        $user->assignRole('student');
        foreach ($request->permissions as $permission) {
            $user->givePermissionTo($permission);
        }

        $student = Student::create([
            'user_id' => $user->id,
            'cycle_id' => $request->cycle_id,
            'faculty_id' => $request->faculty_id,
            'speciality_id' => $request->speciality_id,
            'group_id' => $request->group_id,
            'year_of_study_id' => $request->year_of_study_id,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'patronymic' => $request->patronymic,
            'idnp' => $request->idnp,
            'gender' => $request->gender,
            'personal_email' => $request->personal_email,
            'mobile' => $request->mobile,
            'birth_date' => $request->birth_date,
            'city' => $request->city,
            'address' => $request->address,
            'citizenship' => $request->citizenship,
            'nationality' => $request->nationality,
            'year_of_admission' => $request->year_of_admission,
            'study' => $request->study,
            'budget' => $request->budget,
            'expelled' => $request->expelled,
            'additional_info' => $request->additional_info
        ]);

        StudentChangesInStudy::create([
            'student_id' => $student->id,
            'cycle' => $this->studyHelper->getCycleNameById($request->cycle_id),
            'faculty' => $this->studyHelper->getFacultyNameById($request->faculty_id),
            'speciality' => $this->studyHelper->getSpecialityNameById($request->speciality_id),
            'group' => $this->studyHelper->getGroupNameById($request->group_id),
            'year_of_study' => $this->studyHelper->getYearOfStudyNameById($request->year_of_study_id),
        ]);

        $issetAdditionalCycles = isset($request->additional_cycles) ? true : false;
        $issetAdditionalFaculties = isset($request->additional_faculties) ? true : false;
        $issetAdditionalSpecialities = isset($request->additional_specialities) ? true : false;
        $issetAdditionalGroups = isset($request->additional_groups) ? true : false;

        $issetYearOfStudies = isset($request->additional_year_of_studies) ? true : false;
        $issetYearOfAdmissions = isset($request->year_of_admissions) ? true : false;
        $issetStudies = isset($request->studies) ? true : false;
        $issetBudgets = isset($request->budgets) ? true : false;
        $issetExpelleds = isset($request->expelleds) ? true : false;

        $issetAdditionalInfos = isset($request->additional_infos) ? true : false;

        if($issetAdditionalCycles && $issetAdditionalFaculties && $issetAdditionalSpecialities && $issetAdditionalGroups
            && $issetYearOfStudies && $issetYearOfAdmissions && $issetStudies && $issetBudgets && $issetExpelleds && $issetAdditionalInfos){

            foreach ($request->additional_cycles as $key => $cycleId){
                StudentAdditionalStudy::create([
                    'student_id' => $student->id,
                    'cycle_id' => $cycleId,
                    'faculty_id' => $request->additional_faculties[$key],
                    'speciality_id' => $request->additional_specialities[$key],
                    'group_id' => $request->additional_groups[$key],
                    'year_of_study_id' => $request->additional_year_of_studies[$key],
                    'year_of_admission' => $request->year_of_admissions[$key],
                    'study' => $request->studies[$key],
                    'budget' => $request->budgets[$key],
                    'expelled' => $request->expelleds[$key],
                    'additional_info' => $request->additional_infos[$key]
                ]);
            }
        }

        $this->helper->addLog('human-resource', Auth::user()->humanResource->id, 'students', 1, 'DB id: [' . $student->id . ']');

        return back()->with('success', __('messages.student_created_successfully'));
    }

    public function show($id)
    {
        if (!Auth::user()->hasPermissionTo('view_student')) {
            return redirect()->route('human-resource.not-found');
        }

        $student = Student::where('id', $id)->where('deleted', 0)->first();

        if (!$student) {
            return back()->with('warning', __('messages.student_not_found'));
        }

        $studentPermissions = [];
        foreach ($student->user->getDirectPermissions() as $permission) {
            $studentPermissions[] = $permission->id;
        }
        $student->permissions = $studentPermissions;

        $cycles = Cycle::where('deleted', 0)
            ->orderBy('id', 'ASC')
            ->pluck('name', 'id');

        $faculties = Faculty::where('deleted', 0)
            ->where('cycle_id', $student->cycle->id)
            ->orderBy('id', 'ASC')
            ->pluck('name', 'id');

        $specialities = Speciality::where('deleted', 0)
            ->where('cycle_id', $student->cycle->id)
            ->where('faculty_id', $student->faculty->id)
            ->orderBy('id', 'ASC')
            ->pluck('name', 'id');

        $groups = Group::where('deleted', 0)
            ->where('cycle_id', $student->cycle->id)
            ->where('faculty_id', $student->faculty->id)
            ->where('speciality_id', $student->speciality->id)
            ->orderBy('id', 'ASC')
            ->pluck('name', 'id');

        $yearOfStudies = YearOfStudy::where('deleted', 0)
            ->orderBy('id', 'ASC')
            ->pluck('name', 'id');

        $permissions = $this->helper->getAllowedPermissionsByModel('student');

        $studentChanges = StudentChangesInStudy::where('student_id', $student->id)->orderBy('id', 'ASC')->get();

        $notes = [];
        if (Auth::user()->hasPermissionTo('view_borderou_note')) {
            $notes = $this->studentHelper->getStudentNotes($student);
        }

        $additionalStudies = StudentAdditionalStudy::where('student_id', $student->id)->get();
        $additionalStudiesArray = [];

        if(count($additionalStudies) > 0){
            foreach ($additionalStudies as $key => $additionalStudy) {

                $faculties2 = Faculty::where('deleted', 0)
                    ->where('cycle_id', $additionalStudy->cycle_id)
                    ->orderBy('id', 'ASC')
                    ->pluck('name', 'id');

                $specialities2 = Speciality::where('deleted', 0)
                    ->where('cycle_id', $additionalStudy->cycle_id)
                    ->where('faculty_id', $additionalStudy->faculty_id)
                    ->orderBy('id', 'ASC')
                    ->pluck('name', 'id');

                $groups2 = Group::where('deleted', 0)
                    ->where('cycle_id', $additionalStudy->cycle_id)
                    ->where('faculty_id', $additionalStudy->faculty_id)
                    ->where('speciality_id', $additionalStudy->speciality_id)
                    ->orderBy('id', 'ASC')
                    ->pluck('name', 'id');

                $additionalStudiesArray[$key] = [
                    'id' => $additionalStudy->id,
                    'cycle_id' => $additionalStudy->cycle_id,
                    'faculty_id' => $additionalStudy->faculty_id,
                    'speciality_id' => $additionalStudy->speciality_id,
                    'group_id' => $additionalStudy->group_id,
                    'year_of_study_id' => $additionalStudy->year_of_study_id,
                    'year_of_admission' => $additionalStudy->year_of_admission,
                    'study' => $additionalStudy->study,
                    'budget' => $additionalStudy->budget,
                    'expelled' => $additionalStudy->expelled,
                    'additional_info' => $additionalStudy->additional_info,
                    'faculties' => $faculties2,
                    'specialities' => $specialities2,
                    'groups' => $groups2
                ];
            }
        }

        return view('humanresource::students.show', compact('student', 'cycles', 'faculties', 'specialities',
            'groups', 'yearOfStudies', 'permissions', 'studentPermissions', 'studentChanges', 'notes', 'additionalStudiesArray'));
    }

    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermissionTo('edit_student')) {
            return redirect()->route('human-resource.not-found');
        }

        $student = Student::where('id', $id)->where('deleted', 0)->first();
        if (!$student) {
            return back()->with('warning', __('messages.student_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'email' => [
                'required',
                'email',
                'regex:/^[a-z0-9\@\.\_\-]+$/u',
                Rule::unique('users')->ignore(User::findOrFail($student->user->id)),
            ],
            'password' => 'confirmed',
            'permissions' => 'required|array|min:1',
            'cycle_id' => 'required|numeric|gt:0',
            'faculty_id' => 'required|numeric|gt:0',
            'speciality_id' => 'required|numeric|gt:0',
            'group_id' => 'required|numeric|gt:0',
            'year_of_study_id' => 'required|numeric|gt:0',
            'first_name' => 'required',
            'last_name' => 'required',
            'birth_date' => 'required',
            'year_of_admission' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        User::where('id', $student->user->id)->update([
            'name' => $request->first_name . ' ' . $request->last_name,
            'email' => $this->studentHelper->formatEmail($request->email),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $user = User::find($student->user->id);
        $user->syncPermissions($request->permissions);

        if (trim($request->password) !== '') {
            Student::where('id', $id)->update([
                'password_changed' => 1,
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);

            User::where('id', $student->user->id)->update([
                'password' => Hash::make($request->password),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }

        Student::where('id', $id)->update([
            'cycle_id' => $request->cycle_id,
            'faculty_id' => $request->faculty_id,
            'speciality_id' => $request->speciality_id,
            'group_id' => $request->group_id,
            'year_of_study_id' => $request->year_of_study_id,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'patronymic' => $request->patronymic,
            'idnp' => $request->idnp,
            'gender' => $request->gender,
            'personal_email' => $request->personal_email,
            'mobile' => $request->mobile,
            'birth_date' => $request->birth_date,
            'city' => $request->city,
            'address' => $request->address,
            'citizenship' => $request->citizenship,
            'nationality' => $request->nationality,
            'year_of_admission' => $request->year_of_admission,
            'study' => $request->study,
            'budget' => $request->budget,
            'expelled' => $request->expelled,
            'additional_info' => $request->additional_info,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        if ($student->year_of_admission !== $request->year_of_admission || $student->cycle_id !== $request->cycle_id ||
            $student->faculty_id !== $request->faculty_id || $student->speciality_id !== $request->speciality_id ||
            $student->group_id !== $request->group_id || $student->year_of_study_id !== $request->year_of_study_id) {
            StudentChangesInStudy::create([
                'student_id' => $student->id,
                'year_of_admission' => $request->year_of_admission,
                'cycle' => $this->studyHelper->getCycleNameById($request->cycle_id),
                'faculty' => $this->studyHelper->getFacultyNameById($request->faculty_id),
                'speciality' => $this->studyHelper->getSpecialityNameById($request->speciality_id),
                'group' => $this->studyHelper->getGroupNameById($request->group_id),
                'year_of_study' => $this->studyHelper->getYearOfStudyNameById($request->year_of_study_id),
            ]);
        }

        $issetAdditionalCycles = isset($request->additional_cycles) ? true : false;
        $issetAdditionalFaculties = isset($request->additional_faculties) ? true : false;
        $issetAdditionalSpecialities = isset($request->additional_specialities) ? true : false;
        $issetAdditionalGroups = isset($request->additional_groups) ? true : false;

        $issetYearOfStudies = isset($request->additional_year_of_studies) ? true : false;
        $issetYearOfAdmissions = isset($request->year_of_admissions) ? true : false;
        $issetStudies = isset($request->studies) ? true : false;
        $issetBudgets = isset($request->budgets) ? true : false;
        $issetExpelleds = isset($request->expelleds) ? true : false;

        $issetAdditionalInfos = isset($request->additional_infos) ? true : false;

        if($issetAdditionalCycles && $issetAdditionalFaculties && $issetAdditionalSpecialities && $issetAdditionalGroups
            && $issetYearOfStudies && $issetYearOfAdmissions && $issetStudies && $issetBudgets && $issetExpelleds && $issetAdditionalInfos){

            foreach ($request->additional_cycles as $key => $cycleId){
                if(isset($request->ids[$key])) {
                    StudentAdditionalStudy::where('id', $request->ids[$key])->where('student_id', $id)->update([
                        'cycle_id' => $cycleId,
                        'faculty_id' => $request->additional_faculties[$key],
                        'speciality_id' => $request->additional_specialities[$key],
                        'group_id' => $request->additional_groups[$key],
                        'year_of_study_id' => $request->additional_year_of_studies[$key],
                        'year_of_admission' => $request->year_of_admissions[$key],
                        'study' => $request->studies[$key],
                        'budget' => $request->budgets[$key],
                        'expelled' => $request->expelleds[$key],
                        'additional_info' => $request->additional_infos[$key],
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ]);
                } else {
                    StudentAdditionalStudy::create([
                        'student_id' => $student->id,
                        'cycle_id' => $cycleId,
                        'faculty_id' => $request->additional_faculties[$key],
                        'speciality_id' => $request->additional_specialities[$key],
                        'group_id' => $request->additional_groups[$key],
                        'year_of_study_id' => $request->additional_year_of_studies[$key],
                        'year_of_admission' => $request->year_of_admissions[$key],
                        'study' => $request->studies[$key],
                        'budget' => $request->budgets[$key],
                        'expelled' => $request->expelleds[$key],
                        'additional_info' => $request->additional_infos[$key]
                    ]);
                }
            }
        }

        $this->helper->addLog('human-resource', Auth::user()->humanResource->id, 'students', 2, 'DB id: [' . $id . ']');

        return redirect()->route('human-resource.students.index')->with('success', __('messages.student_updated_successfully'));
    }

    public function block(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_student')) {
            return redirect()->route('human-resource.not-found');
        }

        $validator = Validator::make($request->all(), [
            'student_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $student = Student::where('id', $request->student_id)->where('deleted', 0)->first();
        if (!$student) {
            return back()->with('warning', __('messages.student_not_found'));
        }

        Student::where('id', $request->student_id)->update([
            'blocked' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('human-resource', Auth::user()->humanResource->id, 'students', 4, 'DB id: [' . $request->student_id . ']');

        return back()->with('success', __('messages.student_blocked_successfully'));
    }

    public function unblock(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_student')) {
            return redirect()->route('human-resource.not-found');
        }

        $validator = Validator::make($request->all(), [
            'student_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $student = Student::where('id', $request->student_id)->where('deleted', 0)->first();
        if (!$student) {
            return back()->with('warning', __('messages.student_not_found'));
        }

        Student::where('id', $request->student_id)->update([
            'blocked' => 0,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('human-resource', Auth::user()->humanResource->id, 'students', 5, 'DB id: [' . $request->student_id . ']');

        return back()->with('success', __('messages.student_unblocked_successfully'));
    }

    public function delete(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_student')) {
            return redirect()->route('human-resource.not-found');
        }

        $validator = Validator::make($request->all(), [
            'student_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $student = Student::where('id', $request->student_id)->where('deleted', 0)->first();
        if (!$student) {
            return back()->with('warning', __('messages.student_not_found'));
        }

        Student::where('id', $request->student_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('human-resource', Auth::user()->humanResource->id, 'students', 3, 'DB id: [' . $request->student_id . ']');

        return back()->with('success', __('messages.student_deleted_successfully'));
    }

    public function exportStudentsPdf(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('export_student')) {
            return redirect()->route('human-resource.not-found');
        }

        return $this->studentHelper->exportStudentsPdf($request);
    }

    public function exportStudentsExcel(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('export_student')) {
            return redirect()->route('human-resource.not-found');
        }

        return $this->studentHelper->exportStudentsExcel($request);
    }

    public function exportStudentPdf(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('export_student')) {
            return redirect()->route('human-resource.not-found');
        }

        return $this->studentHelper->exportStudentPdf($request);
    }

    public function exportStudentExcel(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('export_student')) {
            return redirect()->route('human-resource.not-found');
        }

        return $this->studentHelper->exportStudentExcel($request);
    }

    public function deleteAdditionalStudy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['type' => 'warning', 'message' => $validator->errors()->first()]);
        }

        $additionalStudy = StudentAdditionalStudy::where('id', $request->id)->first();
        if (!$additionalStudy) {
            return response()->json(['type' => 'warning', 'message' => __('messages.additional_study_not_found')]);
        }

        $additionalStudy->delete();

        $this->helper->addLog('admin', Auth::user()->admin->id, 'students_additional_study', 3, 'DB id: [' . $request->id . ']');

        return response()->json(['type' => 'success', 'message' => __('messages.additional_study_deleted_successfully')]);
    }
}
