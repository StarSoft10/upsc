<?php

namespace Modules\HumanResource\Http\Controllers;

use App\Helpers\AppHelper;
use App\Helpers\StudyHelper;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Routing\Controller;

class FacultyController extends Controller
{
    public $helper;

    public $studyHelper;

    public function __construct()
    {
        $this->helper = new AppHelper();
        $this->studyHelper = new StudyHelper();
    }

    public function getFacultiesByCycleForSelectSearch(Request $request)
    {
        return $this->studyHelper->getFacultiesByCycleForSelectSearch($request);
    }

    public function getFacultiesByCycleForSelectAddEdit(Request $request)
    {
        return $this->studyHelper->getFacultiesByCycleForSelectAddEdit($request);
    }
}
