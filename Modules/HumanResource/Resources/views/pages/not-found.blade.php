@extends('humanresource::layouts.app')

@section('title')
    {{ __('pages.dashboard') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-bullhorn"></i>
                    <strong class="card-title">{{ __('pages.warning') }}</strong>
                </div>

                <div class="card-body">
                    <div class="alert alert-danger" role="alert">
                        <h1 class="alert-heading"><b>{{ __('messages.your permissions_is_not_enough_to_access_this_page') }}</b></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

@section('custom-css')
@endsection

@section('custom-scripts')
@endsection
