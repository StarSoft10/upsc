<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

Route::group([
    'prefix' => LaravelLocalization::setLocale() . '/human-resource',
    'middleware' => ['auth', 'role:human-resource', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'localize']
], function () {
    Route::get('/', 'HumanResourceController@index')->name('human-resource');
    Route::get('profile', 'HumanResourceController@show')->name('human-resource.profile');
    Route::patch('update', 'HumanResourceController@update')->name('human-resource.update');
    Route::patch('changePhoto', 'HumanResourceController@changePhoto')->name('human-resource.changePhoto');
    Route::get('not-found', 'HumanResourceController@notFound')->name('human-resource.not-found');


    /*
     * User routes
     */
    Route::group(['prefix' => 'teachers'], function () {
        Route::post('delete', 'TeacherController@delete')->name('human-resource.teachers.delete');
        Route::get('search', 'TeacherController@search')->name('human-resource.teachers.search');
        Route::post('block', 'TeacherController@block')->name('human-resource.teachers.block');
        Route::post('unblock', 'TeacherController@unblock')->name('human-resource.teachers.unblock');
        Route::post('exportStudentsPdf', 'StudentController@exportStudentsPdf')->name('human-resource.students.exportStudentsPdf');
        Route::post('exportStudentsExcel', 'StudentController@exportStudentsExcel')->name('human-resource.students.exportStudentsExcel');
        Route::post('exportStudentPdf', 'StudentController@exportStudentPdf')->name('human-resource.students.exportStudentPdf');
        Route::post('exportStudentExcel', 'StudentController@exportStudentExcel')->name('human-resource.students.exportStudentExcel');
        Route::post('deleteAdditionalStudy', 'StudentController@deleteAdditionalStudy')->name('human-resource.students.deleteAdditionalStudy');
    });
    Route::resource('teachers', 'TeacherController', ['names' => [
        'index' => 'human-resource.teachers.index',
        'store' => 'human-resource.teachers.store',
        'show' => 'human-resource.teachers.show',
        'update' => 'human-resource.teachers.update'
    ]]);


    /*
    * Studies routes
    */
    Route::group(['prefix' => 'faculties'], function () {
        Route::post('getFacultiesByCycleForSelectSearch', 'FacultyController@getFacultiesByCycleForSelectSearch')->name('human-resource.faculties.getFacultiesByCycleForSelectSearch');
        Route::post('getFacultiesByCycleForSelectAddEdit', 'FacultyController@getFacultiesByCycleForSelectAddEdit')->name('human-resource.faculties.getFacultiesByCycleForSelectAddEdit');
    });

    Route::group(['prefix' => 'specialities'], function () {
        Route::post('getSpecialitiesByFacultyForSelectSearch', 'SpecialityController@getSpecialitiesByFacultyForSelectSearch')->name('human-resource.specialities.getSpecialitiesByFacultyForSelectSearch');
        Route::post('getSpecialitiesByFacultyForSelectAddEdit', 'SpecialityController@getSpecialitiesByFacultyForSelectAddEdit')->name('human-resource.specialities.getSpecialitiesByFacultyForSelectAddEdit');
    });

    Route::group(['prefix' => 'groups'], function () {
        Route::post('getGroupsBySpecialityForSelectSearch', 'GroupController@getGroupsBySpecialityForSelectSearch')->name('human-resource.groups.getGroupsBySpecialityForSelectSearch');
        Route::post('getGroupsBySpecialityForSelectAddEdit', 'GroupController@getGroupsBySpecialityForSelectAddEdit')->name('human-resource.groups.getGroupsBySpecialityForSelectAddEdit');
    });


    Route::group(['prefix' => 'students'], function () {
        Route::post('delete', 'StudentController@delete')->name('human-resource.students.delete');
        Route::get('search', 'StudentController@search')->name('human-resource.students.search');
        Route::post('block', 'StudentController@block')->name('human-resource.students.block');
        Route::post('unblock', 'StudentController@unblock')->name('human-resource.students.unblock');
    });
    Route::resource('students', 'StudentController', ['names' => [
        'index' => 'human-resource.students.index',
        'store' => 'human-resource.students.store',
        'show' => 'human-resource.students.show',
        'update' => 'human-resource.students.update'
    ]]);
});