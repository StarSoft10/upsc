<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="{{ route('teacher') }}"><img src="{{ asset('images/logo.png') }}" alt="Logo" style="width: 29%;"></a>
            <a class="navbar-brand hidden" href="{{ route('teacher') }}"><img src="{{ asset('images/logo.png') }}" alt="Logo"></a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="{{ route('teacher') }}">
                        <i class="menu-icon fa fa-dashboard"></i>
                        {{ __('aside.dashboard') }}
                    </a>
                </li>

                <h3 class="menu-title">{{ __('aside.borderous') }}</h3>
                <li>
                    <a href="{{ route('teacher.borderous.index') }}" class="@if(Request::is('*borderous*')) current @endif">
                        <i class="menu-icon ti-folder"></i>
                        {{ __('aside.borderous') }}
                    </a>
                </li>

                {{--<li>--}}
                    {{--<a href="javascript:void(0)"> <i class="menu-icon ti-email"></i>Some page</a>--}}
                {{--</li>--}}
            </ul>
        </div>
    </nav>
</aside>