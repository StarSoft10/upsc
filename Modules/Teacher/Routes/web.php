<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

Route::group([
    'prefix' => LaravelLocalization::setLocale() . '/teacher',
    'middleware' => ['auth', 'role:teacher', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'localize']
], function () {
    Route::get('/', 'TeacherController@index')->name('teacher');
    Route::get('profile', 'TeacherController@show')->name('teacher.profile');
    Route::patch('update', 'TeacherController@update')->name('teacher.update');
    Route::patch('changePhoto', 'TeacherController@changePhoto')->name('teacher.changePhoto');
    Route::get('not-found', 'TeacherController@notFound')->name('teacher.not-found');


    /*
    * Studies routes
    */
    Route::group(['prefix' => 'faculties'], function () {
        Route::post('getFacultiesByCycleForSelectSearch', 'FacultyController@getFacultiesByCycleForSelectSearch')->name('teacher.faculties.getFacultiesByCycleForSelectSearch');
        Route::post('getFacultiesByCycleForSelectAddEdit', 'FacultyController@getFacultiesByCycleForSelectAddEdit')->name('teacher.faculties.getFacultiesByCycleForSelectAddEdit');
    });

    Route::group(['prefix' => 'specialities'], function () {
        Route::post('getSpecialitiesByFacultyForSelectSearch', 'SpecialityController@getSpecialitiesByFacultyForSelectSearch')->name('teacher.specialities.getSpecialitiesByFacultyForSelectSearch');
        Route::post('getSpecialitiesByFacultyForSelectAddEdit', 'SpecialityController@getSpecialitiesByFacultyForSelectAddEdit')->name('teacher.specialities.getSpecialitiesByFacultyForSelectAddEdit');
    });

    Route::group(['prefix' => 'groups'], function () {
        Route::post('getGroupsBySpecialityForSelectSearch', 'GroupController@getGroupsBySpecialityForSelectSearch')->name('teacher.groups.getGroupsBySpecialityForSelectSearch');
        Route::post('getGroupsBySpecialityForSelectAddEdit', 'GroupController@getGroupsBySpecialityForSelectAddEdit')->name('teacher.groups.getGroupsBySpecialityForSelectAddEdit');
    });


    /*
   * Borderous routes
   */
    Route::group(['prefix' => 'borderous'], function () {
        Route::post('delete', 'BorderouController@delete')->name('teacher.borderous.delete');
        Route::get('search', 'BorderouController@search')->name('teacher.borderous.search');
        Route::post('getBorderouForView', 'BorderouController@getBorderouForView')->name('teacher.borderous.getBorderouForView');
        Route::post('getBorderouNote', 'BorderouController@getBorderouNote')->name('teacher.borderous.getBorderouNote');
        Route::post('updateBorderouNote', 'BorderouController@updateBorderouNote')->name('teacher.borderous.updateBorderouNote');
        Route::post('deleteStudentBorderouNote', 'BorderouController@deleteStudentBorderouNote')->name('teacher.borderous.deleteStudentBorderouNote');
        Route::post('addStudentBorderouNote', 'BorderouController@addStudentBorderouNote')->name('teacher.borderous.addStudentBorderouNote');
    });
    Route::resource('borderous', 'BorderouController', ['names' => [
        'index' => 'teacher.borderous.index',
        'store' => 'teacher.borderous.store',
        'show' => 'teacher.borderous.show',
        'update' => 'teacher.borderous.update'
    ]]);
});