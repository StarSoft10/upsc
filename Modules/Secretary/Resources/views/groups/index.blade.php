@extends('secretary::layouts.app')

@section('title')
    {{ __('pages.groups') }}
@endsection

@section('content')
    <div class="custom-loader" style="display: none"></div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row mb-1">
                        <div class="col-md-12">
                            {!! Form::open(['route' => 'secretary.groups.search', 'method' => 'GET']) !!}
                            <div class="row">
                                <div class="col-md-2">
                                    {!! Form::text('name', $name, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.name'), 'style' => 'width: 100%;']) !!}
                                </div>
                                <div class="col-md-2">
                                    {!! Form::text('shortName', $shortName, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.short_name'), 'style' => 'width: 100%;']) !!}
                                </div>
                                <div class="col-md-2">
                                    <select data-placeholder="{{ __('pages.chose_cycle') }}" multiple class="standardSelect" name="cycleIds[]" id="cycleIds">
                                        @foreach($cycles as $key => $cycle)
                                            <option value="{{ $key }}" @if(in_array($key, $cycleIds)) selected @endif>{{ $cycle }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <select data-placeholder="{{ __('pages.chose_faculty') }}" multiple class="standardSelect" name="facultyIds[]" id="facultyIds">
                                        @if(count($facultyIds) > 0)
                                            @foreach($facultyIds as $id)
                                                <option value="{{ $id }}" selected >{{ $faculties[$id] }}</option>
                                            @endforeach
                                        @else
                                            <option value="0">{{ __('pages.chose_cycle') }}</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <select data-placeholder="{{ __('pages.chose_speciality') }}" multiple class="standardSelect" name="specialityIds[]" id="specialityIds">
                                        @if(count($specialityIds) > 0)
                                            @foreach($specialityIds as $id)
                                                <option value="{{ $id }}" selected >{{ $specialities[$id] }}</option>
                                            @endforeach
                                        @else
                                            <option value="0">{{ __('pages.chose_faculty') }}</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-2 text-right">
                                    {!! Form::submit(__('pages.search'), ['class' => 'btn btn-primary btn-sm']) !!}
                                    <a href="{{ route('secretary.groups.index') }}" class="btn btn-info btn-sm" title="{{ __('pages.reset_filter') }}">
                                        <i class="fa fa-refresh"></i>
                                    </a>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            @can('create_group')
                                <button type="button" class="btn btn-success mb-1 btn-sm" data-toggle="modal" data-target="#addGroup">
                                    {{ __('pages.add_group') }}
                                </button>
                            @endcan
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th>{{ __('pages.name') }}</th>
                                <th>{{ __('pages.short_name') }}</th>
                                <th>{{ __('pages.cycle') }}</th>
                                <th>{{ __('pages.faculty') }}</th>
                                <th>{{ __('pages.speciality') }}</th>
                                <th>{{ __('pages.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(count($groups) > 0)
                            @foreach($groups as $group)
                                <tr>
                                    <td class="text-center"><span>{{ $group->name }}</span></td>
                                    <td class="text-center"><span>{{ $group->short_name }}</span></td>
                                    <td class="text-center"><span>{{ $group->cycle->name }}</span></td>
                                    <td class="text-center"><span>{{ $group->faculty->name }}</span></td>
                                    <td class="text-center"><span>{{ $group->speciality->name }}</span></td>
                                    <td class="text-center">
                                        @can('view_group')
                                            <a href="{{ route('secretary.groups.show', $group->id) }}" class="btn-no-bg" title="{{ __('pages.show_edit') }}">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        @endcan

                                        @can('delete_group')
                                            <a href="javascript:void(0)" data-groupid="{{ $group->id }}" class="btn-no-bg delete-group" title="{{ __('pages.delete') }}">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="text-center">
                                <td colspan="6">{{ __('messages.not_found_any_groups') }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    <div class="table-footer">
                        {!! $groups->render() !!}
                        <div class="total-found">{{ __('pages.total_found') }}: {{ $groups->total() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @can('create_group')
        <div class="modal fade" id="addGroup" role="dialog" aria-labelledby="addGroup" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    {!! Form::open(['url' => route('secretary.groups.store'), 'method' => 'post', 'autocomplete' => 'off']) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.add_group') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('name', __('pages.name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('short_name', __('pages.short_name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('short_name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('cycle_id', __('pages.cycle'), ['class' => 'control-label']) !!}
                                    <select data-placeholder="{{ __('pages.chose_cycle') }}" class="standardSelect" name="cycle_id" id="cycle_id">
                                        @foreach($cycles as $key => $cycle)
                                            <option value="{{ $key }}">{{ $cycle }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('faculty_id', __('pages.faculty'), ['class' => 'control-label']) !!}
                                    <select data-placeholder="{{ __('pages.chose_faculty') }}" class="standardSelect" name="faculty_id" id="faculty_id">
                                        <option value="0">{{ __('pages.chose_cycle') }}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('speciality_id', __('pages.speciality'), ['class' => 'control-label']) !!}
                                    <select data-placeholder="{{ __('pages.chose_faculty') }}" class="standardSelect" name="speciality_id" id="speciality_id">
                                        <option value="0">{{ __('pages.chose_faculty') }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        {!! Form::submit(__('pages.add'), ['class' => 'btn btn-success btn-sm']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan

    @can('delete_group')
        <div class="modal fade" id="deleteGroup" role="dialog" aria-labelledby="deleteGroup" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['route' => 'secretary.groups.delete']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.delete_group') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-info" role="alert">
                                <div class="alert-text">
                                    {{ __('pages.sure_to_delete_group?') }}
                                </div>
                            </div>
                            <input type="hidden" name="group_id">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            <button type="submit" class="btn btn-danger btn-sm">{{ __('pages.delete') }}</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan
@endsection

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('vendors/chosen/chosen.min.css') }}">
@endsection

@section('custom-scripts')
    <script src="{{ asset('vendors/chosen/chosen.jquery.min.js') }}"></script>
    <script>
        $(function() {
            $('.standardSelect').chosen({
                disable_search_threshold: 10,
                no_results_text: '{{ __('pages.nothing_found') }}',
                width: '100%'
            });
        });
    </script>
    <script>
        let body = $('body');

        body.on('click', '.delete-group', function () {
            let groupId = $(this).attr('data-groupid');
            let modal = $('#deleteGroup');
            modal.find('input[name="group_id"]').val(groupId);
            modal.modal('show');
        });

        $('#deleteGroup').on('hidden.bs.modal', function () {
            $(this).find('input[name="group_id"]').val('');
        });

        $('#addGroup').on('show.bs.modal', function () {
            let totalCycles = $('#cycle_id > option').length;
            if(totalCycles === 1){
                getFacultiesByCycleForSelectAddEdit('#cycle_id');

                let totalFaculties = $('#faculty_id > option').length;
                if(totalFaculties === 1){
                    getSpecialitiesByFacultyForSelectAddEdit('#faculty_id');
                }
            }
        });


        //For search
        body.on('change', '#cycleIds', function () {
            getFacultiesByCycleForSearch()
        });
        body.on('change', '#facultyIds', function () {
            getSpecialitiesByFacultyForSelectSearch();
        });



        //For create
        body.on('change', '#cycle_id', function () {
            getFacultiesByCycleForSelectAddEdit('#cycle_id')
        });
        body.on('change', '#faculty_id', function () {
            getSpecialitiesByFacultyForSelectAddEdit('#faculty_id')
        });



        //Functions
        function getFacultiesByCycleForSearch() {
            let loader = $('.custom-loader');
            let cyclesId = [];
            let facultySelect = $('#facultyIds');
            let specialitySelect = $('#specialityIds');

            $('#cycleIds :selected').each(function(i, selected) {
                cyclesId[i] = $(selected).val();
            });

            if(cyclesId.length === 0){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('secretary.faculties.getFacultiesByCycleForSelectSearch')}}',
                type: 'POST',
                async: true,
                data: {
                    cyclesId: JSON.stringify(cyclesId)
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    facultySelect.empty();
                    facultySelect.html(response.html);
                    facultySelect.trigger('chosen:updated');

                    specialitySelect.empty();
                    specialitySelect.trigger('chosen:updated');
                }
            });
        }

        function getSpecialitiesByFacultyForSelectSearch() {
            let loader = $('.custom-loader');
            let facultiesId = [];
            let specialitySelect = $('#specialityIds');

            $('#facultyIds :selected').each(function(i, selected) {
                facultiesId[i] = $(selected).val();
            });

            if(facultiesId.length === 0){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('secretary.specialities.getSpecialitiesByFacultyForSelectSearch')}}',
                type: 'POST',
                async: true,
                data: {
                    facultiesId: JSON.stringify(facultiesId)
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    specialitySelect.empty();
                    specialitySelect.html(response.html);
                    specialitySelect.trigger('chosen:updated');
                }
            });
        }

        function getFacultiesByCycleForSelectAddEdit(element) {
            let loader = $('.custom-loader');
            let cycleId = $.trim($(element).val());
            let facultySelect = $('#faculty_id');
            let specialitySelect = $('#speciality_id');

            if(cycleId === '' || cycleId === undefined || cycleId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('secretary.faculties.getFacultiesByCycleForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    cycleId: cycleId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    facultySelect.empty();
                    facultySelect.html(response.html);
                    facultySelect.trigger('chosen:updated');

                    specialitySelect.empty();
                    specialitySelect.trigger('chosen:updated');

                    let totalFaculties = $('#faculty_id > option').length;
                    if(totalFaculties === 1){
                        getSpecialitiesByFacultyForSelectAddEdit('#faculty_id');
                    }
                }
            });
        }

        function getSpecialitiesByFacultyForSelectAddEdit(element) {
            let loader = $('.custom-loader');
            let facultyId = $.trim($(element).val());
            let specialitySelect = $('#speciality_id');

            if(facultyId === '' || facultyId === undefined || facultyId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('secretary.specialities.getSpecialitiesByFacultyForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    facultyId: facultyId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    specialitySelect.empty();
                    specialitySelect.html(response.html);
                    specialitySelect.trigger('chosen:updated');
                }
            });
        }
    </script>
@endsection