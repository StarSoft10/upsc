<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="{{ route('secretary') }}"><img src="{{ asset('images/logo.png') }}" alt="Logo" style="width: 29%;"></a>
            <a class="navbar-brand hidden" href="{{ route('secretary') }}"><img src="{{ asset('images/logo.png') }}" alt="Logo"></a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="{{ route('secretary') }}">
                        <i class="menu-icon fa fa-dashboard"></i>
                        {{ __('aside.dashboard') }}
                    </a>
                </li>

                @php
                    $studiesRoutes = false;
                   if(\Request::is('*/faculties*') || \Request::is('*/specialities*') || \Request::is('*/groups*') ||
                      \Request::is('*/courses*')){
                       $studiesRoutes = true;
                    }
                @endphp

                <h3 class="menu-title">{{ __('aside.manage_studies') }}</h3>
                <li class="menu-item-has-children dropdown @if($studiesRoutes) show @endif">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-book"></i>{{ __('aside.studies') }}
                    </a>
                    <ul class="sub-menu children dropdown-menu @if($studiesRoutes) show @endif">
                        @can('view_faculty')
                            <li>
                                <i class="fa fa-building-o"></i>
                                <a href="{{ route('secretary.faculties.index') }}" class="@if(Request::is('*faculties*')) current @endif">
                                    {{ __('aside.faculties') }}
                                </a>
                            </li>
                        @endcan

                        @can('view_speciality')
                            <li>
                                <i class="fa fa-gavel"></i>
                                <a href="{{ route('secretary.specialities.index') }}" class="@if(Request::is('*specialities*')) current @endif">
                                    {{ __('aside.specialities') }}
                                </a>
                            </li>
                        @endcan

                        @can('view_group')
                            <li>
                                <i class="fa fa-users"></i>
                                <a href="{{ route('secretary.groups.index') }}" class="@if(Request::is('*groups*')) current @endif">
                                    {{ __('aside.groups') }}
                                </a>
                            </li>
                        @endcan

                        @can('view_course')
                            <li>
                                <i class="fa fa-book"></i>
                                <a href="{{ route('secretary.courses.index') }}" class="@if(Request::is('*courses*')) current @endif">
                                    {{ __('aside.courses') }}
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>

                {{--<li>--}}
                    {{--<a href="javascript:void(0)"> <i class="menu-icon ti-email"></i>Some page</a>--}}
                {{--</li>--}}
            </ul>
        </div>
    </nav>
</aside>