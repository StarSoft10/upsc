@extends('secretary::layouts.app')

@section('title')
    {{ __('pages.faculties') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            {!! Form::open(['route' => 'secretary.faculties.search', 'method' => 'GET']) !!}
                                <div class="row">
                                    <div class="col-md-3">
                                        {!! Form::text('name', $name, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.name'), 'style' => 'width: 100%;']) !!}
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::text('shortName', $shortName, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.short_name'), 'style' => 'width: 100%;']) !!}
                                    </div>
                                    <div class="col-md-3">
                                        <select data-placeholder="{{ __('pages.chose_cycle') }}" multiple class="standardSelect" name="cycleIds[]" id="cycleIds">
                                            @foreach($cycles as $key => $cycle)
                                                <option value="{{ $key }}" @if(in_array($key, $cycleIds)) selected @endif>{{ $cycle }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::submit(__('pages.search'), ['class' => 'btn btn-primary btn-sm']) !!}
                                        <a href="{{ route('secretary.faculties.index') }}" class="btn btn-info btn-sm" title="{{ __('pages.reset_filter') }}">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-2 text-right">
                            @can('create_faculty')
                                <button type="button" class="btn btn-success mb-1 btn-sm" data-toggle="modal" data-target="#addFaculty">
                                    {{ __('pages.add_faculty') }}
                                </button>
                            @endcan
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th>{{ __('pages.name') }}</th>
                                <th>{{ __('pages.short_name') }}</th>
                                <th>{{ __('pages.cycle') }}</th>
                                <th>{{ __('pages.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($faculties) > 0)
                                @foreach($faculties as $faculty)
                                    <tr>
                                        <td class="text-center"><span>{{ $faculty->name }}</span></td>
                                        <td class="text-center"><span>{{ $faculty->short_name }}</span></td>
                                        <td class="text-center"><span>{{ $faculty->cycle->name }}</span></td>
                                        <td class="text-center">
                                            @can('view_faculty')
                                                <a href="{{ route('secretary.faculties.show', $faculty->id) }}" class="btn-no-bg" title="{{ __('pages.show_edit') }}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            @endcan

                                            @can('delete_faculty')
                                                <a href="javascript:void(0)" data-facultyid="{{ $faculty->id }}" class="btn-no-bg delete-faculty" title="{{ __('pages.delete') }}">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="4">{{ __('messages.not_found_any_faculties') }}</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="table-footer">
                        {!! $faculties->render() !!}
                        <div class="total-found">{{ __('pages.total_found') }}: {{ $faculties->total() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @can('create_faculty')
        <div class="modal fade" id="addFaculty" role="dialog" aria-labelledby="addFaculty" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    {!! Form::open(['url' => route('secretary.faculties.store'), 'method' => 'post', 'autocomplete' => 'off']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.add_faculty') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('name', __('pages.name'), ['class' => 'control-label']) !!}
                                        {!! Form::text('name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('short_name', __('pages.short_name'), ['class' => 'control-label']) !!}
                                        {!! Form::text('short_name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('cycle_id', __('pages.cycle'), ['class' => 'control-label']) !!}
                                        <select data-placeholder="{{ __('pages.chose_cycle') }}" class="standardSelect" name="cycle_id" id="cycle_id">
                                            @foreach($cycles as $key => $cycle)
                                                <option value="{{ $key }}">{{ $cycle }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            {!! Form::submit(__('pages.add'), ['class' => 'btn btn-success btn-sm']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan

    @can('delete_faculty')
        <div class="modal fade" id="deleteFaculty" role="dialog" aria-labelledby="deleteFaculty" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['route' => 'secretary.faculties.delete']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.delete_faculty') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-info" role="alert">
                                <div class="alert-text">
                                    {{ __('pages.sure_to_delete_faculty?') }}
                                </div>
                            </div>
                            <input type="hidden" name="faculty_id">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            <button type="submit" class="btn btn-danger btn-sm">{{ __('pages.delete') }}</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan
@endsection

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('vendors/chosen/chosen.min.css') }}">
@endsection

@section('custom-scripts')
    <script src="{{ asset('vendors/chosen/chosen.jquery.min.js') }}"></script>
    <script>
        $(function() {
            $('.standardSelect').chosen({
                disable_search_threshold: 10,
                no_results_text: '{{ __('pages.nothing_found') }}',
                width: '100%'
            });
        });
    </script>
    <script>
        let body = $('body');

        body.on('click', '.delete-faculty', function () {
            let facultyId = $(this).attr('data-facultyid');
            let modal = $('#deleteFaculty');
            modal.find('input[name="faculty_id"]').val(facultyId);
            modal.modal('show');
        });

        $('#deleteFaculty').on('hidden.bs.modal', function () {
            $(this).find('input[name="faculty_id"]').val('');
        });
    </script>
@endsection