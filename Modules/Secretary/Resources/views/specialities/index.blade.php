@extends('secretary::layouts.app')

@section('title')
    {{ __('pages.specialities') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-9">
                            {!! Form::open(['route' => 'secretary.specialities.search', 'method' => 'GET']) !!}
                            <div class="row">
                                <div class="col-md-2">
                                    {!! Form::text('name', $name, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.name'), 'style' => 'width: 100%;']) !!}
                                </div>
                                <div class="col-md-2">
                                    {!! Form::text('shortName', $shortName, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.short_name'), 'style' => 'width: 100%;']) !!}
                                </div>
                                <div class="col-md-2">
                                    <select data-placeholder="{{ __('pages.chose_cycle') }}" multiple class="standardSelect" name="cycleIds[]" id="cycleIds">
                                        @foreach($cycles as $key => $cycle)
                                            <option value="{{ $key }}" @if(in_array($key, $cycleIds)) selected @endif>{{ $cycle }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select data-placeholder="{{ __('pages.chose_faculty') }}" multiple class="standardSelect" name="facultyIds[]" id="facultyIds">
                                        @if(count($facultyIds) > 0)
                                            @foreach($facultyIds as $id)
                                                <option value="{{ $id }}" selected >{{ $faculties[$id] }}</option>
                                            @endforeach
                                        @else
                                            <option value="0">{{ __('pages.chose_cycle') }}</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    {!! Form::submit(__('pages.search'), ['class' => 'btn btn-primary btn-sm']) !!}
                                    <a href="{{ route('secretary.specialities.index') }}" class="btn btn-info btn-sm" title="{{ __('pages.reset_filter') }}">
                                        <i class="fa fa-refresh"></i>
                                    </a>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-3 text-right">
                            @can('create_speciality')
                                <button type="button" class="btn btn-success mb-1 btn-sm" data-toggle="modal" data-target="#addSpeciality">
                                    {{ __('pages.add_speciality') }}
                                </button>
                            @endcan
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th>{{ __('pages.name') }}</th>
                                <th>{{ __('pages.short_name') }}</th>
                                <th>{{ __('pages.cycle') }}</th>
                                <th>{{ __('pages.faculty') }}</th>
                                <th>{{ __('pages.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(count($specialities) > 0)
                            @foreach($specialities as $speciality)
                                <tr>
                                    <td class="text-center"><span>{{ $speciality->name }}</span></td>
                                    <td class="text-center"><span>{{ $speciality->short_name }}</span></td>
                                    <td class="text-center"><span>{{ $speciality->cycle->name }}</span></td>
                                    <td class="text-center"><span>{{ $speciality->faculty->name }}</span></td>
                                    <td class="text-center">
                                        @can('view_speciality')
                                            <a href="{{ route('secretary.specialities.show', $speciality->id) }}" class="btn-no-bg" title="{{ __('pages.show_edit') }}">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        @endcan

                                        @can('delete_speciality')
                                            <a href="javascript:void(0)" data-specialityid="{{ $speciality->id }}" class="btn-no-bg delete-speciality" title="{{ __('pages.delete') }}">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="text-center">
                                <td colspan="5">{{ __('messages.not_found_any_specialities') }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    <div class="table-footer">
                        {!! $specialities->render() !!}
                        <div class="total-found">{{ __('pages.total_found') }}: {{ $specialities->total() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @can('create_speciality')
        <div class="modal fade" id="addSpeciality" role="dialog" aria-labelledby="addSpeciality" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    {!! Form::open(['url' => route('secretary.specialities.store'), 'method' => 'post', 'autocomplete' => 'off']) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.add_speciality') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('name', __('pages.name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('short_name', __('pages.short_name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('short_name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('cycle_id', __('pages.cycle'), ['class' => 'control-label']) !!}
                                    <select data-placeholder="{{ __('pages.chose_cycle') }}" class="standardSelect" name="cycle_id" id="cycle_id">
                                        @foreach($cycles as $key => $cycle)
                                            <option value="{{ $key }}">{{ $cycle }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('faculty_id', __('pages.faculty'), ['class' => 'control-label']) !!}
                                    <select data-placeholder="{{ __('pages.chose_faculty') }}" class="standardSelect" name="faculty_id" id="faculty_id">
                                        {{--@foreach($faculties as $key => $faculty)--}}
                                            {{--<option value="{{ $key }}">{{ $faculty }}</option>--}}
                                        {{--@endforeach--}}
                                        <option value="0">{{ __('pages.chose_cycle') }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        {!! Form::submit(__('pages.add'), ['class' => 'btn btn-success btn-sm']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan

    @can('delete_speciality')
        <div class="modal fade" id="deleteSpeciality" role="dialog" aria-labelledby="deleteSpeciality" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['route' => 'secretary.specialities.delete']) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.delete_speciality') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info" role="alert">
                            <div class="alert-text">
                                {{ __('pages.sure_to_delete_speciality?') }}
                            </div>
                        </div>
                        <input type="hidden" name="speciality_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        <button type="submit" class="btn btn-danger btn-sm">{{ __('pages.delete') }}</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan
@endsection

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('vendors/chosen/chosen.min.css') }}">
@endsection

@section('custom-scripts')
    <script src="{{ asset('vendors/chosen/chosen.jquery.min.js') }}"></script>
    <script>
        $(function() {
            $('.standardSelect').chosen({
                disable_search_threshold: 10,
                no_results_text: '{{ __('pages.nothing_found') }}',
                width: '100%'
            });
        });
    </script>
    <script>
        let body = $('body');

        body.on('click', '.delete-speciality', function () {
            let specialityId = $(this).attr('data-specialityid');
            let modal = $('#deleteSpeciality');
            modal.find('input[name="speciality_id"]').val(specialityId);
            modal.modal('show');
        });

        $('#deleteSpeciality').on('hidden.bs.modal', function () {
            $(this).find('input[name="speciality_id"]').val('');
        });

        body.on('change', '#cycleIds', function () {
            let cyclesId = [];
            let facultySelect = $('#facultyIds');

            $('#cycleIds :selected').each(function(i, selected) {
                cyclesId[i] = $(selected).val();
            });

            if(cyclesId.length === 0){
                return;
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('secretary.faculties.getFacultiesByCycleForSelectSearch')}}',
                type: 'POST',
                data: {
                    cyclesId: JSON.stringify(cyclesId)
                },
                success: function(response){
                    facultySelect.empty();
                    facultySelect.html(response.html);
                    facultySelect.trigger('chosen:updated');
                }
            });
        });

        body.on('change', '#cycle_id', function () {
            let cycleId = $.trim($(this).val());
            let facultySelect = $('#faculty_id');

            if(cycleId === '' || cycleId === undefined || cycleId == null){
                return;
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('secretary.faculties.getFacultiesByCycleForSelectAddEdit')}}',
                type: 'POST',
                data: {
                    cycleId: cycleId
                },
                success: function(response){
                    facultySelect.empty();
                    facultySelect.html(response.html);
                    facultySelect.trigger('chosen:updated');
                }
            });
        });
    </script>
@endsection