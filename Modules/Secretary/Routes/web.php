<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

Route::group([
    'prefix' => LaravelLocalization::setLocale() . '/secretary',
    'middleware' => ['auth', 'role:secretary', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'localize']
], function () {
    Route::get('/', 'SecretaryController@index')->name('secretary');
    Route::get('profile', 'SecretaryController@show')->name('secretary.profile');
    Route::patch('update', 'SecretaryController@update')->name('secretary.update');
    Route::patch('changePhoto', 'SecretaryController@changePhoto')->name('secretary.changePhoto');
    Route::get('not-found', 'SecretaryController@notFound')->name('secretary.not-found');


    /*
     * Studies routes
     */
    Route::group(['prefix' => 'faculties'], function () {
        Route::post('delete', 'FacultyController@delete')->name('secretary.faculties.delete');
        Route::get('search', 'FacultyController@search')->name('secretary.faculties.search');
        Route::post('getFacultiesByCycleForSelectSearch', 'FacultyController@getFacultiesByCycleForSelectSearch')->name('secretary.faculties.getFacultiesByCycleForSelectSearch');
        Route::post('getFacultiesByCycleForSelectAddEdit', 'FacultyController@getFacultiesByCycleForSelectAddEdit')->name('secretary.faculties.getFacultiesByCycleForSelectAddEdit');
    });
    Route::resource('faculties', 'FacultyController', ['names' => [
        'index' => 'secretary.faculties.index',
        'store' => 'secretary.faculties.store',
        'show' => 'secretary.faculties.show',
        'update' => 'secretary.faculties.update'
    ]]);


    Route::group(['prefix' => 'specialities'], function () {
        Route::post('delete', 'SpecialityController@delete')->name('secretary.specialities.delete');
        Route::get('search', 'SpecialityController@search')->name('secretary.specialities.search');
        Route::post('getSpecialitiesByFacultyForSelectSearch', 'SpecialityController@getSpecialitiesByFacultyForSelectSearch')->name('secretary.specialities.getSpecialitiesByFacultyForSelectSearch');
        Route::post('getSpecialitiesByFacultyForSelectAddEdit', 'SpecialityController@getSpecialitiesByFacultyForSelectAddEdit')->name('secretary.specialities.getSpecialitiesByFacultyForSelectAddEdit');
    });
    Route::resource('specialities', 'SpecialityController', ['names' => [
        'index' => 'secretary.specialities.index',
        'store' => 'secretary.specialities.store',
        'show' => 'secretary.specialities.show',
        'update' => 'secretary.specialities.update'
    ]]);


    Route::group(['prefix' => 'groups'], function () {
        Route::post('delete', 'GroupController@delete')->name('secretary.groups.delete');
        Route::get('search', 'GroupController@search')->name('secretary.groups.search');
        Route::post('getGroupsBySpecialityForSelectSearch', 'GroupController@getGroupsBySpecialityForSelectSearch')->name('secretary.groups.getGroupsBySpecialityForSelectSearch');
        Route::post('getGroupsBySpecialityForSelectAddEdit', 'GroupController@getGroupsBySpecialityForSelectAddEdit')->name('secretary.groups.getGroupsBySpecialityForSelectAddEdit');
    });
    Route::resource('groups', 'GroupController', ['names' => [
        'index' => 'secretary.groups.index',
        'store' => 'secretary.groups.store',
        'show' => 'secretary.groups.show',
        'update' => 'secretary.groups.update'
    ]]);


    Route::group(['prefix' => 'courses'], function () {
        Route::post('delete', 'CourseController@delete')->name('secretary.courses.delete');
        Route::get('search', 'CourseController@search')->name('secretary.courses.search');
    });
    Route::resource('courses', 'CourseController', ['names' => [
        'index' => 'secretary.courses.index',
        'store' => 'secretary.courses.store',
        'show' => 'secretary.courses.show',
        'update' => 'secretary.courses.update'
    ]]);
});