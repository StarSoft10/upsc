<?php

namespace Modules\Secretary\Http\Controllers;

use App\Entities\Cycle;
use App\Entities\Faculty;
use App\Entities\Speciality;
use App\Helpers\StudyHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\AppHelper;

class SpecialityController extends Controller
{
    public $helper;

    public $studyHelper;

    public function __construct()
    {
        $this->middleware(['auth', 'role:secretary']);
        $this->middleware('check_if_blocked');
        $this->helper = new AppHelper();
        $this->studyHelper = new StudyHelper();
    }

    public function index(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_speciality')) {
            return redirect()->route('secretary.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';
        $cycleIds = !empty($request->cycleIds) ? $this->helper->removeValueFromArray($request->cycleIds, 0) : [];
        $facultyIds = !empty($request->facultyIds) ? $this->helper->removeValueFromArray($request->facultyIds, 0) : [];

        $specialities = Speciality::where('deleted', 0)->orderBy('id', 'ASC')->paginate(10);
        $specialities->appends([
            'name' => $name,
            'shortName' => $shortName,
            'cycleIds' => $cycleIds,
            'facultyIds' => $facultyIds
        ]);

        $cycles = Cycle::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $faculties = Faculty::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        return view('secretary::specialities.index', compact('specialities', 'cycles', 'faculties', 'name',
            'shortName', 'cycleIds', 'facultyIds'));
    }

    public function search(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_speciality')) {
            return redirect()->route('secretary.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';
        $cycleIds = !empty($request->cycleIds) ? $this->helper->removeValueFromArray($request->cycleIds, 0) : [];
        $facultyIds = !empty($request->facultyIds) ? $this->helper->removeValueFromArray($request->facultyIds, 0) : [];

        $specialities = Speciality::where('deleted', 0)->where(function ($query) use ($name, $shortName, $cycleIds, $facultyIds) {
            if ($name !== '') {
                $query->where('name', 'LIKE', '%' . trim($name) . '%');
            }
            if ($shortName !== '') {
                $query->where('short_name', 'LIKE', '%' . trim($shortName) . '%');
            }
            if (count($cycleIds) > 0) {
                $query->whereIn('cycle_id', $cycleIds);
            }
            if (count($facultyIds) > 0) {
                $query->whereIn('faculty_id', $facultyIds);
            }
        })->orderBy('id', 'ASC')->paginate(10);
        $specialities->appends([
            'name' => $name,
            'shortName' => $shortName,
            'cycleIds' => $cycleIds,
            'facultyIds' => $facultyIds
        ]);

        $cycles = Cycle::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $faculties = Faculty::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        return view('secretary::specialities.index', compact('specialities', 'cycles', 'faculties',
            'name', 'shortName', 'cycleIds', 'facultyIds'));
    }

    public function store(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('create_speciality')) {
            return redirect()->route('secretary.not-found');
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('specialities')->where(function ($query) use($request) {
                    $query->where('deleted', 0);
                    $query->where('cycle_id', $request->cycle_id);
                    $query->where('faculty_id', $request->faculty_id);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('specialities')->where(function ($query) use($request) {
                    $query->where('deleted', 0);
                    $query->where('cycle_id', $request->cycle_id);
                    $query->where('faculty_id', $request->faculty_id);
                })
            ],
            'cycle_id' => 'required|numeric|gt:0',
            'faculty_id' => 'required|numeric|gt:0'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $speciality = Speciality::create([
            'cycle_id' => $request->cycle_id,
            'faculty_id' => $request->faculty_id,
            'name' => trim($request->name),
            'short_name' => trim($request->short_name)
        ]);

        $this->helper->addLog('secretary', Auth::user()->secretary->id, 'specialities', 1, 'DB id: [' . $speciality->id . ']');

        return back()->with('success', __('messages.speciality_created_successfully'));
    }

    public function show($id)
    {
        if (!Auth::user()->hasPermissionTo('view_speciality')) {
            return redirect()->route('secretary.not-found');
        }

        $speciality = Speciality::where('id', $id)->where('deleted', 0)->first();

        if (!$speciality) {
            return back()->with('warning', __('messages.speciality_not_found'));
        }

        $cycles = Cycle::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $faculties = Faculty::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $faculty = $speciality->faculty->name;

        return view('secretary::specialities.show', compact('speciality', 'cycles', 'faculties', 'faculty'));
    }

    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermissionTo('edit_speciality')) {
            return redirect()->route('secretary.not-found');
        }

        $speciality = Speciality::where('id', $id)->where('deleted', 0)->first();
        if (!$speciality) {
            return back()->with('warning', __('messages.speciality_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('specialities')->where(function ($query) use ($id, $request) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                    $query->where('cycle_id', $request->cycle_id);
                    $query->where('faculty_id', $request->faculty_id);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('specialities')->where(function ($query) use ($id, $request) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                    $query->where('cycle_id', $request->cycle_id);
                    $query->where('faculty_id', $request->faculty_id);
                })
            ],
            'cycle_id' => 'required|numeric|gt:0',
            'faculty_id' => 'required|numeric|gt:0'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        Speciality::where('id', $id)->update([
            'cycle_id' => $request->cycle_id,
            'faculty_id' => $request->faculty_id,
            'name' => trim($request->name),
            'short_name' => trim($request->short_name),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('secretary', Auth::user()->secretary->id, 'specialities', 2, 'DB id: [' . $id . ']');

        return redirect()->route('secretary.specialities.index')->with('success', __('messages.speciality_updated_successfully'));
    }

    public function delete(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_speciality')) {
            return redirect()->route('secretary.not-found');
        }

        $validator = Validator::make($request->all(), [
            'speciality_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $speciality = Speciality::where('id', $request->speciality_id)->where('deleted', 0)->first();
        if (!$speciality) {
            return back()->with('warning', __('messages.speciality_not_found'));
        }

        Speciality::where('id', $request->speciality_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('secretary', Auth::user()->secretary->id, 'specialities', 3, 'DB id: [' . $request->speciality_id . ']');

        return back()->with('success', __('messages.speciality_deleted_successfully'));
    }

    public function getSpecialitiesByFacultyForSelectSearch(Request $request)
    {
        return $this->studyHelper->getSpecialitiesByFacultyForSelectSearch($request);
    }

    public function getSpecialitiesByFacultyForSelectAddEdit(Request $request)
    {
        return $this->studyHelper->getSpecialitiesByFacultyForSelectAddEdit($request);
    }
}
