@extends('chair::layouts.app')

@section('title')
    {{ __('pages.borderous') }}
@endsection

@section('content')
    <div class="custom-loader" style="display: none"></div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            {!! Form::open(['route' => 'chair.borderous.search', 'method' => 'GET']) !!}
                                <div class="row" style="margin-bottom: 10px">
                                    <div class="col-md-3">
                                        {!! Form::text('name', $name, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.name')]) !!}
                                    </div>
                                    <div class="col-md-3">
                                        <select data-placeholder="{{ __('pages.chose_borderou_type') }}" multiple class="standardSelect" name="borderouTypeIds[]" id="borderouTypeIds">
                                            @foreach($borderouTypes as $key => $borderouType)
                                                <option value="{{ $key }}" @if(in_array($key, $borderouTypeIds)) selected @endif>{{ $borderouType }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <select data-placeholder="{{ __('pages.chose_cycle') }}" multiple class="standardSelect" name="cycleIds[]" id="cycleIds">
                                            @foreach($cycles as $key => $cycle)
                                                <option value="{{ $key }}" @if(in_array($key, $cycleIds)) selected @endif>{{ $cycle }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select data-placeholder="{{ __('pages.chose_faculty') }}" multiple class="standardSelect" name="facultyIds[]" id="facultyIds">
                                            @if(count($facultyIds) > 0)
                                                @foreach($facultyIds as $id)
                                                    <option value="{{ $id }}" selected >{{ $faculties[$id] }}</option>
                                                @endforeach
                                            @else
                                                <option value="0">{{ __('pages.chose_cycle') }}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="row" style="margin-bottom: 10px">
                                    <div class="col-md-4">
                                        <select data-placeholder="{{ __('pages.chose_speciality') }}" multiple class="standardSelect" name="specialityIds[]" id="specialityIds">
                                            @if(count($specialityIds) > 0)
                                                @foreach($specialityIds as $id)
                                                    <option value="{{ $id }}" selected >{{ $specialities[$id] }}</option>
                                                @endforeach
                                            @else
                                                <option value="0">{{ __('pages.chose_faculty') }}</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select data-placeholder="{{ __('pages.chose_group') }}" multiple class="standardSelect" name="groupIds[]" id="groupIds">
                                            @if(count($groupIds) > 0)
                                                @foreach($groupIds as $id)
                                                    <option value="{{ $id }}" selected >{{ $groups[$id] }}</option>
                                                @endforeach
                                            @else
                                                <option value="0">{{ __('pages.chose_speciality') }}</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select data-placeholder="{{ __('pages.chose_course') }}" multiple class="standardSelect" name="courseIds[]" id="courseIds">
                                            @foreach($courses as $key => $course)
                                                <option value="{{ $key }}" @if(in_array($key, $courseIds)) selected @endif>{{ $course }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row" style="margin-bottom: 10px">
                                    <div class="col-md-3">
                                        <select data-placeholder="{{ __('pages.chose_year_of_study') }}" multiple class="standardSelect" name="yearOfStudyIds[]" id="yearOfStudyIds">
                                            @foreach($yearOfStudies as $key => $yearOfStudy)
                                                <option value="{{ $key }}" @if(in_array($key, $yearOfStudyIds)) selected @endif>{{ $yearOfStudy }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select data-placeholder="{{ __('pages.chose_semester') }}" multiple class="standardSelect" name="semesterIds[]" id="semesterIds">
                                            @foreach($semesters as $key => $semester)
                                                <option value="{{ $key }}" @if(in_array($key, $semesterIds)) selected @endif>{{ $semester }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        {!! Form::text('yearOfAdmission', $yearOfAdmission, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.borderou_year')]) !!}
                                    </div>
                                    <div class="col-md-4">
                                        <select data-placeholder="{{ __('pages.chose_teacher') }}" multiple class="standardSelect" name="teacherIds[]" id="teacherIds">
                                            @foreach($teachers as $key => $teacher)
                                                <option value="{{ $key }}" @if(in_array($key, $teacherIds)) selected @endif>{{ $teacher }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        {!! Form::submit(__('pages.search'), ['class' => 'btn btn-primary btn-sm']) !!}
                                        <a href="{{ route('chair.borderous.index') }}" class="btn btn-info btn-sm" title="{{ __('pages.reset_filter') }}">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-2 text-right">
                            @can('create_borderou')
                                <button type="button" class="btn btn-success mb-1 btn-sm" data-toggle="modal" data-target="#addBorderou">
                                    {{ __('pages.add_borderou') }}
                                </button>
                            @endcan
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-sm">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th>{{ __('pages.name') }}</th>
                                <th>{{ __('pages.cycle') }}</th>
                                <th>{{ __('pages.faculty') }}</th>
                                <th>{{ __('pages.speciality') }}</th>
                                <th>{{ __('pages.group') }}</th>
                                <th>{{ __('pages.year_of_study') }}</th>
                                <th>{{ __('pages.semester') }}</th>
                                <th>{{ __('pages.course') }}</th>
                                <th>{{ __('pages.teacher') }}</th>
                                <th>{{ __('pages.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if(count($borderous) > 0)
                            @foreach($borderous as $borderou)
                                <tr>
                                    <td class="text-center"><span>{{ $borderou->name }}</span></td>
                                    <td class="text-center"><span>{{ $borderou->cycle->name }}</span></td>
                                    <td class="text-center"><span>{{ $borderou->faculty->name }}</span></td>
                                    <td class="text-center"><span>{{ $borderou->speciality->name }}</span></td>
                                    <td class="text-center"><span>{{ $borderou->group->name }}</span></td>
                                    <td class="text-center"><span>{{ $borderou->yearOfStudy->name }}</span></td>
                                    <td class="text-center"><span>{{ $borderou->semester->name }}</span></td>
                                    <td class="text-center"><span>{{ $borderou->course->name }}</span></td>
                                    <td class="text-center"><span>{{ $borderou->teacher->first_name }} {{ $borderou->teacher->last_name }}</span></td>
                                    <td class="text-center">
                                        @can('edit_borderou')
                                            <a href="{{ route('chair.borderous.show', $borderou->id) }}" class="btn-no-bg" title="{{ __('pages.edit') }}">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        @endcan

                                        @can('view_borderou')
                                            <a href="javascript:void(0)" data-borderouid="{{ $borderou->id }}" class="btn-no-bg view-borderou" title="{{ __('pages.view') }}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        @endcan

                                        @can('create_borderou_note')
                                            <a href="javascript:void(0)" data-borderouid="{{ $borderou->id }}" class="btn-no-bg edit-borderou-note" title="{{ __('pages.update_borderou_note') }}">
                                                <i class="fa fa-file-text"></i>
                                            </a>
                                        @endcan

                                        @can('delete_borderou')
                                            <a href="javascript:void(0)" data-borderouid="{{ $borderou->id }}" class="btn-no-bg delete-borderou" title="{{ __('pages.delete') }}">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                        @endcan

                                        @can('export_borderou_note')
                                            <a href="javascript:void(0)" data-borderouid="{{ $borderou->id }}" class="btn-no-bg export-borderou-notes-pdf" title="{{ __('pages.export') }}">
                                                <i class="fa fa-file-pdf"></i>
                                            </a>
                                            <a href="javascript:void(0)" data-borderouid="{{ $borderou->id }}" class="btn-no-bg export-borderou-notes-excel" title="{{ __('pages.export') }}">
                                                <i class="fa fa-file-excel"></i>
                                            </a>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="text-center">
                                <td colspan="10">{{ __('messages.not_found_any_borderous') }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    <div class="table-footer">
                        {!! $borderous->render() !!}
                        <div class="total-found">{{ __('pages.total_found') }}: {{ $borderous->total() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @can('create_borderou')
        <div class="modal fade" id="addBorderou" role="dialog" aria-labelledby="addBorderou" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    {!! Form::open(['url' => route('chair.borderous.store'), 'method' => 'post', 'autocomplete' => 'off']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.add_borderou') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('name', __('pages.borderou_name'), ['class' => 'control-label']) !!}
                                        {!! Form::text('name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('cycle_id', __('pages.cycle'), ['class' => 'control-label']) !!}
                                        <select data-placeholder="{{ __('pages.chose_cycle') }}" class="standardSelect"
                                                name="cycle_id" id="cycle_id" required title="{{ __('pages.cycle') }}">
                                            <option value="0">{{ __('pages.chose_cycle') }}</option>
                                            @foreach($cycles as $key => $cycle)
                                                <option value="{{ $key }}">{{ $cycle }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('faculty_id', __('pages.faculty'), ['class' => 'control-label']) !!}
                                        <select data-placeholder="{{ __('pages.chose_faculty') }}" class="standardSelect"
                                                name="faculty_id" id="faculty_id" required title="{{ __('pages.faculty') }}">
                                            <option value="0">{{ __('pages.chose_cycle') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('speciality_id', __('pages.speciality'), ['class' => 'control-label']) !!}
                                        <select data-placeholder="{{ __('pages.chose_speciality') }}" class="standardSelect"
                                                name="speciality_id" id="speciality_id" required title="{{ __('pages.speciality') }}">
                                            <option value="0">{{ __('pages.chose_faculty') }}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('group_id', __('pages.group'), ['class' => 'control-label']) !!}
                                        <select data-placeholder="{{ __('pages.chose_group') }}" class="standardSelect"
                                                name="group_id" id="group_id" required title="{{ __('pages.group') }}">
                                            <option value="0">{{ __('pages.chose_speciality') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('year_of_study_id', __('pages.year_of_study'), ['class' => 'control-label']) !!}
                                        <select data-placeholder="{{ __('pages.year_of_study') }}" class="standardSelect"
                                                name="year_of_study_id" id="year_of_study_id" required title="{{ __('pages.year_of_study') }}">
                                            @foreach($yearOfStudies as $key => $yearOfStudy)
                                                <option value="{{ $key }}">{{ $yearOfStudy }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('semester_id', __('pages.semester'), ['class' => 'control-label']) !!}
                                        <select data-placeholder="{{ __('pages.semester') }}" class="standardSelect"
                                                name="semester_id" id="semester_id" required title="{{ __('pages.semester') }}">
                                            @foreach($semesters as $key => $semester)
                                                <option value="{{ $key }}">{{ $semester }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('course_id', __('pages.course'), ['class' => 'control-label']) !!}
                                        <select data-placeholder="{{ __('pages.course') }}" class="standardSelect"
                                                name="course_id" id="course_id" required title="{{ __('pages.course') }}">
                                            @foreach($courses as $key => $course)
                                                <option value="{{ $key }}">{{ $course }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('teacher_id', __('pages.teacher'), ['class' => 'control-label']) !!}
                                        <select data-placeholder="{{ __('pages.teacher') }}" class="standardSelect"
                                                name="teacher_id" id="teacher_id" required title="{{ __('pages.teacher') }}">
                                            @foreach($teachers as $key => $teacher)
                                                <option value="{{ $key }}">{{ $teacher }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::label('year_of_admission', __('pages.borderou_year'), ['class' => 'control-label']) !!}
                                        {!! Form::number('year_of_admission', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::label('total_hours', __('pages.total_hours'), ['class' => 'control-label']) !!}
                                        {!! Form::number('total_hours', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::label('credits', __('pages.credits'), ['class' => 'control-label']) !!}
                                        {!! Form::number('credits', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('course_title', __('pages.course_title'), ['class' => 'control-label']) !!}
                                        {!! Form::text('course_title', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('borderou_type_id', __('pages.borderou_type'), ['class' => 'control-label']) !!}
                                        <select data-placeholder="{{ __('pages.borderou_type') }}" class="standardSelect"
                                                name="borderou_type_id" id="borderou_type_id" required title="{{ __('pages.borderou_type') }}">
                                            <option value="0">{{ __('pages.chose_borderou_type') }}</option>
                                            @foreach($borderouTypes as $key => $borderouType)
                                                <option value="{{ $key }}">{{ $borderouType }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('exam_date', __('pages.exam_date'), ['class' => 'control-label']) !!}
                                        {!! Form::date('exam_date', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        {!! Form::label('date_of_liquidation', __('pages.date_of_liquidation'), ['class' => 'control-label']) !!}
                                        {!! Form::date('date_of_liquidation', '', ['class' => 'input-sm form-control-sm form-control']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            {!! Form::submit(__('pages.add'), ['class' => 'btn btn-success btn-sm']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan

    @can('delete_borderou')
        <div class="modal fade" id="deleteBorderou" role="dialog" aria-labelledby="deleteBorderou" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['route' => 'chair.borderous.delete']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.delete_borderou') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-info" role="alert">
                                <div class="alert-text">
                                    {{ __('pages.sure_to_delete_borderou?') }}
                                </div>
                            </div>
                            <input type="hidden" name="borderou_id">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            <button type="submit" class="btn btn-danger btn-sm">{{ __('pages.delete') }}</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan

    @can('view_borderou')
        <div class="modal fade" id="viewBorderou" role="dialog" aria-labelledby="viewBorderou" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.view_borderou') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body modal-scroll">
                        <div class="alert alert-info" role="alert" style="display: none" id="info_message">
                            <div class="alert-text"></div>
                        </div>

                        <div class="row" id="borderou_info_block">
                            <div class="col-md-6">
                                <ul style="list-style: none">
                                    <li>
                                        <span style="font-weight: bold">{{ __('pages.name') }}:</span>
                                        <span id="b_info_name"></span>
                                    </li>
                                    <li>
                                        <span style="font-weight: bold">{{ __('pages.cycle') }}:</span>
                                        <span id="b_info_cycle"></span>
                                    </li>
                                    <li>
                                        <span style="font-weight: bold">{{ __('pages.faculty') }}:</span>
                                        <span id="b_info_faculty"></span>
                                    </li>
                                    <li>
                                        <span style="font-weight: bold">{{ __('pages.speciality') }}:</span>
                                        <span id="b_info_speciality"></span>
                                    </li>
                                    <li>
                                        <span style="font-weight: bold">{{ __('pages.group') }}:</span>
                                        <span id="b_info_group"></span>
                                    </li>
                                    <li>
                                        <span style="font-weight: bold">{{ __('pages.year_of_study') }}:</span>
                                        <span id="b_info_yearOfStudy"></span>
                                    </li>
                                    <li>
                                        <span style="font-weight: bold">{{ __('pages.semester') }}:</span>
                                        <span id="b_info_semester"></span>
                                    </li>
                                    <li>
                                        <span style="font-weight: bold">{{ __('pages.course') }}:</span>
                                        <span id="b_info_course"></span>
                                    </li>
                                    <li>
                                        <span style="font-weight: bold">{{ __('pages.teacher') }}:</span>
                                        <span id="b_info_teacher"></span>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-md-6">
                                <ul style="list-style: none">
                                    <li>
                                        <span style="font-weight: bold">{{ __('pages.borderou_year') }}:</span>
                                        <span id="b_info_yearOfAdmission"></span>
                                    </li>
                                    <li>
                                        <span style="font-weight: bold">{{ __('pages.total_hours') }}:</span>
                                        <span id="b_info_totalHours"></span>
                                    </li>
                                    <li>
                                        <span style="font-weight: bold">{{ __('pages.credits') }}:</span>
                                        <span id="b_info_credits"></span>
                                    </li>
                                    <li>
                                        <span style="font-weight: bold">{{ __('pages.course_title') }}:</span>
                                        <span id="b_info_courseTitle"></span>
                                    </li>
                                    <li>
                                        <span style="font-weight: bold">{{ __('pages.borderou_type') }}:</span>
                                        <span id="b_info_borderouType"></span>
                                    </li>
                                    <li>
                                        <span style="font-weight: bold">{{ __('pages.exam_date') }}:</span>
                                        <span id="b_info_examDate"></span>
                                    </li>
                                    <li>
                                        <span style="font-weight: bold">{{ __('pages.date_of_liquidation') }}:</span>
                                        <span id="b_info_dateOfLiquidation"></span>
                                    </li>
                                    <li>
                                        <span style="font-weight: bold">{{ __('pages.finished') }}:</span>
                                        <span id="b_info_finished"></span>
                                    </li>
                                    <li>
                                        <span style="font-weight: bold">{{ __('pages.owner') }}:</span>
                                        <span id="b_info_owner"></span>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-md-12 notes"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                    </div>
                </div>
            </div>
        </div>
    @endcan

    @can('edit_borderou_note')
        <div class="modal fade" id="updateBorderouNote" role="dialog" aria-labelledby="updateBorderouNote" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    {!! Form::open(['route' => 'chair.borderous.updateBorderouNote']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.update_borderou_note') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body modal-scroll">
                            <div class="alert alert-info" role="alert" style="display: none" id="info_message_update_note">
                                <div class="alert-text"></div>
                            </div>
                            <div class="row" id="borderou_note_info_block"></div>
                            <input type="hidden" name="borderou_id">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            <button type="submit" class="btn btn-success btn-sm">{{ __('pages.save') }}</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div class="modal fade" id="confirmDeleteBorderouNote" role="dialog" aria-labelledby="confirmDeleteBorderouNote" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.delete_borderou_note') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info" role="alert">
                            <div class="alert-text">
                                {{ __('pages.sure_to_delete_borderou_note?') }}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        <button type="button" class="btn btn-danger btn-sm delete-borderou-note">{{ __('pages.delete') }}</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addStudentBorderouNote" role="dialog" aria-labelledby="addStudentBorderouNote" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.add_students') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        <button type="button" class="btn btn-success btn-sm add-borderou-note">{{ __('pages.add') }}</button>
                    </div>
                </div>
            </div>
        </div>
    @endcan

@endsection

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('vendors/chosen/chosen.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
    <style>
        .modal-xl {
            max-width: 1200px
        }
        #updateBorderouNote .switch.switch-text .switch-label {
            background-color: #ff142a;
        }
        #updateBorderouNote .switch {
            margin-top: 0.7rem;
        }
    </style>
@endsection

@section('custom-scripts')
    <script src="{{ asset('vendors/chosen/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>

    @if (LaravelLocalization::getCurrentLocale() == 'ro')
        <script src="{{ asset('js/bootstrap-datepicker.ro.min.js') }}"></script>
    @endif
    @if(LaravelLocalization::getCurrentLocale() == 'ru')
        <script src="{{ asset('js/bootstrap-datepicker.ru.min.js') }}"></script>
    @endif

    <script>
        $(function() {
            $('.standardSelect').chosen({
                disable_search_threshold: 10,
                no_results_text: '{{ __('pages.nothing_found') }}',
                width: '100%'
            });

            $('.datepicker').datepicker({
                format: 'yyyy/mm/dd',
                language: '{{ LaravelLocalization::getCurrentLocale() }}'
            });
        });
    </script>
    <script>
        function showHideCodeBlock() {
            let modal = $('#updateBorderouNote');

            if ($('#checkbox_code').is(':checked')){
                $('#code_block').show();

                modal.find('#borderou_note_info_block table input').each(function () {
                    $(this).removeAttr('disabled readonly')
                });
                modal.find('.modal-footer .btn-success').attr('disabled', false);
            } else {
                $('#code_block').hide();
                modal.find('#borderou_note_info_block table input').each(function () {
                    $(this).attr('disabled', true);
                    $(this).attr('readonly', true);
                });
                modal.find('.modal-footer .btn-success').attr('disabled', true);
            }
        }

        function getBorderouNotesById(borderouId) {
            let infoMessage = $('#info_message_update_note');
            let borderouInfoBlock = $('#borderou_note_info_block');
            let modal = $('#updateBorderouNote');
            modal.find('input[name="borderou_id"]').val(borderouId);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                async: true,
                url: '{{ route('chair.borderous.getBorderouNote')}}',
                type: 'POST',
                data: {
                    borderou_id: borderouId
                },
                success: function (response) {
                    if(response.type === 'warning'){
                        borderouInfoBlock.hide();
                        infoMessage.find('.alert-text').html(response.message);
                        infoMessage.show();
                        modal.find('.modal-footer .btn-success').attr('disabled', true);
                    } else {
                        infoMessage.hide();
                        infoMessage.find('.alert-text').html('');
                        borderouInfoBlock.html(response.html);
                        borderouInfoBlock.show();

                        $('#addStudentBorderouNote .modal-body').html(response.studentsHtml);
                        $('.standardSelect').chosen({
                            disable_search_threshold: 10,
                            no_results_text: '{{ __('pages.nothing_found') }}',
                            width: '100%'
                        });

                        if(response.disableButton == 1){
                            modal.find('.modal-footer .btn-success').attr('disabled', true);
                        }
                    }
                }
            });
        }

        let body = $('body');

        body.on('click', '.delete-borderou', function () {
            let borderouId = $(this).attr('data-borderouid');
            let modal = $('#deleteBorderou');
            modal.find('input[name="borderou_id"]').val(borderouId);
            modal.modal('show');
        });

        body.on('click', '.view-borderou', function () {
            let borderouId = $(this).attr('data-borderouid');
            let infoMessage = $('#info_message');
            let borderouInfoBlock = $('#borderou_info_block');
            let modal = $('#viewBorderou');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                async: true,
                url: '{{ route('chair.borderous.getBorderouForView')}}',
                type: 'POST',
                data: {
                    borderou_id: borderouId
                },
                success: function (response) {
                    if(response.type === 'warning'){
                        borderouInfoBlock.hide();
                        infoMessage.show();
                        infoMessage.find('.alert-text').html(response.message);
                    } else {
                        let borderouInfo = response.borderouInfo;
                        let borderouInfoNote = response.borderouInfoNote;
                        infoMessage.hide();
                        infoMessage.find('.alert-text').html('');
                        borderouInfoBlock.show();
                        $('#b_info_name').html(borderouInfo.name);
                        $('#b_info_cycle').html(borderouInfo.cycleName);
                        $('#b_info_faculty').html(borderouInfo.faculty);
                        $('#b_info_speciality').html(borderouInfo.speciality);
                        $('#b_info_group').html(borderouInfo.group);
                        $('#b_info_yearOfStudy').html(borderouInfo.yearOfStudy);
                        $('#b_info_semester').html(borderouInfo.semester);
                        $('#b_info_course').html(borderouInfo.course);
                        $('#b_info_teacher').html(borderouInfo.teacher);
                        $('#b_info_yearOfAdmission').html(borderouInfo.yearOfAdmission);
                        $('#b_info_totalHours').html(borderouInfo.totalHours);
                        $('#b_info_credits').html(borderouInfo.credits);
                        $('#b_info_courseTitle').html(borderouInfo.courseTitle);
                        $('#b_info_borderouType').html(borderouInfo.borderouType);
                        $('#b_info_examDate').html(borderouInfo.examDate);
                        $('#b_info_dateOfLiquidation').html(borderouInfo.dateOfLiquidation);
                        $('#b_info_finished').html(borderouInfo.finished);
                        $('#b_info_owner').html(borderouInfo.owner);

                        $('.notes').html(borderouInfoNote);
                    }
                }, complete: function () {
                    modal.modal('show');
                }
            });


        });

        body.on('click', '.edit-borderou-note', function () {
            let borderouId = $(this).attr('data-borderouid');
            let infoMessage = $('#info_message_update_note');
            let borderouInfoBlock = $('#borderou_note_info_block');
            let modal = $('#updateBorderouNote');
            modal.find('input[name="borderou_id"]').val(borderouId);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                async: true,
                url: '{{ route('chair.borderous.getBorderouNote')}}',
                type: 'POST',
                data: {
                    borderou_id: borderouId
                },
                success: function (response) {
                    if(response.type === 'warning'){
                        borderouInfoBlock.hide();
                        infoMessage.find('.alert-text').html(response.message);
                        infoMessage.show();
                        modal.find('.modal-footer .btn-success').attr('disabled', true);
                    } else {
                        infoMessage.hide();
                        infoMessage.find('.alert-text').html('');
                        borderouInfoBlock.html(response.html);
                        borderouInfoBlock.show();

                        $('#addStudentBorderouNote .modal-body').html(response.studentsHtml);
                        $('.standardSelect').chosen({
                            disable_search_threshold: 10,
                            no_results_text: '{{ __('pages.nothing_found') }}',
                            width: '100%'
                        });

                        if(response.disableButton == 1){
                            modal.find('.modal-footer .btn-success').attr('disabled', true);
                        }
                    }
                }, complete: function () {
                    modal.modal('show');
                }
            });
        });

        $('.modal').on('shown.bs.modal', function () {
            $('.modal.show').each(function (index) {
                $(this).css('z-index', 1101 + index*2);
            });
            $('.modal-backdrop').each(function (index) {
                $(this).css('z-index', 1101 + index*2-1);
            });
        });

        let confirmDeleteBorderouNote = $('#confirmDeleteBorderouNote');

        confirmDeleteBorderouNote.on('shown.bs.modal', function (e) {
            let $invoker = $(e.relatedTarget);
            $('.delete-borderou-note').attr('data-borderou-note-id', $invoker.attr('data-borderou-note-id'))
        });

        confirmDeleteBorderouNote.on('hidden.bs.modal', function () {
            $('.delete-borderou-note').removeAttr('data-borderou-note-id')
        });

        body.on('click', '.delete-borderou-note', function () {
            let mainModal = $('#updateBorderouNote');
            let borderouId = mainModal.find('input[name="borderou_id"]').val();
            let borderouNoteId = $(this).attr('data-borderou-note-id');
            let infoMessage = $('#info_message_update_note');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                async: true,
                url: '{{ route('chair.borderous.deleteStudentBorderouNote')}}',
                type: 'POST',
                data: {
                    borderou_id: borderouId,
                    borderouNoteId: borderouNoteId
                },
                success: function (response) {
                    confirmDeleteBorderouNote.modal('hide');
                    infoMessage.find('.alert-text').html(response.message);
                    infoMessage.show();

                    if(response.type === 'success'){
                        getBorderouNotesById(borderouId);
                    } else {
                        setTimeout(function() {
                            infoMessage.hide('slow');
                        }, 4000);
                    }
                }
            });
        });

        body.on('click', '.add-borderou-note', function () {
            let mainModal = $('#updateBorderouNote');
            let borderouId = mainModal.find('input[name="borderou_id"]').val();
            let addedStudentsIds = $('#addedStudentsIds').val();
            let infoMessage = $('#info_message_update_note');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                async: true,
                url: '{{ route('chair.borderous.addStudentBorderouNote')}}',
                type: 'POST',
                data: {
                    borderou_id: borderouId,
                    addedStudentsIds: addedStudentsIds
                },
                success: function (response) {
                    $('#addStudentBorderouNote').modal('hide');
                    infoMessage.find('.alert-text').html(response.message);
                    infoMessage.show();

                    if(response.type === 'success'){
                        getBorderouNotesById(borderouId);
                    } else {
                        setTimeout(function() {
                            infoMessage.hide('slow');
                        }, 4000);
                    }
                }
            });
        });

        $('#deleteBorderou').on('hidden.bs.modal', function () {
            $(this).find('input[name="borderou_id"]').val('');
        });

        $('#viewBorderou').on('hidden.bs.modal', function () {
            let infoMessage = $('#info_message');
            infoMessage.hide();
            infoMessage.find('.alert-text').html('');
            $('#b_info_name').html('');
            $('#b_info_cycle').html('');
            $('#b_info_faculty').html('');
            $('#b_info_speciality').html('');
            $('#b_info_group').html('');
            $('#b_info_yearOfStudy').html('');
            $('#b_info_semester').html('');
            $('#b_info_course').html('');
            $('#b_info_teacher').html('');
            $('#b_info_yearOfAdmission').html('');
            $('#b_info_totalHours').html('');
            $('#b_info_credits').html('');
            $('#b_info_courseTitle').html('');
            $('#b_info_borderouType').html('');
            $('#b_info_examDate').html('');
            $('#b_info_dateOfLiquidation').html('');
            $('#b_info_finished').html('');
            $('#b_info_owner').html('');
        });

        $('#updateBorderouNote').on('hidden.bs.modal', function () {
            $(this).find('#borderou_note_info_block').html('');
            $(this).find('input[name="borderou_id"]').val('');
            $(this).find('.btn-success').attr('disabled', false);
        });

        //For search
        body.on('change', '#cycleIds', function () {
            getFacultiesByCycleForSearch()
        });
        body.on('change', '#facultyIds', function () {
            getSpecialitiesByFacultyForSelectSearch();
        });
        body.on('change', '#specialityIds', function () {
            getGroupsBySpecialityForSelectSearch();
        });



        //For create
        body.on('change', '#cycle_id', function () {
            getFacultiesByCycleForSelectAddEdit('#cycle_id')
        });
        body.on('change', '#faculty_id', function () {
            getSpecialitiesByFacultyForSelectAddEdit('#faculty_id')
        });
        body.on('change', '#speciality_id', function () {
            getGroupsBySpecialityForSelectAddEdit('#speciality_id')
        });


        body.on('click', '.export-borderou-notes-pdf', function () {
            let loader = $('.custom-loader');
            let borderouId = $(this).attr('data-borderouid');

            if(borderouId === '' || borderouId == null || borderouId === undefined){
                $.notify('{{ __('messages.something_wrong') }}', {
                    autoHide: true,
                    autoHideDelay: 5000,
                    globalPosition: 'top right',
                    style: 'bootstrap',
                    className: 'warn'
                });
                return false;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                async: true,
                url: '{{ route('chair.borderous.exportBorderouNotePdf')}}',
                type: 'POST',
                data: {
                    borderou_id: borderouId
                },
                success: function (response) {
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    if (response.type === 'warning') {
                        $.notify(response.message, {
                            autoHide: true,
                            autoHideDelay: 5000,
                            globalPosition: 'top right',
                            style: 'bootstrap',
                            className: 'warn'
                        });
                    } else {
                        $.notify(response.message, {
                            autoHide: true,
                            autoHideDelay: 5000,
                            globalPosition: 'top right',
                            style: 'bootstrap',
                            className: 'success'
                        });

                        let a = document.createElement('a');
                        a.href = response.url;
                        a.download = response.name;
                        document.body.append(a);
                        a.click();
                        a.remove();
                    }
                }
            });
        });

        body.on('click', '.export-borderou-notes-excel', function () {
            let loader = $('.custom-loader');
            let borderouId = $(this).attr('data-borderouid');

            if(borderouId === '' || borderouId == null || borderouId === undefined){
                $.notify('{{ __('messages.something_wrong') }}', {
                    autoHide: true,
                    autoHideDelay: 5000,
                    globalPosition: 'top right',
                    style: 'bootstrap',
                    className: 'warn'
                });
                return false;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                async: true,
                url: '{{ route('chair.borderous.exportBorderouNoteExcel')}}',
                type: 'POST',
                data: {
                    borderou_id: borderouId
                },
                success: function (response) {
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    if (response.type === 'warning') {
                        $.notify(response.message, {
                            autoHide: true,
                            autoHideDelay: 5000,
                            globalPosition: 'top right',
                            style: 'bootstrap',
                            className: 'warn'
                        });
                    } else {
                        $.notify(response.message, {
                            autoHide: true,
                            autoHideDelay: 5000,
                            globalPosition: 'top right',
                            style: 'bootstrap',
                            className: 'success'
                        });

                        let a = document.createElement('a');
                        a.href = response.url;
                        a.download = response.name;
                        document.body.append(a);
                        a.click();
                        a.remove();
                    }
                }
            });
        });


        //FUNCTIONS
        //For search
        function getFacultiesByCycleForSearch() {
            let loader = $('.custom-loader');
            let cyclesId = [];
            let facultySelect = $('#facultyIds');
            let specialitySelect = $('#specialityIds');
            let groupSelect = $('#groupIds');

            $('#cycleIds :selected').each(function(i, selected) {
                cyclesId[i] = $(selected).val();
            });

            if(cyclesId.length === 0){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('chair.faculties.getFacultiesByCycleForSelectSearch')}}',
                type: 'POST',
                async: true,
                data: {
                    cyclesId: JSON.stringify(cyclesId)
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    facultySelect.empty();
                    facultySelect.html(response.html);
                    facultySelect.trigger('chosen:updated');

                    specialitySelect.empty();
                    specialitySelect.trigger('chosen:updated');

                    groupSelect.empty();
                    groupSelect.trigger('chosen:updated');
                }
            });
        }

        function getSpecialitiesByFacultyForSelectSearch() {
            let loader = $('.custom-loader');
            let facultiesId = [];
            let specialitySelect = $('#specialityIds');

            $('#facultyIds :selected').each(function(i, selected) {
                facultiesId[i] = $(selected).val();
            });

            if(facultiesId.length === 0){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('chair.specialities.getSpecialitiesByFacultyForSelectSearch')}}',
                type: 'POST',
                async: true,
                data: {
                    facultiesId: JSON.stringify(facultiesId)
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    specialitySelect.empty();
                    specialitySelect.html(response.html);
                    specialitySelect.trigger('chosen:updated');
                }
            });
        }

        function getGroupsBySpecialityForSelectSearch() {
            let loader = $('.custom-loader');
            let specialitiesId = [];
            let groupSelect = $('#groupIds');

            $('#specialityIds :selected').each(function(i, selected) {
                specialitiesId[i] = $(selected).val();
            });

            if(specialitiesId.length === 0){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('chair.groups.getGroupsBySpecialityForSelectSearch')}}',
                type: 'POST',
                async: true,
                data: {
                    specialitiesId: JSON.stringify(specialitiesId)
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    groupSelect.empty();
                    groupSelect.html(response.html);
                    groupSelect.trigger('chosen:updated');
                }
            });
        }



        //For add/edit
        function getFacultiesByCycleForSelectAddEdit(element) {
            let loader = $('.custom-loader');
            let cycleId = $.trim($(element).val());
            let facultySelect = $('#faculty_id');
            let specialitySelect = $('#speciality_id');
            let groupSelect = $('#group_id');

            if(cycleId === '' || cycleId === undefined || cycleId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('chair.faculties.getFacultiesByCycleForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    cycleId: cycleId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    facultySelect.empty();
                    facultySelect.html(response.html);
                    facultySelect.trigger('chosen:updated');

                    specialitySelect.empty();
                    specialitySelect.trigger('chosen:updated');

                    groupSelect.empty();
                    groupSelect.trigger('chosen:updated');

                    let totalFaculties = $('#faculty_id > option').length;
                    if(totalFaculties === 1){
                        getSpecialitiesByFacultyForSelectAddEdit('#faculty_id');
                    }

                    let totalSpecialities = $('#speciality_id > option').length;
                    if(totalSpecialities === 1){
                        getGroupsBySpecialityForSelectAddEdit('#speciality_id');
                    }
                }
            });
        }

        function getSpecialitiesByFacultyForSelectAddEdit(element) {
            let loader = $('.custom-loader');
            let facultyId = $.trim($(element).val());
            let specialitySelect = $('#speciality_id');

            if(facultyId === '' || facultyId === undefined || facultyId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('chair.specialities.getSpecialitiesByFacultyForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    facultyId: facultyId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    specialitySelect.empty();
                    specialitySelect.html(response.html);
                    specialitySelect.trigger('chosen:updated');

                    let totalSpecialities = $('#speciality_id > option').length;
                    if(totalSpecialities === 1){
                        getGroupsBySpecialityForSelectAddEdit('#speciality_id');
                    }
                }
            });
        }

        function getGroupsBySpecialityForSelectAddEdit(element) {
            let loader = $('.custom-loader');
            let specialityId = $.trim($(element).val());
            let groupSelect = $('#group_id');

            if(specialityId === '' || specialityId === undefined || specialityId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('chair.groups.getGroupsBySpecialityForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    specialityId: specialityId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    groupSelect.empty();
                    groupSelect.html(response.html);
                    groupSelect.trigger('chosen:updated');
                }
            });
        }
    </script>
@endsection