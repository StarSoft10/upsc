<header id="header" class="header">
    <div class="header-menu">
        <div class="col-sm-7">
            <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
        </div>

        <div class="col-sm-5">
            <div class="user-area dropdown float-right">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if(Auth::user()->photo == null || Auth::user()->photo == '')
                        <img class="user-avatar rounded-circle" src="{{ asset('images/default_photo.jpg') }}" alt="Photo">
                    @else
                        <img class="user-avatar rounded-circle" src="{{ asset('storage/users-photo/' . Auth::user()->photo) }}" alt="Photo">
                    @endif
                </a>

                <div class="user-menu dropdown-menu">
                    <a class="nav-link" href="{{ route('chair.profile') }}"><i class="fa fa-user"></i> {{ __('header.my_profile') }}</a>

                    {{--<a class="nav-link" href="#"><i class="fa fa-user"></i> Notifications <span class="count">13</span></a>--}}

                    {{--<a class="nav-link" href="#"><i class="fa fa-cog"></i> Settings</a>--}}

                    <a class="nav-link" href="{{ route('logout') }}"><i class="fa fa-power-off"></i> {{ __('header.logout') }}</a>
                </div>
            </div>

            <div class="language-select dropdown" id="language-select">
                <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
                    @if (LaravelLocalization::getCurrentLocale() == 'ro')
                        <i class="flag-icon flag-icon-ro"></i>
                    @elseif(LaravelLocalization::getCurrentLocale() == 'en')
                        <i class="flag-icon flag-icon-gb"></i>
                    @elseif(LaravelLocalization::getCurrentLocale() == 'ru')
                        <i class="flag-icon flag-icon-ru"></i>
                    @endif
                </a>

                <div class="dropdown-menu" aria-labelledby="language">
                    @if (LaravelLocalization::getCurrentLocale() == 'ro')
                        <div class="dropdown-item">
                            <a href="{{ LaravelLocalization::getLocalizedURL('en', null, [], true) }}"><i class="flag-icon flag-icon-gb"></i></a>
                        </div>
                        <div class="dropdown-item">
                            <a href="{{ LaravelLocalization::getLocalizedURL('ru', null, [], true) }}"><i class="flag-icon flag-icon-ru"></i></a>
                        </div>
                    @elseif(LaravelLocalization::getCurrentLocale() == 'en')
                        <div class="dropdown-item">
                            <a href="{{ LaravelLocalization::getLocalizedURL('ro', null, [], true) }}"><i class="flag-icon flag-icon-ro"></i></a>
                        </div>
                        <div class="dropdown-item">
                            <a href="{{ LaravelLocalization::getLocalizedURL('ru', null, [], true) }}"><i class="flag-icon flag-icon-ru"></i></a>
                        </div>
                    @elseif(LaravelLocalization::getCurrentLocale() == 'ru')
                        <div class="dropdown-item">
                            <a href="{{ LaravelLocalization::getLocalizedURL('ro', null, [], true) }}"><i class="flag-icon flag-icon-ro"></i></a>
                        </div>
                        <div class="dropdown-item">
                            <a href="{{ LaravelLocalization::getLocalizedURL('en', null, [], true) }}"><i class="flag-icon flag-icon-gb"></i></a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</header>