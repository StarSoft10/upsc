<?php

namespace Modules\Chair\Http\Controllers;

use App\Helpers\AppHelper;
use App\Helpers\StudyHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Routing\Controller;

class FacultyController extends Controller
{
    public $helper;

    public $studyHelper;

    public function __construct()
    {
        $this->helper = new AppHelper();
        $this->studyHelper = new StudyHelper();
    }

    public function getFacultiesByCycleForSelectSearch(Request $request)
    {
        $allowedFaculties = $this->studyHelper->getAllowedFacultiesByModel(Auth::user()->id);
        $request->merge(['allowedFaculties' => $allowedFaculties]);
        return $this->studyHelper->getFacultiesByCycleForSelectSearch($request);
    }

    public function getFacultiesByCycleForSelectAddEdit(Request $request)
    {
        $allowedFaculties = $this->studyHelper->getAllowedFacultiesByModel(Auth::user()->id);
        $request->merge(['allowedFaculties' => $allowedFaculties]);
        return $this->studyHelper->getFacultiesByCycleForSelectAddEdit($request);
    }
}
