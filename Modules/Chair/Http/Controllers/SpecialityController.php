<?php

namespace Modules\Chair\Http\Controllers;

use App\Helpers\AppHelper;
use App\Helpers\StudyHelper;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Routing\Controller;

class SpecialityController extends Controller
{
    public $helper;

    public $studyHelper;

    public function __construct()
    {
        $this->helper = new AppHelper();
        $this->studyHelper = new StudyHelper();
    }

    public function getSpecialitiesByFacultyForSelectSearch(Request $request)
    {
        return $this->studyHelper->getSpecialitiesByFacultyForSelectSearch($request);
    }

    public function getSpecialitiesByFacultyForSelectAddEdit(Request $request)
    {
        return $this->studyHelper->getSpecialitiesByFacultyForSelectAddEdit($request);
    }
}
