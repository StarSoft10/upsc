<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

Route::group([
    'prefix' => LaravelLocalization::setLocale() . '/chair',
    'middleware' => ['auth', 'role:chair', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'localize']
], function () {
    Route::get('/', 'ChairController@index')->name('chair');
    Route::get('profile', 'ChairController@show')->name('chair.profile');
    Route::patch('update', 'ChairController@update')->name('chair.update');
    Route::patch('changePhoto', 'ChairController@changePhoto')->name('chair.changePhoto');
    Route::get('not-found', 'ChairController@notFound')->name('chair.not-found');


    /*
     * User routes
     */
    Route::group(['prefix' => 'students'], function () {
        Route::post('delete', 'StudentController@delete')->name('chair.students.delete');
        Route::get('search', 'StudentController@search')->name('chair.students.search');
        Route::post('block', 'StudentController@block')->name('chair.students.block');
        Route::post('unblock', 'StudentController@unblock')->name('chair.students.unblock');
        Route::post('exportStudentsPdf', 'StudentController@exportStudentsPdf')->name('chair.students.exportStudentsPdf');
        Route::post('exportStudentsExcel', 'StudentController@exportStudentsExcel')->name('chair.students.exportStudentsExcel');
        Route::post('exportStudentPdf', 'StudentController@exportStudentPdf')->name('chair.students.exportStudentPdf');
        Route::post('exportStudentExcel', 'StudentController@exportStudentExcel')->name('chair.students.exportStudentExcel');
        Route::post('deleteAdditionalStudy', 'StudentController@deleteAdditionalStudy')->name('chair.students.deleteAdditionalStudy');
    });
    Route::resource('students', 'StudentController', ['names' => [
        'index' => 'chair.students.index',
        'store' => 'chair.students.store',
        'show' => 'chair.students.show',
        'update' => 'chair.students.update'
    ]]);

    /*
    * Studies routes
    */
    Route::group(['prefix' => 'faculties'], function () {
        Route::post('getFacultiesByCycleForSelectSearch', 'FacultyController@getFacultiesByCycleForSelectSearch')->name('chair.faculties.getFacultiesByCycleForSelectSearch');
        Route::post('getFacultiesByCycleForSelectAddEdit', 'FacultyController@getFacultiesByCycleForSelectAddEdit')->name('chair.faculties.getFacultiesByCycleForSelectAddEdit');
    });

    Route::group(['prefix' => 'specialities'], function () {
        Route::post('getSpecialitiesByFacultyForSelectSearch', 'SpecialityController@getSpecialitiesByFacultyForSelectSearch')->name('chair.specialities.getSpecialitiesByFacultyForSelectSearch');
        Route::post('getSpecialitiesByFacultyForSelectAddEdit', 'SpecialityController@getSpecialitiesByFacultyForSelectAddEdit')->name('chair.specialities.getSpecialitiesByFacultyForSelectAddEdit');
    });

    Route::group(['prefix' => 'groups'], function () {
        Route::post('getGroupsBySpecialityForSelectSearch', 'GroupController@getGroupsBySpecialityForSelectSearch')->name('chair.groups.getGroupsBySpecialityForSelectSearch');
        Route::post('getGroupsBySpecialityForSelectAddEdit', 'GroupController@getGroupsBySpecialityForSelectAddEdit')->name('chair.groups.getGroupsBySpecialityForSelectAddEdit');
    });


    /*
   * Borderous routes
   */
    Route::group(['prefix' => 'borderous'], function () {
        Route::post('delete', 'BorderouController@delete')->name('chair.borderous.delete');
        Route::get('search', 'BorderouController@search')->name('chair.borderous.search');
        Route::post('getBorderouForView', 'BorderouController@getBorderouForView')->name('chair.borderous.getBorderouForView');
        Route::post('getBorderouNote', 'BorderouController@getBorderouNote')->name('chair.borderous.getBorderouNote');
        Route::post('updateBorderouNote', 'BorderouController@updateBorderouNote')->name('chair.borderous.updateBorderouNote');
        Route::post('exportBorderouNotePdf', 'BorderouController@exportBorderouNotePdf')->name('chair.borderous.exportBorderouNotePdf');
        Route::post('exportBorderouNoteExcel', 'BorderouController@exportBorderouNoteExcel')->name('chair.borderous.exportBorderouNoteExcel');
        Route::post('deleteStudentBorderouNote', 'BorderouController@deleteStudentBorderouNote')->name('chair.borderous.deleteStudentBorderouNote');
        Route::post('addStudentBorderouNote', 'BorderouController@addStudentBorderouNote')->name('chair.borderous.addStudentBorderouNote');
    });
    Route::resource('borderous', 'BorderouController', ['names' => [
        'index' => 'chair.borderous.index',
        'store' => 'chair.borderous.store',
        'show' => 'chair.borderous.show',
        'update' => 'chair.borderous.update'
    ]]);
});