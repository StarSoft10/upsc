@extends('admin::layouts.app')

@section('title')
    {{ __('pages.group') }} {{ $group->name }}
@endsection

@section('content')
    <div class="custom-loader" style="display: none"></div>
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-info-circle"></i>
                    <strong class="card-title">{{ __('pages.group_information') }}</strong>
                </div>
                <div class="card-body">
                    <ul style="list-style: none;">
                        <li><span>{{ __('pages.name') }}:</span><b>&nbsp;{{ $group->name }}</b></li>
                        <li><span>{{ __('pages.short_name') }}:</span><b>&nbsp;{{ $group->short_name }}</b></li>
                        <li><span>{{ __('pages.cycle') }}:</span><b>&nbsp;{{ $group->cycle->name }}</b></li>
                        <li><span>{{ __('pages.faculty') }}:</span><b>&nbsp;{{ $group->faculty->name }}</b></li>
                        <li><span>{{ __('pages.speciality') }}:</span><b>&nbsp;{{ $group->speciality->name }}</b></li>
                    </ul>
                </div>
            </div>
        </div>

        @can('edit_group')
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-edit"></i>
                        <strong class="card-title">{{ __('pages.edit_group') }}</strong>
                    </div>
                    <div class="card-body">
                        {!! Form::model($group, ['method' => 'PATCH', 'route' => ['admin.groups.update', $group->id], 'autocomplete' => 'off']) !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('name', __('pages.name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('short_name', __('pages.short_name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('short_name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('cycle_id', __('pages.cycle'), ['class' => 'control-label']) !!}
                                    <select data-placeholder="{{ __('pages.chose_cycle') }}" class="standardSelect" name="cycle_id" id="cycle_id">
                                        @foreach($cycles as $key => $cycle)
                                            <option value="{{ $key }}" @if($key == $group->cycle_id) selected @endif>{{ $cycle }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('faculty_id', __('pages.faculty'), ['class' => 'control-label']) !!}
                                    <select data-placeholder="{{ __('pages.chose_cycle') }}" class="standardSelect" name="faculty_id" id="faculty_id">
                                        <option value="{{ $group->faculty_id }}" selected>{{ $faculty }}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('speciality_id', __('pages.speciality'), ['class' => 'control-label']) !!}
                                    <select data-placeholder="{{ __('pages.chose_faculty') }}" class="standardSelect" name="speciality_id" id="speciality_id">
                                        <option value="{{ $group->speciality_id }}" selected>{{ $speciality }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        {!! Form::submit(__('pages.edit'), ['class' => 'btn btn-primary btn-sm']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        @endcan
    </div>
@endsection

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('vendors/chosen/chosen.min.css') }}">
@endsection

@section('custom-scripts')
    <script src="{{ asset('vendors/chosen/chosen.jquery.min.js') }}"></script>
    <script>
        $(function() {
            $('.standardSelect').chosen({
                disable_search_threshold: 10,
                no_results_text: '{{ __('pages.nothing_found') }}',
                width: '100%'
            });
        });
    </script>
    <script>
        let body = $('body');

        body.on('change', '#cycle_id', function () {
            let loader = $('.custom-loader');
            let cycleId = $.trim($(this).val());
            let facultySelect = $('#faculty_id');
            let specialitySelect = $('#speciality_id');

            if(cycleId === '' || cycleId === undefined || cycleId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('admin.faculties.getFacultiesByCycleForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    cycleId: cycleId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    facultySelect.empty();
                    facultySelect.html(response.html);
                    facultySelect.trigger('chosen:updated');

                    specialitySelect.empty();
                    specialitySelect.trigger('chosen:updated');

                    let totalFaculties = $('#faculty_id > option').length;
                    if(totalFaculties === 1){
                        getSpecialitiesByFacultyForSelectAddEdit('#faculty_id');
                    }
                }
            });
        });

        body.on('change', '#faculty_id', function () {
            getSpecialitiesByFacultyForSelectAddEdit('#faculty_id');
        });

        function getSpecialitiesByFacultyForSelectAddEdit(element) {
            let loader = $('.custom-loader');
            let facultyId = $.trim($(element).val());
            let specialitySelect = $('#speciality_id');

            if(facultyId === '' || facultyId === undefined || facultyId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('admin.specialities.getSpecialitiesByFacultyForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    facultyId: facultyId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    specialitySelect.empty();
                    specialitySelect.html(response.html);
                    specialitySelect.trigger('chosen:updated');
                }
            });
        }
    </script>
@endsection