@extends('admin::layouts.app')

@section('title')
    {{ __('pages.borderou_type') }} {{ $borderouType->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-info-circle"></i>
                    <strong class="card-title">{{ __('pages.borderou_type_information') }}</strong>
                </div>
                <div class="card-body">
                    <ul style="list-style: none;">
                        <li><span>{{ __('pages.name') }}:</span><b>&nbsp;{{ $borderouType->name }}</b></li>
                        <li><span>{{ __('pages.semester_index') }}:</span><b>&nbsp;{{ $borderouType->semester_index }}</b></li>
                        <li><span>{{ __('pages.exam_index') }}:</span><b>&nbsp;{{ $borderouType->exam_index }}</b></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-edit"></i>
                    <strong class="card-title">{{ __('pages.edit_borderou_type') }}</strong>
                </div>
                <div class="card-body">
                    {!! Form::model($borderouType, ['method' => 'PATCH', 'route' => ['admin.borderou-types.update', $borderouType->id], 'autocomplete' => 'off']) !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('name', __('pages.name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('semester_index', __('pages.semester_index'), ['class' => 'control-label']) !!}
                                    {!! Form::text('semester_index', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('exam_index', __('pages.exam_index'), ['class' => 'control-label']) !!}
                                    {!! Form::text('exam_index', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                        </div>
                        {!! Form::submit(__('pages.edit'), ['class' => 'btn btn-primary btn-sm']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
@endsection

@section('custom-scripts')
@endsection
