@extends('admin::layouts.app')

@section('title')
    {{ __('pages.teacher_ranks') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            {!! Form::open(['route' => 'admin.teacher-ranks.search', 'method' => 'GET']) !!}
                                <div class="row">
                                    <div class="col-md-4">
                                        {!! Form::text('name', $name, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.name')]) !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::text('short_name', $shortName, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.short_name')]) !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::submit(__('pages.search'), ['class' => 'btn btn-primary btn-sm']) !!}
                                        <a href="{{ route('admin.teacher-ranks.index') }}" class="btn btn-info btn-sm" title="{{ __('pages.reset_filter') }}">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-2 text-right">
                            @can('create_teacher_rank')
                                <button type="button" class="btn btn-success mb-1 btn-sm" data-toggle="modal" data-target="#addTeacherRank">
                                    {{ __('pages.add_teacher_rank') }}
                                </button>
                            @endcan
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th>{{ __('pages.name') }}</th>
                                <th>{{ __('pages.short_name') }}</th>
                                <th>{{ __('pages.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($teacherRanks) > 0)
                                @foreach($teacherRanks as $teacherRank)
                                    <tr>
                                        <td class="text-center"><span>{{ $teacherRank->name }}</span></td>
                                        <td class="text-center"><span>{{ $teacherRank->short_name }}</span></td>
                                        <td class="text-center">
                                            @can('view_teacher_rank')
                                                <a href="{{ route('admin.teacher-ranks.show', $teacherRank->id) }}" class="btn-no-bg" title="{{ __('pages.show_edit') }}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            @endcan

                                            @can('delete_teacher_rank')
                                                <a href="javascript:void(0)" data-teacherrankid="{{ $teacherRank->id }}" class="btn-no-bg delete-teacher-rank" title="{{ __('pages.delete') }}">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="3">{{ __('messages.not_found_any_teacher_ranks') }}</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="table-footer">
                        {!! $teacherRanks->render() !!}
                        <div class="total-found">{{ __('pages.total_found') }}: {{ $teacherRanks->total() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @can('create_teacher_rank')
        <div class="modal fade" id="addTeacherRank" role="dialog" aria-labelledby="addTeacherRank" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    {!! Form::open(['url' => route('admin.teacher-ranks.store'), 'method' => 'post', 'autocomplete' => 'off']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.add_teacher_rank') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('name', __('pages.name'), ['class' => 'control-label']) !!}
                                        {!! Form::text('name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('short_name', __('pages.short_name'), ['class' => 'control-label']) !!}
                                        {!! Form::text('short_name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            {!! Form::submit(__('pages.add'), ['class' => 'btn btn-success btn-sm']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan

    @can('delete_teacher_rank')
        <div class="modal fade" id="deleteTeacherRank" role="dialog" aria-labelledby="deleteTeacherRank" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['route' => 'admin.teacher-ranks.delete']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.delete_teacher_rank') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-info" role="alert">
                                <div class="alert-text">
                                    {{ __('pages.sure_to_delete_teacher_rank?') }}
                                </div>
                            </div>
                            <input type="hidden" name="teacher_rank_id">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            <button type="submit" class="btn btn-danger btn-sm">{{ __('pages.delete') }}</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan
@endsection

@section('custom-css')
@endsection

@section('custom-scripts')
    <script>
        let body = $('body');

        body.on('click', '.delete-teacher-rank', function () {
            let teacherRankId = $(this).attr('data-teacherrankid');
            let modal = $('#deleteTeacherRank');
            modal.find('input[name="teacher_rank_id"]').val(teacherRankId);
            modal.modal('show');
        });

        $('#deleteTeacherRank').on('hidden.bs.modal', function () {
            $(this).find('input[name="teacher_rank_id"]').val('');
        });
    </script>
@endsection