<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="{{ route('admin') }}"><img src="{{ asset('images/logo.png') }}" alt="Logo" style="width: 29%;"></a>
            <a class="navbar-brand hidden" href="{{ route('admin') }}"><img src="{{ asset('images/logo.png') }}" alt="Logo"></a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="{{ route('admin') }}">
                        <i class="menu-icon fa fa-dashboard"></i>
                        {{ __('aside.dashboard') }}
                    </a>
                </li>

                @php
                    $userRoutes = false;
                    if(\Request::is('*/unconfirmed-users*') ||
                       \Request::is('*/rectors*') || \Request::is('*/secretaries*') ||
                       \Request::is('*/deans*') || \Request::is('*/chairs*') ||
                       \Request::is('*/human-resources*') ||
                       \Request::is('*/teacher*') || \Request::is('*/students*')){
                       $userRoutes = true;
                    }
                @endphp

                <h3 class="menu-title">{{ __('aside.all_user_types') }}</h3>
                <li class="menu-item-has-children dropdown @if($userRoutes) show @endif">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-users"></i>{{ __('aside.users') }}
                    </a>
                    <ul class="sub-menu children dropdown-menu @if($userRoutes) show @endif">
                        @can('view_rector')
                            <li>
                                <i class="fa fa-user"></i>
                                <a href="{{ route('admin.rectors.index') }}" class="@if(Request::is('*rectors*')) current @endif">
                                    {{ __('aside.rectors') }}
                                </a>
                            </li>
                        @endcan

                        @can('view_secretary')
                            <li>
                                <i class="fa fa-user"></i>
                                <a href="{{ route('admin.secretaries.index') }}" class="@if(Request::is('*secretaries*')) current @endif">
                                    {{ __('aside.secretaries') }}
                                </a>
                            </li>
                        @endcan

                        @can('view_dean')
                            <li>
                                <i class="fa fa-user"></i>
                                <a href="{{ route('admin.deans.index') }}" class="@if(Request::is('*deans*')) current @endif">
                                    {{ __('aside.deans') }}
                                </a>
                            </li>
                        @endcan

                        @can('view_chair')
                            <li>
                                <i class="fa fa-user"></i>
                                <a href="{{ route('admin.chairs.index') }}" class="@if(Request::is('*chairs*')) current @endif">
                                    {{ __('aside.chairs') }}
                                </a>
                            </li>
                        @endcan

                        @can('view_human_resource')
                            <li>
                                <i class="fa fa-user"></i>
                                <a href="{{ route('admin.human-resources.index') }}" class="@if(Request::is('*human-resources*')) current @endif">
                                    {{ __('aside.human_resources') }}
                                </a>
                            </li>
                        @endcan

                        @can('view_teacher')
                            <li>
                                <i class="fa fa-user"></i>
                                <a href="{{ route('admin.teachers.index') }}" class="@if(Request::is('*teachers*')) current @endif">
                                    {{ __('aside.teachers') }}
                                </a>
                            </li>
                        @endcan

                        @can('view_student')
                            <li>
                                <i class="fa fa-user"></i>
                                <a href="{{ route('admin.students.index') }}" class="@if(Request::is('*students*')) current @endif">
                                    {{ __('aside.students') }}
                                </a>
                            </li>
                        @endcan

                        @can('view_unconfirmed_users')
                            <li>
                                <i class="fa fa-user"></i>
                                <a href="{{ route('admin.unconfirmed-users.index') }}" class="@if(Request::is('*unconfirmed-users*')) current @endif">
                                    {{ __('aside.unconfirmed_users') }}
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>

                @php
                    $studiesRoutes = false;
                   if(\Request::is('*/year-of-studies*') || \Request::is('*/cycles*') || \Request::is('*/faculties*') ||
                      \Request::is('*/specialities*') || \Request::is('*/groups*') || \Request::is('*/courses*') ||
                      \Request::is('*/course-types*') || \Request::is('*/days*') || \Request::is('*/duration-types*') ||
                      \Request::is('*/durations*') || \Request::is('*/semesters*') || \Request::is('*/week-types*') ||
                      \Request::is('*/weeks*') || \Request::is('*/teacher-ranks*')){
                       $studiesRoutes = true;
                    }
                @endphp

                <h3 class="menu-title">{{ __('aside.manage_studies') }}</h3>
                <li class="menu-item-has-children dropdown @if($studiesRoutes) show @endif">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-book"></i>{{ __('aside.studies') }}
                    </a>
                    <ul class="sub-menu children dropdown-menu @if($studiesRoutes) show @endif">
                        @can('view_year_of_study')
                            <li>
                                <i class="fa fa-eraser"></i>
                                <a href="{{ route('admin.year-of-studies.index') }}" class="@if(Request::is('*year-of-studies*')) current @endif">
                                    {{ __('aside.year_of_studies') }}
                                </a>
                            </li>
                        @endcan
                        @can('view_cycle')
                            <li>
                                <i class="fa fa-desktop"></i>
                                <a href="{{ route('admin.cycles.index') }}" class="@if(Request::is('*cycles*')) current @endif">
                                    {{ __('aside.cycles') }}
                                </a>
                            </li>
                        @endcan

                        @can('view_faculty')
                            <li>
                                <i class="fa fa-building-o"></i>
                                <a href="{{ route('admin.faculties.index') }}" class="@if(Request::is('*faculties*')) current @endif">
                                    {{ __('aside.faculties') }}
                                </a>
                            </li>
                        @endcan

                        @can('view_speciality')
                            <li>
                                <i class="fa fa-gavel"></i>
                                <a href="{{ route('admin.specialities.index') }}" class="@if(Request::is('*specialities*')) current @endif">
                                    {{ __('aside.specialities') }}
                                </a>
                            </li>
                        @endcan

                        @can('view_group')
                            <li>
                                <i class="fa fa-users"></i>
                                <a href="{{ route('admin.groups.index') }}" class="@if(Request::is('*groups*')) current @endif">
                                    {{ __('aside.groups') }}
                                </a>
                            </li>
                        @endcan

                        @can('view_course')
                            <li>
                                <i class="fa fa-book"></i>
                                <a href="{{ route('admin.courses.index') }}" class="@if(Request::is('*courses*')) current @endif">
                                    {{ __('aside.courses') }}
                                </a>
                            </li>
                        @endcan

                        @can('view_course_type')
                            <li>
                                <i class="fa fa-book"></i>
                                <a href="{{ route('admin.course-types.index') }}" class="@if(Request::is('*course-types*')) current @endif">
                                    {{ __('aside.course_types') }}
                                </a>
                            </li>
                        @endcan

                        @can('view_day')
                            <li>
                                <i class="fa fa-sun-o"></i>
                                <a href="{{ route('admin.days.index') }}" class="@if(Request::is('*days*')) current @endif">
                                    {{ __('aside.days') }}
                                </a>
                            </li>
                            @endcan

                        @can('view_duration_type')
                            <li>
                                <i class="fa fa-clock-o"></i>
                                <a href="{{ route('admin.duration-types.index') }}" class="@if(Request::is('*duration-types*')) current @endif">
                                    {{ __('aside.duration_types') }}
                                </a>
                            </li>
                        @endcan

                        @can('view_duration')
                            <li>
                                <i class="fa fa-clock-o"></i>
                                <a href="{{ route('admin.durations.index') }}" class="@if(Request::is('*durations*')) current @endif">
                                    {{ __('aside.durations') }}
                                </a>
                            </li>
                        @endcan

                        @can('view_semester')
                            <li>
                                <i class="fa fa-file-text-o"></i>
                                <a href="{{ route('admin.semesters.index') }}" class="@if(Request::is('*semesters*')) current @endif">
                                    {{ __('aside.semesters') }}
                                </a>
                            </li>
                        @endcan

                        @can('view_week_type')
                            <li>
                                <i class="fa fa-calendar"></i>
                                <a href="{{ route('admin.week-types.index') }}" class="@if(Request::is('*week-types*')) current @endif">
                                    {{ __('aside.week_types') }}
                                </a>
                            </li>
                        @endcan

                        @can('view_week')
                            <li>
                                <i class="fa fa-calendar"></i>
                                <a href="{{ route('admin.weeks.index') }}" class="@if(Request::is('*weeks*')) current @endif">
                                    {{ __('aside.weeks') }}
                                </a>
                            </li>
                        @endcan

                        @can('view_teacher_rank')
                            <li>
                                <i class="fa fa-superscript"></i>
                                <a href="{{ route('admin.teacher-ranks.index') }}" class="@if(Request::is('*teacher-ranks*')) current @endif">
                                    {{ __('aside.teacher_ranks') }}
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>


                @php
                    $borderousRoute = false;
                    if(\Request::is('*/borderous*') || \Request::is('*/borderou-types*')){
                       $borderousRoute = true;
                    }
                @endphp
                <h3 class="menu-title">{{ __('aside.borderous') }}</h3>
                <li class="menu-item-has-children dropdown @if($borderousRoute) show @endif">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon ti-folder"></i>{{ __('aside.borderous') }}
                    </a>
                    <ul class="sub-menu children dropdown-menu @if($borderousRoute) show @endif">
                        <li>
                            <i class="menu-icon ti-folder"></i>
                            <a href="{{ route('admin.borderous.index') }}" class="@if(Request::is('*borderous*')) current @endif">
                                {{ __('aside.list_of_borderous') }}
                            </a>
                        </li>
                        <li>
                            <i class="menu-icon ti-list-ol"></i>
                            <a href="{{ route('admin.borderou-types.index') }}" class="@if(Request::is('*borderou-types*')) current @endif">
                                {{ __('aside.borderou_types') }}
                            </a>
                        </li>
                    </ul>
                </li>

                @php
                    $settingsRoutes = false;
                    if(\Request::is('*/logs*')){
                       $settingsRoutes = true;
                    }
                @endphp

                <h3 class="menu-title">{{ __('aside.settings') }}</h3>
                <li class="menu-item-has-children dropdown @if($settingsRoutes) show @endif">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="menu-icon fa fa-cogs"></i>{{ __('aside.settings') }}
                    </a>
                    <ul class="sub-menu children dropdown-menu @if($settingsRoutes) show @endif">
                        <li>
                            <i class="fa fa-list-ol"></i>
                            <a href="{{ route('admin.logs.index') }}" class="@if(Request::is('*logs*')) current @endif">
                                {{ __('aside.logs') }}
                            </a>
                        </li>
                    </ul>
                </li>

                {{--<li>--}}
                    {{--<a href="javascript:void(0)"> <i class="menu-icon ti-email"></i>Some page</a>--}}
                {{--</li>--}}
            </ul>
        </div>
    </nav>
</aside>