@extends('admin::layouts.app')

@section('title')
    {{ __('pages.week') }} {{ $week->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-info-circle"></i>
                    <strong class="card-title">{{ __('pages.week_information') }}</strong>
                </div>
                <div class="card-body">
                    <ul style="list-style: none;">
                        <li><span>{{ __('pages.year') }}:</span><b>&nbsp;{{ $week->year }}</b></li>
                        <li><span>{{ __('pages.start') }}:</span><b>&nbsp;{{ $week->start }}</b></li>
                        <li><span>{{ __('pages.end') }}:</span><b>&nbsp;{{ $week->end }}</b></li>
                        <li><span>{{ __('pages.order') }}:</span><b>&nbsp;{{ $week->order }}</b></li>
                        <li><span>{{ __('pages.week_type') }}:</span><b>&nbsp;{{ $week->weekType->name }}</b></li>
                        <li><span>{{ __('pages.semester') }}:</span><b>&nbsp;{{ $week->semester->name }}</b></li>
                    </ul>
                </div>
            </div>
        </div>

        @can('edit_week')
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-edit"></i>
                        <strong class="card-title">{{ __('pages.edit_week') }}</strong>
                    </div>
                    <div class="card-body">
                        {!! Form::model($week, ['method' => 'PATCH', 'route' => ['admin.weeks.update', $week->id], 'autocomplete' => 'off']) !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('year', __('pages.year'), ['class' => 'control-label']) !!}
                                    {!! Form::text('year', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('start', __('pages.start'), ['class' => 'control-label']) !!}
                                    {!! Form::date('start', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('end', __('pages.end'), ['class' => 'control-label']) !!}
                                    {!! Form::date('end', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('order', __('pages.order'), ['class' => 'control-label']) !!}
                                    {!! Form::number('order', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('week_type_id', __('pages.week_type'), ['class' => 'control-label']) !!}
                                    <select data-placeholder="{{ __('pages.chose_week_type') }}" class="standardSelect" name="week_type_id" id="week_type_id">
                                        @foreach($weekTypes as $key => $weekType)
                                            <option value="{{ $key }}" @if($key == $week->week_type_id) selected @endif>{{ $weekType }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('semester_id', __('pages.semester'), ['class' => 'control-label']) !!}
                                    <select data-placeholder="{{ __('pages.chose_semester') }}" class="standardSelect" name="semester_id" id="semester_id">
                                        @foreach($semesters as $key => $semester)
                                            <option value="{{ $key }}" @if($key == $week->semester_id) selected @endif>{{ $semester }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        {!! Form::submit(__('pages.edit'), ['class' => 'btn btn-primary btn-sm']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        @endcan
    </div>
@endsection

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('vendors/chosen/chosen.min.css') }}">
@endsection

@section('custom-scripts')
    <script src="{{ asset('vendors/chosen/chosen.jquery.min.js') }}"></script>
    <script>
        $(function() {
            $('.standardSelect').chosen({
                disable_search_threshold: 10,
                no_results_text: '{{ __('pages.nothing_found') }}',
                width: '100%'
            });
        });
    </script>
@endsection