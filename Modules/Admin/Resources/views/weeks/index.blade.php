@extends('admin::layouts.app')

@section('title')
    {{ __('pages.weeks') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            {!! Form::open(['route' => 'admin.weeks.search', 'method' => 'GET']) !!}
                                <div class="row">
                                    <div class="col-md-3">
                                        {!! Form::text('year', $year, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.year'), 'style' => 'width: 100%;']) !!}
                                    </div>
                                    <div class="col-md-3">
                                        <select data-placeholder="{{ __('pages.chose_week_types') }}" multiple class="standardSelect" name="weekTypeIds[]" id="weekTypeIds">
                                            @foreach($weekTypes as $key => $weekType)
                                                <option value="{{ $key }}" @if(in_array($key, $weekTypeIds)) selected @endif>{{ $weekType }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select data-placeholder="{{ __('pages.chose_semesters') }}" multiple class="standardSelect" name="semesterIds[]" id="semesterIds">
                                            @foreach($semesters as $key => $semester)
                                                <option value="{{ $key }}" @if(in_array($key, $semesterIds)) selected @endif>{{ $semester }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::submit(__('pages.search'), ['class' => 'btn btn-primary btn-sm']) !!}
                                        <a href="{{ route('admin.weeks.index') }}" class="btn btn-info btn-sm" title="{{ __('pages.reset_filter') }}">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-2 text-right">
                            @can('create_week')
                                <button type="button" class="btn btn-success mb-1 btn-sm" data-toggle="modal" data-target="#addWeek">
                                    {{ __('pages.add_week') }}
                                </button>
                            @endcan
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th>{{ __('pages.year') }}</th>
                                <th>{{ __('pages.start') }}</th>
                                <th>{{ __('pages.end') }}</th>
                                <th>{{ __('pages.order') }}</th>
                                <th>{{ __('pages.week_type') }}</th>
                                <th>{{ __('pages.semester') }}</th>
                                <th>{{ __('pages.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($weeks) > 0)
                                @foreach($weeks as $week)
                                    <tr>
                                        <td class="text-center"><span>{{ $week->year }}</span></td>
                                        <td class="text-center"><span>{{ $week->start }}</span></td>
                                        <td class="text-center"><span>{{ $week->end }}</span></td>
                                        <td class="text-center"><span>{{ $week->order }}</span></td>
                                        <td class="text-center"><span>{{ $week->weekType->name }}</span></td>
                                        <td class="text-center"><span>{{ $week->semester->name }}</span></td>
                                        <td class="text-center">
                                            @can('view_week')
                                                <a href="{{ route('admin.weeks.show', $week->id) }}" class="btn-no-bg" title="{{ __('pages.show_edit') }}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            @endcan

                                            @can('delete_week')
                                                <a href="javascript:void(0)" data-weekid="{{ $week->id }}" class="btn-no-bg delete-week" title="{{ __('pages.delete') }}">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="7">{{ __('messages.not_found_any_weeks') }}</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="table-footer">
                        {!! $weeks->render() !!}
                        <div class="total-found">{{ __('pages.total_found') }}: {{ $weeks->total() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @can('create_week')
        <div class="modal fade" id="addWeek" role="dialog" aria-labelledby="addWeek" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    {!! Form::open(['url' => route('admin.weeks.store'), 'method' => 'post', 'autocomplete' => 'off']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.add_week') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('year', __('pages.year'), ['class' => 'control-label']) !!}
                                        {!! Form::number('year', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('start', __('pages.start'), ['class' => 'control-label']) !!}
                                        {!! Form::date('start', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('end', __('pages.end'), ['class' => 'control-label']) !!}
                                        {!! Form::date('end', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('order', __('pages.order'), ['class' => 'control-label']) !!}
                                        {!! Form::number('order', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('week_type_id', __('pages.week_type'), ['class' => 'control-label']) !!}
                                        <select data-placeholder="{{ __('pages.week_type') }}" class="standardSelect" name="week_type_id" id="week_type_id">
                                            @foreach($weekTypes as $key => $weekType)
                                                <option value="{{ $key }}">{{ $weekType }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('semester_id', __('pages.semester'), ['class' => 'control-label']) !!}
                                        <select data-placeholder="{{ __('pages.semester') }}" class="standardSelect" name="semester_id" id="semester_id">
                                            @foreach($semesters as $key => $semester)
                                                <option value="{{ $key }}">{{ $semester }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            {!! Form::submit(__('pages.add'), ['class' => 'btn btn-success btn-sm']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan

    @can('delete_week')
        <div class="modal fade" id="deleteWeek" role="dialog" aria-labelledby="deleteWeek" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['route' => 'admin.weeks.delete']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.delete_week') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-info" role="alert">
                                <div class="alert-text">
                                    {{ __('pages.sure_to_delete_week?') }}
                                </div>
                            </div>
                            <input type="hidden" name="week_id">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            <button type="submit" class="btn btn-danger btn-sm">{{ __('pages.delete') }}</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan
@endsection

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('vendors/chosen/chosen.min.css') }}">
@endsection

@section('custom-scripts')
    <script src="{{ asset('vendors/chosen/chosen.jquery.min.js') }}"></script>
    <script>
        $(function() {
            $('.standardSelect').chosen({
                disable_search_threshold: 10,
                no_results_text: '{{ __('pages.nothing_found') }}',
                width: '100%'
            });
        });
    </script>
    <script>
        let body = $('body');

        body.on('click', '.delete-week', function () {
            let weekId = $(this).attr('data-weekid');
            let modal = $('#deleteWeek');
            modal.find('input[name="week_id"]').val(weekId);
            modal.modal('show');
        });

        $('#deleteWeek').on('hidden.bs.modal', function () {
            $(this).find('input[name="week_id"]').val('');
        });
    </script>
@endsection