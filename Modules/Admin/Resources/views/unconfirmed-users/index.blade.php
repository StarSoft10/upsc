@extends('admin::layouts.app')

@section('title')
    {{ __('pages.unconfirmed_users') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            {!! Form::open(['route' => 'admin.unconfirmed-users.search', 'method' => 'GET']) !!}
                                <div class="row">
                                    <div class="col-md-4">
                                        {!! Form::text('name', $name, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.name')]) !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::text('email', $email, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.email')]) !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::submit(__('pages.search'), ['class' => 'btn btn-primary btn-sm']) !!}
                                        <a href="{{ route('admin.unconfirmed-users.index') }}" class="btn btn-info btn-sm" title="{{ __('pages.reset_filter') }}">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th>{{ __('pages.first_name') }}</th>
                                <th>{{ __('pages.last_name') }}</th>
                                <th>{{ __('pages.email') }}</th>
                                <th>{{ __('pages.phone') }}</th>
                                <th>{{ __('pages.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($unconfirmedUsers) > 0)
                                @foreach($unconfirmedUsers as $unconfirmedUser)
                                    <tr>
                                        <td class="text-center"><span>{{ $unconfirmedUser->first_name }}</span></td>
                                        <td class="text-center"><span>{{ $unconfirmedUser->last_name }}</span></td>
                                        <td class="text-center"><span>{{ $unconfirmedUser->email }}</span></td>
                                        <td class="text-center"><span>{{ $unconfirmedUser->phone }}</span></td>
                                        <td class="text-center">
                                            @can('view_unconfirmed_users')
                                                <a href="{{ route('admin.unconfirmed-users.show', $unconfirmedUser->id) }}" class="btn-no-bg" title="{{ __('pages.show_edit') }}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            @endcan

                                            @can('delete_unconfirmed_users')
                                                <a href="javascript:void(0)" data-unconfirmeduserid="{{ $unconfirmedUser->id }}" class="btn-no-bg delete-unconfirmed-user" title="{{ __('pages.delete') }}">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            @endcan

                                            @can('confirm_unconfirmed_users')
                                                @if($unconfirmedUser->confirmed == 0)
                                                    <a href="javascript:void(0)" data-unconfirmeduserid="{{ $unconfirmedUser->id }}" class="btn-no-bg confirm-unconfirmed-user" title="{{ __('pages.confirm') }}">
                                                        <i class="fa fa-check-square-o"></i>
                                                    </a>
                                                @endif
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="5">{{ __('messages.not_found_any_unconfirmed_users') }}</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="table-footer">
                        {!! $unconfirmedUsers->render() !!}
                        <div class="total-found">{{ __('pages.total_found') }}: {{ $unconfirmedUsers->total() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @can('delete_unconfirmed_users')
        <div class="modal fade" id="deleteUnconfirmedUser" role="dialog" aria-labelledby="deleteUnconfirmedUser" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['route' => 'admin.unconfirmed-users.delete']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.delete_unconfirmed_user') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-info" role="alert">
                                <div class="alert-text">
                                    {{ __('pages.sure_to_delete_unconfirmed_user?') }}
                                </div>
                            </div>
                            <input type="hidden" name="unconfirmed_user_id">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            <button type="submit" class="btn btn-danger btn-sm">{{ __('pages.delete') }}</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan

    @can('confirm_unconfirmed_users')
        <div class="modal fade" id="confirmUnconfirmedUser" role="dialog" aria-labelledby="confirmUnconfirmedUser" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    {!! Form::open(['route' => 'admin.unconfirmed-users.confirm']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.confirm_unconfirmed_user') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-info" role="alert">
                                <div class="alert-text">
                                    {{ __('pages.sure_to_confirm_unconfirmed_user?') }}
                                </div>
                            </div>
                            <input type="hidden" name="unconfirmed_user_id">

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        {!! Form::label('permissions', __('pages.permissions'), ['class' => 'control-label']) !!}
                                        <select data-placeholder="{{ __('pages.chose_permissions') }}" multiple class="standardSelect" name="permissions[]" id="permissions" required>
                                            @foreach($permissions as $permission)
                                                <option value="{{ $permission->id }}" data-permission="{{ $permission->name }}">{{ __('permissions.' . $permission->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" class="btn btn-success btn-sm select-all-js mb-5px">{{ __('pages.select_all') }}</button>
                                        <button type="button" class="btn btn-danger btn-sm select-none-js mb-5px">{{ __('pages.select_none') }}</button>
                                        <button type="button" class="btn btn-primary btn-sm select-view-js mb-5px">{{ __('pages.select_view') }}</button>
                                        <button type="button" class="btn btn-success btn-sm select-create-js mb-5px">{{ __('pages.select_create') }}</button>
                                        <button type="button" class="btn btn-info btn-sm select-edit-js mb-5px">{{ __('pages.select_edit') }}</button>
                                        <button type="button" class="btn btn-danger btn-sm select-delete-js mb-5px">{{ __('pages.select_delete') }}</button>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {!! Form::label('role', __('pages.roles'), ['class' => 'control-label']) !!}
                                        <select data-placeholder="{{ __('pages.chose_role') }}" class="standardSelect" name="role" id="role" required>
                                            @foreach($roles as $role)
                                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group teacher-ranks-block" style="display: none">
                                        {!! Form::label('teacher_rank_id', __('pages.teacher_rank'), ['class' => 'control-label']) !!}
                                        <select data-placeholder="{{ __('pages.teacher_rank') }}" class="standardSelect" name="teacher_rank_id" id="teacher_rank_id" required>
                                            @foreach($teacherRanks as $id => $teacherRank)
                                                <option value="{{ $id }}">{{ $teacherRank }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            <button type="submit" class="btn btn-success btn-sm">{{ __('pages.confirm') }}</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan
@endsection

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('vendors/chosen/chosen.min.css') }}">
@endsection

@section('custom-scripts')
    <script src="{{ asset('vendors/chosen/chosen.jquery.min.js') }}"></script>
    <script>
        $(function() {
            $('.standardSelect').chosen({
                disable_search_threshold: 10,
                no_results_text: '{{ __('pages.nothing_found') }}',
                width: '100%'
            });
        });
    </script>
    <script>
        let body = $('body');

        body.on('click', '.delete-unconfirmed-user', function () {
            let unconfirmedUserId = $(this).attr('data-unconfirmeduserid');
            let modal = $('#deleteUnconfirmedUser');
            modal.find('input[name="unconfirmed_user_id"]').val(unconfirmedUserId);
            modal.modal('show');
        });

        body.on('click', '.confirm-unconfirmed-user', function () {
            let unconfirmedUserId = $(this).attr('data-unconfirmeduserid');
            let modal = $('#confirmUnconfirmedUser');
            modal.find('input[name="unconfirmed_user_id"]').val(unconfirmedUserId);
            modal.modal('show');
        });

        body.on('change', '#role', function () {
            if(parseInt($(this).val()) === 8){
                $('.teacher-ranks-block').show();
            } else {
                $('.teacher-ranks-block').hide();
            }
        });

        $('#deleteUnconfirmedUser').on('hidden.bs.modal', function () {
            $(this).find('input[name="unconfirmed_user_id"]').val('');
        });
        $('#confirmUnconfirmedUser').on('hidden.bs.modal', function () {
            $(this).find('input[name="unconfirmed_user_id"]').val('');
        });
    </script>
@endsection