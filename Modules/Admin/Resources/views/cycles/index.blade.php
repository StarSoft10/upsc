@extends('admin::layouts.app')

@section('title')
    {{ __('pages.cycles') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            {!! Form::open(['route' => 'admin.cycles.search', 'method' => 'GET']) !!}
                                <div class="row">
                                    <div class="col-md-4">
                                        {!! Form::text('name', $name, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.name')]) !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::text('short_name', $shortName, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.short_name')]) !!}
                                    </div>
                                    <div class="col-md-4">
                                        {!! Form::submit(__('pages.search'), ['class' => 'btn btn-primary btn-sm']) !!}
                                        <a href="{{ route('admin.cycles.index') }}" class="btn btn-info btn-sm" title="{{ __('pages.reset_filter') }}">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-2 text-right">
                            @can('create_cycle')
                                <button type="button" class="btn btn-success mb-1 btn-sm" data-toggle="modal" data-target="#addCycle">
                                    {{ __('pages.add_cycle') }}
                                </button>
                            @endcan
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th>{{ __('pages.name') }}</th>
                                <th>{{ __('pages.short_name') }}</th>
                                <th>{{ __('pages.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($cycles) > 0)
                                @foreach($cycles as $cycle)
                                    <tr>
                                        <td class="text-center"><span>{{ $cycle->name }}</span></td>
                                        <td class="text-center"><span>{{ $cycle->short_name }}</span></td>
                                        <td class="text-center">
                                            @can('view_cycle')
                                                <a href="{{ route('admin.cycles.show', $cycle->id) }}" class="btn-no-bg" title="{{ __('pages.show_edit') }}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            @endcan

                                            @can('delete_cycle')
                                                <a href="javascript:void(0)" data-cycleid="{{ $cycle->id }}" class="btn-no-bg delete-cycle" title="{{ __('pages.delete') }}">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="3">{{ __('messages.not_found_any_cycles') }}</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="table-footer">
                        {!! $cycles->render() !!}
                        <div class="total-found">{{ __('pages.total_found') }}: {{ $cycles->total() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @can('create_cycle')
        <div class="modal fade" id="addCycle" role="dialog" aria-labelledby="addCycle" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    {!! Form::open(['url' => route('admin.cycles.store'), 'method' => 'post', 'autocomplete' => 'off']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.add_cycle') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('name', __('pages.name'), ['class' => 'control-label']) !!}
                                        {!! Form::text('name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('short_name', __('pages.short_name'), ['class' => 'control-label']) !!}
                                        {!! Form::text('short_name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            {!! Form::submit(__('pages.add'), ['class' => 'btn btn-success btn-sm']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan

    @can('delete_cycle')
        <div class="modal fade" id="deleteCycle" role="dialog" aria-labelledby="deleteCycle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['route' => 'admin.cycles.delete']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.delete_cycle') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-info" role="alert">
                                <div class="alert-text">
                                    {{ __('pages.sure_to_delete_cycle?') }}
                                </div>
                            </div>
                            <input type="hidden" name="cycle_id">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            <button type="submit" class="btn btn-danger btn-sm">{{ __('pages.delete') }}</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan
@endsection

@section('custom-css')
@endsection

@section('custom-scripts')
    <script>
        let body = $('body');

        body.on('click', '.delete-cycle', function () {
            let cycleId = $(this).attr('data-cycleid');
            let modal = $('#deleteCycle');
            modal.find('input[name="cycle_id"]').val(cycleId);
            modal.modal('show');
        });

        $('#deleteCycle').on('hidden.bs.modal', function () {
            $(this).find('input[name="cycle_id"]').val('');
        });
    </script>
@endsection