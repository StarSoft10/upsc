@extends('admin::layouts.app')

@section('title')
    {{ __('pages.durations') }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-9">
                            {!! Form::open(['route' => 'admin.durations.search', 'method' => 'GET']) !!}
                                <div class="row">
                                    <div class="col-md-3">
                                        {!! Form::text('name', $name, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.name'), 'style' => 'width: 100%;']) !!}
                                    </div>
                                    {{--<div class="col-md-3">--}}
                                        {{--{!! Form::text('shortName', $shortName, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.short_name'), 'style' => 'width: 100%;']) !!}--}}
                                    {{--</div>--}}
                                    <div class="col-md-3">
                                        <select data-placeholder="{{ __('pages.chose_duration_types') }}" multiple class="standardSelect" name="durationTypeIds[]" id="durationTypeIds">
                                            @foreach($durationTypes as $key => $durationType)
                                                <option value="{{ $key }}" @if(in_array($key, $durationTypeIds)) selected @endif>{{ $durationType }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::submit(__('pages.search'), ['class' => 'btn btn-primary btn-sm']) !!}
                                        <a href="{{ route('admin.durations.index') }}" class="btn btn-info btn-sm" title="{{ __('pages.reset_filter') }}">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-3 text-right">
                            @can('create_duration')
                                <button type="button" class="btn btn-success mb-1 btn-sm" data-toggle="modal" data-target="#addDuration">
                                    {{ __('pages.add_duration') }}
                                </button>
                            @endcan
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr class="text-center">
                                <th>{{ __('pages.name') }}</th>
                                <th>{{ __('pages.start') }}</th>
                                <th>{{ __('pages.end') }}</th>
                                <th>{{ __('pages.order') }}</th>
                                <th>{{ __('pages.duration_type') }}</th>
                                <th>{{ __('pages.actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($durations) > 0)
                                @foreach($durations as $duration)
                                    <tr>
                                        <td class="text-center"><span>{{ $duration->name }}</span></td>
                                        <td class="text-center"><span>{{ $duration->start }}</span></td>
                                        <td class="text-center"><span>{{ $duration->end }}</span></td>
                                        <td class="text-center"><span>{{ $duration->order }}</span></td>
                                        <td class="text-center"><span>{{ $duration->durationType->name }}</span></td>
                                        <td class="text-center">
                                            @can('view_duration')
                                                <a href="{{ route('admin.durations.show', $duration->id) }}" class="btn-no-bg" title="{{ __('pages.show_edit') }}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            @endcan

                                            @can('delete_duration')
                                                <a href="javascript:void(0)" data-durationid="{{ $duration->id }}" class="btn-no-bg delete-duration" title="{{ __('pages.delete') }}">
                                                    <i class="fa fa-trash-o"></i>
                                                </a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="6">{{ __('messages.not_found_any_durations') }}</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    <div class="table-footer">
                        {!! $durations->render() !!}
                        <div class="total-found">{{ __('pages.total_found') }}: {{ $durations->total() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @can('create_duration')
        <div class="modal fade" id="addDuration" role="dialog" aria-labelledby="addDuration" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    {!! Form::open(['url' => route('admin.durations.store'), 'method' => 'post', 'autocomplete' => 'off']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.add_duration') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('name', __('pages.name'), ['class' => 'control-label']) !!}
                                        {!! Form::text('name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('start', __('pages.start'), ['class' => 'control-label']) !!}
                                        {!! Form::date('start', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('end', __('pages.end'), ['class' => 'control-label']) !!}
                                        {!! Form::date('end', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('order', __('pages.order'), ['class' => 'control-label']) !!}
                                        {!! Form::number('order', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('duration_type_id', __('pages.duration_type'), ['class' => 'control-label']) !!}
                                        <select data-placeholder="{{ __('pages.chose_duration_type') }}" class="standardSelect" name="duration_type_id" id="duration_type_id">
                                            @foreach($durationTypes as $key => $durationType)
                                                <option value="{{ $key }}">{{ $durationType }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            {!! Form::submit(__('pages.add'), ['class' => 'btn btn-success btn-sm']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan

    @can('delete_duration')
        <div class="modal fade" id="deleteDuration" role="dialog" aria-labelledby="deleteDuration" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['route' => 'admin.durations.delete']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.delete_duration') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-info" role="alert">
                                <div class="alert-text">
                                    {{ __('pages.sure_to_delete_duration?') }}
                                </div>
                            </div>
                            <input type="hidden" name="duration_id">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            <button type="submit" class="btn btn-danger btn-sm">{{ __('pages.delete') }}</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan
@endsection

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('vendors/chosen/chosen.min.css') }}">
@endsection

@section('custom-scripts')
    <script src="{{ asset('vendors/chosen/chosen.jquery.min.js') }}"></script>
    <script>
        $(function() {
            $('.standardSelect').chosen({
                disable_search_threshold: 10,
                no_results_text: '{{ __('pages.nothing_found') }}',
                width: '100%'
            });
        });
    </script>
    <script>
        let body = $('body');

        body.on('click', '.delete-duration', function () {
            let durationId = $(this).attr('data-durationid');
            let modal = $('#deleteDuration');
            modal.find('input[name="duration_id"]').val(durationId);
            modal.modal('show');
        });

        $('#deleteDuration').on('hidden.bs.modal', function () {
            $(this).find('input[name="duration_id"]').val('');
        });
    </script>
@endsection