@extends('admin::layouts.app')

@section('title')
    {{ __('pages.duration_type') }} {{ $durationType->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-info-circle"></i>
                    <strong class="card-title">{{ __('pages.duration_type_information') }}</strong>
                </div>
                <div class="card-body">
                    <ul style="list-style: none;">
                        <li><span>{{ __('pages.name') }}:</span><b>&nbsp;{{ $durationType->name }}</b></li>
                        <li><span>{{ __('pages.short_name') }}:</span><b>&nbsp;{{ $durationType->short_name }}</b></li>
                    </ul>
                </div>
            </div>
        </div>

        @can('edit_duration_type')
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-edit"></i>
                        <strong class="card-title">{{ __('pages.edit_duration_type') }}</strong>
                    </div>
                    <div class="card-body">
                        {!! Form::model($durationType, ['method' => 'PATCH', 'route' => ['admin.duration-types.update', $durationType->id], 'autocomplete' => 'off']) !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('name', __('pages.name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('short_name', __('pages.short_name'), ['class' => 'control-label']) !!}
                                    {!! Form::text('short_name', null, ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                        </div>
                        {!! Form::submit(__('pages.edit'), ['class' => 'btn btn-primary btn-sm']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        @endcan
    </div>
@endsection

@section('custom-css')
@endsection

@section('custom-scripts')
@endsection
