@extends('admin::layouts.app')

@section('title')
    {{ __('pages.students') }}
@endsection

@section('content')
    <div class="custom-loader" style="display: none"></div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <button class="btn btn-primary mb-1 btn-sm" type="button" data-toggle="collapse" data-target="#searchBlock"
                                    aria-expanded="false" aria-controls="searchBlock">{{ __('pages.search/export') }}
                            </button>
                        </div>
                        <div class="col-md-5 col-sm-6">
                            <div class="row">
                                <div class="col-md-4 text-center">
                                    <div class="info-circle-blocked"></div>
                                    <span>{{ __('pages.blocked') }}</span>
                                </div>
                                <div class="col-md-4 text-center">
                                    <div class="info-circle-expelled"></div>
                                    <span>{{ __('pages.expelled') }}</span>
                                </div>
                                <div class="col-md-4 text-center">
                                    <div class="info-circle-blocked-expelled"></div>
                                    <span>{{ __('pages.blocked_and_expelled') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 text-right">
                            @can('create_student')
                                <button type="button" class="btn btn-success mb-1 btn-sm" data-toggle="modal" data-target="#addStudent">
                                    {{ __('pages.add_student') }}
                                </button>

                                @if($existAdmission && $existAdmission !== 0)
                                    <button type="button" class="btn btn-info mb-1 btn-sm" data-toggle="modal" data-target="#importAbiturients">
                                        {{ __('pages.import_abiturients') }}
                                    </button>
                                @endif
                            @endcan
                        </div>

                        <div class="collapse col-md-12 @if($searchMode) show @endif" id="searchBlock" style="margin-top: 10px;">
                            {!! Form::open(['route' => 'admin.students.search', 'method' => 'GET']) !!}
                                <div class="row" style="margin-bottom: 10px">
                                    <div class="col-md-3">
                                        {!! Form::text('name', $name, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.first_name') . ' / ' . __('pages.last_name'), 'id' => 'name_export']) !!}
                                    </div>
                                    <div class="col-md-2">
                                        {!! Form::text('idnp', $idnp, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.idnp'), 'id' => 'idnp_export']) !!}
                                    </div>
                                    <div class="col-md-2">
                                        {!! Form::text('phone', $phone, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.phone') . ' / ' . __('pages.mobile'), 'id' => 'phone_export']) !!}
                                    </div>
                                    <div class="col-md-2">
                                        {!! Form::text('email', $email, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.email'), 'id' => 'email_export']) !!}
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::text('city', $city, ['class' => 'input-sm form-control-sm form-control', 'placeholder' => __('pages.city'), 'id' => 'city_export']) !!}
                                    </div>
                                </div>

                                <div class="row" style="margin-bottom: 10px">
                                    <div class="col-md-3">
                                        {!! Form::label('cycleIds', __('pages.cycle'), ['class' => 'col-form-label-sm mb-0px pad-0px']) !!}
                                        <select data-placeholder="{{ __('pages.chose_cycle') }}" multiple class="standardSelect" name="cycleIds[]" id="cycleIds">
                                            @foreach($cycles as $key => $cycle)
                                                <option value="{{ $key }}" @if(in_array($key, $cycleIds)) selected @endif>{{ $cycle }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::label('facultyIds', __('pages.faculty'), ['class' => 'col-form-label-sm mb-0px pad-0px']) !!}
                                        <select data-placeholder="{{ __('pages.chose_faculty') }}" multiple class="standardSelect" name="facultyIds[]" id="facultyIds">
                                            @if(count($facultyIds) > 0)
                                                @foreach($facultyIds as $id)
                                                    <option value="{{ $id }}" selected >{{ $faculties[$id] }}</option>
                                                @endforeach
                                            @else
                                                <option value="0">{{ __('pages.chose_cycle') }}</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::label('specialityIds', __('pages.speciality'), ['class' => 'col-form-label-sm mb-0px pad-0px']) !!}
                                        <select data-placeholder="{{ __('pages.chose_speciality') }}" multiple class="standardSelect" name="specialityIds[]" id="specialityIds">
                                            @if(count($specialityIds) > 0)
                                                @foreach($specialityIds as $id)
                                                    <option value="{{ $id }}" selected >{{ $specialities[$id] }}</option>
                                                @endforeach
                                            @else
                                                <option value="0">{{ __('pages.chose_faculty') }}</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::label('groupIds', __('pages.group'), ['class' => 'col-form-label-sm mb-0px pad-0px']) !!}
                                        <select data-placeholder="{{ __('pages.chose_group') }}" multiple class="standardSelect" name="groupIds[]" id="groupIds">
                                            @if(count($groupIds) > 0)
                                                @foreach($groupIds as $id)
                                                    <option value="{{ $id }}" selected >{{ $groups[$id] }}</option>
                                                @endforeach
                                            @else
                                                <option value="0">{{ __('pages.chose_speciality') }}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="row" style="margin-bottom: 10px">
                                    <div class="col-md-3">
                                        {!! Form::label('yearOfStudyIds', __('pages.year_of_study'), ['class' => 'col-form-label-sm mb-0px pad-0px']) !!}
                                        <select data-placeholder="{{ __('pages.chose_year_of_study') }}" multiple class="standardSelect" name="yearOfStudyIds[]" id="yearOfStudyIds">
                                            @foreach($yearOfStudies as $key => $yearOfStudy)
                                                <option value="{{ $key }}" @if(in_array($key, $yearOfStudyIds)) selected @endif>{{ $yearOfStudy }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::label('budget_export', __('pages.budget'), ['class' => 'col-form-label-sm mb-0px pad-0px']) !!}
                                        <select data-placeholder="{{ __('pages.budget') }}" class="standardSelect" name="budget" title="{{ __('pages.budget') }}" id="budget_export">
                                            <option value="" @if($budget !== '1' && $budget !== '0') selected @endif>{{ __('pages.select') }}</option>
                                            <option value="1" @if($budget == '1') selected @endif>{{ __('pages.yes') }}</option>
                                            <option value="0" @if($budget == '0') selected @endif>{{ __('pages.no') }}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::label('expelled_export', __('pages.expelled'), ['class' => 'col-form-label-sm mb-0px pad-0px']) !!}
                                        <select data-placeholder="{{ __('pages.expelled') }}" class="standardSelect" name="expelled" title="{{ __('pages.expelled') }}" id="expelled_export">
                                            <option value="" @if($expelled !== '1' && $expelled !== '0') selected @endif>{{ __('pages.select') }}</option>
                                            <option value="1" @if($expelled == '1') selected @endif>{{ __('pages.yes') }}</option>
                                            <option value="0" @if($expelled == '0') selected @endif>{{ __('pages.no') }}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        {!! Form::label('study_export', __('pages.study'), ['class' => 'col-form-label-sm mb-0px pad-0px']) !!}
                                        <select data-placeholder="{{ __('pages.study') }}" class="standardSelect" name="study" title="{{ __('pages.study') }}" id="study_export">
                                            <option value="" @if($study !== '1' && $study !== '2') selected @endif>{{ __('pages.select') }}</option>
                                            <option value="1" @if($study == '1') selected @endif>{{ __('pages.morning') }}</option>
                                            <option value="2" @if($study == '2') selected @endif>{{ __('pages.afternoon') }}</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        {!! Form::submit(__('pages.search'), ['class' => 'btn btn-primary btn-sm']) !!}
                                        <a href="{{ route('admin.students.index') }}" class="btn btn-info btn-sm" title="{{ __('pages.reset_filter') }}">
                                            <i class="fa fa-refresh"></i>
                                        </a>

                                        @can('export_student')
                                            <button type="button" class="btn btn-warning btn-sm export-students-pdf">
                                                <i class="fa fa-file-pdf"></i>
                                            </button>
                                            <button type="button" class="btn btn-warning btn-sm export-students-excel">
                                                <i class="fa fa-file-excel"></i>
                                            </button>
                                        @endcan
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                        <tr class="text-center">
                            <th scope="row">#</th>
                            <th>{{ __('pages.full_name') }}</th>
                            <th>{{ __('pages.year_of_study') }}</th>
                            <th>{{ __('pages.cycle') }}</th>
                            <th>{{ __('pages.faculty') }}</th>
                            <th>{{ __('pages.speciality') }}</th>
                            <th>{{ __('pages.group') }}</th>
                            <th>{{ __('pages.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($students) > 0)
                            @foreach($students as $student)
                                @php
                                    if($student->user->photo == null || $student->user->photo == ''){
                                        $photo = asset('images/default_photo.jpg');
                                    } else {
                                        $photo = asset('storage/users-photo/' . $student->user->photo);
                                    }
                                @endphp

                                @php
                                    $class = '';
                                    $title = '';

                                    if($student->blocked == 1 && $student->expelled == 1) {
                                        $class = 'blocked-expelled';
                                        $title = __('pages.blocked_and_expelled');
                                    } elseif ($student->blocked == 1 && $student->expelled == 0) {
                                        $class = 'blocked';
                                        $title = __('pages.blocked');
                                    } elseif ($student->blocked == 0 && $student->expelled == 1) {
                                        $class = 'expelled';
                                        $title = __('pages.expelled');
                                    }
                                @endphp

                                <tr class="{{ $class }}" title="{{ $title }}">
                                    <td class="text-center"><img class="user-avatar rounded-circle" src="{{ $photo }}" alt="Photo" style="width: 32px;"></td>
                                    <td class="text-center"><span>{{ $student->last_name }} {{ $student->first_name }}</span></td>
                                    <td class="text-center"><span>{!! $student->getAllStudentYearOfStudies() !!}</span></td>
                                    <td class="text-center" style="width: 10%;"><span>{!! $student->getAllStudentCycles() !!}</span></td>
                                    <td class="text-center"><span>{!! $student->getAllStudentFaculties() !!}</span></td>
                                    <td class="text-center"><span>{!! $student->getAllStudentSpecialities() !!}</span></td>
                                    <td class="text-center"><span>{!! $student->getAllStudentGroups() !!}</span></td>
                                    <td class="text-center">
                                        @can('view_student')
                                            <a href="{{ route('admin.students.show', $student->id) }}" class="btn-no-bg" title="{{ __('pages.show_edit') }}">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        @endcan

                                        @can('delete_student')
                                            <a href="javascript:void(0)" data-studentid="{{ $student->id }}" class="btn-no-bg delete-student" title="{{ __('pages.delete') }}">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                            @if($student->blocked)
                                                <a href="javascript:void(0)" data-studentid="{{ $student->id }}" class="btn-no-bg unblock-student" title="{{ __('pages.unblock') }}">
                                                    <i class="fa fa-unlock"></i>
                                                </a>
                                            @else
                                                <a href="javascript:void(0)" data-studentid="{{ $student->id }}" class="btn-no-bg block-student" title="{{ __('pages.block') }}">
                                                    <i class="fa fa-lock"></i>
                                                </a>
                                            @endif
                                        @endcan

                                        @can('export_student')
                                            <a href="javascript:void(0)" data-studentid="{{ $student->id }}" class="btn-no-bg export-student-pdf" title="{{ __('pages.export') }}">
                                                <i class="fa fa-file-pdf"></i>
                                            </a>
                                            <a href="javascript:void(0)" data-studentid="{{ $student->id }}" class="btn-no-bg export-student-excel" title="{{ __('pages.export') }}">
                                                <i class="fa fa-file-excel"></i>
                                            </a>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="text-center">
                                <td colspan="8">{{ __('messages.not_found_any_students') }}</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    <div class="table-footer">
                        {!! $students->render() !!}
                        <div class="total-found">{{ __('pages.total_found') }}: {{ $students->total() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @can('create_student')
        <div class="modal fade" id="addStudent" role="dialog" aria-labelledby="addStudent" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    {!! Form::open(['url' => route('admin.students.store'), 'method' => 'post', 'autocomplete' => 'off']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.add_student') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="custom-tab">
                                <nav>
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        <a class="nav-item nav-link active show" id="information-tab" data-toggle="tab" href="#information" role="tab" aria-controls="information" aria-selected="true">
                                            <i class="fa fa-info-circle"></i>
                                            {{ __('pages.base_info') }}
                                        </a>
                                        <a class="nav-link" id="new-tab" href="javascript:void(0)" role="tab">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </nav>

                                <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                                    <div class="tab-pane fade active show" id="information" role="tabpanel" aria-labelledby="information-tab">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('first_name', __('pages.first_name'), ['class' => 'control-label']) !!}
                                                    {!! Form::text('first_name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('last_name', __('pages.last_name'), ['class' => 'control-label']) !!}
                                                    {!! Form::text('last_name', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('patronymic', __('pages.patronymic'), ['class' => 'control-label']) !!}
                                                    {!! Form::text('patronymic', '', ['class' => 'input-sm form-control-sm form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('idnp', __('pages.idnp'), ['class' => 'control-label']) !!}
                                                    {!! Form::text('idnp', '', ['class' => 'input-sm form-control-sm form-control']) !!}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('cycle_id', __('pages.cycle'), ['class' => 'control-label']) !!}
                                                    <select data-placeholder="{{ __('pages.chose_cycle') }}" class="standardSelect"
                                                            name="cycle_id" id="cycle_id" required title="{{ __('pages.cycle') }}">
                                                        <option value="0">{{ __('pages.chose_cycle') }}</option>
                                                        @foreach($cycles as $key => $cycle)
                                                            <option value="{{ $key }}">{{ $cycle }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('faculty_id', __('pages.faculty'), ['class' => 'control-label']) !!}
                                                    <select data-placeholder="{{ __('pages.chose_faculty') }}" class="standardSelect"
                                                            name="faculty_id" id="faculty_id" required title="{{ __('pages.faculty') }}">
                                                        <option value="0">{{ __('pages.chose_cycle') }}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('speciality_id', __('pages.speciality'), ['class' => 'control-label']) !!}
                                                    <select data-placeholder="{{ __('pages.chose_speciality') }}" class="standardSelect"
                                                            name="speciality_id" id="speciality_id" required title="{{ __('pages.speciality') }}">
                                                        <option value="0">{{ __('pages.chose_faculty') }}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('group_id', __('pages.group'), ['class' => 'control-label']) !!}
                                                    <select data-placeholder="{{ __('pages.chose_group') }}" class="standardSelect"
                                                            name="group_id" id="group_id" required title="{{ __('pages.group') }}">
                                                        <option value="0">{{ __('pages.chose_speciality') }}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    {!! Form::label('year_of_study_id', __('pages.year_of_study'), ['class' => 'control-label']) !!}
                                                    <select data-placeholder="{{ __('pages.year_of_study') }}" class="standardSelect"
                                                            name="year_of_study_id" id="year_of_study_id" required title="{{ __('pages.year_of_study') }}">
                                                        @foreach($yearOfStudies as $key => $yearOfStudy)
                                                            <option value="{{ $key }}">{{ $yearOfStudy }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group">
                                                    {!! Form::label('gender', __('pages.gender'), ['class' => 'control-label']) !!}
                                                    <select data-placeholder="{{ __('pages.chose_gender') }}" class="standardSelect"
                                                            name="gender" id="gender" title="{{ __('pages.gender') }}">
                                                        <option value="M">{{ __('pages.male') }}</option>
                                                        <option value="F">{{ __('pages.female') }}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('personal_email', __('pages.personal_email'), ['class' => 'control-label']) !!}
                                                    {!! Form::email('personal_email', '', ['class' => 'input-sm form-control-sm form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('mobile', __('pages.mobile'), ['class' => 'control-label']) !!}
                                                    {!! Form::text('mobile', '', ['class' => 'input-sm form-control-sm form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('birth_date', __('pages.birth_date'), ['class' => 'control-label']) !!}
                                                    {!! Form::text('birth_date', '', ['class' => 'input-sm form-control-sm form-control datepicker', 'required' => 'required']) !!}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('city', __('pages.city'), ['class' => 'control-label']) !!}
                                                    {!! Form::text('city', '', ['class' => 'input-sm form-control-sm form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('address', __('pages.address'), ['class' => 'control-label']) !!}
                                                    {!! Form::text('address', '', ['class' => 'input-sm form-control-sm form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('citizenship', __('pages.citizenship'), ['class' => 'control-label']) !!}
                                                    {!! Form::text('citizenship', '', ['class' => 'input-sm form-control-sm form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('nationality', __('pages.nationality'), ['class' => 'control-label']) !!}
                                                    {!! Form::text('nationality', '', ['class' => 'input-sm form-control-sm form-control']) !!}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('year_of_admission', __('pages.year_of_admission'), ['class' => 'control-label']) !!}
                                                    {!! Form::text('year_of_admission', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('study', __('pages.study'), ['class' => 'control-label']) !!}
                                                    <select data-placeholder="{{ __('pages.study') }}" class="standardSelect"
                                                            name="study" id="study" title="{{ __('pages.study') }}">
                                                        <option value="1">{{ __('pages.morning') }}</option>
                                                        <option value="2">{{ __('pages.afternoon') }}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('budget', __('pages.budget'), ['class' => 'control-label']) !!}
                                                    <select data-placeholder="{{ __('pages.budget') }}" class="standardSelect"
                                                            name="budget" id="budget" title="{{ __('pages.budget') }}">
                                                        <option value="1">{{ __('pages.yes') }}</option>
                                                        <option value="0">{{ __('pages.no') }}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('expelled', __('pages.expelled'), ['class' => 'control-label']) !!}
                                                    <select data-placeholder="{{ __('pages.expelled') }}" class="standardSelect"
                                                            name="expelled" id="expelled" title="{{ __('pages.expelled') }}">
                                                        <option value="1">{{ __('pages.yes') }}</option>
                                                        <option value="0">{{ __('pages.no') }}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('email', __('pages.email'), ['class' => 'control-label']) !!}
                                                    {!! Form::email('email', '', ['class' => 'input-sm form-control-sm form-control', 'required' => 'required']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('password', __('pages.password'), ['class' => 'control-label']) !!}
                                                    {!! Form::input('password', 'password', '', ['class' => 'input-sm form-control-sm form-control',
                                                        'autocomplete' => 'off', 'required' => 'required']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('password_confirmation', __('pages.confirm_password'), ['class' => 'control-label']) !!}
                                                    {!! Form::input('password', 'password_confirmation', null, ['class' => 'input-sm form-control-sm form-control',
                                                        'autocomplete' => 'off', 'required' => 'required']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {!! Form::label('permissions', __('pages.permissions'), ['class' => 'control-label']) !!}
                                                    <select data-placeholder="{{ __('pages.chose_permissions') }}" multiple class="standardSelect" name="permissions[]" id="permissions" required>
                                                        @foreach($permissions as $permission)
                                                            <option value="{{ $permission->id }}" data-permission="{{ $permission->name }}">{{ __('permissions.' . $permission->name) }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {!! Form::label('additional_info', __('pages.additional_info'), ['class' => 'control-label']) !!}
                                                    {!! Form::textarea('additional_info', '', ['class' => 'input-sm form-control-sm form-control', 'rows' => 4, 'style' => 'resize: none']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            {!! Form::submit(__('pages.add'), ['class' => 'btn btn-success btn-sm']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        @include('imports.students-modals')
    @endcan

    @can('delete_student')
        <div class="modal fade" id="deleteStudent" role="dialog" aria-labelledby="deleteStudent" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['route' => 'admin.students.delete']) !!}
                    <div class="modal-header">
                        <h5 class="modal-title">{{ __('pages.delete_student') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info" role="alert">
                            <div class="alert-text">
                                {{ __('pages.sure_to_delete_student?') }}
                            </div>
                        </div>
                        <input type="hidden" name="student_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                        <button type="submit" class="btn btn-danger btn-sm">{{ __('pages.delete') }}</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div class="modal fade" id="unblockStudent" role="dialog" aria-labelledby="unblockStudent" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['route' => 'admin.students.unblock']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.unblock_student') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-info" role="alert">
                                <div class="alert-text">
                                    {{ __('pages.sure_to_unblock_student?') }}
                                </div>
                            </div>
                            <input type="hidden" name="student_id">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            <button type="submit" class="btn btn-danger btn-sm">{{ __('pages.unblock') }}</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div class="modal fade" id="blockStudent" role="dialog" aria-labelledby="blockStudent" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {!! Form::open(['route' => 'admin.students.block']) !!}
                        <div class="modal-header">
                            <h5 class="modal-title">{{ __('pages.block_student') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-info" role="alert">
                                <div class="alert-text">
                                    {{ __('pages.sure_to_block_student?') }}
                                </div>
                            </div>
                            <input type="hidden" name="student_id">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('pages.close') }}</button>
                            <button type="submit" class="btn btn-warning btn-sm">{{ __('pages.block') }}</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endcan
@endsection

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('vendors/chosen/chosen.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
    <style>
        .modal-xl {
            max-width: 1200px
        }
    </style>
@endsection

@section('custom-scripts')
    <script src="{{ asset('vendors/chosen/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>

    @if (LaravelLocalization::getCurrentLocale() == 'ro')
        <script src="{{ asset('js/bootstrap-datepicker.ro.min.js') }}"></script>
    @endif
    @if(LaravelLocalization::getCurrentLocale() == 'ru')
        <script src="{{ asset('js/bootstrap-datepicker.ru.min.js') }}"></script>
    @endif

    <script src="{{ asset('js/import-students.js') }}"></script>

    <script>
        $(function() {
            $('.standardSelect').chosen({
                disable_search_threshold: 10,
                no_results_text: '{{ __('pages.nothing_found') }}',
                width: '100%'
            });

            $('.datepicker').datepicker({
                format: 'yyyy/mm/dd',
                language: '{{ LaravelLocalization::getCurrentLocale() }}'
            });

            let cycleIds = $('#cycleIds');

            if(cycleIds.val() !== ''){
                getFacultiesByCycleForSearch();
            }
        });
    </script>
    <script>
        let body = $('body');

        let additionalTabs = 0;
        body.on('click', '#new-tab', function () {
            let navTabs = $('#nav-tab');
            let tabContents = $('#nav-tabContent');
            additionalTabs ++;

            let buttonHtml = '' +
                '<a class="nav-item nav-link active show" id="information-tab-'+ additionalTabs +'" data-toggle="tab" ' +
                '    href="#information'+ additionalTabs +'" role="tab" aria-controls="information'+ additionalTabs +'" aria-selected="true">' +
                '    <i class="fa fa-info-circle"></i>' +
                '    {{ __('pages.additional_study') }}' +
                '</a>';

            let tabHtml = '' +
                '<div class="tab-pane fade active show" id="information'+ additionalTabs +'" role="tabpanel" aria-labelledby="information-tab">' +
                '    <div class="row">' +
                '        <div class="col-md-12 text-center">' +
                '            <button type="button" class="btn btn-danger mb-1 btn-sm remove-tab" title="{{ __('pages.delete') }}"' +
                '                data-tab="information'+ additionalTabs +'" data-tab-counter="'+ additionalTabs +'">' +
                '                <i class="fa fa-remove"></i>' +
                '            </button>' +
                '        </div>' +
                '    </div>' +


                '    <div class="row">' +
                '        <div class="col-md-3">' +
                '            <div class="form-group">' +
                '                <label for="cycle_id_'+ additionalTabs +'" class="control-label">{{ __('pages.cycle') }}</label>' +
                '                <select data-placeholder="{{ __('pages.chose_cycle') }}" class="standardSelect"' +
                '                    name="additional_cycles[]" id="cycle_id_'+ additionalTabs +'" required title="{{ __('pages.cycle') }}">' +
                '                    <option value="0">{{ __('pages.chose_cycle') }}</option>' +
                '                        @foreach($cycles as $key => $cycle)' +
                '                            <option value="{{ $key }}">{{ $cycle }}</option>' +
                '                        @endforeach' +
                '                </select>' +
                '            </div>' +
                '        </div>' +

                '        <div class="col-md-3">' +
                '            <div class="form-group">' +
                '                <label for="faculty_id_'+ additionalTabs +'" class="control-label">{{ __('pages.faculty') }}</label>' +
                '                <select data-placeholder="{{ __('pages.chose_faculty') }}" class="standardSelect"' +
                '                    name="additional_faculties[]" id="faculty_id_'+ additionalTabs +'" required title="{{ __('pages.faculty') }}">' +
                '                    <option value="0">{{ __('pages.chose_cycle') }}</option>' +
                '                </select>' +
                '            </div>' +
                '        </div>' +

                '        <div class="col-md-3">' +
                '            <div class="form-group">' +
                '                <label for="speciality_id_'+ additionalTabs +'" class="control-label">{{ __('pages.speciality') }}</label>' +
                '                <select data-placeholder="{{ __('pages.chose_speciality') }}" class="standardSelect"' +
                '                    name="additional_specialities[]" id="speciality_id_'+ additionalTabs +'" required title="{{ __('pages.speciality') }}">' +
                '                    <option value="0">{{ __('pages.chose_faculty') }}</option>' +
                '                </select>' +
                '            </div>' +
                '        </div>' +

                '        <div class="col-md-3">' +
                '            <div class="form-group">' +
                '                <label for="group_id_'+ additionalTabs +'" class="control-label">{{ __('pages.group') }}</label>' +
                '                <select data-placeholder="{{ __('pages.chose_group') }}" class="standardSelect"' +
                '                    name="additional_groups[]" id="group_id_'+ additionalTabs +'" required title="{{ __('pages.group') }}">' +
                '                    <option value="0">{{ __('pages.chose_speciality') }}</option>' +
                '                </select>' +
                '            </div>' +
                '        </div>' +
                '    </div>' +


                '    <div class="row">' +
                '        <div class="col-md-2">' +
                '            <div class="form-group">' +
                '                <label for="year_of_study_id_'+ additionalTabs +'" class="control-label">{{ __('pages.year_of_study') }}</label>' +
                '                <select data-placeholder="{{ __('pages.year_of_study') }}" class="standardSelect"' +
                '                    name="additional_year_of_studies[]" id="year_of_study_id_'+ additionalTabs +'" required title="{{ __('pages.year_of_study') }}">' +
                '                    @foreach($yearOfStudies as $key => $yearOfStudy)' +
                '                        <option value="{{ $key }}">{{ $yearOfStudy }}</option>' +
                '                    @endforeach' +
                '                </select>' +
                '            </div>' +
                '        </div>' +

                '        <div class="col-md-2">' +
                '            <div class="form-group">' +
                '                <label for="year_of_admission_'+ additionalTabs +'" class="control-label">{{ __('pages.year_of_admission') }}</label>' +
                '                <input class="input-sm form-control-sm form-control" required name="year_of_admissions[]" type="text" id="year_of_admission_'+ additionalTabs +'">' +
                '            </div>' +
                '        </div>' +

                '        <div class="col-md-2">' +
                '            <div class="form-group">' +
                '                <label for="study_'+ additionalTabs +'" class="control-label">{{ __('pages.study') }}</label>' +
                '                <select data-placeholder="{{ __('pages.study') }}" class="standardSelect" name="studies[]" id="study_'+ additionalTabs +'" title="{{ __('pages.study') }}">' +
                '                    <option value="1">{{ __('pages.morning') }}</option>' +
                '                    <option value="2">{{ __('pages.afternoon') }}</option>' +
                '                </select>' +
                '            </div>' +
                '        </div>' +

                '        <div class="col-md-2">' +
                '            <div class="form-group">' +
                '                <label for="budget_'+ additionalTabs +'" class="control-label">{{ __('pages.budget') }}</label>' +
                '                <select data-placeholder="{{ __('pages.budget') }}" class="standardSelect" name="budgets[]" id="budget_'+ additionalTabs +'" title="{{ __('pages.budget') }}">' +
                '                    <option value="1">{{ __('pages.yes') }}</option>' +
                '                    <option value="0">{{ __('pages.no') }}</option>' +
                '                </select>' +
                '            </div>' +
                '        </div>' +

                '        <div class="col-md-2">' +
                '            <div class="form-group">' +
                '                <label for="expelled_'+ additionalTabs +'" class="control-label">{{ __('pages.expelled') }}</label>' +
                '                <select data-placeholder="{{ __('pages.expelled') }}" class="standardSelect" name="expelleds[]" id="expelled_'+ additionalTabs +'" title="{{ __('pages.expelled') }}">' +
                '                    <option value="1">{{ __('pages.yes') }}</option>' +
                '                    <option value="0">{{ __('pages.no') }}</option>' +
                '                </select>' +
                '           </div>' +
                '        </div>' +
                '    </div>' +


                '    <div class="row">' +
                '        <div class="col-md-6">' +
                '            <div class="form-group">' +
                '                <label for="additional_info_'+ additionalTabs +'" class="control-label">{{ __('pages.additional_info') }}</label>' +
                '                <textarea class="input-sm form-control-sm form-control" rows="4" style="resize: none" name="additional_infos[]" cols="50" id="additional_info_'+ additionalTabs +'"></textarea>' +
                '            </div>' +
                '        </div>' +
                '    </div>' +
                '</div>';

            tabContents.find('div').removeClass('active show');
            navTabs.find('a').removeClass('active show');

            $(buttonHtml).insertBefore($(this));
            tabContents.append(tabHtml);

            $('#information'+ additionalTabs +' .standardSelect').chosen({
                disable_search_threshold: 10,
                no_results_text: '{{ __('pages.nothing_found') }}',
                width: '100%'
            });

            console.log('ADD: ' + additionalTabs);
        });

        body.on('click', '.remove-tab', function () {
            let navTabs = $('#nav-tab');
            let tabContents = $('#nav-tabContent');

            let tab = $(this).attr('data-tab');
            let tabCounter = $(this).attr('data-tab-counter');
            let deletedTabButton = $('#information-tab-' + tabCounter);
            let deletedTabContent = $('#' + tab);

            if(tabCounter > 0){
                additionalTabs --;
            }

            tabContents.find('div').removeClass('active show');
            navTabs.find('a').removeClass('active show');

            deletedTabButton.prev().addClass('active show');
            deletedTabButton.remove();

            deletedTabContent.prev().addClass('active show');
            deletedTabContent.remove();

            console.log('REMOVE: ' + additionalTabs);
        });

        body.on('click', '.delete-student', function () {
            let studentId = $(this).attr('data-studentid');
            let modal = $('#deleteStudent');
            modal.find('input[name="student_id"]').val(studentId);
            modal.modal('show');
        });

        body.on('click', '.unblock-student', function () {
            let studentId = $(this).attr('data-studentid');
            let modal = $('#unblockStudent');
            modal.find('input[name="student_id"]').val(studentId);
            modal.modal('show');
        });

        body.on('click', '.block-student', function () {
            let studentId = $(this).attr('data-studentid');
            let modal = $('#blockStudent');
            modal.find('input[name="student_id"]').val(studentId);
            modal.modal('show');
        });

        body.on('click', '#get_abiturients_for_import', function () {
            getAbiturientsForImport('{{ route('general.getStudentsForImport') }}', '{{ __('messages.all_fields_is_required') }}', '{{ __('pages.nothing_found') }}');
        });

        body.on('click', '#search_external', function () {
            getAbiturientsForImport('{{ route('general.getStudentsForImport') }}', '{{ __('messages.all_fields_is_required') }}', '{{ __('pages.nothing_found') }}');
        });

        body.on('click', '#import_external', function () {
            importStudents('{{ route('general.importStudents')}}', '{{ __('messages.please_select_cycle') }}',
                '{{ __('messages.please_select_at_least_one_abiturient') }}');
        });

        body.on('change', '#cycle_id_external', function () {
            getExternalSpecialitiesByCycleOrFaculty('{{ route('general.getExternalSpecialitiesByCycleOrFaculty')}}');
        });

        body.on('change', '#faculty_id_external', function () {
            getExternalSpecialitiesByCycleOrFaculty('{{ route('general.getExternalSpecialitiesByCycleOrFaculty')}}');
        });

        $('#deleteStudent').on('hidden.bs.modal', function () {
            $(this).find('input[name="student_id"]').val('');
        });

        $('#unblockStudent').on('hidden.bs.modal', function () {
            $(this).find('input[name="student_id"]').val('');
        });

        $('#blockStudent').on('hidden.bs.modal', function () {
            $(this).find('input[name="student_id"]').val('');
        });


        //For search
        body.on('change', '#cycleIds', function () {
            getFacultiesByCycleForSearch();
        });
        body.on('change', '#facultyIds', function () {
            getSpecialitiesByFacultyForSelectSearch();
        });
        body.on('change', '#specialityIds', function () {
            getGroupsBySpecialityForSelectSearch();
        });



        //For create
        body.on('change', '#cycle_id', function () {
            getFacultiesByCycleForSelectAddEdit('#cycle_id')
        });
        body.on('change', '#faculty_id', function () {
            getSpecialitiesByFacultyForSelectAddEdit('#faculty_id')
        });
        body.on('change', '#speciality_id', function () {
            getGroupsBySpecialityForSelectAddEdit('#speciality_id')
        });



        //For additional info
        body.on('change', 'select[name="additional_cycles[]"]', function () {
            let id = $(this).attr('id');
            getFacultiesByCycleForSelectAdditionalInfo('#' + id);
        });
        body.on('change', 'select[name="additional_faculties[]"]', function () {
            let id = $(this).attr('id');
            getSpecialitiesByFacultyForSelectAdditionalInfo('#' + id)
        });
        body.on('change', 'select[name="additional_specialities[]"]', function () {
            let id = $(this).attr('id');
            getGroupsBySpecialityForSelectAdditionalInfo('#' + id)
        });



        //For import
        body.on('change', '#cycle_id_import', function () {
            getFacultiesByCycleForSelectImport('#cycle_id_import', '{{ route('general.getFacultiesByCycleForSelectImport')}}')
        });
        body.on('change', '#faculty_id_import', function () {
            getSpecialitiesByFacultyForSelectImport('#faculty_id_import', '{{ route('general.getSpecialitiesByFacultyForSelectImport')}}')
        });
        body.on('change', '#speciality_id_import', function () {
            getGroupsBySpecialityForSelectImport('#speciality_id_import', '{{ route('general.getGroupsBySpecialityForSelectImport')}}')
        });


        body.on('click', '.export-students-pdf', function () {
            let loader = $('.custom-loader');

            let name = $.trim($('#name_export').val());
            let idnp = $.trim($('#idnp_export').val());
            let phone = $.trim($('#phone_export').val());
            let email = $.trim($('#email_export').val());
            let city = $.trim($('#city_export').val());

            let cycleIds = [];
            let facultyIds = [];
            let specialityIds = [];
            let groupIds = [];

            let yearOfStudyIds = [];
            let budget = $.trim($('#budget_export').val());
            let expelled = $.trim($('#expelled_export').val());
            let study = $.trim($('#study_export').val());

            $('#cycleIds :selected').each(function(i, selected) {
                cycleIds[i] = $(selected).val();
            });
            $('#facultyIds :selected').each(function(i, selected) {
                facultyIds[i] = $(selected).val();
            });
            $('#specialityIds :selected').each(function(i, selected) {
                specialityIds[i] = $(selected).val();
            });
            $('#groupIds :selected').each(function(i, selected) {
                groupIds[i] = $(selected).val();
            });
            $('#yearOfStudyIds :selected').each(function(i, selected) {
                yearOfStudyIds[i] = $(selected).val();
            });

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                async: true,
                url: '{{ route('admin.students.exportStudentsPdf') }}',
                type: 'POST',
                data: {
                    name: name,
                    idnp: idnp,
                    phone: phone,
                    email: email,
                    city: city,
                    cycleIds: cycleIds,
                    facultyIds: facultyIds,
                    specialityIds: specialityIds,
                    groupIds: groupIds,
                    yearOfStudyIds: yearOfStudyIds,
                    budget: budget,
                    expelled: expelled,
                    study: study,
                },
                success: function (response) {
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    if (response.type === 'warning') {
                        $.notify(response.message, {
                            autoHide: true,
                            autoHideDelay: 5000,
                            globalPosition: 'top right',
                            style: 'bootstrap',
                            className: 'warn'
                        });
                    } else {
                        $.notify(response.message, {
                            autoHide: true,
                            autoHideDelay: 5000,
                            globalPosition: 'top right',
                            style: 'bootstrap',
                            className: 'success'
                        });

                        let a = document.createElement('a');
                        a.href = response.url;
                        a.download = response.name;
                        document.body.append(a);
                        a.click();
                        a.remove();
                    }
                }
            });
        });

        body.on('click', '.export-students-excel', function () {
            let loader = $('.custom-loader');

            let name = $.trim($('#name_export').val());
            let idnp = $.trim($('#idnp_export').val());
            let phone = $.trim($('#phone_export').val());
            let email = $.trim($('#email_export').val());
            let city = $.trim($('#city_export').val());

            let cycleIds = [];
            let facultyIds = [];
            let specialityIds = [];
            let groupIds = [];

            let yearOfStudyIds = [];
            let budget = $.trim($('#budget_export').val());
            let expelled = $.trim($('#expelled_export').val());
            let study = $.trim($('#study_export').val());

            $('#cycleIds :selected').each(function(i, selected) {
                cycleIds[i] = $(selected).val();
            });
            $('#facultyIds :selected').each(function(i, selected) {
                facultyIds[i] = $(selected).val();
            });
            $('#specialityIds :selected').each(function(i, selected) {
                specialityIds[i] = $(selected).val();
            });
            $('#groupIds :selected').each(function(i, selected) {
                groupIds[i] = $(selected).val();
            });
            $('#yearOfStudyIds :selected').each(function(i, selected) {
                yearOfStudyIds[i] = $(selected).val();
            });

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                async: true,
                url: '{{ route('admin.students.exportStudentsExcel') }}',
                type: 'POST',
                data: {
                    name: name,
                    idnp: idnp,
                    phone: phone,
                    email: email,
                    city: city,
                    cycleIds: cycleIds,
                    facultyIds: facultyIds,
                    specialityIds: specialityIds,
                    groupIds: groupIds,
                    yearOfStudyIds: yearOfStudyIds,
                    budget: budget,
                    expelled: expelled,
                    study: study,
                },
                success: function (response) {
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    if (response.type === 'warning') {
                        $.notify(response.message, {
                            autoHide: true,
                            autoHideDelay: 5000,
                            globalPosition: 'top right',
                            style: 'bootstrap',
                            className: 'warn'
                        });
                    } else {
                        $.notify(response.message, {
                            autoHide: true,
                            autoHideDelay: 5000,
                            globalPosition: 'top right',
                            style: 'bootstrap',
                            className: 'success'
                        });

                        let a = document.createElement('a');
                        a.href = response.url;
                        a.download = response.name;
                        document.body.append(a);
                        a.click();
                        a.remove();
                    }
                }
            });
        });

        body.on('click', '.export-student-pdf', function () {
            let loader = $('.custom-loader');
            let studentId = $(this).attr('data-studentid');

            if(studentId === '' || studentId == null || studentId === undefined){
                $.notify('{{ __('messages.something_wrong') }}', {
                    autoHide: true,
                    autoHideDelay: 5000,
                    globalPosition: 'top right',
                    style: 'bootstrap',
                    className: 'warn'
                });
                return false;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                async: true,
                url: '{{ route('admin.students.exportStudentPdf')}}',
                type: 'POST',
                data: {
                    student_id: studentId
                },
                success: function (response) {
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    if (response.type === 'warning') {
                        $.notify(response.message, {
                            autoHide: true,
                            autoHideDelay: 5000,
                            globalPosition: 'top right',
                            style: 'bootstrap',
                            className: 'warn'
                        });
                    } else {
                        $.notify(response.message, {
                            autoHide: true,
                            autoHideDelay: 5000,
                            globalPosition: 'top right',
                            style: 'bootstrap',
                            className: 'success'
                        });

                        let a = document.createElement('a');
                        a.href = response.url;
                        a.download = response.name;
                        document.body.append(a);
                        a.click();
                        a.remove();
                    }
                }
            });
        });

        body.on('click', '.export-student-excel', function () {
            let loader = $('.custom-loader');
            let studentId = $(this).attr('data-studentid');

            if(studentId === '' || studentId == null || studentId === undefined){
                $.notify('{{ __('messages.something_wrong') }}', {
                    autoHide: true,
                    autoHideDelay: 5000,
                    globalPosition: 'top right',
                    style: 'bootstrap',
                    className: 'warn'
                });
                return false;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                async: true,
                url: '{{ route('admin.students.exportStudentExcel')}}',
                type: 'POST',
                data: {
                    student_id: studentId
                },
                success: function (response) {
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    if (response.type === 'warning') {
                        $.notify(response.message, {
                            autoHide: true,
                            autoHideDelay: 5000,
                            globalPosition: 'top right',
                            style: 'bootstrap',
                            className: 'warn'
                        });
                    } else {
                        $.notify(response.message, {
                            autoHide: true,
                            autoHideDelay: 5000,
                            globalPosition: 'top right',
                            style: 'bootstrap',
                            className: 'success'
                        });

                        let a = document.createElement('a');
                        a.href = response.url;
                        a.download = response.name;
                        document.body.append(a);
                        a.click();
                        a.remove();
                    }
                }
            });
        });

        //FUNCTIONS
        //For search
        function getFacultiesByCycleForSearch() {
            let loader = $('.custom-loader');
            let cyclesId = [];
            let facultySelect = $('#facultyIds');
            let alreadySelectedFaculties = facultySelect.val();

            $('#cycleIds :selected').each(function(i, selected) {
                cyclesId[i] = $(selected).val();
            });

            if(cyclesId.length === 0){
                facultySelect.empty();
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('admin.faculties.getFacultiesByCycleForSelectSearch')}}',
                type: 'POST',
                async: true,
                data: {
                    cyclesId: JSON.stringify(cyclesId)
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    facultySelect.empty();
                    facultySelect.html(response.html);
                    facultySelect.find('option').each(function(i, option) {
                        let optionValue = $(option).val();

                        if($.inArray(optionValue, alreadySelectedFaculties) !== -1){
                            $(this).attr('selected', true);
                        }
                    });
                    facultySelect.trigger('chosen:updated');

                    getSpecialitiesByFacultyForSelectSearch();
                }
            });
        }

        function getSpecialitiesByFacultyForSelectSearch() {
            let loader = $('.custom-loader');
            let facultiesId = [];
            let specialitySelect = $('#specialityIds');
            let alreadySelectedSpecialities = specialitySelect.val();

            $('#facultyIds :selected').each(function(i, selected) {
                facultiesId[i] = $(selected).val();
            });

            if(facultiesId.length === 0){
                specialitySelect.empty();
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('admin.specialities.getSpecialitiesByFacultyForSelectSearch')}}',
                type: 'POST',
                async: true,
                data: {
                    facultiesId: JSON.stringify(facultiesId)
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    specialitySelect.empty();
                    specialitySelect.html(response.html);
                    specialitySelect.find('option').each(function(i, option) {
                        let optionValue = $(option).val();

                        if($.inArray(optionValue, alreadySelectedSpecialities) !== -1){
                            $(this).attr('selected', true);
                        }
                    });
                    specialitySelect.trigger('chosen:updated');

                    getGroupsBySpecialityForSelectSearch();
                }
            });
        }

        function getGroupsBySpecialityForSelectSearch() {
            let loader = $('.custom-loader');
            let specialitiesId = [];
            let groupSelect = $('#groupIds');
            let alreadySelectedGroups = groupSelect.val();

            $('#specialityIds :selected').each(function(i, selected) {
                specialitiesId[i] = $(selected).val();
            });

            if(specialitiesId.length === 0){
                groupSelect.empty();
                groupSelect.trigger('chosen:updated');
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('admin.groups.getGroupsBySpecialityForSelectSearch')}}',
                type: 'POST',
                async: true,
                data: {
                    specialitiesId: JSON.stringify(specialitiesId)
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    groupSelect.empty();
                    groupSelect.html(response.html);
                    groupSelect.find('option').each(function(i, option) {
                        let optionValue = $(option).val();

                        if($.inArray(optionValue, alreadySelectedGroups) !== -1){
                            $(this).attr('selected', true);
                        }
                    });
                    groupSelect.trigger('chosen:updated');
                }
            });
        }



        //For add/edit
        function getFacultiesByCycleForSelectAddEdit(element) {
            let loader = $('.custom-loader');
            let cycleId = $.trim($(element).val());
            let facultySelect = $('#faculty_id');
            let specialitySelect = $('#speciality_id');
            let groupSelect = $('#group_id');

            if(cycleId === '' || cycleId === undefined || cycleId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('admin.faculties.getFacultiesByCycleForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    cycleId: cycleId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    facultySelect.empty();
                    facultySelect.html(response.html);
                    facultySelect.trigger('chosen:updated');

                    specialitySelect.empty();
                    specialitySelect.trigger('chosen:updated');

                    groupSelect.empty();
                    groupSelect.trigger('chosen:updated');

                    let totalFaculties = $('#faculty_id > option').length;
                    if(totalFaculties === 1){
                        getSpecialitiesByFacultyForSelectAddEdit('#faculty_id');
                    }

                    let totalSpecialities = $('#speciality_id > option').length;
                    if(totalSpecialities === 1){
                        getGroupsBySpecialityForSelectAddEdit('#speciality_id');
                    }
                }
            });
        }

        function getSpecialitiesByFacultyForSelectAddEdit(element) {
            let loader = $('.custom-loader');
            let facultyId = $.trim($(element).val());
            let specialitySelect = $('#speciality_id');

            if(facultyId === '' || facultyId === undefined || facultyId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('admin.specialities.getSpecialitiesByFacultyForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    facultyId: facultyId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    specialitySelect.empty();
                    specialitySelect.html(response.html);
                    specialitySelect.trigger('chosen:updated');

                    let totalSpecialities = $('#speciality_id > option').length;
                    if(totalSpecialities === 1){
                        getGroupsBySpecialityForSelectAddEdit('#speciality_id');
                    }
                }
            });
        }

        function getGroupsBySpecialityForSelectAddEdit(element) {
            let loader = $('.custom-loader');
            let specialityId = $.trim($(element).val());
            let groupSelect = $('#group_id');

            if(specialityId === '' || specialityId === undefined || specialityId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('admin.groups.getGroupsBySpecialityForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    specialityId: specialityId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    groupSelect.empty();
                    groupSelect.html(response.html);
                    groupSelect.trigger('chosen:updated');
                }
            });
        }



        //For additional info
        function getFacultiesByCycleForSelectAdditionalInfo(element) {
            let loader = $('.custom-loader');
            let cycleId = $.trim($(element).val());
            let parent = $(element).parents('.tab-pane');

            let facultySelect = parent.find('select[name="additional_faculties[]"]');
            let specialitySelect = parent.find('select[name="additional_specialities[]"]');
            let groupSelect = parent.find('select[name="additional_groups[]"]');

            if(cycleId === '' || cycleId === undefined || cycleId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('admin.faculties.getFacultiesByCycleForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    cycleId: cycleId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    facultySelect.empty();
                    facultySelect.html(response.html);
                    facultySelect.trigger('chosen:updated');

                    specialitySelect.empty();
                    specialitySelect.trigger('chosen:updated');

                    groupSelect.empty();
                    groupSelect.trigger('chosen:updated');

                    // // let totalFaculties = $('#faculty_id > option').length;
                    // let totalFaculties = facultySelect.find('option').length;
                    // if(totalFaculties === 1){
                    //     getSpecialitiesByFacultyForSelectAddEdit('#faculty_id');
                    // }
                    //
                    // // let totalSpecialities = $('#speciality_id > option').length;
                    // let totalSpecialities = specialitySelect.find('option').length;
                    // if(totalSpecialities === 1){
                    //     getGroupsBySpecialityForSelectAddEdit('#speciality_id');
                    // }
                }
            });
        }

        function getSpecialitiesByFacultyForSelectAdditionalInfo(element) {
            let loader = $('.custom-loader');
            let facultyId = $.trim($(element).val());
            let parent = $(element).parents('.tab-pane');
            let specialitySelect = parent.find('select[name="additional_specialities[]"]');

            if(facultyId === '' || facultyId === undefined || facultyId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('admin.specialities.getSpecialitiesByFacultyForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    facultyId: facultyId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    specialitySelect.empty();
                    specialitySelect.html(response.html);
                    specialitySelect.trigger('chosen:updated');

                    // // let totalSpecialities = $('#speciality_id > option').length;
                    // let totalSpecialities = specialitySelect.find('option').length;
                    // if(totalSpecialities === 1){
                    //     getGroupsBySpecialityForSelectAddEdit('#speciality_id');
                    // }
                }
            });
        }

        function getGroupsBySpecialityForSelectAdditionalInfo(element) {
            let loader = $('.custom-loader');
            let specialityId = $.trim($(element).val());
            let parent = $(element).parents('.tab-pane');
            let groupSelect = parent.find('select[name="additional_groups[]"]');

            if(specialityId === '' || specialityId === undefined || specialityId == null){
                return;
            }

            loader.show();
            body.css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('admin.groups.getGroupsBySpecialityForSelectAddEdit')}}',
                type: 'POST',
                async: true,
                data: {
                    specialityId: specialityId
                },
                success: function(response){
                    loader.hide();
                    body.css({
                        'pointer-events': '',
                        'opacity': ''
                    });

                    groupSelect.empty();
                    groupSelect.html(response.html);
                    groupSelect.trigger('chosen:updated');
                }
            });
        }
    </script>
@endsection