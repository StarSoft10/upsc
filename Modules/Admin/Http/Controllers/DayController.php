<?php

namespace Modules\Admin\Http\Controllers;

use App\Entities\Day;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Routing\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Helpers\AppHelper;

class DayController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
        $this->middleware('check_if_blocked');
        $this->helper = new AppHelper();
    }

    public function index()
    {
        if (!Auth::user()->hasPermissionTo('view_day')) {
            return redirect()->route('admin.not-found');
        }

        $days = Day::where('deleted', 0)->orderBy('order')->paginate(10);

        return view('admin::days.index', compact('days'));
    }

    public function store(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('create_day')) {
            return redirect()->route('admin.not-found');
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('days')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ],
            'order' => [
                'required',
                'numeric',
                Rule::unique('days')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $day = Day::create(['name' => trim($request->name), 'order' => trim($request->order)]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'days', 1, 'DB id: [' . $day->id . ']');

        return back()->with('success', __('messages.day_created_successfully'));
    }

    public function show($id)
    {
        if (!Auth::user()->hasPermissionTo('view_day')) {
            return redirect()->route('admin.not-found');
        }

        $day = Day::where('id', $id)->where('deleted', 0)->first();

        if (!$day) {
            return back()->with('warning', __('messages.day_not_found'));
        }

        return view('admin::days.show', compact('day'));
    }

    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermissionTo('edit_day')) {
            return redirect()->route('admin.not-found');
        }

        $day = Day::where('id', $id)->where('deleted', 0)->first();
        if (!$day) {
            return back()->with('warning', __('messages.day_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('days')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ],
            'order' => [
                'required',
                'numeric',
                Rule::unique('days')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        Day::where('id', $id)->update([
            'name' => trim($request->name),
            'order' => trim($request->order),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'days', 2, 'DB id: [' . $id . ']');

        return redirect()->route('admin.days.index')->with('success', __('messages.day_updated_successfully'));
    }

    public function delete(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_day')) {
            return redirect()->route('admin.not-found');
        }

        $validator = Validator::make($request->all(), [
            'day_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $day = Day::where('id', $request->day_id)->where('deleted', 0)->first();
        if (!$day) {
            return back()->with('warning', __('messages.day_not_found'));
        }

        Day::where('id', $request->day_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'days', 3, 'DB id: [' . $request->day_id . ']');

        return back()->with('success', __('messages.day_deleted_successfully'));
    }
}
