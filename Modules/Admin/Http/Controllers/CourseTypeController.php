<?php

namespace Modules\Admin\Http\Controllers;

use App\Entities\CourseType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\AppHelper;

class CourseTypeController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
        $this->middleware('check_if_blocked');
        $this->helper = new AppHelper();
    }

    public function index(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_course_type')) {
            return redirect()->route('admin.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';

        $courseTypes = CourseType::where('deleted', 0)->orderBy('id', 'ASC')->paginate(10);
        $courseTypes->appends([
            'name' => $name,
            'shortName' => $shortName
        ]);

        return view('admin::course-types.index', compact('courseTypes', 'name', 'shortName'));
    }

    public function search(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_course_type')) {
            return redirect()->route('admin.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';

        $courseTypes = CourseType::where('deleted', 0)->where(function ($query) use ($name, $shortName) {
            if ($name !== '') {
                $query->where('name', 'LIKE', '%' . trim($name) . '%');
            }
            if ($shortName !== '') {
                $query->where('short_name', 'LIKE', '%' . trim($shortName) . '%');
            }
        })->orderBy('id', 'ASC')->paginate(10);
        $courseTypes->appends([
            'name' => $name,
            'shortName' => $shortName
        ]);

        return view('admin::course-types.index', compact('courseTypes', 'name', 'shortName'));
    }

    public function store(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('create_course_type')) {
            return redirect()->route('admin.not-found');
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('course_types')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('course_types')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ],
            'color' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $courseType = CourseType::create([
            'name' => trim($request->name),
            'short_name' => trim($request->short_name),
            'color' => $request->color
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'course_types', 1, 'DB id: [' . $courseType->id . ']');

        return back()->with('success', __('messages.course_type_created_successfully'));
    }

    public function show($id)
    {
        if (!Auth::user()->hasPermissionTo('view_course_type')) {
            return redirect()->route('admin.not-found');
        }

        $courseType = CourseType::where('id', $id)->where('deleted', 0)->first();

        if (!$courseType) {
            return back()->with('warning', __('messages.course_type_not_found'));
        }

        return view('admin::course-types.show', compact('courseType'));
    }

    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermissionTo('edit_course_type')) {
            return redirect()->route('admin.not-found');
        }

        $courseType = CourseType::where('id', $id)->where('deleted', 0)->first();
        if (!$courseType) {
            return back()->with('warning', __('messages.course_type_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('course_types')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('course_types')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ],
            'color' => 'required'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        CourseType::where('id', $id)->update([
            'name' => trim($request->name),
            'short_name' => trim($request->short_name),
            'color' => $request->color,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'course_types', 2, 'DB id: [' . $id . ']');

        return redirect()->route('admin.course-types.index')->with('success', __('messages.course_type_updated_successfully'));
    }

    public function delete(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_course_type')) {
            return redirect()->route('admin.not-found');
        }

        $validator = Validator::make($request->all(), [
            'course_type_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $courseType = CourseType::where('id', $request->course_type_id)->where('deleted', 0)->first();
        if (!$courseType) {
            return back()->with('warning', __('messages.course_type_not_found'));
        }

        CourseType::where('id', $request->course_type_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'course_types', 3, 'DB id: [' . $request->course_type_id . ']');

        return back()->with('success', __('messages.course_type_deleted_successfully'));
    }
}
