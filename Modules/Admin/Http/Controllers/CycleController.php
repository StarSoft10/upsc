<?php

namespace Modules\Admin\Http\Controllers;

use App\Entities\Cycle;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\AppHelper;

class CycleController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
        $this->middleware('check_if_blocked');
        $this->helper = new AppHelper();
    }

    public function index(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_cycle')) {
            return redirect()->route('admin.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';

        $cycles = Cycle::where('deleted', 0)->orderBy('id', 'ASC')->paginate(10);
        $cycles->appends([
            'name' => $name,
            'shortName' => $shortName
        ]);

        return view('admin::cycles.index', compact('cycles', 'name', 'shortName'));
    }

    public function search(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_cycle')) {
            return redirect()->route('admin.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';

        $cycles = Cycle::where('deleted', 0)->where(function ($query) use ($name, $shortName) {
            if ($name !== '') {
                $query->where('name', 'LIKE', '%' . trim($name) . '%');
            }
            if ($shortName !== '') {
                $query->where('short_name', 'LIKE', '%' . trim($shortName) . '%');
            }
        })->orderBy('id', 'ASC')->paginate(10);
        $cycles->appends([
            'name' => $name,
            'shortName' => $shortName
        ]);

        return view('admin::cycles.index', compact('cycles', 'name', 'shortName'));
    }

    public function store(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('create_cycle')) {
            return redirect()->route('admin.not-found');
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('cycles')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('cycles')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $cycle = Cycle::create(['name' => trim($request->name), 'short_name' => trim($request->short_name)]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'cycles', 1, 'DB id: [' . $cycle->id . ']');

        return back()->with('success', __('messages.cycle_created_successfully'));
    }

    public function show($id)
    {
        if (!Auth::user()->hasPermissionTo('view_cycle')) {
            return redirect()->route('admin.not-found');
        }

        $cycle = Cycle::where('id', $id)->where('deleted', 0)->first();

        if (!$cycle) {
            return back()->with('warning', __('messages.cycle_not_found'));
        }

        return view('admin::cycles.show', compact('cycle'));
    }

    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermissionTo('edit_cycle')) {
            return redirect()->route('admin.not-found');
        }

        $cycle = Cycle::where('id', $id)->where('deleted', 0)->first();
        if (!$cycle) {
            return back()->with('warning', __('messages.cycle_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('cycles')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('cycles')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        Cycle::where('id', $id)->update([
            'name' => trim($request->name),
            'short_name' => trim($request->short_name),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'cycles', 2, 'DB id: [' . $id . ']');

        return redirect()->route('admin.cycles.index')->with('success', __('messages.cycle_updated_successfully'));
    }

    public function delete(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_cycle')) {
            return redirect()->route('admin.not-found');
        }

        $validator = Validator::make($request->all(), [
            'cycle_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $cycle = Cycle::where('id', $request->cycle_id)->where('deleted', 0)->first();
        if (!$cycle) {
            return back()->with('warning', __('messages.cycle_not_found'));
        }

        Cycle::where('id', $request->cycle_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'cycles', 3, 'DB id: [' . $request->cycle_id . ']');

        return back()->with('success', __('messages.cycle_deleted_successfully'));
    }
}
