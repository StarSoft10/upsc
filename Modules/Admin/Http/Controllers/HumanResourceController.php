<?php

namespace Modules\Admin\Http\Controllers;

use App\Entities\HumanResource;
use App\Entities\User;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;
use Illuminate\Database\Eloquent\Builder;

class HumanResourceController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
        $this->middleware('check_if_blocked');
        $this->helper = new AppHelper();
    }

    public function index(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_human_resource')) {
            return redirect()->route('admin.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $email = !empty($request->email) ? trim($request->email) : '';
        $phone = !empty($request->phone) ? trim($request->phone) : '';

        $humanResources = HumanResource::where('deleted', 0)->orderBy('last_name')->orderBy('first_name')->paginate(10);
        $humanResources->appends([
            'name' => $name,
            'email' => $email,
            'phone' => $phone
        ]);

        $permissions = $this->helper->getAllowedPermissionsByModel('human-resource');

        return view('admin::human-resources.index', compact('humanResources', 'name', 'email', 'phone', 'permissions'));
    }

    public function search(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_human_resource')) {
            return redirect()->route('admin.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $email = !empty($request->email) ? trim($request->email) : '';
        $phone = !empty($request->phone) ? trim($request->phone) : '';

        $humanResources = HumanResource::where('deleted', 0)->with('user')
            ->whereHas('user', function (Builder $query) use ($email) {
                if ($email !== '') {
                    $query->where('email', 'LIKE', '%' . trim($email) . '%');
                }
            })->where(function ($query) use ($name, $phone) {
                if ($name !== '') {
                    $query->where('first_name', 'LIKE', '%' . trim($name) . '%');
                    $query->orWhere('last_name', 'LIKE', '%' . trim($name) . '%');
                }
                if ($phone !== '') {
                    $query->where('phone', 'LIKE', '%' . trim($phone) . '%');
                }
            })->orderBy('last_name')->orderBy('first_name')->paginate(10);
        $humanResources->appends([
            'name' => $name,
            'email' => $email,
            'phone' => $phone
        ]);

        $permissions = $this->helper->getAllowedPermissionsByModel('human-resource');

        return view('admin::human-resources.index', compact('humanResources', 'name', 'email', 'phone', 'permissions'));
    }

    public function store(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('create_human_resource')) {
            return redirect()->route('admin.not-found');
        }

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users|regex:/^[a-z0-9\@\.\_\-]+$/u',
            'password' => 'required|confirmed',
            'permissions' => 'required|array|min:1'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $user = User::create([
            'name' => $request->first_name . ' ' . $request->last_name,
            'email' => mb_strtolower(trim($request->email), 'UTF-8'),
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make($request->password)
        ]);
        $user->assignRole('human-resource');
        foreach ($request->permissions as $permission) {
            $user->givePermissionTo($permission);
        }

        $humanResource = HumanResource::create([
            'user_id' => $user->id,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'human_resources', 1, 'DB id: [' . $humanResource->id . ']');

        return back()->with('success', __('messages.human_resource_created_successfully'));
    }

    public function show($id)
    {
        if (!Auth::user()->hasPermissionTo('view_human_resource')) {
            return redirect()->route('admin.not-found');
        }

        $humanResource = HumanResource::where('id', $id)->where('deleted', 0)->first();

        if (!$humanResource) {
            return back()->with('warning', __('messages.human_resource_not_found'));
        }
        $humanResourcePermissions = [];
        foreach ($humanResource->user->getDirectPermissions() as $permission) {
            $humanResourcePermissions[] = $permission->id;
        }
        $humanResource->permissions = $humanResourcePermissions;

        $permissions = $this->helper->getAllowedPermissionsByModel('human-resource');

        return view('admin::human-resources.show', compact('humanResource', 'permissions', 'humanResourcePermissions'));
    }

    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermissionTo('edit_human_resource')) {
            return redirect()->route('admin.not-found');
        }

        $humanResource = HumanResource::where('id', $id)->where('deleted', 0)->first();
        if (!$humanResource) {
            return back()->with('warning', __('messages.human_resource_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => [
                'required',
                'email',
                'regex:/^[a-z0-9\@\.\_\-]+$/u',
                Rule::unique('users')->ignore(User::findOrFail($humanResource->user->id)),
            ],
            'password' => 'confirmed',
            'permissions' => 'required|array|min:1'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        User::where('id', $humanResource->user->id)->update([
            'name' => $request->first_name . ' ' . $request->last_name,
            'email' => mb_strtolower(trim($request->email), 'UTF-8'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $user = User::find($humanResource->user->id);
        $user->syncPermissions($request->permissions);

        if (trim($request->password) !== '') {
            User::where('id', $humanResource->user->id)->update([
                'password' => Hash::make($request->password),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }

        HumanResource::where('id', $id)->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'human_resources', 2, 'DB id: [' . $id . ']');

        return redirect()->route('admin.human-resources.index')->with('success', __('messages.human_resource_updated_successfully'));
    }

    public function block(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_human_resource')) {
            return redirect()->route('admin.not-found');
        }

        $validator = Validator::make($request->all(), [
            'human_resource_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $humanResource = HumanResource::where('id', $request->human_resource_id)->where('deleted', 0)->first();
        if (!$humanResource) {
            return back()->with('warning', __('messages.human_resource_not_found'));
        }

        HumanResource::where('id', $request->human_resource_id)->update([
            'blocked' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'human_resources', 4, 'DB id: [' . $request->human_resource_id . ']');

        return back()->with('success', __('messages.human_resource_blocked_successfully'));
    }

    public function unblock(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_human_resource')) {
            return redirect()->route('admin.not-found');
        }

        $validator = Validator::make($request->all(), [
            'human_resource_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $humanResource = HumanResource::where('id', $request->human_resource_id)->where('deleted', 0)->first();
        if (!$humanResource) {
            return back()->with('warning', __('messages.human_resource_not_found'));
        }

        HumanResource::where('id', $request->human_resource_id)->update([
            'blocked' => 0,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'human_resources', 5, 'DB id: [' . $request->human_resource_id . ']');

        return back()->with('success', __('messages.human_resource_unblocked_successfully'));
    }

    public function delete(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_human_resource')) {
            return redirect()->route('admin.not-found');
        }

        $validator = Validator::make($request->all(), [
            'human_resource_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $humanResource = HumanResource::where('id', $request->human_resource_id)->where('deleted', 0)->first();
        if (!$humanResource) {
            return back()->with('warning', __('messages.human_resource_not_found'));
        }

        HumanResource::where('id', $request->human_resource_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'human_resources', 3, 'DB id: [' . $request->human_resource_id . ']');

        return back()->with('success', __('messages.human_resource_deleted_successfully'));
    }
}
