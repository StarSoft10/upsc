<?php

namespace Modules\Admin\Http\Controllers;

use App\Entities\Semester;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Routing\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Helpers\AppHelper;

class SemesterController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
        $this->middleware('check_if_blocked');
        $this->helper = new AppHelper();
    }

    public function index()
    {
        if (!Auth::user()->hasPermissionTo('view_semester')) {
            return redirect()->route('admin.not-found');
        }

        $semesters = Semester::where('deleted', 0)->orderBy('order')->paginate(10);

        return view('admin::semesters.index', compact('semesters'));
    }

    public function store(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('create_semester')) {
            return redirect()->route('admin.not-found');
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('semesters')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ],
            'order' => [
                'required',
                'numeric',
                Rule::unique('semesters')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $semester = Semester::create(['name' => trim($request->name), 'order' => trim($request->order)]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'semesters', 1, 'DB id: [' . $semester->id . ']');

        return back()->with('success', __('messages.semester_created_successfully'));
    }

    public function show($id)
    {
        if (!Auth::user()->hasPermissionTo('view_semester')) {
            return redirect()->route('admin.not-found');
        }

        $semester = Semester::where('id', $id)->where('deleted', 0)->first();

        if (!$semester) {
            return back()->with('warning', __('messages.semester_not_found'));
        }

        return view('admin::semesters.show', compact('semester'));
    }

    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermissionTo('edit_semester')) {
            return redirect()->route('admin.not-found');
        }

        $semester = Semester::where('id', $id)->where('deleted', 0)->first();
        if (!$semester) {
            return back()->with('warning', __('messages.semester_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('semesters')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ],
            'order' => [
                'required',
                'numeric',
                Rule::unique('semesters')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        Semester::where('id', $id)->update([
            'name' => trim($request->name),
            'order' => trim($request->order),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'semesters', 2, 'DB id: [' . $id . ']');

        return redirect()->route('admin.semesters.index')->with('success', __('messages.semester_updated_successfully'));
    }

    public function delete(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_semester')) {
            return redirect()->route('admin.not-found');
        }

        $validator = Validator::make($request->all(), [
            'semester_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $semester = Semester::where('id', $request->semester_id)->where('deleted', 0)->first();
        if (!$semester) {
            return back()->with('warning', __('messages.semester_not_found'));
        }

        Semester::where('id', $request->semester_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'semesters', 3, 'DB id: [' . $request->semester_id . ']');

        return back()->with('success', __('messages.semester_deleted_successfully'));
    }
}
