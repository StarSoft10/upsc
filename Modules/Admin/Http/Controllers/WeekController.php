<?php

namespace Modules\Admin\Http\Controllers;

use App\Entities\WeekType;
use App\Entities\Semester;
use App\Entities\Week;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\AppHelper;

class WeekController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
        $this->middleware('check_if_blocked');
        $this->helper = new AppHelper();
    }

    public function index(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_week')) {
            return redirect()->route('admin.not-found');
        }

        $year = !empty($request->year) ? trim($request->year) : '';
        $weekTypeIds = !empty($request->weekTypeIds) ? $request->weekTypeIds : [];
        $semesterIds = !empty($request->semesterIds) ? $request->semesterIds : [];

        $weeks = Week::where('deleted', 0)->orderBy('id', 'ASC')->paginate(10);
        $weeks->appends([
            'year' => $year,
            'weekTypeIds' => $weekTypeIds,
            'semesterIds' => $semesterIds
        ]);

        $weekTypes = WeekType::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $semesters = Semester::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        return view('admin::weeks.index', compact('weeks', 'weekTypes', 'semesters', 'year',
            'weekTypeIds', 'semesterIds'));
    }

    public function search(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_week')) {
            return redirect()->route('admin.not-found');
        }

        $year = !empty($request->year) ? trim($request->year) : '';
        $weekTypeIds = !empty($request->weekTypeIds) ? $request->weekTypeIds : [];
        $semesterIds = !empty($request->semesterIds) ? $request->semesterIds : [];

        $weeks = Week::where('deleted', 0)->where(function ($query) use ($year, $weekTypeIds, $semesterIds) {
            if ($year !== '') {
                $query->where('year', 'LIKE', '%' . trim($year) . '%');
            }
            if (count($weekTypeIds) > 0) {
                $query->whereIn('week_type_id', $weekTypeIds);
            }
            if (count($semesterIds) > 0) {
                $query->whereIn('semester_id', $semesterIds);
            }
        })->orderBy('id', 'ASC')->paginate(10);
        $weeks->appends([
            'year' => $year,
            'weekTypeIds' => $weekTypeIds,
            'semesterIds' => $semesterIds
        ]);

        $weekTypes = WeekType::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $semesters = Semester::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        return view('admin::weeks.index', compact('weeks', 'weekTypes', 'semesters', 'year',
            'weekTypeIds', 'semesterIds'));
    }

    public function store(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('create_week')) {
            return redirect()->route('admin.not-found');
        }

        $validator = Validator::make($request->all(), [
            'week_type_id' => 'required|numeric',
            'semester_id' => 'required|numeric',
            'year' => [
                'required',
                Rule::unique('weeks')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ],
            'start' => 'required',
            'end' => 'required',
            'order' => [
                'required',
                'numeric',
                Rule::unique('weeks')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $week = Week::create([
            'week_type_id' => $request->week_type_id,
            'semester_id' => $request->semester_id,
            'year' => trim($request->year),
            'start' => trim($request->start),
            'end' => trim($request->end),
            'order' => trim($request->order)
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'weeks', 1, 'DB id: [' . $week->id . ']');

        return back()->with('success', __('messages.week_created_successfully'));
    }

    public function show($id)
    {
        if (!Auth::user()->hasPermissionTo('view_week')) {
            return redirect()->route('admin.not-found');
        }

        $week = Week::where('id', $id)->where('deleted', 0)->first();

        if (!$week) {
            return back()->with('warning', __('messages.week_not_found'));
        }

        $weekTypes = WeekType::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');
        $semesters = Semester::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        return view('admin::weeks.show', compact('week', 'weekTypes', 'semesters'));
    }

    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermissionTo('edit_week')) {
            return redirect()->route('admin.not-found');
        }

        $week = Week::where('id', $id)->where('deleted', 0)->first();
        if (!$week) {
            return back()->with('warning', __('messages.week_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'week_type_id' => 'required|numeric',
            'semester_id' => 'required|numeric',
            'year' => [
                'required',
                Rule::unique('weeks')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ],
            'start' => 'required',
            'end' => 'required',
            'order' => [
                'required',
                'numeric',
                Rule::unique('weeks')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        Week::where('id', $id)->update([
            'week_type_id' => $request->week_type_id,
            'semester_id' => $request->semester_id,
            'year' => trim($request->year),
            'start' => trim($request->start),
            'end' => trim($request->end),
            'order' => trim($request->order),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'weeks', 2, 'DB id: [' . $id . ']');

        return redirect()->route('admin.weeks.index')->with('success', __('messages.week_updated_successfully'));
    }

    public function delete(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_week')) {
            return redirect()->route('admin.not-found');
        }

        $validator = Validator::make($request->all(), [
            'week_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $week = Week::where('id', $request->week_id)->where('deleted', 0)->first();
        if (!$week) {
            return back()->with('warning', __('messages.week_not_found'));
        }

        Week::where('id', $request->week_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'weeks', 3, 'DB id: [' . $request->week_id . ']');

        return back()->with('success', __('messages.week_deleted_successfully'));
    }
}
