<?php

namespace Modules\Admin\Http\Controllers;

use App\Entities\WeekType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\AppHelper;

class WeekTypeController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
        $this->middleware('check_if_blocked');
        $this->helper = new AppHelper();
    }

    public function index(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_week_type')) {
            return redirect()->route('admin.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';

        $weekTypes = WeekType::where('deleted', 0)->orderBy('id', 'ASC')->paginate(10);
        $weekTypes->appends([
            'name' => $name,
            'shortName' => $shortName
        ]);

        return view('admin::week-types.index', compact('weekTypes', 'name', 'shortName'));
    }

    public function search(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_week_type')) {
            return redirect()->route('admin.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $shortName = !empty($request->shortName) ? trim($request->shortName) : '';

        $weekTypes = WeekType::where('deleted', 0)->where(function ($query) use ($name, $shortName) {
            if ($name !== '') {
                $query->where('name', 'LIKE', '%' . trim($name) . '%');
            }
            if ($shortName !== '') {
                $query->where('short_name', 'LIKE', '%' . trim($shortName) . '%');
            }
        })->orderBy('id', 'ASC')->paginate(10);
        $weekTypes->appends([
            'name' => $name,
            'shortName' => $shortName
        ]);

        return view('admin::week-types.index', compact('weekTypes', 'name', 'shortName'));
    }

    public function store(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('create_week_type')) {
            return redirect()->route('admin.not-found');
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('week_types')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('week_types')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $weekType = WeekType::create(['name' => trim($request->name), 'short_name' => trim($request->short_name)]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'week_types', 1, 'DB id: [' . $weekType->id . ']');

        return back()->with('success', __('messages.week_type_created_successfully'));
    }

    public function show($id)
    {
        if (!Auth::user()->hasPermissionTo('view_week_type')) {
            return redirect()->route('admin.not-found');
        }

        $weekType = WeekType::where('id', $id)->where('deleted', 0)->first();

        if (!$weekType) {
            return back()->with('warning', __('messages.week_type_not_found'));
        }

        return view('admin::week-types.show', compact('weekType'));
    }

    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermissionTo('edit_week_type')) {
            return redirect()->route('admin.not-found');
        }

        $weekType = WeekType::where('id', $id)->where('deleted', 0)->first();
        if (!$weekType) {
            return back()->with('warning', __('messages.week_type_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('week_types')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ],
            'short_name' => [
                'required',
                Rule::unique('week_types')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        WeekType::where('id', $id)->update([
            'name' => trim($request->name),
            'short_name' => trim($request->short_name),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'week_types', 2, 'DB id: [' . $id . ']');

        return redirect()->route('admin.week-types.index')->with('success', __('messages.week_type_updated_successfully'));
    }

    public function delete(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_week_type')) {
            return redirect()->route('admin.not-found');
        }

        $validator = Validator::make($request->all(), [
            'week_type_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $weekType = WeekType::where('id', $request->week_type_id)->where('deleted', 0)->first();
        if (!$weekType) {
            return back()->with('warning', __('messages.week_type_not_found'));
        }

        WeekType::where('id', $request->week_type_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'week_types', 3, 'DB id: [' . $request->week_type_id . ']');

        return back()->with('success', __('messages.week_type_deleted_successfully'));
    }
}
