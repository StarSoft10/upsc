<?php

namespace Modules\Admin\Http\Controllers;

use App\Entities\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;
use Validator;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
        $this->middleware('check_if_blocked');
    }

    public function index()
    {
        $userWithRoles = [];
        $roles = Role::where('name', '!=', 'superadmin')->pluck('name');

        foreach ($roles as $roleName){
            $roleNameInitial = $roleName;
            $roleName = str_replace('-r', 'R', $roleName);
            $model = "App\Entities" . '\\'. ucfirst($roleName);

            $userWithRoles[$roleNameInitial] = $model::with('users')->where('deleted', 0)->count();
        }

        return view('admin::index', compact('userWithRoles'));
    }

    public function show()
    {
        $user = Auth::user();
        return view('admin::admin.show', compact('user'));
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => [
                'required',
                'email',
                'regex:/^[a-z0-9\@\.\_\-]+$/u',
                Rule::unique('users')->ignore($request->user, 'id')
            ],
            'name' => 'required',
            'password' => 'confirmed'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        if (trim($request->password) !== '') {
            User::where('id', $request->user)->update([
                'name' => $request->name,
                'email' => mb_strtolower(trim($request->email), 'UTF-8'),
                'password' => Hash::make($request->password),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        } else {
            User::where('id', $request->user)->update([
                'name' => $request->name,
                'email' => mb_strtolower(trim($request->email), 'UTF-8'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }

        return back()->with('success', __('messages.information_update_successfully'));
    }

    public function changePhoto(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'photo' => 'required|mimes:jpeg,png,jpg,gif,svg|max:10000'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        if (!$request->hasFile('photo')) {
            return back()->with('warning', __('messages.photo_is_required'));
        }

        $updatedUser = User::find($request->user);
        $photoName = $updatedUser->photo;
        if (Storage::disk('public')->exists('users-photo/' . $photoName)) {
            Storage::disk('public')->delete('users-photo/' . $photoName);
        }
        $photo = $request->file('photo');
        $fileName = time() . $request->user . '.' . $photo->getClientOriginalExtension();
        Storage::disk('public')->put('users-photo/' . $fileName, File::get($photo));
        $photoName = $fileName;

        User::where('id', $request->user)->update([
            'photo' => $photoName,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return back()->with('success', __('messages.information_update_successfully'));
    }

    public function notFound()
    {
        return view('admin::pages.not-found');
    }
}
