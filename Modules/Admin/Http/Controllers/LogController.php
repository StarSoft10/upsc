<?php

namespace Modules\Admin\Http\Controllers;

use App\Entities\Log;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Validator;
use Illuminate\Routing\Controller;

class LogController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    public function index(Request $request)
    {
        $roleIds = !empty($request->roleIds) ? $request->roleIds : [];
        $typeIds = !empty($request->typeIds) ? $request->typeIds : [];
        $tableNames = !empty($request->tableNames) ? $request->tableNames : [];
        $from = !empty($request->from) ? trim($request->from) : '';
        $to = !empty($request->to) ? trim($request->to) : '';

        $logs = Log::where('entity_role', '!=', 'superadmin')->orderBy('id', 'DESC')->paginate(10);
        $logs->appends([
            'roleIds' => $roleIds,
            'typeIds' => $typeIds,
            'tableNames' => $tableNames,
            'from' => $from,
            'to' => $to
        ]);
        $logs->transform(function ($item) {
            $item->user = $item->getUserByRoleAndId($item->entity_role, $item->entity_id);
            $item->log = $this->helper->getLogTypeById($item->log_type);
            $item->date = Carbon::parse($item->created_at)->format('d/m/Y H:i:s');

            return $item;
        });

        $roles = $this->getRoles();
        $types = $this->helper->getLogTypes();
        $tables = $this->getTables();

        return view('admin::logs.index', compact('logs', 'roles', 'types', 'tables',
            'roleIds', 'typeIds', 'tableNames', 'from', 'to'));
    }

    public function search(Request $request)
    {
        $roleIds = !empty($request->roleIds) ? $request->roleIds : [];
        $typeIds = !empty($request->typeIds) ? $request->typeIds : [];
        $tableNames = !empty($request->tableNames) ? $request->tableNames : [];
        $from = !empty($request->from) ? trim($request->from) : '';
        $to = !empty($request->to) ? trim($request->to) : '';

        $logs = Log::where('entity_role', '!=', 'superadmin')->where(function ($query) use ($roleIds, $typeIds, $tableNames, $from, $to) {
            if (count($roleIds) > 0) {
                $query->whereIn('entity_role', $roleIds);
            }
            if (count($typeIds) > 0) {
                $query->whereIn('log_type', $typeIds);
            }
            if (count($tableNames) > 0) {
                $query->whereIn('action_table', $tableNames);
            }
            if ($from !== '' && $to !== '') {
                $from = Carbon::createFromFormat('Y-m-d\TH:i', $from)->format('Y-m-d H:i:s');
                $to = Carbon::createFromFormat('Y-m-d\TH:i', $to)->format('Y-m-d H:i:s');

                $query->whereBetween('created_at', [$from, $to])->get();
            }
            if ($from !== '') {
                $from = Carbon::createFromFormat('Y-m-d\TH:i', $from)->format('Y-m-d H:i:s');

                $query->where('created_at', '>=', $from)->get();
            }
            if ($to !== '') {
                $to = Carbon::createFromFormat('Y-m-d\TH:i', $to)->format('Y-m-d H:i:s');

                $query->where('created_at', '<=', $to)->get();
            }
        })->orderBy('id', 'DESC')->paginate(10);
        $logs->appends([
            'roleIds' => $roleIds,
            'typeIds' => $typeIds,
            'tableNames' => $tableNames,
            'from' => $from,
            'to' => $to
        ]);
        $logs->transform(function ($item) {
            $item->user = $item->getUserByRoleAndId($item->entity_role, $item->entity_id);
            $item->log = $this->helper->getLogTypeById($item->log_type);
            $item->date = Carbon::parse($item->created_at)->format('d/m/Y H:i:s');

            return $item;
        });

        $roles = $this->getRoles();
        $types = $this->helper->getLogTypes();
        $tables = $this->getTables();

        return view('admin::logs.index', compact('logs', 'roles', 'types', 'tables',
            'roleIds', 'typeIds', 'tableNames', 'from', 'to'));
    }

    public function getTables()
    {
        $tables = DB::select('SHOW TABLES');
        $tables = array_map('current', $tables);

        $tableNames = [];
        foreach ($tables as $table) {
            $tableNames[$table] = $table;
        }

        return $tableNames;
    }

    public function getRoles()
    {
        $roles = Role::where('name', '!=', 'superadmin')->pluck('name', 'id');

        $roleNames = [];
        foreach ($roles as $role) {
            $roleNames[$role] = $role;
        }

        return $roleNames;
    }
}
