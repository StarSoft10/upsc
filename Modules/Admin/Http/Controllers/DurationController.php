<?php

namespace Modules\Admin\Http\Controllers;

use App\Entities\DurationType;
use App\Entities\Duration;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\AppHelper;

class DurationController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
        $this->middleware('check_if_blocked');
        $this->helper = new AppHelper();
    }

    public function index(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_duration')) {
            return redirect()->route('admin.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $durationTypeIds = !empty($request->durationTypeIds) ? $request->durationTypeIds : [];

        $durations = Duration::where('deleted', 0)->orderBy('id', 'ASC')->paginate(10);
        $durations->appends([
            'name' => $name,
            'durationTypeIds' => $durationTypeIds
        ]);

        $durationTypes = DurationType::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        return view('admin::durations.index', compact('durations', 'durationTypes', 'name', 'durationTypeIds'));
    }

    public function search(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('view_duration')) {
            return redirect()->route('admin.not-found');
        }

        $name = !empty($request->name) ? trim($request->name) : '';
        $durationTypeIds = !empty($request->durationTypeIds) ? $request->durationTypeIds : [];

        $durations = Duration::where('deleted', 0)->where(function ($query) use ($name, $durationTypeIds) {
            if ($name !== '') {
                $query->where('name', 'LIKE', '%' . trim($name) . '%');
            }
            if (count($durationTypeIds) > 0) {
                $query->whereIn('duration_type_id', $durationTypeIds);
            }
        })->orderBy('id', 'ASC')->paginate(10);
        $durations->appends([
            'name' => $name,
            'durationTypeIds' => $durationTypeIds
        ]);

        $durationTypes = DurationType::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        return view('admin::durations.index', compact('durations', 'durationTypes', 'name', 'durationTypeIds'));
    }

    public function store(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('create_duration')) {
            return redirect()->route('admin.not-found');
        }

        $validator = Validator::make($request->all(), [
            'duration_type_id' => 'required|numeric',
            'name' => [
                'required',
                Rule::unique('durations')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ],
            'start' => 'required',
            'end' => 'required',
            'order' => [
                'required',
                'numeric',
                Rule::unique('durations')->where(function ($query) {
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $duration = Duration::create([
            'duration_type_id' => $request->duration_type_id,
            'name' => trim($request->name),
            'start' => trim($request->start),
            'end' => trim($request->end),
            'order' => trim($request->order)
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'durations', 1, 'DB id: [' . $duration->id . ']');

        return back()->with('success', __('messages.duration_created_successfully'));
    }

    public function show($id)
    {
        if (!Auth::user()->hasPermissionTo('view_duration')) {
            return redirect()->route('admin.not-found');
        }

        $duration = Duration::where('id', $id)->where('deleted', 0)->first();

        if (!$duration) {
            return back()->with('warning', __('messages.duration_not_found'));
        }

        $durationTypes = DurationType::where('deleted', 0)->orderBy('id', 'ASC')->pluck('name', 'id');

        return view('admin::durations.show', compact('duration', 'durationTypes'));
    }

    public function update(Request $request, $id)
    {
        if (!Auth::user()->hasPermissionTo('edit_duration')) {
            return redirect()->route('admin.not-found');
        }

        $duration = Duration::where('id', $id)->where('deleted', 0)->first();
        if (!$duration) {
            return back()->with('warning', __('messages.duration_not_found'));
        }

        $validator = Validator::make($request->all(), [
            'duration_type_id' => 'required|numeric',
            'name' => [
                'required',
                Rule::unique('durations')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ],
            'start' => 'required',
            'end' => 'required',
            'order' => [
                'required',
                'numeric',
                Rule::unique('durations')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                    $query->where('deleted', 0);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        Duration::where('id', $id)->update([
            'duration_type_id' => $request->duration_type_id,
            'name' => trim($request->name),
            'start' => trim($request->start),
            'end' => trim($request->end),
            'order' => trim($request->order),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'durations', 2, 'DB id: [' . $id . ']');

        return redirect()->route('admin.durations.index')->with('success', __('messages.duration_updated_successfully'));
    }

    public function delete(Request $request)
    {
        if (!Auth::user()->hasPermissionTo('delete_duration')) {
            return redirect()->route('admin.not-found');
        }

        $validator = Validator::make($request->all(), [
            'duration_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $duration = Duration::where('id', $request->duration_id)->where('deleted', 0)->first();
        if (!$duration) {
            return back()->with('warning', __('messages.duration_not_found'));
        }

        Duration::where('id', $request->duration_id)->update([
            'deleted' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $this->helper->addLog('admin', Auth::user()->admin->id, 'durations', 3, 'DB id: [' . $request->duration_id . ']');

        return back()->with('success', __('messages.duration_deleted_successfully'));
    }
}
