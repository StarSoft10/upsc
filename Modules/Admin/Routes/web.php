<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

Route::group([
    'prefix' => LaravelLocalization::setLocale() . '/admin',
    'middleware' => ['auth', 'role:admin', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'localize']
], function () {
    Route::get('/', 'AdminController@index')->name('admin');
    Route::get('profile', 'AdminController@show')->name('admin.profile');
    Route::patch('update', 'AdminController@update')->name('admin.update');
    Route::patch('changePhoto', 'AdminController@changePhoto')->name('admin.changePhoto');
    Route::get('not-found', 'AdminController@notFound')->name('admin.not-found');


    /*
   * Log routes
   */
    Route::group(['prefix' => 'logs'], function () {
        Route::get('search', 'LogController@search')->name('admin.logs.search');
    });
    Route::resource('logs', 'LogController', ['names' => [
        'index' => 'admin.logs.index'
    ]]);


    /*
     * User routes
     */
    Route::group(['prefix' => 'unconfirmed-users'], function () {
        Route::post('delete', 'UnconfirmedUserController@delete')->name('admin.unconfirmed-users.delete');
        Route::get('search', 'UnconfirmedUserController@search')->name('admin.unconfirmed-users.search');
        Route::post('confirm', 'UnconfirmedUserController@confirm')->name('admin.unconfirmed-users.confirm');
    });
    Route::resource('unconfirmed-users', 'UnconfirmedUserController', ['names' => [
        'index' => 'admin.unconfirmed-users.index',
        'show' => 'admin.unconfirmed-users.show',
        'update' => 'admin.unconfirmed-users.update'
    ]]);


    Route::group(['prefix' => 'rectors'], function () {
        Route::post('delete', 'RectorController@delete')->name('admin.rectors.delete');
        Route::get('search', 'RectorController@search')->name('admin.rectors.search');
        Route::post('block', 'RectorController@block')->name('admin.rectors.block');
        Route::post('unblock', 'RectorController@unblock')->name('admin.rectors.unblock');
    });
    Route::resource('rectors', 'RectorController', ['names' => [
        'index' => 'admin.rectors.index',
        'store' => 'admin.rectors.store',
        'show' => 'admin.rectors.show',
        'update' => 'admin.rectors.update'
    ]]);


    Route::group(['prefix' => 'secretaries'], function () {
        Route::post('delete', 'SecretaryController@delete')->name('admin.secretaries.delete');
        Route::get('search', 'SecretaryController@search')->name('admin.secretaries.search');
        Route::post('block', 'SecretaryController@block')->name('admin.secretaries.block');
        Route::post('unblock', 'SecretaryController@unblock')->name('admin.secretaries.unblock');
    });
    Route::resource('secretaries', 'SecretaryController', ['names' => [
        'index' => 'admin.secretaries.index',
        'store' => 'admin.secretaries.store',
        'show' => 'admin.secretaries.show',
        'update' => 'admin.secretaries.update'
    ]]);


    Route::group(['prefix' => 'deans'], function () {
        Route::post('delete', 'DeanController@delete')->name('admin.deans.delete');
        Route::get('search', 'DeanController@search')->name('admin.deans.search');
        Route::post('block', 'DeanController@block')->name('admin.deans.block');
        Route::post('unblock', 'DeanController@unblock')->name('admin.deans.unblock');
    });
    Route::resource('deans', 'DeanController', ['names' => [
        'index' => 'admin.deans.index',
        'store' => 'admin.deans.store',
        'show' => 'admin.deans.show',
        'update' => 'admin.deans.update'
    ]]);


    Route::group(['prefix' => 'chairs'], function () {
        Route::post('delete', 'ChairController@delete')->name('admin.chairs.delete');
        Route::get('search', 'ChairController@search')->name('admin.chairs.search');
        Route::post('block', 'ChairController@block')->name('admin.chairs.block');
        Route::post('unblock', 'ChairController@unblock')->name('admin.chairs.unblock');
    });
    Route::resource('chairs', 'ChairController', ['names' => [
        'index' => 'admin.chairs.index',
        'store' => 'admin.chairs.store',
        'show' => 'admin.chairs.show',
        'update' => 'admin.chairs.update'
    ]]);


    Route::group(['prefix' => 'human-resources'], function () {
        Route::post('delete', 'HumanResourceController@delete')->name('admin.human-resources.delete');
        Route::get('search', 'HumanResourceController@search')->name('admin.human-resources.search');
        Route::post('block', 'HumanResourceController@block')->name('admin.human-resources.block');
        Route::post('unblock', 'HumanResourceController@unblock')->name('admin.human-resources.unblock');
    });
    Route::resource('human-resources', 'HumanResourceController', ['names' => [
        'index' => 'admin.human-resources.index',
        'store' => 'admin.human-resources.store',
        'show' => 'admin.human-resources.show',
        'update' => 'admin.human-resources.update'
    ]]);


    Route::group(['prefix' => 'teachers'], function () {
        Route::post('delete', 'TeacherController@delete')->name('admin.teachers.delete');
        Route::get('search', 'TeacherController@search')->name('admin.teachers.search');
        Route::post('block', 'TeacherController@block')->name('admin.teachers.block');
        Route::post('unblock', 'TeacherController@unblock')->name('admin.teachers.unblock');
    });
    Route::resource('teachers', 'TeacherController', ['names' => [
        'index' => 'admin.teachers.index',
        'store' => 'admin.teachers.store',
        'show' => 'admin.teachers.show',
        'update' => 'admin.teachers.update'
    ]]);


    Route::group(['prefix' => 'students'], function () {
        Route::post('delete', 'StudentController@delete')->name('admin.students.delete');
        Route::get('search', 'StudentController@search')->name('admin.students.search');
        Route::post('block', 'StudentController@block')->name('admin.students.block');
        Route::post('unblock', 'StudentController@unblock')->name('admin.students.unblock');
        Route::post('exportStudentsPdf', 'StudentController@exportStudentsPdf')->name('admin.students.exportStudentsPdf');
        Route::post('exportStudentsExcel', 'StudentController@exportStudentsExcel')->name('admin.students.exportStudentsExcel');
        Route::post('exportStudentPdf', 'StudentController@exportStudentPdf')->name('admin.students.exportStudentPdf');
        Route::post('exportStudentExcel', 'StudentController@exportStudentExcel')->name('admin.students.exportStudentExcel');
        Route::post('deleteAdditionalStudy', 'StudentController@deleteAdditionalStudy')->name('admin.students.deleteAdditionalStudy');
    });
    Route::resource('students', 'StudentController', ['names' => [
        'index' => 'admin.students.index',
        'store' => 'admin.students.store',
        'show' => 'admin.students.show',
        'update' => 'admin.students.update'
    ]]);


    /*
     * Studies routes
     */
    Route::group(['prefix' => 'year-of-studies'], function () {
        Route::post('delete', 'YearOfStudyController@delete')->name('admin.year-of-studies.delete');
        Route::get('search', 'YearOfStudyController@search')->name('admin.year-of-studies.search');
    });
    Route::resource('year-of-studies', 'YearOfStudyController', ['names' => [
        'index' => 'admin.year-of-studies.index',
        'store' => 'admin.year-of-studies.store',
        'show' => 'admin.year-of-studies.show',
        'update' => 'admin.year-of-studies.update'
    ]]);


    Route::group(['prefix' => 'cycles'], function () {
        Route::post('delete', 'CycleController@delete')->name('admin.cycles.delete');
        Route::get('search', 'CycleController@search')->name('admin.cycles.search');
    });
    Route::resource('cycles', 'CycleController', ['names' => [
        'index' => 'admin.cycles.index',
        'store' => 'admin.cycles.store',
        'show' => 'admin.cycles.show',
        'update' => 'admin.cycles.update'
    ]]);


    Route::group(['prefix' => 'faculties'], function () {
        Route::post('delete', 'FacultyController@delete')->name('admin.faculties.delete');
        Route::get('search', 'FacultyController@search')->name('admin.faculties.search');
        Route::post('getFacultiesByCycleForSelectSearch', 'FacultyController@getFacultiesByCycleForSelectSearch')->name('admin.faculties.getFacultiesByCycleForSelectSearch');
        Route::post('getFacultiesByCycleForSelectAddEdit', 'FacultyController@getFacultiesByCycleForSelectAddEdit')->name('admin.faculties.getFacultiesByCycleForSelectAddEdit');
    });
    Route::resource('faculties', 'FacultyController', ['names' => [
        'index' => 'admin.faculties.index',
        'store' => 'admin.faculties.store',
        'show' => 'admin.faculties.show',
        'update' => 'admin.faculties.update'
    ]]);


    Route::group(['prefix' => 'specialities'], function () {
        Route::post('delete', 'SpecialityController@delete')->name('admin.specialities.delete');
        Route::get('search', 'SpecialityController@search')->name('admin.specialities.search');
        Route::post('getSpecialitiesByFacultyForSelectSearch', 'SpecialityController@getSpecialitiesByFacultyForSelectSearch')->name('admin.specialities.getSpecialitiesByFacultyForSelectSearch');
        Route::post('getSpecialitiesByFacultyForSelectAddEdit', 'SpecialityController@getSpecialitiesByFacultyForSelectAddEdit')->name('admin.specialities.getSpecialitiesByFacultyForSelectAddEdit');
    });
    Route::resource('specialities', 'SpecialityController', ['names' => [
        'index' => 'admin.specialities.index',
        'store' => 'admin.specialities.store',
        'show' => 'admin.specialities.show',
        'update' => 'admin.specialities.update'
    ]]);


    Route::group(['prefix' => 'groups'], function () {
        Route::post('delete', 'GroupController@delete')->name('admin.groups.delete');
        Route::get('search', 'GroupController@search')->name('admin.groups.search');
        Route::post('getGroupsBySpecialityForSelectSearch', 'GroupController@getGroupsBySpecialityForSelectSearch')->name('admin.groups.getGroupsBySpecialityForSelectSearch');
        Route::post('getGroupsBySpecialityForSelectAddEdit', 'GroupController@getGroupsBySpecialityForSelectAddEdit')->name('admin.groups.getGroupsBySpecialityForSelectAddEdit');
    });
    Route::resource('groups', 'GroupController', ['names' => [
        'index' => 'admin.groups.index',
        'store' => 'admin.groups.store',
        'show' => 'admin.groups.show',
        'update' => 'admin.groups.update'
    ]]);


    Route::group(['prefix' => 'courses'], function () {
        Route::post('delete', 'CourseController@delete')->name('admin.courses.delete');
        Route::get('search', 'CourseController@search')->name('admin.courses.search');
    });
    Route::resource('courses', 'CourseController', ['names' => [
        'index' => 'admin.courses.index',
        'store' => 'admin.courses.store',
        'show' => 'admin.courses.show',
        'update' => 'admin.courses.update'
    ]]);


    Route::group(['prefix' => 'course-types'], function () {
        Route::post('delete', 'CourseTypeController@delete')->name('admin.course-types.delete');
        Route::get('search', 'CourseTypeController@search')->name('admin.course-types.search');
    });
    Route::resource('course-types', 'CourseTypeController', ['names' => [
        'index' => 'admin.course-types.index',
        'store' => 'admin.course-types.store',
        'show' => 'admin.course-types.show',
        'update' => 'admin.course-types.update'
    ]]);


    Route::group(['prefix' => 'days'], function () {
        Route::post('delete', 'DayController@delete')->name('admin.days.delete');
    });
    Route::resource('days', 'DayController', ['names' => [
        'index' => 'admin.days.index',
        'store' => 'admin.days.store',
        'show' => 'admin.days.show',
        'update' => 'admin.days.update'
    ]]);


    Route::group(['prefix' => 'duration-types'], function () {
        Route::post('delete', 'DurationTypeController@delete')->name('admin.duration-types.delete');
        Route::get('search', 'DurationTypeController@search')->name('admin.duration-types.search');
    });
    Route::resource('duration-types', 'DurationTypeController', ['names' => [
        'index' => 'admin.duration-types.index',
        'store' => 'admin.duration-types.store',
        'show' => 'admin.duration-types.show',
        'update' => 'admin.duration-types.update'
    ]]);


    Route::group(['prefix' => 'durations'], function () {
        Route::post('delete', 'DurationController@delete')->name('admin.durations.delete');
        Route::get('search', 'DurationController@search')->name('admin.durations.search');
    });
    Route::resource('durations', 'DurationController', ['names' => [
        'index' => 'admin.durations.index',
        'store' => 'admin.durations.store',
        'show' => 'admin.durations.show',
        'update' => 'admin.durations.update'
    ]]);


    Route::group(['prefix' => 'semesters'], function () {
        Route::post('delete', 'SemesterController@delete')->name('admin.semesters.delete');
    });
    Route::resource('semesters', 'SemesterController', ['names' => [
        'index' => 'admin.semesters.index',
        'store' => 'admin.semesters.store',
        'show' => 'admin.semesters.show',
        'update' => 'admin.semesters.update'
    ]]);


    Route::group(['prefix' => 'week-types'], function () {
        Route::post('delete', 'WeekTypeController@delete')->name('admin.week-types.delete');
        Route::get('search', 'WeekTypeController@search')->name('admin.week-types.search');
    });
    Route::resource('week-types', 'WeekTypeController', ['names' => [
        'index' => 'admin.week-types.index',
        'store' => 'admin.week-types.store',
        'show' => 'admin.week-types.show',
        'update' => 'admin.week-types.update'
    ]]);


    Route::group(['prefix' => 'weeks'], function () {
        Route::post('delete', 'WeekController@delete')->name('admin.weeks.delete');
        Route::get('search', 'WeekController@search')->name('admin.weeks.search');
    });
    Route::resource('weeks', 'WeekController', ['names' => [
        'index' => 'admin.weeks.index',
        'store' => 'admin.weeks.store',
        'show' => 'admin.weeks.show',
        'update' => 'admin.weeks.update'
    ]]);


    Route::group(['prefix' => 'teacher-ranks'], function () {
        Route::post('delete', 'TeacherRankController@delete')->name('admin.teacher-ranks.delete');
        Route::get('search', 'TeacherRankController@search')->name('admin.teacher-ranks.search');
    });
    Route::resource('teacher-ranks', 'TeacherRankController', ['names' => [
        'index' => 'admin.teacher-ranks.index',
        'store' => 'admin.teacher-ranks.store',
        'show' => 'admin.teacher-ranks.show',
        'update' => 'admin.teacher-ranks.update'
    ]]);


    /*
   * Borderous routes
   */
    Route::group(['prefix' => 'borderou-types'], function () {
        Route::post('delete', 'BorderouTypeController@delete')->name('admin.borderou-types.delete');
        Route::get('search', 'BorderouTypeController@search')->name('admin.borderou-types.search');
    });
    Route::resource('borderou-types', 'BorderouTypeController', ['names' => [
        'index' => 'admin.borderou-types.index',
        'store' => 'admin.borderou-types.store',
        'show' => 'admin.borderou-types.show',
        'update' => 'admin.borderou-types.update'
    ]]);


    Route::group(['prefix' => 'borderous'], function () {
        Route::post('delete', 'BorderouController@delete')->name('admin.borderous.delete');
        Route::get('search', 'BorderouController@search')->name('admin.borderous.search');
        Route::post('getBorderouForView', 'BorderouController@getBorderouForView')->name('admin.borderous.getBorderouForView');
        Route::post('getBorderouNote', 'BorderouController@getBorderouNote')->name('admin.borderous.getBorderouNote');
        Route::post('updateBorderouNote', 'BorderouController@updateBorderouNote')->name('admin.borderous.updateBorderouNote');
        Route::post('exportBorderouNotePdf', 'BorderouController@exportBorderouNotePdf')->name('admin.borderous.exportBorderouNotePdf');
        Route::post('exportBorderouNoteExcel', 'BorderouController@exportBorderouNoteExcel')->name('admin.borderous.exportBorderouNoteExcel');
        Route::post('deleteStudentBorderouNote', 'BorderouController@deleteStudentBorderouNote')->name('admin.borderous.deleteStudentBorderouNote');
        Route::post('addStudentBorderouNote', 'BorderouController@addStudentBorderouNote')->name('admin.borderous.addStudentBorderouNote');
    });
    Route::resource('borderous', 'BorderouController', ['names' => [
        'index' => 'admin.borderous.index',
        'store' => 'admin.borderous.store',
        'show' => 'admin.borderous.show',
        'update' => 'admin.borderous.update'
    ]]);
});