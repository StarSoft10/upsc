@if(count($groups) > 0)
    <option value="0">{{ __('pages.chose_group') }}</option>
    @foreach($groups as $key => $group)
        <option value="{{ $key }}">{{ $group }}</option>
    @endforeach
@else
    <option value="0">{{ __('messages.not_found_any_groups') }}</option>
@endif