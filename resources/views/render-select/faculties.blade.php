@if(count($faculties) > 0)
    <option value="0">{{ __('pages.chose_faculty') }}</option>
    @foreach($faculties as $key => $faculty)
        <option value="{{ $key }}">{{ $faculty }}</option>
    @endforeach
@else
    <option value="0">{{ __('messages.not_found_any_faculties') }}</option>
@endif