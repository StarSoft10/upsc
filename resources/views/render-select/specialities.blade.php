@if(count($specialities) > 0)
    <option value="0">{{ __('pages.chose_speciality') }}</option>
    @foreach($specialities as $key => $speciality)
        <option value="{{ $key }}">{{ $speciality }}</option>
    @endforeach
@else
    <option value="0">{{ __('messages.not_found_any_specialities') }}</option>
@endif