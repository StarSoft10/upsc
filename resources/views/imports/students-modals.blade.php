<div class="modal fade" id="importAbiturients" role="dialog" aria-labelledby="importAbiturients" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document" style="overflow-y: initial !important">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ __('pages.import_students') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height: 70vh;overflow-y: auto;">
                <div id="firstBlock">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h3>{{ __('messages.import_abiturients_to') }}</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('database_id', __('pages.admission'), ['class' => 'control-label']) !!}
                                <select data-placeholder="{{ __('pages.chose_admission') }}" class="standardSelect"
                                        name="database_id" id="database_id" required
                                        title="{{ __('pages.admission') }}">
                                    <option value="0">{{ __('pages.chose_admission') }}</option>
                                    @foreach($databases as $key => $database)
                                        <option value="{{ $key }}">{{ $database }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('cycle_id_import', __('pages.cycle'), ['class' => 'control-label']) !!}
                                <select data-placeholder="{{ __('pages.chose_cycle') }}" class="standardSelect"
                                        name="cycle_id_import" id="cycle_id_import" required
                                        title="{{ __('pages.cycle') }}">
                                    <option value="0">{{ __('pages.chose_cycle') }}</option>
                                    @foreach($cycles as $key => $cycle)
                                        <option value="{{ $key }}">{{ $cycle }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('faculty_id_import', __('pages.faculty'), ['class' => 'control-label']) !!}
                                <select data-placeholder="{{ __('pages.chose_faculty') }}" class="standardSelect"
                                        name="faculty_id_import" id="faculty_id_import" required
                                        title="{{ __('pages.faculty') }}">
                                    <option value="0">{{ __('pages.chose_cycle') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('speciality_id_import', __('pages.speciality'), ['class' => 'control-label']) !!}
                                <select data-placeholder="{{ __('pages.chose_speciality') }}" class="standardSelect"
                                        name="speciality_id_import" id="speciality_id_import" required
                                        title="{{ __('pages.speciality') }}">
                                    <option value="0">{{ __('pages.chose_faculty') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('group_id_import', __('pages.group'), ['class' => 'control-label']) !!}
                                <select data-placeholder="{{ __('pages.chose_group') }}" class="standardSelect"
                                        name="group_id_import" id="group_id_import" required
                                        title="{{ __('pages.group') }}">
                                    <option value="0">{{ __('pages.chose_speciality') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('year_of_study_id_import', __('pages.year_of_study'), ['class' => 'control-label']) !!}
                                <select data-placeholder="{{ __('pages.year_of_study') }}" class="standardSelect"
                                        name="year_of_study_id_import" id="year_of_study_id_import" required
                                        title="{{ __('pages.year_of_study') }}">
                                    @foreach($yearOfStudies as $key => $yearOfStudy)
                                        <option value="{{ $key }}">{{ $yearOfStudy }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="secondBlock" style="border: 1px solid;padding: 10px;">
                    <div id="list_of_abiturients_for_import"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm"
                        data-dismiss="modal">{{ __('pages.close') }}</button>
                <button type="submit" class="btn btn-warning btn-sm"
                        id="get_abiturients_for_import">{{ __('pages.get_abiturients') }}</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirmImport" role="dialog" aria-labelledby="confirmImport" aria-hidden="true"
     data-backdrop="static" style="z-index: 1052">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ __('pages.confirm_import') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-info" role="alert">
                    <div class="alert-text">
                        {{ __('pages.please_confirm_import') }}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm"
                        data-dismiss="modal">{{ __('pages.close') }}</button>
                <button type="button" class="btn btn-success btn-sm"
                        id="import_external">{{ __('pages.import') }}</button>
            </div>
        </div>
    </div>
</div>