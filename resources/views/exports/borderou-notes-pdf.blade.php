<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css" media="all">
        body {
            padding: 0;
            margin: 0;
            width: 100vw;
            height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center;

            font-family: DejaVu Sans, serif;
            font-size: 10px;
        }
        table {
            border: 1px solid #000000;
            width: 100%;
            max-width: 900px;
            border-collapse: collapse;
        }
        tr, td {
            border: 1px solid #000000;
            text-align: center;
        }
        td {
            min-width: 20px;
            padding: 5px;
            font-weight: bold;
            font-family: Arial, Helvetica, sans-serif;
        }
        .blocked {
            color: #ff8d00;
        }
        .expelled {
            color: #ff0000;
        }
        .blocked-expelled {
            background: linear-gradient(to bottom, #ff8d00 50%, #ff0000 50%) no-repeat;
            background-size: calc(100%) calc(100%);
            background-position: top;
            color: #ffffff;
        }

        #watermark {
            position: fixed;
            z-index: -1000;
        }

        @page {
            header: page-header;
            footer: page-footer;
            margin-top: 100pt;
            margin-bottom: 40pt;
        }
    </style>
</head>

<body>
<htmlpageheader name="page-header">
    <div id="watermark">
        <img src="{{ $logo }}">
        <div style="margin-left: 75%; margin-top: -11%;font-weight: bold">
            {{ __('borderou_export.pdf_header_1') }} <br>
            {{ __('borderou_export.pdf_header_2') }} <br>
            {{ __('borderou_export.pdf_header_3') }}
        </div>
        <img src="{{ $logoBackground }}" height="100%" width="100%"/>
    </div>
</htmlpageheader>

{!! $html !!}

<htmlpagefooter name="page-footer">
    <div style="text-align: center">
        © {{ \Carbon\Carbon::now()->format('Y') }} {{ __('borderou_export.pdf_footer') }} <br>
        {{ __('pages.page') }}: {PAGENO}
    </div>
</htmlpagefooter>
</body>
</html>