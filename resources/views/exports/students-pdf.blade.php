<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css" media="all">
        body {
            font-family: DejaVu Sans, serif;
            font-size: 10px;
        }

        .table-bordered {
            border: 1px solid #dee2e6;
        }

        .table {
            width: 100%;
            margin-bottom: 1rem;
            background-color: transparent;
        }

        .table-bordered thead td, .table-bordered thead th {
            border-bottom-width: 2px;
        }

        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #dee2e6;
        }

        .table-bordered td, .table-bordered th {
            border: 1px solid #dee2e6;
        }

        .table td, .table th {
            padding: .75rem;
        }

        .table-bordered td, .table-bordered th {
            border: 1px solid #dee2e6;
        }

        .table td, .table th {
            padding: .75rem;
            vertical-align: top;
        }

        .blocked {
            color: #ff8d00;
        }
        .expelled {
            color: #ff0000;
        }
        .blocked-expelled {
            background: linear-gradient(to bottom, #ff8d00 50%, #ff0000 50%) no-repeat;
            background-size: calc(100%) calc(100%);
            background-position: top;
            color: #ffffff;
        }

        #watermark {
            position: fixed;
            /*bottom:   10cm;*/
            /*left:     5.5cm;*/
            /*width:    8cm;*/
            /*height:   8cm;*/
            z-index: -1000;
        }

        @page {
            header: page-header;
            footer: page-footer;
            margin-top: 100pt;
            margin-bottom: 40pt;
        }
    </style>
</head>
<body>
    <htmlpageheader name="page-header">
        <div id="watermark">
            <img src="{{ $logo }}">
            <div style="margin-left: 75%; margin-top: -11%;font-weight: bold">
                str. Ion Creangă, nr. 1, MD-2069, <br>
                Chișinău, Republica Moldova <br>
                www.upsc.md
            </div>
            <img src="{{ $logoBackground }}" height="100%" width="100%"/>
        </div>
    </htmlpageheader>

    {!! $html !!}

    <htmlpagefooter name="page-footer">
        <div style="text-align: center">
            © {{ \Carbon\Carbon::now()->format('Y') }} UPSC - Universitatea Pedagogică de Stat "ION CREANGĂ" din Chișinău –
            Toate drepturile rezervate <br>
            {{ __('pages.page') }}: {PAGENO}
        </div>
    </htmlpagefooter>
</body>
</html>