<table>
    <thead>
        <tr>
            <th>{{ __('pages.year_of_study') }}</th>
            <th>{{ __('pages.cycle') }}</th>
            <th>{{ __('pages.faculty') }}</th>
            <th>{{ __('pages.speciality') }}</th>
            <th>{{ __('pages.group') }}</th>
            <th>{{ __('pages.semester') }}</th>
            <th>{{ __('pages.course') }}</th>
            <th>{{ __('pages.course_title') }}</th>
            <th>{{ __('pages.exam_date') }}</th>
            <th>{{ __('pages.teacher') }}</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>{{ $borderou->yearOfStudy->name }}</th>
            <th>{{ $borderou->cycle->name }}</th>
            <th>{{ $borderou->faculty->name }}</th>
            <th>{{ $borderou->speciality->name }}</th>
            <th>{{ $borderou->group->name }}</th>
            <th>{{ $borderou->semester->name }}</th>
            <th>{{ $borderou->course->name }}</th>
            <th>{{ $borderou->course_title }}</th>
            <th>{{ Carbon\Carbon::createFromFormat('Y-m-d', $borderou->exam_date)->format('d/m/Y') }}</th>
            <th>{{ $borderou->teacher->first_name .' '. $borderou->teacher->last_name }}</th>
        </tr>
    </tbody>
</table>

<table>
    <thead>
        <tr>
            <th>#</th>
            <th>{{ __('pages.full_name') }}</th>
            <th>{{ __('pages.semester_note') }}</th>
            <th>{{ __('pages.exam_note') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($borderouNotes as $key => $borderouNote)
            @php
                $studentFullName = $borderouNote->student->last_name . ' ' . $borderouNote->student->first_name;
                $semesterNote = $borderouNote->semester_note;
                $examNote = $borderouNote->exam_note;

                if (is_null($semesterNote) || $semesterNote == '') {
                    $semesterNoteShow = '';
                }
                if ((int)$semesterNote > 0) {
                    $semesterNoteShow = $semesterNote;
                } else {
                    if ((int)$semesterNote == 0) {
                        $semesterNoteShow = __('pages.accepted');
                    } elseif ((int)$semesterNote == -2) {
                        $semesterNoteShow = __('pages.not_accepted');
                    } elseif ((int)$semesterNote == -3) {
                        $semesterNoteShow = __('pages.negative');
                    } else {
                        $semesterNoteShow = __('pages.absent');
                    }
                }

                if (is_null($examNote) || $examNote == '') {
                    $examNoteShow = '';
                }
                if ((int)$examNote > 0) {
                    $examNoteShow = $examNote;
                } else {
                    if ((int)$examNote == 0) {
                        $examNoteShow = __('pages.accepted');
                    } elseif ((int)$examNote == -2) {
                        $examNoteShow = __('pages.not_accepted');
                    } elseif ((int)$examNote == -3) {
                        $examNoteShow = __('pages.negative');
                    } else {
                        $examNoteShow = __('pages.absent');
                    }
                }
            @endphp

            <tr>
                <th>{{ ($key + 1) }}</th>
                <th>{{ $studentFullName }}</th>
                <td>{{ $semesterNoteShow }}</td>
                <td>{{ $examNoteShow }}</td>
            </tr>
        @endforeach
    </tbody>
</table>