<table>
    <thead>
        <tr>
            <th>{{ __('pages.full_name') }}</th>
            <th>{{ __('pages.year_of_study') }}</th>
            <th>{{ __('pages.cycle') }}</th>
            <th>{{ __('pages.faculty') }}</th>
            <th>{{ __('pages.speciality') }}</th>
            <th>{{ __('pages.group') }}</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{ $student->last_name .' '. $student->first_name }}</td>
            <td>{!! $student->getAllStudentYearOfStudies() !!}</td>
            <td>{!! $student->getAllStudentCycles() !!}</td>
            <td>{!! $student->getAllStudentFaculties() !!}</td>
            <td>{!! $student->getAllStudentSpecialities() !!}</td>
            <td>{!! $student->getAllStudentGroups() !!}</td>
        </tr>
    </tbody>
</table>