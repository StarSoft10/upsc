<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" sizes="64x64" href="{{ asset('images/icon.png') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700">
    <title>{{ config('app.name', 'UPSC') }}</title>
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
</head>
<body>

<div class="container">
    @if (session()->has('error'))
        <div class="l_error">
            <strong>{{ __('pages.sorry') }}</strong>
            {{ session()->get('error') }}
        </div>
    @endif

    @if (session()->has('warning'))
        <div class="l_warning">
            <strong>{{ __('pages.warning') }}</strong>
            {{ session()->get('warning') }}
        </div>
    @endif

    @if(session()->has('success'))
        <div class="l_success">
            <strong>{{ __('pages.success') }}</strong>
            {{ session()->get('success') }}
        </div>
    @endif

    <section id="formHolder">
        <div class="row">
            <div class="col-sm-6 brand">
                <a href="javascript:void(0)" class="logo"><img src="{{ asset('images/icon.png') }}"></a>
                <div class="heading">
                    <h2>UPSC</h2>
                    <p>Universitatea Pedagogică de Stat "ION CREANGĂ" din Chișinău</p>
                </div>
            </div>

            <div class="col-sm-6 form">
                <div class="signup form-peice switched">
                    <form class="signup-form" action="{{ route('signUpPost') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="first_name">{{ __('pages.first_name') }}</label>
                            <input type="text" name="first_name" id="first_name" class="name">
                            <span class="error"></span>
                        </div>
                        <div class="form-group">
                            <label for="last_name">{{ __('pages.last_name') }}</label>
                            <input type="text" name="last_name" id="last_name" class="name">
                            <span class="error"></span>
                        </div>
                        <div class="form-group">
                            <label for="register_email">{{ __('pages.email') }}</label>
                            <input type="email" name="email" id="register_email" class="email" required>
                            <span class="error"></span>
                        </div>
                        <div class="form-group">
                            <label for="phone">{{ __('pages.phone') }} - <small>{{ __('pages.optional') }}</small></label>
                            <input type="text" name="phone" id="phone">
                        </div>
                        <div class="form-group">
                            <label for="register_password">{{ __('pages.password') }}</label>
                            <input type="password" name="password" id="register_password" class="pass" required>
                            <span class="password-toggle show"></span>
                            <span class="error"></span>
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">{{ __('pages.confirm_password') }}</label>
                            <input type="password" name="password_confirmation" id="password_confirmation" class="passConfirm" required>
                            <span class="password-toggle show"></span>
                            <span class="error"></span>
                        </div>
                        <div class="CTA">
                            <input type="submit" value="{{ __('pages.register') }}" id="submit">
                            <a href="#" class="switch">{{ __('pages.already_have_account') }}</a>
                        </div>
                    </form>
                </div>

                <div class="login form-peice">
                    <form class="login-form" action="{{ route('signInPost') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="login_email">{{ __('pages.email') }}</label>
                            <input type="email" name="email" id="login_email" required>
                        </div>
                        <div class="form-group">
                            <label for="login_password">{{ __('pages.password') }}</label>
                            <input type="password" name="password" id="login_password" required>
                            <span class="password-toggle show"></span>
                        </div>
                        <div class="CTA">
                            <input type="submit" value="{{ __('pages.login') }}">
                            <a href="#" class="switch">{{ __('pages.register') }}</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

<script src="{{ asset('js/jquery3.5.1.min.js') }}"></script>
<script src="{{ asset('js/login.js') }}"></script>
</body>
</html>
