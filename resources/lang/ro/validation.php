<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'Acest :attribute trebuie să fie acceptat.',
    'active_url' => 'Acest :attribute nu este o adresă validă.',
    'after' => 'Acest :attribute trebuie să fie mai mare ca :date.',
    'after_or_equal' => 'Acest :attribute trebuie să fie mai maire sau egal ca :date.',
    'alpha' => 'Acest :attribute trebuie să conțină doar litere.',
    'alpha_dash' => 'Acest :attribute trebuie să conțină numai litere, cifre, liniuțe și liniuțe de subliniere.',
    'alpha_num' => 'Acest :attribute trebuie să conțină numai litere și cifre.',
    'array' => 'Acest :attribute trebuie să fie o matrice.',
    'before' => 'Acest :attribute trebuie să fie mai mic ca :date.',
    'before_or_equal' => 'Acest :attribute trebuie să fie mai mic sau egal ca :date.',
    'between' => [
        'numeric' => 'Acest :attribute trebuie să fie între :min și :max.',
        'file' => 'Acest :attribute trebuie să fie între :min și :max kiloocteți.',
        'string' => 'Acest :attribute trebuie să fie între :min și :max caractere.',
        'array' => 'Acest :attribute trebuie să fie între :min și :max itemi.',
    ],
    'boolean' => 'Acest :attribute field must be true or false.',
    'confirmed' => 'Acest :attribute confirmation does not match.',
    'date' => 'Acest :attribute is not a valid date.',
    'date_equals' => 'Acest :attribute must be a date equal to :date.',
    'date_format' => 'Acest :attribute does not match the format :format.',
    'different' => 'Acest :attribute and :other must be different.',
    'digits' => 'Acest :attribute must be :digits digits.',
    'digits_between' => 'Acest :attribute must be between :min and :max digits.',
    'dimensions' => 'Acest :attribute has invalid image dimensions.',
    'distinct' => 'Acest :attribute field has a duplicate value.',
    'email' => 'Acest :attribute must be a valid email address.',
    'ends_with' => 'Acest :attribute must end with one of the following: :values',
    'exists' => 'Valoarea introdusă :attribute nu este validă.',
    'file' => 'Acest :attribute trebuie să fie un fișier.',
    'filled' => 'Acest :attribute trebuie să conțină o valoare.',
    'gt' => [
        'numeric' => 'Acest :attribute trebuie să fie mai mare decât :value.',
        'file' => 'Acest :attribute trebuie să fie mai mare decât :value kiloocteți.',
        'string' => 'Acest :attribute trebuie să fie mai mare decât :value caractere.',
        'array' => 'Acest :attribute trebuie să fie mai mare decât :value itemi.',
    ],
    'gte' => [
        'numeric' => 'Acest :attribute trebuie să fie mai mare sau egal :value.',
        'file' => 'Acest :attribute trebuie să fie mai mare sau egal :value kiloocteți.',
        'string' => 'Acest :attribute trebuie să fie mai mare sau egal :value caractere.',
        'array' => 'Acest :attribute trebuie să aibă :value unul sau mai multe.',
    ],
    'image' => 'Acest :attribute trebuie să fie o imagine.',
    'in' => 'Valoarea introdusă :attribute nu este validă.',
    'in_array' => 'Acest :attribute câmpul nu există în :other.',
    'integer' => 'Acest :attribute trebuie să fie un număr întreg.',
    'ip' => 'Acest :attribute trebuie să fie o adresă IP validă.',
    'ipv4' => 'Acest :attribute trebuie să fie o adresă IPv4.',
    'ipv6' => 'Acest :attribute trebuie să fie o adresă IPv6.',
    'json' => 'Acest :attribute trebuie să fie text JSON.',
    'lt' => [
        'numeric' => 'Acest :attribute trebuie să fie mai mic decât :value.',
        'file' => 'Acest :attribute trebuie să fie mai mic decât :value kiloocteți.',
        'string' => 'Acest :attribute trebuie să fie mai mic decât :value caractere.',
        'array' => 'Acest :attribute trebuie să fie mai mic decât :value itemi.',
    ],
    'lte' => [
        'numeric' => 'Acest :attribute trebuie să fie mai mic sau egal :value.',
        'file' => 'Acest :attribute trebuie să fie mai mic sau egal :value kiloocteți.',
        'string' => 'Acest :attribute trebuie să fie mai mic sau egal :value caractere.',
        'array' => 'Acest :attribute trebuie să fie mai mic sau egal :value itemi.',
    ],
    'max' => [
        'numeric' => 'Acest :attribute nu trebuie să fie mai mare decât :max.',
        'file' => 'Acest :attribute nu trebuie să fie mai mare decât :max kiloocteți.',
        'string' => 'Acest :attribute nu trebuie să fie mai mare decât :max caractere.',
        'array' => 'Acest :attribute nu trebuie să fie mai mare decât :max itemi.',
    ],
    'mimes' => 'Acest :attribute trebuie să fie un fișier de tip: :values.',
    'mimetypes' => 'Acest :attribute trebuie să fie un fișier de tip: :values.',
    'min' => [
        'numeric' => 'Acest :attribute trebuie să fie cel puțin :min.',
        'file' => 'Acest :attribute trebuie să fie cel puțin :min kiloocteți.',
        'string' => 'Acest :attribute trebuie să fie cel puțin :min caractere.',
        'array' => 'Acest :attribute trebuie să fie cel puțin :min itemi.',
    ],
    'not_in' => 'Valoarea introdusă :attribute nu este validă.',
    'not_regex' => 'Acest :attribute format nu este valid.',
    'numeric' => 'Acest :attribute trebuie să fie un număr.',
    'present' => 'Acest :attribute câmpul trebuie să fie prezent.',
    'regex' => 'Acest :attribute format nu este valid.',
    'required' => 'Acest :attribute câmpul este obligatoriu.',
    'required_if' => 'Acest :attribute câmpul este obligatoriu când :other este :value.',
    'required_unless' => 'Acest :attribute câmpul este obligatoriu, cu excepția cazului în care :other este :values.',
    'required_with' => 'Acest :attribute câmpul este obligatoriu când :values este present.',
    'required_with_all' => 'Acest :attribute câmpul este obligatoriu când :values sunt present.',
    'required_without' => 'Acest :attribute câmpul este obligatoriu când :values nu sunt present.',
    'required_without_all' => 'Acest :attribute câmpul este obligatoriu atunci când niciunul dintre :values sunt present.',
    'same' => 'Acest :attribute și :other trebuie să se potrivească.',
    'size' => [
        'numeric' => 'Acest :attribute trebuie să fie :size.',
        'file' => 'Acest :attribute trebuie să fie :size kiloocteți.',
        'string' => 'Acest :attribute trebuie să fie :size caractere.',
        'array' => 'Acest :attribute trebuie să conțină :size itemi.',
    ],
    'starts_with' => 'Acest :attribute trebuie să înceapă cu una dintre următoarele: :values',
    'string' => 'Acest :attribute trebuie să fie un șir.',
    'timezone' => 'Acest :attribute trebuie să fie o zonă validă.',
    'unique' => 'Acest :attribute a fost deja introdus.',
    'uploaded' => 'Acest :attribute nu s-a putut încărca.',
    'url' => 'Acest :attribute format nu este valid.',
    'uuid' => 'Acest :attribute trebuie să fie valid UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
