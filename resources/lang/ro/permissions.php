<?php

return [
    'create_rector' => 'Creare Rector',
    'edit_rector' => 'Editare Rector',
    'delete_rector' => 'Ștergere Rector',
    'view_rector' => 'Vizualizare Rector',

    'create_secretary' => 'Creare Secretar',
    'edit_secretary' => 'Editare Secretar',
    'delete_secretary' => 'Ștergere Secretar',
    'view_secretary' => 'Vizualizare Secretar',

    'create_dean' => 'Creare Decan',
    'edit_dean' => 'Editare Decan',
    'delete_dean' => 'Ștergere Decan',
    'view_dean' => 'Vizualizare Decan',

    'create_chair' => 'Creare Șef catedră',
    'edit_chair' => 'Editare Șef catedră',
    'delete_chair' => 'Ștergere Șef catedră',
    'view_chair' => 'Vizualizare Șef catedră',

    'create_human_resource' => 'Creare RU',
    'edit_human_resource' => 'Editare RU',
    'delete_human_resource' => 'Ștergere RU',
    'view_human_resource' => 'Vizualizare RU',

    'create_teacher' => 'Creare Profesor',
    'edit_teacher' => 'Editare Profesor',
    'delete_teacher' => 'Ștergere Profesor',
    'view_teacher' => 'Vizualizare Profesor',

    'create_student' => 'Creare Student',
    'edit_student' => 'Editare Student',
    'delete_student' => 'Ștergere Student',
    'view_student' => 'Vizualizare Student',

    'confirm_unconfirmed_users' => 'Confirmare utilizator neconfirmat',
    'edit_unconfirmed_users' => 'Editare utilizator neconfirmat',
    'delete_unconfirmed_users' => 'Ștergere utilizator neconfirmat',
    'view_unconfirmed_users' => 'Vizualizare utilizator neconfirmat',


    'create_year_of_study' => 'Creare an de studii',
    'edit_year_of_study' => 'Editare an de studii',
    'delete_year_of_study' => 'Ștergere an de studii',
    'view_year_of_study' => 'Vizualizare an de studii',

    'create_cycle' => 'Creare Ciclu de studii',
    'edit_cycle' => 'Editare Ciclu de studii',
    'delete_cycle' => 'Ștergere Ciclu de studii',
    'view_cycle' => 'Vizualizare Ciclu de studii',

    'create_faculty' => 'Creare Facultate',
    'edit_faculty' => 'Editare Facultate',
    'delete_faculty' => 'Ștergere Facultate',
    'view_faculty' => 'Vizualizare Facultate',

    'create_speciality' => 'Creare Program de studii',
    'edit_speciality' => 'Editare Program de studii',
    'delete_speciality' => 'Ștergere Program de studii',
    'view_speciality' => 'Vizualizare Program de studii',

    'create_group' => 'Creare Grup',
    'edit_group' => 'Editare Grup',
    'delete_group' => 'Ștergere Grup',
    'view_group' => 'Vizualizare Grup',

    'create_course' => 'Creare Curs',
    'edit_course' => 'Editare Curs',
    'delete_course' => 'Ștergere Curs',
    'view_course' => 'Vizualizare Curs',

    'create_course_type' => 'Creare Tip curs',
    'edit_course_type' => 'Editare Tip curs',
    'delete_course_type' => 'Ștergere Tip curs',
    'view_course_type' => 'Vizualizare Tip curs',

    'create_day' => 'Creare Zi',
    'edit_day' => 'Editare Zi',
    'delete_day' => 'Ștergere Zi',
    'view_day' => 'Vizualizare Zi',

    'create_duration_type' => 'Creare Tip perioadă',
    'edit_duration_type' => 'Editare Tip perioadă',
    'delete_duration_type' => 'Ștergere Tip perioadă',
    'view_duration_type' => 'Vizualizare Tip perioadă',

    'create_duration' => 'Creare Perioadă',
    'edit_duration' => 'Editare Perioadă',
    'delete_duration' => 'Ștergere Perioadă',
    'view_duration' => 'Vizualizare Perioadă',

    'create_semester' => 'Creare Semestru',
    'edit_semester' => 'Editare Semestru',
    'delete_semester' => 'Ștergere Semestru',
    'view_semester' => 'Vizualizare Semestru',

    'create_week_type' => 'Creare Tip săptămână',
    'edit_week_type' => 'Editare Tip săptămână',
    'delete_week_type' => 'Ștergere Tip săptămână',
    'view_week_type' => 'Vizualizare Tip săptămână',

    'create_week' => 'Creare Săptămână',
    'edit_week' => 'Editare Săptămână',
    'delete_week' => 'Ștergere Săptămână',
    'view_week' => 'Vizualizare Săptămână',

    'create_teacher_rank' => 'Creare Grad profesor',
    'edit_teacher_rank' => 'Editare Grad profesor',
    'delete_teacher_rank' => 'Ștergere Grad profesor',
    'view_teacher_rank' => 'Vizualizare Grad profesor',

    'create_borderou' => 'Creare Borderou',
    'edit_borderou' => 'Editare Borderou',
    'delete_borderou' => 'Ștergere Borderou',
    'view_borderou' => 'Vizualizare Borderou',

    'create_borderou_note' => 'Creare Note borderou',
    'edit_borderou_note' => 'Editare Note borderou',
    'delete_borderou_note' => 'Ștergere Note borderou',
    'view_borderou_note' => 'Vizualizare Note borderou',
    'export_borderou_note' => 'Export Note borderou',
    'export_student'=>'Export student'

];
