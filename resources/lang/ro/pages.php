<?php

return [
    'absent'=>'Absent',
    'accepted'=>'Admis',
    'act_graduation_year' => 'Anul diplomei de studii',
    'act_number_study' => 'Numărul diplomei de studii',
    'act_seriously_study' => 'Seria actului de studii',
    'action' => 'Acțiune',
    'actions' => 'Acțiuni',
    'admission'=>'Admiterea',
    'add' => 'Adaugă',
    'add_admin' => 'Adaugă administrator',
    'add_borderou' => 'Adaugă borderou',
    'add_borderou_type'=>'Adaugă tip borderou',
    'add_chair' => 'Adaugă responsabil decanat',
    'add_course' => 'Adaugă curs',
    'add_course_type' => 'Adaugă tipul cursului',
    'add_cycle' => 'Adaugă ciclul de studii',
    'add_database'=>'Adaugă baza de date',
    'add_day' => 'Adaugă zi',
    'add_dean' => 'Adaugă decan',
    'add_duration' => 'Adaugă perioadă',
    'add_duration_type' => 'Adaugă tipul perioadei',
    'add_faculty' => 'Adaugă facultate',
    'add_group' => 'Adaugă grup',
    'add_human_resource' => 'Adaugă responsabil resurse umane',
    'add_rector' => 'Adaugă rector',
    'add_secretary' => 'Adaugă responsabil secția studii',
    'add_semester' => 'Adaugă semestru',
    'add_speciality' => 'Adaugă program de studii',
    'add_student' => 'Adaugă student',
    'add_students' => 'Adaugă studenți',
    'add_teacher' => 'Adaugă profesor',
    'add_teacher_rank' => 'Adaugă grad profesor',
    'add_year_of_study' => 'Adaugă an de studii',
    'add_week' => 'Adaugă săptămână',
    'add_week_type' => 'Adaugă tipul săptămânii',
    'address' => 'Adresa',
    'additional_info'=>'Informații adiționale',
    'additional_study'=>'Studii adiționale',
    'afternoon'=>'Fr',
    'already_have_account' => 'Există deja un cont',
    'admin' => 'Admin',
    'admin_information' => 'Informație administrator',
    'admins' => 'Administratori',
    'base_info'=>'Informații de bază',
    'birth_date' => 'Ziua de naștere',
    'block' => 'Blocare',
    'blocked'=>'Blocat',
    'blocked_and_expelled'=>'Blocat și exmatriculat',
    'block_admin' => 'Blocare administrator',
    'block_chair' => 'Blocare responsabil decanat',
    'block_dean' => 'Blocare decan',
    'block_human_resource' => 'Blocare responsabil resurse umane',
    'block_rector' => 'Blocare rector',
    'block_secretary' => 'Blocare secția studii',
    'block_student' => 'Blocare student',
    'block_teacher' => 'Blocare profesor',
    'borderou'=>'Borderou',
    'borderou_name'=>'Nume borderou',
    'borderou_type'=>'Tip borderou',
    'borderou_types'=>'Tipuri de borderou',
    'borderou_type_information'=>'Informații tip borderou',
    'borderou_year'=>'Anul',
    'borderous'=>'Borderouri',
    'budget' => 'Buget',
    'carnet_number' => 'Număr carnet',
    'chair' => 'Responsabil decanat',
    'chair_information' => 'Informații responsabil decanat',
    'chairs' => 'Responsabili decanat',
    'changes_in_studies'=>'Schimbare studii',
    'change' => 'Schimbă',
    'change_photo' => 'Schimbă fotografia',
    'change_using_code'=>'Schimbă utilizând codul',
    'chose_admission'=>'Alege anul admiterii',
    'chose_borderou_type'=>'Alege tipul borderoului',
    'chose_course' => 'Alege cursul',
    'chose_cycle' => 'Alege ciclul',
    'chose_duration_type' => 'Alege tipul perioadei',
    'chose_duration_types' => 'Alege tipurile perioadei',
    'chose_faculties' => 'Alege facultate',
    'chose_faculty' => 'Alege facultate',
    'chose_gender' => 'Alege genul',
    'chose_group' => 'Alege grupul',
    'chose_insurance_policy' => 'Alege polița de asigurare',
    'chose_permissions' => 'Alege permisiuni',
    'choose_photo' => 'Alege fotografie',
    'chose_role' => 'Alege rolul',
    'chose_roles' => 'Alege rolul',
    'chose_semester' => 'Alege semestrul',
    'chose_semesters' => 'Alege semestrele',
    'chose_speciality' => 'Alege programul de studii',
    'chose_students' => 'Alege studenți',
    'chose_tables' => 'Alege tabelul',
    'chose_teacher' => 'Alege profesorul',
    'chose_teacher_rank' => 'Alege gradul profesorului',
    'chose_teacher_ranks' => 'Alege gradele profesorului',
    'chose_types' => 'Alege tipul',
    'chose_week_type' => 'Alege tipul săptămânii',
    'chose_week_types' => 'Alege tipurile săptămâni',
    'chose_year_of_study' => 'Alege anul de studii',
    'citizenship' => 'Cetățenie',
    'city' => 'Oraș',
    'close' => 'Închide',
    'code' => 'Cod',
    'color' => 'Culoare',
    'confirm_import'=>'Confirmare import',
    'connection_name'=>'Nume conectare',
    'confirm_password' => 'Confirmare parolă',
    'confirm_unconfirmed_user' => 'Confirmați utilizatorul neconfirmat',
    'course' => 'Curs',
    'course_information' => 'Informații curs',
    'course_title'=>'Titular la curs',
    'course_types' => 'Tipuri de cursuri',
    'course_type' => 'Tip curs',
    'course_type_information' => 'Informații tip curs',
    'courses' => 'Cursuri',
    'create'=>'Creare',
    'credits'=>'Credite',
    'cycles' => 'Cicluri',
    'cycle' => 'Ciclul',
    'cycle1'=>'Ciclul 1',
    'cycle2'=>'Ciclul 2',
    'cycle_information' => 'Informații ciclul',
    'dashboard' => 'Panou de administrare',
    'database_information'=>'Informații cu privire la baza de date',
    'database_name'=>'Nume baza de date',
    'databases'=>'Baze de date',
    'date' => 'Data',
    'date_of_liquidation'=>'Data lichidării restanței',
    'day' => 'Zi',
    'day_information' => 'Informații zi',
    'days' => 'Zile',
    'dean' => 'Decan',
    'dean_information' => 'Informații decan',
    'deans' => 'Decani',
    'delete' => 'Șterge',
    'delete_admin' => 'Șterge administrator',
    'delete_borderou'=>'Șterge borderou',
    'delete_borderou_note'=>'Șterge studentul din borderou',
    'delete_borderou_type'=>'Șterge tip borderou',
    'delete_chair' => 'Șterge responsabil decanat',
    'delete_course' => 'Șterge curs',
    'delete_course_type' => 'Șterge tip curs',
    'delete_cycle' => 'Șterge ciclul',
    'delete_database'=>'Șterge baza de date',
    'delete_day' => 'Șterge ziua',
    'delete_dean' => 'Șterge decan',
    'delete_duration' => 'Șterge perioada',
    'delete_duration_type' => 'Șterge tip perioadă',
    'delete_faculty' => 'Șterge facultatea',
    'delete_group' => 'Șterge grupul',
    'delete_human_resource' => 'Șterge responsabil resurse umane',
    'delete_rector' => 'Șterge rector',
    'delete_secretary' => 'Șterge secția studii',
    'delete_semester' => 'Șterge semestru',
    'delete_speciality' => 'Șterge programul de studii',
    'delete_student' => 'Șterge student',
    'delete_teacher' => 'Șterge profesor',
    'delete_teacher_rank' => 'Șterge grad profesor',
    'delete_unconfirmed_user' => 'Șterge utilizator neconfirmat',
    'delete_week' => 'Șterge săptămână',
    'delete_week_type' => 'Șterge tip săptămână',
    'district_code' => 'Cod poștal',
    'duration_type' => 'Tipul perioadei',
    'duration_type_information' => 'Informații tip perioadă',
    'duration_types' => 'Tipuri perioade',
    'duration' => 'Perioadă',
    'duration_information' => 'Informații perioade',
    'durations' => 'Perioade',
    'ects'=>'ECTS',
    'edit' => 'Editare',
    'edit_admin' => 'Editare administrator',
    'edit_borderou'=>'Editare borderou',
    'edit_borderou_type'=>'Editare tip borderou',
    'edit_chair' => 'Editare responsabil decanat',
    'edit_course' => 'Editare curs',
    'edit_course_type' => 'Editare tip curs',
    'edit_cycle' => 'Editare ciclul',
    'edit_database'=>'Editare baza de date',
    'edit_day' => 'Editare zi',
    'edit_dean' => 'Editare decan',
    'edit_duration_type' => 'Editare tip perioadă',
    'edit_faculty' => 'Editare facultate',
    'edit_group' => 'Editare grup',
    'edit_human_resource' => 'Editare responsabil resurse umane',
    'edit_rector' => 'Editare rector',
    'edit_secretary' => 'Editare secția studii',
    'edit_semester' => 'Editare semestru',
    'edit_speciality' => 'Editare program de studii',
    'edit_student' => 'Editare student',
    'edit_teacher' => 'Editare profesor',
    'edit_teacher_rank' => 'Editare grad profesor',
    'edit_week' => 'Editare săptămână',
    'edit_week_type' => 'Editare tip săptămână',
    'email' => 'Email',
    'end' => 'Sfârșit',
    'error' => 'Eroare',
    'exam_date'=>'Data examenului',
    'exam_index'=>'Coeficient examen',
    'exam_note'=>'Nota la examen',
    'exam_score'=>'Punctaj la examen',
    'expelled'=>'Exmatriculat',
    'export'=>'Export',
    'faculty' => 'Facultate',
    'faculty_information' => 'Informații facultate',
    'faculties' => 'Facultăți',
    'female' => 'Feminin',
    'final_note'=>'Nota finală',
    'finished'=>'Finisat',
    'first_name' => 'Prenume',
    'friday' => 'Vineri',
    'full_name' => 'Nume Prenume',
    'gender' => 'Gen',
    'get_abiturients'=>'Caută abiturienți',
    'graduate_institution' => 'Instituție absolvită',
    'group' => 'Grup',
    'group_information' => 'Informații grup',
    'groups' => 'Grupuri',
    'human_resource' => 'Resurse umane',
    'human-resource' => 'Resurse umane',
    'human_resource_information' => 'Informații resurse umane',
    'human_resources' => 'Resurse umane',
    'idnp' => 'IDNP',
    'import'=>'Import',
    'import_abiturients'=>'Caută abiturient',
    'import_students'=>'Importă student',
    'information_of_users' => 'Informații despre utilizatori',
    'insurance_policy' => 'Polița de asigurare',
    'last_name'=> 'Nume',
    'login' => 'Logare',
    'logs'=>'Înregistrări',
    'male' => 'Masculin',
    'mobile' => 'Mobil',
    'monday' => 'Luni',
    'morning'=>'zi',
    'name' => 'Nume',
    'nationality' => 'Naţionalitate',
    'negative'=>'Negativ',
    'negatives'=>'Negativ',
    'no' => 'Nu',
    'not_accepted'=>'Neadmis',
    'not_found_any_admins' => 'Nu am găsit niciun administrator',
    'notes'=>'Note',
    'nothing_found' => 'Nu am găsit nimic',
    'off'=>'Nu',
    'OFF'=>'Nu',
    'on'=>'Da',
    'ON'=>'Da',
    'optional'=>'Opțional',
    'order' => 'Ordin',
    'owner'=>'Utilizator',
    'page'=>'Pagina',
    'password' => 'Parola',
    'patronymic' => 'Patronimic',
    'permissions' => 'Permisiuni',
    'personal_email'=>'Email personal',
    'personal_information'=>'Informații personale',
    'please_confirm_import'=>'Confirmare import',
    'phone' => 'Telefon',
    'port'=>'Port',
    'profile' => 'Profil',
    'profile_card' => 'Contul personal',
    'rank' => 'Grad',
    'rector' => 'Rector',
    'rector_information' => 'Informații rector',
    'rectors' => 'Rectori',
    'register' => 'Înregistrare',
    'reset_filter' => 'Resetați căutarea',
    'role' => 'Rol',
    'saturday' => 'Sâmbătă',
    'save'=>'Salvare',
    'search' => 'Căutare',
    'search/export'=>'Căutare/Export',
    'secretary' => 'Secția studii',
    'secretary_information' => 'Informații responsabil secția studii',
    'secretaries' => 'Secția studii',
    'select'=>'Selectare',
    'select_all' => 'Selectați toate',
    'select_create' => 'Selectați creare',
    'select_delete' => 'Selectați ștergere',
    'select_edit' => 'Selectați editare',
    'select_none' => 'Anulați selecția',
    'select_view' => 'Selectați vizualizare',
    'semester' => 'Semestru',
    'semester_index'=>'Coeficient semestru',
    'semester_information' => 'Informații semestru',
    'semester_note'=>'Nota semestrială',
    'semester_score'=>'Punctaj pe semestru',
    'semesters' => 'Semestre',
    'server_ip'=>'IP Server',
    'settings'=>'Setări',
    'sex'=>'Gen',
    'short_name' => 'Nume scurt',
    'show_edit' => 'Afișare editare',
    'something_wrong'=>'Ceva nu este bine',
    'start' => 'Început',
    'specialities' => 'Programe de studii',
    'speciality' => 'Program de studii',
    'speciality_information' => 'Informații program de studii',
    'study' => 'Studii',
    'stayed' => 'Absolvenți',
    'student' => 'Student',
    'student_information'=>'Informații student',
    'student_notes'=>'Note studenți',
    'students' => 'Student',
    'studies'=>'Studii',
    'success' => 'Succes',
    'sunday' => 'Duminică',
    'superadmin'=>'Administrator',
    'sure_to_block_admin?' => 'Sigur blocați administratorul?',
    'sure_to_block_chair?' => 'Sigur blocați responsabil decanat?',
    'sure_to_block_dean?' => 'Sigur blocați decan?',
    'sure_to_block_human_resource?' => 'Sigur blocați responsabil resurse umane?',
    'sure_to_block_rector?' => 'Sigur blocați rector?',
    'sure_to_block_secretary?' => 'Sigur blocați responsabil secția studii?',
    'sure_to_block_student?' => 'Sigur blocați student?',
    'sure_to_block_teacher?' => 'Sigur blocați profesor?',
    'sure_to_confirm_unconfirmed_user?' => 'Sigur confirmați utilizatorul?',
    'sure_to_delete_admin?' => 'Sigur ștergeți administratorul?',
    'sure_to_delete_borderou?'=>'Sigur ștergeți borderoul?',
    'sure_to_delete_borderou_note?'=>'Sigur ștergeți studentul din borderou?',
    'sure_to_delete_borderou_type?'=>'Sigur ștergeți tipul de borderou?',
    'sure_to_delete_chair?' => 'Sigur ștergeți șresponsabil decanat?',
    'sure_to_delete_course?' => 'Sigur ștergeți cursul?',
    'sure_to_delete_course_type?'=>'Sigur ștergeți tipul cursului?',
    'sure_to_delete_cycle?' => 'Sigur ștergeți ciclul?',
    'sure_to_delete_database?'=>'Sigur ștergeți baza de date?',
    'sure_to_delete_day?' => 'Sigur ștergeți ziua?',
    'sure_to_delete_dean?' => 'Sigur ștergeți decanul?',
    'sure_to_delete_duration?' => 'Sigur ștergeți perioada?',
    'sure_to_delete_duration_type?' => 'Sigur ștergeți tipul perioadei?',
    'sure_to_delete_faculty?' => 'Sigur ștergeți facultatea?',
    'sure_to_delete_group?' => 'SSigur ștergeți grupul?',
    'sure_to_delete_human_resource?' => 'Sigur ștergeți responsabil resurse umane?',
    'sure_to_delete_rector?' => 'Sigur ștergeți rectorul?',
    'sure_to_delete_secretary?' => 'Sigur ștergeți responsabil secția studii?',
    'sure_to_delete_semester?' => 'Sigur ștergeți semestrul?',
    'sure_to_delete_speciality?' => 'Sigur ștergeți programul de studii?',
    'sure_to_delete_student?' => 'Sigur ștergeți studentul?',
    'sure_to_delete_teacher?' => 'Sigur ștergeți profesorul?',
    'sure_to_delete_teacher_rank?' => 'Sigur ștergeți grad profesor?',
    'sure_to_delete_unconfirmed_user?' => 'Sigur ștergeți utilizator neconfirmat?',
    'sure_to_delete_week?' => 'Sigur ștergeți săptămâna?',
    'sure_to_delete_week_type?' => 'Sigur ștergeți tipul săptămânii?',
    'sure_to_unblock_admin?' => 'Sigur doriți să deblocați administratorul?',
    'sure_to_unblock_chair?' => 'Sigur doriți să deblocați responsabil decanat?',
    'sure_to_unblock_dean?' => 'Sigur doriți să deblocați decanul?',
    'sure_to_unblock_human_resource?' => 'Sigur doriți să deblocați responsabil resurse umane?',
    'sure_to_unblock_rector?' => 'Sigur doriți să deblocați rectorul?',
    'sure_to_unblock_secretary?' => 'Sigur doriți să deblocați responsabil secția studii?',
    'sure_to_unblock_student?' => 'Sigur doriți să deblocați studentul?',
    'sure_to_unblock_teacher?' => 'Sigur doriți să deblocați profesorul?',
    'table' => 'Tabel',
    'teacher_information' => 'Informație profesor',
    'teacher_rank' => 'Grad profesor',
    'teacher_rank_information' => 'Informații grad profesori',
    'teacher_ranks' => 'Grade profesori',
    'teacher' => 'Profesor',
    'teachers' => 'Profesori',
    'thursday' => 'Joi',
    'total_found'=>'Total găsiți',
    'total_hours'=>'Total ore',
    'total_abiturients'=>'Total abiturienţi',
    'tuesday' => 'Marți',
    'type' => 'Tip',
    'unblock' => 'Deblocare',
    'unblock_admin' => 'Deblocare administrator',
    'unblock_chair' => 'Deblocare responsabil decanat',
    'unblock_dean' => 'Deblocare decan',
    'unblock_human_resource' => 'Deblocare responsabil resurse umane',
    'unblock_rector' => 'Deblocare rector',
    'unblock_secretary' => 'Deblocare responsabil secția studii',
    'unblock_student' => 'Deblocare student',
    'unblock_teacher' => 'Deblocare profesor',
    'unconfirmed_user' => 'Utilizator neconfirmat',
    'unconfirmed_user_information' => 'Informații utilizator neconfirmat',
    'unconfirmed_users' => 'Utilizatori neconfirmati',
    'update' => 'Actualizare',
    'update_borderou_note'=>'Actualizare borderou',
    'username'=>'Utilizator',
    'users' => 'Utilizatori',
    'view'=>'Vizualizare',
    'view_borderou'=>'Vizualizare borderou',
    'year' => 'An',
    'year_of_admission' => 'Anul admiterii',
    'year_of_study' => 'Anul de studii',
    'year_of_studies' => 'Ani de studii',
    'year_of_studyies' => 'Ani de studii',
    'yes' => 'Da',
    'warning' => 'Avertizare',
    'wednesday' => 'Miercuri',
    'week' => 'Săptămână',
    'week_information' => 'Informații săptămână',
    'week_type' => 'Tipul săptămânii',
    'week_type_information' => 'Informații tip săptămână',
    'week_types' => 'Tip săptămână',
    'weeks' => 'Săptămâni',
    'whole_name'=>'Nume complet'

];
