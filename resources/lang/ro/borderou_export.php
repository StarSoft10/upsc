<?php

return [
    'university_name' => '"ION CREANGĂ" STATE PEDAGOGICAL UNIVERSITY',
    'evaluation_border' => 'EVALUATION BOARD (frequency studies)',
    'nr' => 'Nr.',
    'd/o' => 'd/o',
    'full_name' => 'Student\'s first and last name',
    'calculation_grid' => 'Final grade calculation grid',
    'final_note' => 'Final Note',
    'grading_scale_ects' => 'Grading Scale ECTS',
    'signature' => 'Signature',
    'current_required_evaluation' => 'Current Required Evaluation',
    'date' => 'Date',
    'snn' => 'SNN',
    'first_evaluation' => 'First Evaluation',
    'second_evaluation' => 'Second Evaluation',
    'third_evaluation' => 'Third Evaluation',
    'note' => 'Note',
    'scores' => 'Scores',
    'exam_grade' => 'Exam Grade',
    'n' => 'n.',
    'p' => 'p.',
    'faculty' => 'Faculty',
    'speciality' => 'Speciality',
    'year_of_study' => 'Year of study',
    'unit_of_course' => 'Unit of course',
    'nr_total_hours' => 'Nr. Total hours',
    'course_title' => 'Course title',
    'credits' => 'Credits',
    'pdf_header_1' => 'str. Ion Creangă, nr. 1, MD-2069,',
    'pdf_header_2' => 'Chișinău, Republica Moldova',
    'pdf_header_3' => 'www.upsc.md',
    'pdf_footer' => 'UPSC - Universitatea Pedagogică de Stat "ION CREANGĂ" din Chișinău – Toate drepturile rezervate',
];
