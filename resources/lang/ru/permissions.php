<?php

return [
    'create_rector' => 'Create Rector',
    'edit_rector' => 'Edit Rector',
    'delete_rector' => 'Delete Rector',
    'view_rector' => 'View Rector',

    'create_secretary' => 'Create Secretary',
    'edit_secretary' => 'Edit Secretary',
    'delete_secretary' => 'Delete Secretary',
    'view_secretary' => 'View Secretary',

    'create_dean' => 'Create Dean',
    'edit_dean' => 'Edit Dean',
    'delete_dean' => 'Delete Dean',
    'view_dean' => 'View Dean',

    'create_chair' => 'Create Chair',
    'edit_chair' => 'Edit Chair',
    'delete_chair' => 'Delete Chair',
    'view_chair' => 'View Chair',

    'create_human_resource' => 'Create HumanResource',
    'edit_human_resource' => 'Edit HumanResource',
    'delete_human_resource' => 'Delete HumanResource',
    'view_human_resource' => 'View HumanResource',

    'create_teacher' => 'Create Teacher',
    'edit_teacher' => 'Edit Teacher',
    'delete_teacher' => 'Delete Teacher',
    'view_teacher' => 'View Teacher',

    'create_student' => 'Create Student',
    'edit_student' => 'Edit Student',
    'delete_student' => 'Delete Student',
    'view_student' => 'View Student',

    'confirm_unconfirmed_users' => 'Confirm Unconfirmed Users',
    'edit_unconfirmed_users' => 'Edit Unconfirmed Users',
    'delete_unconfirmed_users' => 'Delete Unconfirmed Users',
    'view_unconfirmed_users' => 'View Unconfirmed Users',


    'create_year_of_study' => 'Create Year Of Study',
    'edit_year_of_study' => 'Edit Year Of Study',
    'delete_year_of_study' => 'Delete Year Of Study',
    'view_year_of_study' => 'View Year Of Study',

    'create_cycle' => 'Create Cycle',
    'edit_cycle' => 'Edit Cycle',
    'delete_cycle' => 'Delete Cycle',
    'view_cycle' => 'View Cycle',

    'create_faculty' => 'Create Faculty',
    'edit_faculty' => 'Edit Faculty',
    'delete_faculty' => 'Delete Faculty',
    'view_faculty' => 'View Faculty',

    'create_speciality' => 'Create Speciality',
    'edit_speciality' => 'Edit Speciality',
    'delete_speciality' => 'Delete Speciality',
    'view_speciality' => 'View Speciality',

    'create_group' => 'Create Group',
    'edit_group' => 'Edit Group',
    'delete_group' => 'Delete Group',
    'view_group' => 'View Group',

    'create_course' => 'Create Course',
    'edit_course' => 'Edit Course',
    'delete_course' => 'Delete Course',
    'view_course' => 'View Course',

    'create_course_type' => 'Create Course Type',
    'edit_course_type' => 'Edit Course Type',
    'delete_course_type' => 'Delete Course Type',
    'view_course_type' => 'View Course Type',

    'create_day' => 'Create Day',
    'edit_day' => 'Edit Day',
    'delete_day' => 'Delete Day',
    'view_day' => 'View Day',

    'create_duration_type' => 'Create Duration Type',
    'edit_duration_type' => 'Edit Duration Type',
    'delete_duration_type' => 'Delete Duration Type',
    'view_duration_type' => 'View Duration Type',

    'create_duration' => 'Create Duration',
    'edit_duration' => 'Edit Duration',
    'delete_duration' => 'Delete Duration',
    'view_duration' => 'View Duration',

    'create_semester' => 'Create Semester',
    'edit_semester' => 'Edit Semester',
    'delete_semester' => 'Delete Semester',
    'view_semester' => 'View Semester',

    'create_week_type' => 'Create Week Type',
    'edit_week_type' => 'Edit Week Type',
    'delete_week_type' => 'Delete Week Type',
    'view_week_type' => 'View Week Type',

    'create_week' => 'Create Week',
    'edit_week' => 'Edit Week',
    'delete_week' => 'Delete Week',
    'view_week' => 'View Week',

    'create_teacher_rank' => 'Create Teacher Rank',
    'edit_teacher_rank' => 'Edit Teacher Rank',
    'delete_teacher_rank' => 'Delete Teacher Rank',
    'view_teacher_rank' => 'View Teacher Rank',

    'create_borderou' => 'Create Borderou',
    'edit_borderou' => 'Edit Borderou',
    'delete_borderou' => 'Delete Borderou',
    'view_borderou' => 'View Borderou',

    'create_borderou_note' => 'Create Borderou Note',
    'edit_borderou_note' => 'Edit Borderou Note',
    'delete_borderou_note' => 'Delete Borderou Note',
    'view_borderou_note' => 'View Borderou Note',
    'export_borderou_note' => 'Export Borderou Note',
    'export_student'=>'Export student'

];
