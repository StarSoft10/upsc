<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'localize']
    ], function () {

    Route::get('/', function () {
        if (Auth::check()) {
            $role = Auth::user()->getRoleNames()[0];

            if ($role == 'superadmin') {
                return redirect()->route('superadmin');
            } elseif ($role == 'admin') {
                return redirect()->route('admin');
            } elseif ($role == 'rector') {
                return redirect()->route('rector');
            } elseif ($role == 'secretary') {
                return redirect()->route('secretary');
            } elseif ($role == 'dean') {
                return redirect()->route('dean');
            } elseif ($role == 'chair') {
                return redirect()->route('chair');
            } elseif ($role == 'human-resource') {
                return redirect()->route('human-resource');
            } elseif ($role == 'teacher') {
                return redirect()->route('teacher');
            } elseif ($role == 'student') {
                return redirect()->route('student');
            } else {
                return redirect()->route('signInGet');
            }
        } else {
            return redirect()->route('signInGet');
        }
    });

    Route::get('/signin', function () {
        return view('signin');
    })->name('signInGet');
    Route::post('signin', 'Auth\AuthController@signInPost')->name('signInPost');
    Route::post('signup', 'Auth\AuthController@signUpPost')->name('signUpPost');
});

Route::get('logout', 'Auth\AuthController@logout')->name('logout');

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['auth', 'role:superadmin|admin|rector|dean|chair|human-resource', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'localize']
], function () {
    Route::post('getStudentsForImport', 'General\GeneralController@getStudentsForImport')->name('general.getStudentsForImport');
    Route::post('searchStudentsForImport', 'General\GeneralController@searchStudentsForImport')->name('general.searchStudentsForImport');
    Route::post('getExternalSpecialitiesByCycleOrFaculty', 'General\GeneralController@getExternalSpecialitiesByCycleOrFaculty')->name('general.getExternalSpecialitiesByCycleOrFaculty');
    Route::post('importStudents', 'General\GeneralController@importStudents')->name('general.importStudents');

    Route::post('getFacultiesByCycleForSelectImport', 'General\GeneralController@getFacultiesByCycleForSelectImport')->name('general.getFacultiesByCycleForSelectImport');
    Route::post('getSpecialitiesByFacultyForSelectImport', 'General\GeneralController@getSpecialitiesByFacultyForSelectImport')->name('general.getSpecialitiesByFacultyForSelectImport');
    Route::post('getGroupsBySpecialityForSelectImport', 'General\GeneralController@getGroupsBySpecialityForSelectImport')->name('general.getGroupsBySpecialityForSelectImport');
});
