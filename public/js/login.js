$(document).ready(function () {

    'use strict';

    let input = $('input');

    // Detect browser for css purpose
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $('.form form label').addClass('fontSwitch');
    }

    // Label effect
    input.focus(function () {
        $(this).siblings('label').addClass('active');
    });

    // Form validation
    input.blur(function () {
        // label effect
        if ($(this).val().length > 0) {
            $(this).siblings('label').addClass('active');
        } else {
            $(this).siblings('label').removeClass('active');
        }
    });

    // form switch
    $('a.switch').on('click', function (e) {
        $(this).toggleClass('active');
        e.preventDefault();

        if ($('a.switch').hasClass('active')) {
            $(this).parents('.form-peice').addClass('switched').siblings('.form-peice').removeClass('switched');
        } else {
            $(this).parents('.form-peice').removeClass('switched').siblings('.form-peice').addClass('switched');
        }
    });

    // Reload page
    $('a.profile').on('click', function () {
        location.reload(true);
    });

    // Show?hide password
    $('.password-toggle').on('click', function() {
        const parent = this.parentNode;
        const input = parent.querySelector('input');

        if(this.classList.contains('show')) {
            this.classList.remove('show');
            this.classList.add('hide');
            input.type = 'text';
            return
        }

        this.classList.remove('hide');
        this.classList.add('show');
        input.type = 'password';
    });
});
