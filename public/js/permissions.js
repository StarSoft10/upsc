$('.select-all-js').on('click', function(e){
    e.preventDefault();
    $('#permissions option').prop('selected', true).trigger('chosen:updated');
});

$('.select-none-js').on('click', function(e){
    e.preventDefault();
    $('#permissions option:selected').removeAttr('selected').trigger('chosen:updated');
});

$('.select-view-js').on('click', function(e){
    e.preventDefault();
    let select =  $('#permissions');
    let options =  $('#permissions option');

    options.each(function(){
        if($(this).data('permission').startsWith('view_')){
            if($(this).is(':selected')){
                $(this).prop('selected', false);
            } else {
                $(this).prop('selected', true);
            }
        }
    });
    select.trigger('chosen:updated')
});

$('.select-create-js').on('click', function(e){
    e.preventDefault();
    let select =  $('#permissions');
    let options =  $('#permissions option');

    options.each(function(){
        if($(this).data('permission').startsWith('create_')){
            if($(this).is(':selected')){
                $(this).prop('selected', false);
            } else {
                $(this).prop('selected', true);
            }
        }
    });
    select.trigger('chosen:updated')
});

$('.select-edit-js').on('click', function(e){
    e.preventDefault();
    let select =  $('#permissions');
    let options =  $('#permissions option');

    options.each(function(){
        if($(this).data('permission').startsWith('edit_')){
            if($(this).is(':selected')){
                $(this).prop('selected', false);
            } else {
                $(this).prop('selected', true);
            }
        }
    });
    select.trigger('chosen:updated')
});

$('.select-delete-js').on('click', function(e){
    e.preventDefault();
    let select =  $('#permissions');
    let options =  $('#permissions option');

    options.each(function(){
        if($(this).data('permission').startsWith('delete_')){
            if($(this).is(':selected')){
                $(this).prop('selected', false);
            } else {
                $(this).prop('selected', true);
            }
        }
    });
    select.trigger('chosen:updated')
});