$(function () {
    $('#confirmImport').on('shown.bs.modal', function () {
        $('.modal-backdrop').not(':first').css('z-index', '1051');
    });
});


function getAbiturientsForImport(route, message1, message2) {
    let loader = $('.custom-loader');
    let responseBlock = $('#list_of_abiturients_for_import');

    let databaseId = $('#database_id').val();
    let cycleId = $('#cycle_id_import').val();
    let facultyId = $('#faculty_id_import').val();
    let specialityId = $('#speciality_id_import').val();
    let groupId = $('#group_id_import').val();
    let yearOfStudyId = $('#year_of_study_id_import').val();

    //For Search
    let cycleIdSearch = $('#cycle_id_external').val();
    let facultyIdSearch = $('#faculty_id_external').val();
    let specialityIdSearch = $('#speciality_id_external').val();
    let fullName = $('#name_external').val();

    if (databaseId == '0' || cycleId == '0' || facultyId == '0' || specialityId == '0' || groupId == '0' || yearOfStudyId == '0') {
        $.notify(message1, {
            autoHide: true,
            autoHideDelay: 5000,
            globalPosition: 'top right',
            style: 'bootstrap',
            className: 'warn'
        });
        return false;
    }

    responseBlock.html('');
    loader.show();
    body.css({
        'pointer-events': 'none',
        'opacity': '0.5'
    });

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        async: true,
        url: route,
        type: 'POST',
        data: {
            databaseId: databaseId,
            cycleId: cycleId,
            facultyId: facultyId,
            specialityId: specialityId,
            groupId: groupId,
            yearOfStudyId: yearOfStudyId,
            cycleIdSearch: cycleIdSearch,
            facultyIdSearch: facultyIdSearch,
            specialityIdSearch: specialityIdSearch,
            fullName: fullName
        },
        success: function (response) {
            loader.hide();
            body.css({
                'pointer-events': '',
                'opacity': ''
            });

            if (response.type === 'warning') {
                $.notify(response.message, {
                    autoHide: true,
                    autoHideDelay: 5000,
                    globalPosition: 'top right',
                    style: 'bootstrap',
                    className: 'warn'
                });
            } else {
                $.notify(response.message, {
                    autoHide: true,
                    autoHideDelay: 5000,
                    globalPosition: 'top right',
                    style: 'bootstrap',
                    className: 'success'
                });
            }

            responseBlock.html(response.html);

            $('.standardSelect').chosen({
                disable_search_threshold: 10,
                no_results_text: message2,
                width: '100%'
            });
        }
    });
}

function importStudents(route, message1, message2) {
    let loader = $('.custom-loader');
    let databaseId = $('#database_id').val();
    let cycleId = $('#cycle_id_import').val();
    let facultyId = $('#faculty_id_import').val();
    let specialityId = $('#speciality_id_import').val();
    let groupId = $('#group_id_import').val();
    let yearOfStudyId = $('#year_of_study_id_import').val();

    let secondBlock = $('#secondBlock');
    let cycleIdExternal = $('#cycle_id_external').val();
    let cycles = [1, 2];
    let abiturients = [];

    if ($.inArray(parseInt(cycleIdExternal), cycles) === -1) {
        $.notify(message1, {
            autoHide: true,
            autoHideDelay: 5000,
            globalPosition: 'top right',
            style: 'bootstrap',
            className: 'warn'
        });
        return false;
    }

    secondBlock.find('input.abiturient:checkbox:checked').each(function () {
        abiturients.push($(this).val());
    });

    if (abiturients.length === 0) {
        $.notify(message2, {
            autoHide: true,
            autoHideDelay: 5000,
            globalPosition: 'top right',
            style: 'bootstrap',
            className: 'warn'
        });
        return false;
    }

    loader.show();
    body.css({
        'pointer-events': 'none',
        'opacity': '0.5'
    });

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: route,
        type: 'POST',
        async: false,
        data: {
            databaseId: databaseId,
            cycleId: cycleId,
            facultyId: facultyId,
            specialityId: specialityId,
            groupId: groupId,
            yearOfStudyId: yearOfStudyId,
            cycleIdExternal: cycleIdExternal,
            abiturients: abiturients
        },
        success: function (response) {
            loader.hide();
            body.css({
                'pointer-events': '',
                'opacity': ''
            });

            $('#confirmImport').modal('hide');

            if (response.type === 'warning') {
                $.notify(response.message, {
                    autoHide: true,
                    autoHideDelay: 5000,
                    globalPosition: 'top right',
                    style: 'bootstrap',
                    className: 'warn'
                });
            } else {
                $.notify(response.message, {
                    autoHide: true,
                    autoHideDelay: 5000,
                    globalPosition: 'top right',
                    style: 'bootstrap',
                    className: 'success'
                });
            }
        }
    });
}

function getExternalSpecialitiesByCycleOrFaculty(route) {
    let loader = $('.custom-loader');
    let databaseId = $('#database_id').val();
    let cycleIdSearch = $('#cycle_id_external').val();
    let facultyIdSearch = $('#faculty_id_external').val();
    let specialityExternalBlock = $('#speciality_id_external');

    loader.show();
    body.css({
        'pointer-events': 'none',
        'opacity': '0.5'
    });

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        async: true,
        url: route,
        type: 'POST',
        data: {
            databaseId: databaseId,
            cycleIdSearch: cycleIdSearch,
            facultyIdSearch: facultyIdSearch
        },
        success: function (response) {
            loader.hide();
            body.css({
                'pointer-events': '',
                'opacity': ''
            });

            if (response.type === 'warning') {
                $.notify(response.message, {
                    autoHide: true,
                    autoHideDelay: 5000,
                    globalPosition: 'top right',
                    style: 'bootstrap',
                    className: 'warn'
                });
            } else {
                $.notify(response.message, {
                    autoHide: true,
                    autoHideDelay: 5000,
                    globalPosition: 'top right',
                    style: 'bootstrap',
                    className: 'success'
                });
            }

            specialityExternalBlock.empty();
            specialityExternalBlock.html(response.html);
            specialityExternalBlock.trigger('chosen:updated');
        }
    });
}

function getFacultiesByCycleForSelectImport(element, route) {
    let loader = $('.custom-loader');
    let cycleId = $('#cycle_id_import').val();
    let facultySelect = $('#faculty_id_import');
    let specialitySelect = $('#speciality_id_import');
    let groupSelect = $('#group_id_import');

    if (cycleId === '' || cycleId === undefined || cycleId == null) {
        return;
    }

    loader.show();
    body.css({
        'pointer-events': 'none',
        'opacity': '0.5'
    });

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: route,
        type: 'POST',
        async: true,
        data: {
            cycleId: cycleId
        },
        success: function (response) {
            loader.hide();
            body.css({
                'pointer-events': '',
                'opacity': ''
            });

            facultySelect.empty();
            facultySelect.html(response.html);
            facultySelect.trigger('chosen:updated');

            specialitySelect.empty();
            specialitySelect.trigger('chosen:updated');

            groupSelect.empty();
            groupSelect.trigger('chosen:updated');
        }
    });
}

function getSpecialitiesByFacultyForSelectImport(element, route) {
    let loader = $('.custom-loader');
    let facultyId = $.trim($(element).val());
    let specialitySelect = $('#speciality_id_import');

    if (facultyId === '' || facultyId === undefined || facultyId == null) {
        return;
    }

    loader.show();
    body.css({
        'pointer-events': 'none',
        'opacity': '0.5'
    });

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: route,
        type: 'POST',
        async: true,
        data: {
            facultyId: facultyId
        },
        success: function (response) {
            loader.hide();
            body.css({
                'pointer-events': '',
                'opacity': ''
            });

            specialitySelect.empty();
            specialitySelect.html(response.html);
            specialitySelect.trigger('chosen:updated');
        }
    });
}

function getGroupsBySpecialityForSelectImport(element, route) {
    let loader = $('.custom-loader');
    let specialityId = $.trim($(element).val());
    let groupSelect = $('#group_id_import');

    if (specialityId === '' || specialityId === undefined || specialityId == null) {
        return;
    }

    loader.show();
    body.css({
        'pointer-events': 'none',
        'opacity': '0.5'
    });

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: route,
        type: 'POST',
        async: true,
        data: {
            specialityId: specialityId
        },
        success: function (response) {
            loader.hide();
            body.css({
                'pointer-events': '',
                'opacity': ''
            });

            groupSelect.empty();
            groupSelect.html(response.html);
            groupSelect.trigger('chosen:updated');
        }
    });
}