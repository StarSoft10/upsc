<?php

namespace App\Exports;

use App\Entities\Borderou;
use App\Entities\BorderouNote;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class BorderouNotesExport implements FromView, ShouldAutoSize, WithEvents
{
    use Exportable, RegistersEventListeners;

    protected $request;
    public function __construct($request)
    {
        $this->request = $request;
    }

    public function view(): View
    {
        $request = $this->request;

        $borderou = Borderou::where('id', $request->borderou_id)->where('deleted', 0)->first();
        $borderouNotes = BorderouNote::where('borderou_id', $borderou->id)->get();

        return view('exports.borderou-notes-excel', compact('borderou', 'borderouNotes'));
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {

                $event->sheet->styleCells(
                    'A1:J1',
                    [
                        'font' => [
                            'name'      =>  'Calibri',
                            'size'      =>  13,
                            'bold'      =>  true,
                            'color' => ['argb' => '000000'],
                        ]
                    ]
                );

                $event->sheet->styleCells(
                    'A4:D4',
                    [
                        'font' => [
                            'name'      =>  'Calibri',
                            'size'      =>  13,
                            'bold'      =>  true,
                            'color' => ['argb' => '000000'],
                        ]
                    ]
                );
            },
        ];
    }
}
