<?php

namespace App\Exports;

use App\Entities\Student;
use App\Helpers\AppHelper;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Database\Eloquent\Builder;

class StudentsExport implements FromView, ShouldAutoSize, WithEvents
{
    use Exportable, RegistersEventListeners;

    public $helper;
    protected $request;

    public function __construct($request)
    {
        $this->request = $request;
        $this->helper = new AppHelper();
    }

    public function view(): View
    {
        $request = $this->request;

        $name = !empty($request->name) ? trim($request->name) : '';
        $idnp = !empty($request->idnp) ? trim($request->idnp) : '';
        $phone = !empty($request->phone) ? trim($request->phone) : '';
        $email = !empty($request->email) ? trim($request->email) : '';
        $city = !empty($request->city) ? trim($request->city) : '';

        $cycleIds = !empty($request->cycleIds) ? $this->helper->removeValueFromArray($request->cycleIds, 0) : [];
        $facultyIds = !empty($request->facultyIds) ? $this->helper->removeValueFromArray($request->facultyIds, 0) : [];
        $specialityIds = !empty($request->specialityIds) ? $this->helper->removeValueFromArray($request->specialityIds, 0) : [];
        $groupIds = !empty($request->groupIds) ? $this->helper->removeValueFromArray($request->groupIds, 0) : [];

        $yearOfStudyIds = !empty($request->yearOfStudyIds) ? $this->helper->removeValueFromArray($request->yearOfStudyIds, 0) : [];
        $budget = isset($request->budget) ? trim($request->budget) : '';
        $expelled = isset($request->expelled) ? trim($request->expelled) : '';
        $study = isset($request->study) ? trim($request->study) : '';

        $students = Student::where('deleted', 0)->with('user')
            ->whereHas('user', function (Builder $query) use ($email) {
                if ($email !== '') {
                    $query->where('email', 'LIKE', '%' . trim($email) . '%');
                }
            })->where(function ($query) use($request) {
                if(isset($request->allowedFaculties)) {
                    $query->whereIn('faculty_id', $request->allowedFaculties);
                }
            })->where(function ($query)
            use (
                $name, $idnp, $phone, $city, $cycleIds, $facultyIds, $specialityIds, $groupIds, $yearOfStudyIds,
                $budget, $expelled, $study
            ) {
                if ($name !== '') {
                    $query->where(
                        function ($query) use ($name) {
                            return $query
                                ->where('first_name', 'LIKE', '%' . trim($name) . '%')
                                ->orWhere('last_name', 'LIKE', '%' . trim($name) . '%');
                        });
                }
                if ($idnp !== '') {
                    $query->where('idnp', 'LIKE', '%' . trim($idnp) . '%');
                }
                if ($phone !== '') {
                    $query->where(
                        function ($query) use ($phone) {
                            return $query
                                ->where('phone', 'LIKE', '%' . trim($phone) . '%')
                                ->orWhere('mobile', 'LIKE', '%' . trim($phone) . '%');
                        });
                }
                if ($city !== '') {
                    $query->where('city', 'LIKE', '%' . trim($city) . '%');
                }

                if (count($cycleIds) > 0) {
                    $query->whereIn('cycle_id', $cycleIds);

                    $query->orWhere(
                        function ($query2) use ($cycleIds) {
                            $implode = implode(',', $cycleIds);
                            return $query2
                                ->orWhereRaw('exists(SELECT * FROM students_additional_study WHERE students_additional_study.student_id = students.id and cycle_id IN ('. $implode .')) = 1');
                        });
                }
                if (count($facultyIds) > 0) {
                    $query->whereIn('faculty_id', $facultyIds);

                    $query->orWhere(
                        function ($query2) use ($facultyIds) {
                            $implode = implode(',', $facultyIds);
                            return $query2
                                ->orWhereRaw('exists(SELECT * FROM students_additional_study WHERE students_additional_study.student_id = students.id and faculty_id IN ('. $implode .')) = 1');
                        });
                }
                if (count($specialityIds) > 0) {
                    $query->whereIn('speciality_id', $specialityIds);

                    $query->orWhere(
                        function ($query2) use ($specialityIds) {
                            $implode = implode(',', $specialityIds);
                            return $query2
                                ->orWhereRaw('exists(SELECT * FROM students_additional_study WHERE students_additional_study.student_id = students.id and speciality_id IN ('. $implode .')) = 1');
                        });
                }
                if (count($groupIds) > 0) {
                    $query->whereIn('group_id', $groupIds);

                    $query->orWhere(
                        function ($query2) use ($groupIds) {
                            $implode = implode(',', $groupIds);
                            return $query2
                                ->orWhereRaw('exists(SELECT * FROM students_additional_study WHERE students_additional_study.student_id = students.id and group_id IN ('. $implode .')) = 1');
                        });
                }

                if (count($yearOfStudyIds) > 0) {
                    $query->whereIn('year_of_study_id', $yearOfStudyIds);

                    $query->orWhere(
                        function ($query2) use ($yearOfStudyIds) {
                            $implode = implode(',', $yearOfStudyIds);
                            return $query2
                                ->orWhereRaw('exists(SELECT * FROM students_additional_study WHERE students_additional_study.student_id = students.id and year_of_study_id IN ('. $implode .')) = 1');
                        });
                }
                if ($budget !== '') {
                    $query->where('budget', '=', trim($budget));
                }
                if ($expelled !== '') {
                    $query->where('expelled', '=', trim($expelled));
                }
                if ($study !== '') {
                    $query->where('study', '=', trim($study));
                }
            })->orderBy('last_name')->orderBy('first_name')->get();

        return view('exports.students-excel', compact('students'));
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {

                $event->sheet->styleCells(
                    'A1:F1',
                    [
                        'font' => [
                            'name' => 'Calibri',
                            'size' => 13,
                            'bold' => true,
                            'color' => ['argb' => '000000'],
                        ]
                    ]
                );
            },
        ];
    }
}
