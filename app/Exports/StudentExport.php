<?php

namespace App\Exports;

use App\Entities\Student;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class StudentExport implements FromView, ShouldAutoSize, WithEvents
{
    use Exportable, RegistersEventListeners;

    protected $request;
    public function __construct($request)
    {
        $this->request = $request;
    }

    public function view(): View
    {
        $request = $this->request;

        $student = Student::where('id', $request->student_id)->where('deleted', 0)->first();

        return view('exports.student-excel', compact('student'));
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {

                $event->sheet->styleCells(
                    'A1:F1',
                    [
                        'font' => [
                            'name'      =>  'Calibri',
                            'size'      =>  13,
                            'bold'      =>  true,
                            'color' => ['argb' => '000000'],
                        ]
                    ]
                );
            },
        ];
    }
}
