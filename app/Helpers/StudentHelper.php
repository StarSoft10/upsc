<?php

namespace App\Helpers;

use App\Entities\Borderou;
use App\Entities\BorderouNote;
use App\Entities\Semester;
use App\Entities\Student;
use App\Entities\StudentAdditionalStudy;
use App\Entities\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use PDF;
use Validator;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\StudentsExport;
use App\Exports\StudentExport;
use Illuminate\Database\Eloquent\Builder;

class StudentHelper
{
    public $helper;
    public $studyHelper;

    public function __construct()
    {
        $this->helper = new AppHelper();
        $this->studyHelper = new StudyHelper();
    }

//    public function getStudentNotesBySemesters($student)
//    {
//        $semesters = Semester::all();
//        $notes = [];
//
//        foreach ($semesters as $semester) {
//            $borderous = Borderou::where('semester_id', $semester->id)
//                ->where('group_id', $student->group_id)
////                ->where('finished', 1)
//                ->where('deleted', 0)
//                ->get();
//
//            $borderouNoteInfo = [];
//            foreach ($borderous as $borderou) {
//                $courseName = $borderou->course->name;
//                $courseTitle = $borderou->course_title;
//                $borderouNote = BorderouNote::where('borderou_id', $borderou->id)
//                    ->where('student_id', $student->id)->first();
//
//                $borderouNoteInfo[] = [
//                    'courseName' => $courseName,
//                    'courseTitle' => $courseTitle,
//                    'semesterNote' => $borderouNote ? $this->convertNoteToText($borderouNote->semester_note) : '',
//                    'examNote' => $borderouNote ? $this->convertNoteToText($borderouNote->exam_note) : '',
//                    'finalNote' => $borderouNote ? $borderouNote->final_note: ''
//                ];
//            }
//
//            $notes[$semester->name] = $borderouNoteInfo;
//        }
//
//        return $notes;
//    }

    public function getStudentNotes($student)
    {
        $semestersObj = Semester::all();
        $notes = [];

        $additionalStudies = StudentAdditionalStudy::where('student_id', $student->id)->get();

        if(count($additionalStudies) > 0){
            foreach ($additionalStudies as $additionalStudy) {
                $cycleName = $this->studyHelper->getCycleNameById($additionalStudy->cycle_id);
                $facultyName = $this->studyHelper->getFacultyNameById($additionalStudy->faculty_id);
                $specialityName = $this->studyHelper->getSpecialityNameById($additionalStudy->speciality_id);
                $groupName = $this->studyHelper->getGroupNameById($additionalStudy->group_id);
                $displayName = $cycleName . ' | ' . $facultyName . ' | ' . $specialityName . ' | ' . $groupName;

                $semesters = [];
                foreach ($semestersObj as $semester) {
                    $borderous = Borderou::where('semester_id', $semester->id)
                        ->where('group_id', $additionalStudy->group_id)
                        ->where('deleted', 0)
                        ->get();

                    $borderouNoteInfo = [];
                    foreach ($borderous as $borderou) {
                        $courseName = $borderou->course->name;
                        $courseTitle = $borderou->course_title;
                        $borderouNote = BorderouNote::where('borderou_id', $borderou->id)
                            ->where('student_id', $additionalStudy->student_id)->first();

                        $borderouNoteInfo[] = [
                            'courseName' => $courseName,
                            'courseTitle' => $courseTitle,
                            'semesterNote' => $borderouNote ? $this->convertNoteToText($borderouNote->semester_note) : '',
                            'examNote' => $borderouNote ? $this->convertNoteToText($borderouNote->exam_note) : '',
                            'finalNote' => $borderouNote ? $borderouNote->final_note: ''
                        ];
                    }

                    $semesters[$semester->name] = $borderouNoteInfo;
                }

                $notes[$displayName] = $semesters;
            }

            $semesters = [];
            foreach ($semestersObj as $semester) {
                $borderous = Borderou::where('semester_id', $semester->id)
                    ->where('group_id', $student->group_id)
                    ->where('deleted', 0)
                    ->get();

                $borderouNoteInfo = [];
                foreach ($borderous as $borderou) {
                    $courseName = $borderou->course->name;
                    $courseTitle = $borderou->course_title;
                    $borderouNote = BorderouNote::where('borderou_id', $borderou->id)
                        ->where('student_id', $student->id)->first();

                    if($borderouNote) {
                        $borderouNoteInfo[] = [
                            'courseName' => $courseName,
                            'courseTitle' => $courseTitle,
                            'semesterNote' => $borderouNote ? $this->convertNoteToText($borderouNote->semester_note) : '',
                            'examNote' => $borderouNote ? $this->convertNoteToText($borderouNote->exam_note) : '',
                            'finalNote' => $borderouNote ? $borderouNote->final_note: ''
                        ];
                    }
                }

                $semesters[$semester->name] = $borderouNoteInfo;
            }

            $cycleName = $this->studyHelper->getCycleNameById($student->cycle_id);
            $facultyName = $this->studyHelper->getFacultyNameById($student->faculty_id);
            $specialityName = $this->studyHelper->getSpecialityNameById($student->speciality_id);
            $groupName = $this->studyHelper->getGroupNameById($student->group_id);
            $displayName = $cycleName . ' | ' . $facultyName . ' | ' . $specialityName . ' | ' . $groupName;

            $notes[$displayName] = $semesters;
        } else {
            $semesters = [];

            foreach ($semestersObj as $semester) {
                $borderous = Borderou::where('semester_id', $semester->id)
                    ->where('group_id', $student->group_id)
                    ->where('deleted', 0)
                    ->get();

                $borderouNoteInfo = [];
                foreach ($borderous as $borderou) {
                    $courseName = $borderou->course->name;
                    $courseTitle = $borderou->course_title;
                    $borderouNote = BorderouNote::where('borderou_id', $borderou->id)
                        ->where('student_id', $student->id)->first();

                    if($borderouNote) {
                        $borderouNoteInfo[] = [
                            'courseName' => $courseName,
                            'courseTitle' => $courseTitle,
                            'semesterNote' => $borderouNote ? $this->convertNoteToText($borderouNote->semester_note) : '',
                            'examNote' => $borderouNote ? $this->convertNoteToText($borderouNote->exam_note) : '',
                            'finalNote' => $borderouNote ? $borderouNote->final_note: ''
                        ];
                    }
                }

                $semesters[$semester->name] = $borderouNoteInfo;
            }

            $cycleName = $this->studyHelper->getCycleNameById($student->cycle_id);
            $facultyName = $this->studyHelper->getFacultyNameById($student->faculty_id);
            $specialityName = $this->studyHelper->getSpecialityNameById($student->speciality_id);
            $groupName = $this->studyHelper->getGroupNameById($student->group_id);
            $displayName = $cycleName . ' ' . $facultyName . ' ' . $specialityName . ' ' . $groupName;

            $notes[$displayName] = $semesters;
        }

        return $notes;
    }

    public function exportStudentsPdf($request)
    {
        ini_set('pcre.backtrack_limit', '5000000');

        $name = !empty($request->name) ? trim($request->name) : '';
        $idnp = !empty($request->idnp) ? trim($request->idnp) : '';
        $phone = !empty($request->phone) ? trim($request->phone) : '';
        $email = !empty($request->email) ? trim($request->email) : '';
        $city = !empty($request->city) ? trim($request->city) : '';

        $cycleIds = !empty($request->cycleIds) ? $this->helper->removeValueFromArray($request->cycleIds, 0) : [];
        $facultyIds = !empty($request->facultyIds) ? $this->helper->removeValueFromArray($request->facultyIds, 0) : [];
        $specialityIds = !empty($request->specialityIds) ? $this->helper->removeValueFromArray($request->specialityIds, 0) : [];
        $groupIds = !empty($request->groupIds) ? $this->helper->removeValueFromArray($request->groupIds, 0) : [];

        $yearOfStudyIds = !empty($request->yearOfStudyIds) ? $this->helper->removeValueFromArray($request->yearOfStudyIds, 0) : [];
        $budget = isset($request->budget) ? trim($request->budget) : '';
        $expelled = isset($request->expelled) ? trim($request->expelled) : '';
        $study = isset($request->study) ? trim($request->study) : '';

        $students = Student::where('deleted', 0)->with('user')
            ->whereHas('user', function (Builder $query) use ($email) {
                if ($email !== '') {
                    $query->where('email', 'LIKE', '%' . trim($email) . '%');
                }
            })->where(function ($query) use($request) {
                if(isset($request->allowedFaculties)) {
                    $query->whereIn('faculty_id', $request->allowedFaculties);
                }
            })->where(function ($query)
            use (
                $name, $idnp, $phone, $city, $cycleIds, $facultyIds, $specialityIds, $groupIds, $yearOfStudyIds,
                $budget, $expelled, $study
            ) {
                if ($name !== '') {
                    $query->where(
                        function ($query) use ($name) {
                            return $query
                                ->where('first_name', 'LIKE', '%' . trim($name) . '%')
                                ->orWhere('last_name', 'LIKE', '%' . trim($name) . '%');
                        });
                }
                if ($idnp !== '') {
                    $query->where('idnp', 'LIKE', '%' . trim($idnp) . '%');
                }
                if ($phone !== '') {
                    $query->where(
                        function ($query) use ($phone) {
                            return $query
                                ->where('phone', 'LIKE', '%' . trim($phone) . '%')
                                ->orWhere('mobile', 'LIKE', '%' . trim($phone) . '%');
                        });
                }
                if ($city !== '') {
                    $query->where('city', 'LIKE', '%' . trim($city) . '%');
                }

                if (count($cycleIds) > 0) {
                    $query->whereIn('cycle_id', $cycleIds);

                    $query->orWhere(
                        function ($query2) use ($cycleIds) {
                            $implode = implode(',', $cycleIds);
                            return $query2
                                ->orWhereRaw('exists(SELECT * FROM students_additional_study WHERE students_additional_study.student_id = students.id and cycle_id IN ('. $implode .')) = 1');
                        });
                }
                if (count($facultyIds) > 0) {
                    $query->whereIn('faculty_id', $facultyIds);

                    $query->orWhere(
                        function ($query2) use ($facultyIds) {
                            $implode = implode(',', $facultyIds);
                            return $query2
                                ->orWhereRaw('exists(SELECT * FROM students_additional_study WHERE students_additional_study.student_id = students.id and faculty_id IN ('. $implode .')) = 1');
                        });
                }
                if (count($specialityIds) > 0) {
                    $query->whereIn('speciality_id', $specialityIds);

                    $query->orWhere(
                        function ($query2) use ($specialityIds) {
                            $implode = implode(',', $specialityIds);
                            return $query2
                                ->orWhereRaw('exists(SELECT * FROM students_additional_study WHERE students_additional_study.student_id = students.id and speciality_id IN ('. $implode .')) = 1');
                        });
                }
                if (count($groupIds) > 0) {
                    $query->whereIn('group_id', $groupIds);

                    $query->orWhere(
                        function ($query2) use ($groupIds) {
                            $implode = implode(',', $groupIds);
                            return $query2
                                ->orWhereRaw('exists(SELECT * FROM students_additional_study WHERE students_additional_study.student_id = students.id and group_id IN ('. $implode .')) = 1');
                        });
                }

                if (count($yearOfStudyIds) > 0) {
                    $query->whereIn('year_of_study_id', $yearOfStudyIds);

                    $query->orWhere(
                        function ($query2) use ($yearOfStudyIds) {
                            $implode = implode(',', $yearOfStudyIds);
                            return $query2
                                ->orWhereRaw('exists(SELECT * FROM students_additional_study WHERE students_additional_study.student_id = students.id and year_of_study_id IN ('. $implode .')) = 1');
                        });
                }
                if ($budget !== '') {
                    $query->where('budget', '=', trim($budget));
                }
                if ($expelled !== '') {
                    $query->where('expelled', '=', trim($expelled));
                }
                if ($study !== '') {
                    $query->where('study', '=', trim($study));
                }
            })->orderBy('last_name')->orderBy('first_name')->get();

        if (!Storage::exists('public/exports/students')) {
            Storage::makeDirectory('public/exports/students', 0777, true);
        }

        $html = '<table class="table table-bordered">
                     <thead>
                         <tr>
                             <th>' . __('pages.full_name') . '</th>
                             <th>' . __('pages.year_of_study') . '</th>
                             <th>' . __('pages.cycle') . '</th>
                             <th>' . __('pages.faculty') . '</th>
                             <th>' . __('pages.speciality') . '</th>
                             <th>' . __('pages.group') . '</th>
                         </tr>
                     </thead>
                     <tbody>';
        if (count($students) > 0) {
            foreach ($students as $student) {
                if ($student->blocked == 1) {
                    $class = 'blocked';
                } else {
                    $class = '';
                }

                $html .= '<tr>
                              <td class="' . $class . '"><span>' . $student->last_name . ' ' . $student->first_name . '</span></td>
                              <td class="' . $class . '"><span>' . $student->getAllStudentYearOfStudies() . '</span></td>
                              <td class="' . $class . '"><span>' . $student->getAllStudentCycles() . '</span></td>
                              <td class="' . $class . '"><span>' . $student->getAllStudentFaculties() . '</span></td>
                              <td class="' . $class . '"><span>' . $student->getAllStudentSpecialities() . '</span></td>
                              <td class="' . $class . '"><span>' . $student->getAllStudentGroups() . '</span></td>
                          </tr>';
            }
        } else {
            $html .= '<tr class="text-center">
                          <td colspan="6">' . __('messages.not_found_any_students') . '</td>
                      </tr>';
        }

        $html .= '</tbody></table>';

        $path = public_path('images/logo_full.png');
        $base64 = base64_encode(file_get_contents($path));
        $logo = 'data:' . mime_content_type($path) . ';base64,' . $base64;

        $pathBackground = public_path('images/4.png');
        $base64Background = base64_encode(file_get_contents($pathBackground));
        $logoBackground = 'data:' . mime_content_type($pathBackground) . ';base64,' . $base64Background;

        $pdfName = 'students_' . Carbon::now()->format('d_m_Y_H_i_s') . '.pdf';
        PDF::loadView('exports.students-pdf', compact('html', 'logo', 'logoBackground'))
            ->save(storage_path('app/public/exports/students/' . $pdfName));

        if (!Storage::exists('public/exports/students/' . $pdfName)) {
            return response()->json(['type' => 'warning', 'message' => __('messages.something_wrong')]);
        }

        return response()->json([
            'type' => 'success',
            'url' => asset('storage/exports/students/' . $pdfName),
            'name' => $pdfName,
            'message' => __('messages.file_download_successfully')
        ]);
    }

    public function exportStudentsExcel($request)
    {
        ini_set('pcre.backtrack_limit', '5000000');

        if (!Storage::exists('public/exports/students')) {
            Storage::makeDirectory('public/exports/students', 0777, true);
        }

        $excelName = 'students' . Carbon::now()->format('d_m_Y_H_i_s') . '.xlsx';

        Excel::store(new StudentsExport($request), 'public/exports/students/' . $excelName);

        if (!Storage::exists('public/exports/students/' . $excelName)) {
            return response()->json(['type' => 'warning', 'message' => __('messages.something_wrong')]);
        }

        return response()->json([
            'type' => 'success',
            'url' => asset('storage/exports/students/' . $excelName),
            'name' => $excelName,
            'message' => __('messages.file_download_successfully')
        ]);
    }

    public function exportStudentPdf($request)
    {
        ini_set('pcre.backtrack_limit', '5000000');

        $validator = Validator::make($request->all(), [
            'student_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['type' => 'warning', $validator->errors()->first()]);
        }

        if(isset($request->allowedFaculties)) {
            $student = Student::where('id', $request->student_id)->where('deleted', 0)->whereIn('faculty_id', $request->allowedFaculties)->first();
        } else {
            $student = Student::where('id', $request->student_id)->where('deleted', 0)->first();
        }
        if (!$student) {
            return response()->json(['type' => 'warning', 'message' => __('messages.student_not_found')]);
        }

        if (!Storage::exists('public/exports/student')) {
            Storage::makeDirectory('public/exports/student', 0777, true);
        }

        $html = '';

        $path = public_path('images/logo_full.png');
        $base64 = base64_encode(file_get_contents($path));
        $logo = 'data:' . mime_content_type($path) . ';base64,' . $base64;

        $pathBackground = public_path('images/4.png');
        $base64Background = base64_encode(file_get_contents($pathBackground));
        $logoBackground = 'data:' . mime_content_type($pathBackground) . ';base64,' . $base64Background;

        $pdfName = $student->first_name . '_' . $student->last_name . '_' . Carbon::now()->format('d_m_Y_H_i_s') . '.pdf';
        PDF::loadView('exports.student-pdf', compact('html', 'logo', 'logoBackground'))
            ->save(storage_path('app/public/exports/student/' . $pdfName));

        if (!Storage::exists('public/exports/student/' . $pdfName)) {
            return response()->json(['type' => 'warning', 'message' => __('messages.something_wrong')]);
        }

        return response()->json([
            'type' => 'success',
            'url' => asset('storage/exports/student/' . $pdfName),
            'name' => $pdfName,
            'message' => __('messages.file_download_successfully')
        ]);
    }

    public function exportStudentExcel($request)
    {
        ini_set('pcre.backtrack_limit', '5000000');

        $validator = Validator::make($request->all(), [
            'student_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['type' => 'warning', $validator->errors()->first()]);
        }

        if(isset($request->allowedFaculties)) {
            $student = Student::where('id', $request->student_id)->where('deleted', 0)->whereIn('faculty_id', $request->allowedFaculties)->first();
        } else {
            $student = Student::where('id', $request->student_id)->where('deleted', 0)->first();
        }
        if (!$student) {
            return response()->json(['type' => 'warning', 'message' => __('messages.student_not_found')]);
        }

        if (!Storage::exists('public/exports/student')) {
            Storage::makeDirectory('public/exports/student', 0777, true);
        }

        $excelName = $student->first_name . '_' . $student->last_name . '_' . Carbon::now()->format('d_m_Y_H_i_s') . '.xlsx';

        Excel::store(new StudentExport($request), 'public/exports/student/' . $excelName);

        if (!Storage::exists('public/exports/student/' . $excelName)) {
            return response()->json(['type' => 'warning', 'message' => __('messages.something_wrong')]);
        }

        return response()->json([
            'type' => 'success',
            'url' => asset('storage/exports/student/' . $excelName),
            'name' => $excelName,
            'message' => __('messages.file_download_successfully')
        ]);
    }

    public function checkIfStudentExistByEmail($email)
    {
        return User::where('email', $email)->exists();
    }

    public function generateStudentEmail($lastName, $firstName)
    {
        $generateNewEmail = '';
        for ($i = 1; $i <= 100000; $i++){
            $email = $lastName . '.' . $firstName . $i . '@upsc.md';
            $generateNewEmail = $this->formatEmail($email);
            $exist = User::where('email', $generateNewEmail)->exists();

            if(!$exist){
                break;
            }
        }

        return $generateNewEmail;
    }

    public function formatEmail($email)
    {
        return  str_replace(['ă', 'â', 'î', 'ș', 'ț'], ['a', 'a', 'i', 's', 't'],  mb_strtolower(trim($email), 'UTF-8'));
    }

    public function convertNoteToText($note)
    {
        if (is_null($note) || $note == '') {
            return '';
        }
        if ((int)$note > 0) {
            $semNote = $note;
        } else {
            if ((int)$note == 0) {
                $semNote = __('pages.accepted');
            } elseif ((int)$note == -2) {
                $semNote = __('pages.not_accepted');
            } elseif ((int)$note == -3) {
                $semNote = __('pages.negative');
            } else {
                $semNote = __('pages.absent');
            }
        }

        return $semNote;
    }
}