<?php

namespace App\Helpers;

use App\Entities\Admin;
use App\Entities\Borderou;
use App\Entities\BorderouNote;
use App\Entities\Chair;
use App\Entities\Dean;
use App\Entities\HumanResource;
use App\Entities\Rector;
use App\Entities\Secretary;
use App\Entities\Student;
use App\Entities\StudentAdditionalStudy;
use App\Entities\Teacher;
use App\Entities\User;
use App\Exports\BorderouNotesExport;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;
use PDF;
use Validator;
use Maatwebsite\Excel\Facades\Excel;

class BorderouHelper
{
//    public function exportBorderouNotePdfOld($request)
//    {
//        ini_set('pcre.backtrack_limit', '5000000');
//
//        $validator = Validator::make($request->all(), [
//            'borderou_id' => 'required|numeric'
//        ]);
//
//        if ($validator->fails()) {
//            return response()->json(['type' => 'warning', $validator->errors()->first()]);
//        }
//
//        $borderou = Borderou::where('id', $request->borderou_id)->where('deleted', 0)->first();
//        if (!$borderou) {
//            return response()->json(['type' => 'warning', 'message' => __('messages.borderou_not_found')]);
//        }
//
//        $borderouNotes = BorderouNote::where('borderou_id', $request->borderou_id)->get();
//
//        $html = '<div style="font-weight: bold; font-size: 12px;margin-bottom: 0;float: left;width: 50%">
//                     '. __('pages.cycle') .': '. $borderou->cycle->name .' <br>
//                     '. __('pages.faculty') .': '. $borderou->faculty->name .' <br>
//                     '. __('pages.speciality') .': '. $borderou->speciality->name .' <br>
//                     '. __('pages.group') .': '. $borderou->group->name .' <br>
//                     '. __('pages.year_of_study') .': '. $borderou->yearOfStudy->name .'
//                 </div>
//                 <div style="font-weight: bold; font-size: 12px;margin-bottom: 0;float: right;width: 50%">
//                     '. __('pages.semester') .': '. $borderou->semester->name .' <br>
//                     '. __('pages.course') .': '. $borderou->course->name .' <br>
//                     '. __('pages.course_title') .': '. $borderou->course_title .' <br>
//                     '. __('pages.exam_date') .': '. Carbon::createFromFormat('Y-m-d', $borderou->exam_date)->format('d/m/Y')  .' <br>
//                     '. __('pages.teacher') .': '. $borderou->teacher->first_name .' '. $borderou->teacher->last_name .'
//                 </div>
//                 <div style="clear: both"></div>
//                 <table class="table table-bordered" style="margin-top: 10px">
//                     <thead>
//                         <tr>
//                             <th>#</th>
//                             <th>'. __('pages.full_name') .'</th>
//                             <th>'. __('pages.semester_note') .'</th>
//                             <th>'. __('pages.exam_note') .'</th>
//                         </tr>
//                     </thead>
//                     <tbody>';
//
//        foreach ($borderouNotes as $key => $borderouNote){
//            $studentFullName = $borderouNote->student->last_name .' '. $borderouNote->student->first_name;
//            $semesterNote = $this->convertNoteToText($borderouNote->semester_note);
//            $examNote = $this->convertNoteToText($borderouNote->exam_note);
//
//            $html .= '<tr>
//                          <th>'. ($key + 1) .'</th>
//                          <th>'. $studentFullName .'</th>
//                          <td>'. $semesterNote .'</td>
//                          <td>'. $examNote .'</td>
//                      </tr>';
//        }
//
//        $html .= '</tbody></table>';
//
//        if(!Storage::exists('public/exports/borderou-notes')) {
//            Storage::makeDirectory('public/exports/borderou-notes', 0777, true);
//        }
//
//        $path = public_path('images/logo_full.png');
//        $base64 = base64_encode(file_get_contents($path));
//        $logo = 'data:' . mime_content_type($path) . ';base64,' . $base64;
//
//        $pathBackground = public_path('images/4.png');
//        $base64Background = base64_encode(file_get_contents($pathBackground));
//        $logoBackground = 'data:' . mime_content_type($pathBackground) . ';base64,' . $base64Background;
//
//        $pdfName = 'borderou_' . Carbon::now()->format('d_m_Y_H_i_s') . '.pdf';
//        PDF::loadView('exports.borderou-notes-pdf', compact('html', 'logo', 'logoBackground'))
//            ->save(storage_path('app/public/exports/borderou-notes/' . $pdfName));
//
//        if (!Storage::exists('public/exports/borderou-notes/' . $pdfName)) {
//            return response()->json(['type' => 'warning', 'message' =>  __('messages.something_wrong')]);
//        }
//
//        return response()->json([
//            'type' => 'success',
//            'url' => asset('storage/exports/borderou-notes/'. $pdfName),
//            'name' => $pdfName,
//            'message' => __('messages.file_download_successfully')
//        ]);
//    }

    public function exportBorderouNotePdf($request)
    {
        ini_set('pcre.backtrack_limit', '5000000');

        $validator = Validator::make($request->all(), [
            'borderou_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['type' => 'warning', $validator->errors()->first()]);
        }

        $borderou = Borderou::where('id', $request->borderou_id)->where('deleted', 0)->first();
        if (!$borderou) {
            return response()->json(['type' => 'warning', 'message' => __('messages.borderou_not_found')]);
        }

        $borderouNotes = BorderouNote::where('borderou_id', $request->borderou_id)->get();
        $borderouType = $borderou->borderouType;

        $html = '<div style="text-align: center;margin-bottom: 10px;font-weight: bold;font-size: 13px;">
                    '. __('borderou_export.university_name') .'<br>
                    '. __('borderou_export.evaluation_border') .'
                 </div>
                 <div style="font-weight: bold; font-size: 12px;margin-bottom: 0;text-transform: uppercase;">
                     <div style="float: left;width: 30%">
                         <div style="margin-bottom: 6px">'. __('borderou_export.faculty') .'</div>
                         <div style="margin-bottom: 6px">'. __('borderou_export.speciality') .'</div>
                         <div style="margin-bottom: 6px">'. __('borderou_export.year_of_study') .'</div>
                         <div style="margin-bottom: 5px">'. __('borderou_export.unit_of_course') .'</div>
                         <div style="margin-bottom: 5px">'. __('borderou_export.nr_total_hours') .'</div>
                         <div>'. __('borderou_export.course_title') .'</div>
                     </div>
                     <div style="float: left;width: 70%">
                         <div style="border-bottom: 1px solid;margin-bottom: 5px">'. $borderou->faculty->name .'</div>
                         <div style="border-bottom: 1px solid;margin-bottom: 5px">'. $borderou->speciality->name .'</div>
                         <div style="border-bottom: 1px solid;margin-bottom: 5px">'. $borderou->yearOfStudy->name .' '. $borderou->semester->name .'</div>
                         <div style="border-bottom: 1px solid;margin-bottom: 5px">'. $borderou->course->name .'</div>
                         <div style="border-bottom: 1px solid;margin-bottom: 5px">'. $borderou->total_hours .' '. __('borderou_export.credits') .' '. $borderou->credits .'</div>
                         <div style="border-bottom: 1px solid">'. $borderou->course_title .'</div>
                     </div>
                 </div>

                 <table style="margin-top: 10px">
                     <tr>
                         <td rowspan="6">'. __('borderou_export.nr') .'<br>'. __('borderou_export.d/o') .'</td>
                         <td rowspan="6">'. __('borderou_export.full_name') .'</td>
                         <td colspan="8">'. __('borderou_export.calculation_grid') .'</td>
                         <td>'. __('borderou_export.final_note') .'</td>
                         <td rowspan="6" width="50">'. __('borderou_export.grading_scale_ects') .'</td>
                         <td rowspan="6">'. __('borderou_export.signature') .'</td>
                     </tr>
                     <tr>
                         <td rowspan="3" colspan="2">
                             '. __('borderou_export.current_required_evaluation') .' <br/>
                             x '. $borderouType->semester_index .' ('. ($borderouType->semester_index * 100) .'%)
                         </td>
                         <td colspan="2">'. __('borderou_export.date') .'</td>
                         <td colspan="2">'. __('borderou_export.date') .'</td>
                         <td colspan="2">'. __('borderou_export.date') .'</td>
                         <td>'. __('borderou_export.snn') .'</td>
                     </tr>
                     <tr>
                         <td colspan="2">'. __('borderou_export.first_evaluation') .'</td>
                         <td colspan="2">'. __('borderou_export.second_evaluation') .'</td>
                         <td colspan="2">'. __('borderou_export.third_evaluation') .'</td>
                     </tr>
                     <tr>
                         <td colspan="2">'. Carbon::createFromFormat('Y-m-d', $borderou->exam_date)->format('d/m/Y') .'</td>
                         <td colspan="2">'. Carbon::createFromFormat('Y-m-d', $borderou->date_of_liquidation)->format('d/m/Y') .'</td>
                         <td colspan="2"></td>
                     </tr>
                     <tr>
                         <td rowspan="2">'. __('borderou_export.note') .'</td>
                         <td rowspan="2">'. __('borderou_export.scores') .'</td>
                         <td colspan="2">
                             '. __('borderou_export.exam_grade') .' <br/>
                             x '. $borderouType->exam_index .' ('. ($borderouType->exam_index * 100) .'%)
                         </td>
                         <td colspan="2">
                             '. __('borderou_export.exam_grade') .' <br/>
                             x '. $borderouType->exam_index .' ('. ($borderouType->exam_index * 100) .'%)
                         </td>
                         <td colspan="2">
                             '. __('borderou_export.exam_grade') .' <br/>
                             x '. $borderouType->exam_index .' ('. ($borderouType->exam_index * 100) .'%)
                         </td>
                         <td></td>
                     </tr>
                     <tr>
                         <td>'. __('borderou_export.n') .'</td>
                         <td>'. __('borderou_export.p') .'</td>
                         <td>'. __('borderou_export.n') .'</td>
                         <td>'. __('borderou_export.p') .'</td>
                         <td>'. __('borderou_export.n') .'</td>
                         <td>'. __('borderou_export.p') .'</td>
                         <td></td>
                     </tr>';

        foreach ($borderouNotes as $key => $borderouNote){
            $studentFullName = $borderouNote->student->last_name .' '. $borderouNote->student->first_name;

            $semNote = $this->getSemesterNote($borderouNote);
            $semScore = $this->getSemesterScore($borderouNote);

            $examNote = $this->getExamNote($borderouNote);
            $examScore = $this->getExamScore($borderouNote);

            $html .= '<tr>
                          <td>'. ($key + 1) .'</td>
                          <td style="text-align: left" width="150">'. $studentFullName .'</td>
                          <td>'. $semNote .'</td>
                          <td>'. $semScore .'</td>
                          <td>'. $examNote .'</td>
                          <td>'. $examScore .'</td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td>' . $borderouNote->final_note . '</td>
                          <td>' . $borderouNote->ects . '</td>
                          <td></td>
                      </tr>';
        }

        $html .= '</table>';

        if(!Storage::exists('public/exports/borderou-notes')) {
            Storage::makeDirectory('public/exports/borderou-notes', 0777, true);
        }

        $path = public_path('images/logo_full.png');
        $base64 = base64_encode(file_get_contents($path));
        $logo = 'data:' . mime_content_type($path) . ';base64,' . $base64;

        $pathBackground = public_path('images/4.png');
        $base64Background = base64_encode(file_get_contents($pathBackground));
        $logoBackground = 'data:' . mime_content_type($pathBackground) . ';base64,' . $base64Background;

        $pdfName = 'borderou_' . Carbon::now()->format('d_m_Y_H_i_s') . '.pdf';
        PDF::loadView('exports.borderou-notes-pdf', compact('html', 'logo', 'logoBackground'))
            ->save(storage_path('app/public/exports/borderou-notes/' . $pdfName));

        if (!Storage::exists('public/exports/borderou-notes/' . $pdfName)) {
            return response()->json(['type' => 'warning', 'message' =>  __('messages.something_wrong')]);
        }

        return response()->json([
            'type' => 'success',
            'url' => asset('storage/exports/borderou-notes/'. $pdfName),
            'name' => $pdfName,
            'message' => __('messages.file_download_successfully')
        ]);
    }

    public function exportBorderouNoteExcel($request)
    {
        ini_set('pcre.backtrack_limit', '5000000');

        $validator = Validator::make($request->all(), [
            'borderou_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['type' => 'warning', $validator->errors()->first()]);
        }

        $borderou = Borderou::where('id', $request->borderou_id)->where('deleted', 0)->first();
        if (!$borderou) {
            return response()->json(['type' => 'warning', 'message' => __('messages.borderou_not_found')]);
        }

        if(!Storage::exists('public/exports/borderou-notes')) {
            Storage::makeDirectory('public/exports/borderou-notes', 0777, true);
        }

        $excelName = 'borderou_' . Carbon::now()->format('d_m_Y_H_i_s') . '.xlsx';

        Excel::store(new BorderouNotesExport($request), 'public/exports/borderou-notes/' . $excelName);

        if (!Storage::exists('public/exports/borderou-notes/' . $excelName)) {
            return response()->json(['type' => 'warning', 'message' =>  __('messages.something_wrong')]);
        }

        return response()->json([
            'type' => 'success',
            'url' => asset('storage/exports/borderou-notes/'. $excelName),
            'name' => $excelName,
            'message' => __('messages.file_download_successfully')
        ]);
    }

    public function getBorderouNote($request)
    {
        $validator = Validator::make($request->all(), [
            'borderou_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json(['type' => 'warning', $validator->errors()->first()]);
        }

        $borderou = Borderou::where('id', $request->borderou_id)->where('deleted', 0)->first();
        if (!$borderou) {
            return response()->json(['type' => 'warning', 'message' => __('messages.borderou_not_found')]);
        }

        $students1 = [];
        $students2 = [];
        if(Student::where('group_id', $borderou->group_id)->count() > 0){
            $students1 = collect(Student::select('id', 'first_name', 'last_name')
                ->where('group_id', $borderou->group_id)
                ->where('deleted', 0)
                ->where('expelled', 0)
                ->orderBy('last_name')
                ->orderBy('first_name')
                ->get());
        }

        if(StudentAdditionalStudy::where('group_id', $borderou->group_id)->count() > 0) {
            $studentIds = StudentAdditionalStudy::select('student_id')->where('group_id', $borderou->group_id)->get()->toArray();
            $studentIdsArray = [];

            foreach($studentIds as $studentId) {
                $studentIdsArray[] = $studentId['student_id'];
            }

            $students2 = collect(Student::select('id', 'first_name', 'last_name')
                ->whereIn('id', $studentIdsArray)
                ->where('deleted', 0)
                ->where('expelled', 0)
                ->orderBy('last_name')
                ->orderBy('first_name')
                ->get());
        }

        $collectionOfStudents = new Collection;
        $collectionOfStudents->push($students1);
        $collectionOfStudents->push($students2);

        $borderouNotes = BorderouNote::where('borderou_id', $request->borderou_id)->get();

        $studentsHtml = '<div class="row">
                             <div class="col-md-12">
                                 <select data-placeholder="'. __('pages.chose_students') .'" multiple class="standardSelect" name="addedStudentsIds[]" id="addedStudentsIds">';
        $canAddDisabled = 'disabled';
        foreach ($collectionOfStudents as $students){
            foreach ($students as $student) {
                $checkIfExist = BorderouNote::where('borderou_id', $request->borderou_id)->where('student_id', $student->id)->first();
                if (!$checkIfExist) {
                    $canAddDisabled = '';
                    $studentsHtml .= '<option value="' . $student->id . '" >' . $student->last_name . ' ' . $student->first_name . '</option>';
                }
            }
        }

        $studentsHtml .= '</select></div></div>';

        $html = '
                 <div class="col-md-2" style="padding-top: 5px;"><b>'. __('pages.accepted') .'</b> <span class="badge badge-primary">0</span></div>
                 <div class="col-md-2" style="padding-top: 5px;"><b>'. __('pages.not_accepted') .'</b> <span class="badge badge-primary">-2</span></div>
                 <div class="col-md-3" style="padding-top: 5px;"><b>'. __('pages.negative') .'</b> <span class="badge badge-primary">-3</span></div>
                 <div class="col-md-2" style="padding-top: 5px;"><b>'. __('pages.absent') .'</b> <span class="badge badge-primary">-1</span></div>
                 <div class="col-md-3">
                     <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#addStudentBorderouNote"
                         title="'. __('pages.add') .'" '. $canAddDisabled .'>
                         <i class="fa fa-plus-square"></i> '. __('pages.add_students') .'
                     </button>
                 </div>
                 <div class="col-md-12">
                     <table class="table table-sm">
                        <colgroup>
                           <col span="1" style="width: 10%;">
                           <col span="1" style="width: 10%;">
                           <col span="1" style="width: 50%;">
                           <col span="1" style="width: 15%;">
                           <col span="1" style="width: 15%;">
                        </colgroup>
                     <thead>
                         <tr>
                             <th style="border-top: none;">#</th>
                             <th style="border-top: none;">'. __('pages.delete') .'</th>
                             <th style="border-top: none;">'. __('pages.full_name') .'</th>
                             <th style="border-top: none;">'. __('pages.semester_note') .'</th>
                             <th style="border-top: none;">'. __('pages.exam_note') .'</th>
                         </tr>
                     </thead>
                     <tbody>';

        $props = '';
        $disableButton = 0;
        $style = ' style="display:none" ';
        if($borderou->finished == 1){
            $props = 'disabled readonly';
            $disableButton = 1;
            $style = '';
        }

        foreach ($borderouNotes as $key => $borderouNote){
            $studentFullName = $borderouNote->student->last_name .' '. $borderouNote->student->first_name;
            $semesterNote = $borderouNote->semester_note;
            $examNote = $borderouNote->exam_note;
            $borderouNoteId = $borderouNote->id;

            $html .= '<tr>
                          <th>'. ($key + 1) .'</th>
                          <th>
                              <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#confirmDeleteBorderouNote"
                                  data-borderou-note-id="'. $borderouNote->id .'" title="'. __('pages.delete') .'">
                                  <i class="fa fa-trash"></i>
                              </button>
                          </th>
                          <th>'. $studentFullName .'</th>
                          <td><input type="number" step=".01" name="semester_note_'. $borderouNoteId .'" value="'. $semesterNote .'" class="input-sm form-control-sm form-control" '. $props .'></td>
                          <td><input type="number" step=".01" name="exam_note_'. $borderouNoteId .'" value="'. $examNote .'" class="input-sm form-control-sm form-control" '. $props .'></td>
                      </tr>';
        }

        $html .= '</tbody></table></div>';

        $html .= '<div class="col-md-6" '. $style .'>
                      <div style="font-weight: bold">'. __('pages.change_using_code') .'</div>
                      <label class="switch switch-text switch-primary switch-pill">
                          <input type="checkbox" class="switch-input" id="checkbox_code" onchange="showHideCodeBlock()"> 
                          <span data-on="'. __('pages.on') .'" data-off="'. __('pages.off') .'" class="switch-label"></span> 
                          <span class="switch-handle"></span>
                      </label>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group" id="code_block" style="display: none">
                          <label for="code" class="control-label">'. __('pages.code') .'</label>
                          <input type="text" id="code" name="code" placeholder="'. __('pages.code') .'" class="input-sm form-control-sm form-control">
                      </div>
                  </div>';

        return response()->json(['type' => 'success', 'html' => $html, 'disableButton' => $disableButton, 'studentsHtml' => $studentsHtml]);
    }

    public function convertNoteToText($note)
    {
        if (is_null($note) || $note == '') {
            return '';
        }
        if ((int)$note > 0) {
            $semNote = $note;
        } else {
            if ((int)$note == 0) {
                $semNote = __('pages.accepted');
            } elseif ((int)$note == -2) {
                $semNote = __('pages.not_accepted');
            } elseif ((int)$note == -3) {
                $semNote = __('pages.negative');
            } else {
                $semNote = __('pages.absent');
            }
        }

        return $semNote;
    }

    public function getEctsByFinalNote($finalNote)
    {

        if (9.01 <= $finalNote && $finalNote <= 10) {
            return 'A';
        }

        if (8.01 <= $finalNote && $finalNote <= 9.00) {
            return 'B';
        }

        if (7.01 <= $finalNote && $finalNote <= 8.00) {
            return 'C';
        }

        if (6.01 <= $finalNote && $finalNote <= 7.00) {
            return 'D';
        }

        if (5.00 <= $finalNote && $finalNote <= 6.00) {
            return 'E';
        }

        if (3.01 <= $finalNote && $finalNote <= 4.99) {
            return 'FX';
        }

        if (1.00 <= $finalNote && $finalNote <= 3.00) {
            return 'F';
        }

        return '';
    }

    public function getSemesterNote($borderouNote)
    {
        if (is_null($borderouNote->semester_note) || $borderouNote->semester_note == '') {
            return '';
        }
        if ($borderouNote->semester_note > 0) {
            $semNote = $borderouNote->semester_note;
        } else {
            if ($borderouNote->semester_note == 0) {
                $semNote = __('pages.accepted');
            } elseif ($borderouNote->semester_note == -2) {
                $semNote = __('pages.not_accepted');
            } elseif ($borderouNote->semester_note == -3) {
                $semNote = __('pages.negative');
            } else {
                $semNote = __('pages.absent');
            }
        }

        return $semNote;
    }

    public function getSemesterScore($borderouNote)
    {
        if (is_null($borderouNote->semester_note) || $borderouNote->semester_note == '') {
            return '';
        }

        if ($borderouNote->semester_note > 0) {
            $semScore = $borderouNote->semester_score;
        } else {
            if ($borderouNote->semester_note == 0) {
                $semScore = __('pages.accepted');
            } elseif ($borderouNote->semester_note == -2) {
                $semScore = __('pages.not_accepted');
            } elseif ($borderouNote->semester_note == -3) {
                $semScore = __('pages.negative');
            } else {
                $semScore = __('pages.absent');
            }
        }

        return $semScore;
    }

    public function getExamNote($borderouNote)
    {
        if (is_null($borderouNote->exam_note) || $borderouNote->exam_note == '') {
            return '';
        }

        if ($borderouNote->exam_note > 0) {
            $examNote = $borderouNote->exam_note;
        } else {
            if ($borderouNote->exam_note == 0) {
                $examNote = __('pages.accepted');
            } elseif ($borderouNote->exam_note == -2) {
                $examNote = __('pages.not_accepted');
            } elseif ($borderouNote->exam_note == -3) {
                $examNote = __('pages.negative');
            } else {
                $examNote = __('pages.absent');
            }
        }

        return $examNote;
    }

    public function getExamScore($borderouNote)
    {
        if (is_null($borderouNote->exam_note) || $borderouNote->exam_note == '') {
            return '';
        }

        if ($borderouNote->exam_note > 0) {
            $examScore = $borderouNote->exam_score;
        } else {
            if ($borderouNote->exam_note == 0) {
                $examScore = __('pages.accepted');
            } elseif ($borderouNote->exam_note == -2) {
                $examScore = __('pages.not_accepted');
            } elseif ($borderouNote->exam_note == -3) {
                $examScore = __('pages.negative');
            } else {
                $examScore = __('pages.absent');
            }
        }

        return $examScore;
    }

    public function getBorderouOwner($userId)
    {
        $user = User::where('id', $userId)->first();

        if ($user) {
            $role = $user->getRoleNames()[0];

            if ($role == 'superadmin') {
                return $user->name . ' (' . __('pages.superadmin') . ')';
            } elseif ($role == 'admin') {
                $model = Admin::where('user_id', $user->id)->first();
                if ($model) {
                    return $model->first_name . ' ' . $model->last_name . ' (' . __('pages.admin') . ')';
                }
                return '';
            } elseif ($role == 'rector') {
                $model = Rector::where('user_id', $user->id)->first();
                if ($model) {
                    return $model->first_name . ' ' . $model->last_name . ' (' . __('pages.rector') . ')';
                }
                return '';
            } elseif ($role == 'secretary') {
                $model = Secretary::where('user_id', $user->id)->first();
                if ($model) {
                    return $model->first_name . ' ' . $model->last_name . ' (' . __('pages.secretary') . ')';
                }
                return '';
            } elseif ($role == 'dean') {
                $model = Dean::where('user_id', $user->id)->first();
                if ($model) {
                    return $model->first_name . ' ' . $model->last_name . ' (' . __('pages.dean') . ')';
                }
                return '';
            } elseif ($role == 'chair') {
                $model = Chair::where('user_id', $user->id)->first();
                if ($model) {
                    return $model->first_name . ' ' . $model->last_name . ' (' . __('pages.chair') . ')';
                }
                return '';
            } elseif ($role == 'human-resource') {
                $model = HumanResource::where('user_id', $user->id)->first();
                if ($model) {
                    return $model->first_name . ' ' . $model->last_name . ' (' . __('pages.human_resource') . ')';
                }
                return '';
            } elseif ($role == 'teacher') {
                $model = Teacher::where('user_id', $user->id)->first();
                if ($model) {
                    return $model->first_name . ' ' . $model->last_name . ' (' . __('pages.teacher') . ')';
                }
                return '';
            } else {
                return '';
            }
        }

        return '';
    }
}