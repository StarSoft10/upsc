<?php

namespace App\Helpers;

use App\Entities\AllowedFacultiesByModel;
use App\Entities\BorderouType;
use App\Entities\Course;
use App\Entities\Cycle;
use App\Entities\Faculty;
use App\Entities\Group;
use App\Entities\Semester;
use App\Entities\Speciality;
use App\Entities\Teacher;
use App\Entities\YearOfStudy;
use Illuminate\Support\Facades\Log;

class StudyHelper
{
    /*
     * Faculty
     */
    public function getFacultiesByCycleForSelectSearch($request)
    {
        $cyclesId = !empty($request->cyclesId) ? json_decode($request->cyclesId) : [];

        if(isset($request->allowedFaculties)) {
            $faculties = Faculty::whereIn('cycle_id', $cyclesId)
                ->where('deleted', 0)
                ->whereIn('id', $request->allowedFaculties)
                ->orderBy('id', 'ASC')
                ->pluck('name', 'id');
        } else {
            $faculties = Faculty::whereIn('cycle_id', $cyclesId)
                ->where('deleted', 0)
                ->orderBy('id', 'ASC')
                ->pluck('name', 'id');
        }

        try {
            $html = view('render-select.faculties')->with('faculties', $faculties)->render();

            return response()->json(['type' => 'success', 'html' => $html]);
        } catch (\Throwable $e) {
            Log::info('StudyHelper getFacultiesByCycleForSelectSearch() ERROR');
            Log::info($e->getMessage());

            return response()->json(['type' => 'success', 'html' => '<option value="0">' . __('messages.not_found_any_faculties') . '</option>']);
        }
    }

    public function getFacultiesByCycleForSelectAddEdit($request)
    {
        $cycleId = !empty($request->cycleId) ? trim($request->cycleId) : 0;

        if(isset($request->allowedFaculties)) {
            $faculties = Faculty::where('cycle_id', $cycleId)
                ->where('deleted', 0)
                ->whereIn('id', $request->allowedFaculties)
                ->orderBy('id', 'ASC')
                ->pluck('name', 'id');
        } else {
            $faculties = Faculty::where('cycle_id', $cycleId)
                ->where('deleted', 0)
                ->orderBy('id', 'ASC')
                ->pluck('name', 'id');
        }


        try {
            $html = view('render-select.faculties')->with('faculties', $faculties)->render();

            return response()->json(['type' => 'success', 'html' => $html]);
        } catch (\Throwable $e) {
            Log::info('StudyHelper getFacultiesByCycleForSelectAddEdit() ERROR');
            Log::info($e->getMessage());

            return response()->json(['type' => 'success', 'html' => '<option value="0">' . __('messages.not_found_any_faculties') . '</option>']);
        }
    }

    public function getFacultiesByCycleForSelectImport($request)
    {
        $cycleId = !empty($request->cycleId) ? trim($request->cycleId) : 0;

        if(isset($request->allowedFaculties)) {
            $faculties = Faculty::where('cycle_id', $cycleId)
                ->where('deleted', 0)
                ->whereIn('id', $request->allowedFaculties)
                ->orderBy('id', 'ASC')
                ->pluck('name', 'id');
        } else {
            $faculties = Faculty::where('cycle_id', $cycleId)
                ->where('deleted', 0)
                ->orderBy('id', 'ASC')
                ->pluck('name', 'id');
        }


        try {
            $html = view('render-select.faculties')->with('faculties', $faculties)->render();

            return response()->json(['type' => 'success', 'html' => $html]);
        } catch (\Throwable $e) {
            Log::info('StudyHelper getFacultiesByCycleForSelectImport() ERROR');
            Log::info($e->getMessage());

            return response()->json(['type' => 'success', 'html' => '<option value="0">' . __('messages.not_found_any_faculties') . '</option>']);
        }
    }

    /*
     * Speciality
     */
    public function getSpecialitiesByFacultyForSelectSearch($request)
    {
        $facultiesId = !empty($request->facultiesId) ? json_decode($request->facultiesId) : [];

        $specialities = Speciality::whereIn('faculty_id', $facultiesId)
            ->where('deleted', 0)
            ->orderBy('id', 'ASC')
            ->pluck('name', 'id');

        try {
            $html = view('render-select.specialities')->with('specialities', $specialities)->render();

            return response()->json(['type' => 'success', 'html' => $html]);
        } catch (\Throwable $e) {
            Log::info('StudyHelper getSpecialitiesByFacultyForSelectSearch() ERROR');
            Log::info($e->getMessage());

            return response()->json(['type' => 'success', 'html' => '<option value="0">' . __('messages.not_found_any_specialities') . '</option>']);
        }
    }

    public function getSpecialitiesByFacultyForSelectAddEdit($request)
    {
        $facultyId = !empty($request->facultyId) ? trim($request->facultyId) : 0;

        $specialities = Speciality::where('faculty_id', $facultyId)
            ->where('deleted', 0)
            ->orderBy('id', 'ASC')
            ->pluck('name', 'id');

        try {
            $html = view('render-select.specialities')->with('specialities', $specialities)->render();

            return response()->json(['type' => 'success', 'html' => $html]);
        } catch (\Throwable $e) {
            Log::info('StudyHelper getSpecialitiesByFacultyForSelectAddEdit() ERROR');
            Log::info($e->getMessage());

            return response()->json(['type' => 'success', 'html' => '<option value="0">' . __('messages.not_found_any_specialities') . '</option>']);
        }
    }

    public function getSpecialitiesByFacultyForSelectImport($request)
    {
        $facultyId = !empty($request->facultyId) ? trim($request->facultyId) : 0;

        $specialities = Speciality::where('faculty_id', $facultyId)
            ->where('deleted', 0)
            ->orderBy('id', 'ASC')
            ->pluck('name', 'id');

        try {
            $html = view('render-select.specialities')->with('specialities', $specialities)->render();

            return response()->json(['type' => 'success', 'html' => $html]);
        } catch (\Throwable $e) {
            Log::info('StudyHelper getSpecialitiesByFacultyForSelectImport() ERROR');
            Log::info($e->getMessage());

            return response()->json(['type' => 'success', 'html' => '<option value="0">' . __('messages.not_found_any_specialities') . '</option>']);
        }
    }

    /*
    * Group
    */
    public function getGroupsBySpecialityForSelectSearch($request)
    {
        $specialitiesId = !empty($request->specialitiesId) ? json_decode($request->specialitiesId) : [];

        $groups = Group::whereIn('speciality_id', $specialitiesId)
            ->where('deleted', 0)
            ->orderBy('id', 'ASC')
            ->pluck('name', 'id');

        try {
            $html = view('render-select.groups')->with('groups', $groups)->render();

            return response()->json(['type' => 'success', 'html' => $html]);
        } catch (\Throwable $e) {
            Log::info('StudyHelper getGroupsBySpecialityForSelectSearch() ERROR');
            Log::info($e->getMessage());

            return response()->json(['type' => 'success', 'html' => '<option value="0">' . __('messages.not_found_any_groups') . '</option>']);
        }
    }

    public function getGroupsBySpecialityForSelectAddEdit($request)
    {
        $specialityId = !empty($request->specialityId) ? trim($request->specialityId) : 0;

        $groups = Group::where('speciality_id', $specialityId)
            ->where('deleted', 0)
            ->orderBy('id', 'ASC')
            ->pluck('name', 'id');

        try {
            $html = view('render-select.groups')->with('groups', $groups)->render();

            return response()->json(['type' => 'success', 'html' => $html]);
        } catch (\Throwable $e) {
            Log::info('StudyHelper getGroupsBySpecialityForSelectAddEdit() ERROR');
            Log::info($e->getMessage());

            return response()->json(['type' => 'success', 'html' => '<option value="0">' . __('messages.not_found_any_groups') . '</option>']);
        }
    }

    public function getGroupsBySpecialityForSelectImport($request)
    {
        $specialityId = !empty($request->specialityId) ? trim($request->specialityId) : 0;

        $groups = Group::where('speciality_id', $specialityId)
            ->where('deleted', 0)
            ->orderBy('id', 'ASC')
            ->pluck('name', 'id');

        try {
            $html = view('render-select.groups')->with('groups', $groups)->render();

            return response()->json(['type' => 'success', 'html' => $html]);
        } catch (\Throwable $e) {
            Log::info('StudyHelper getGroupsBySpecialityForSelectImport() ERROR');
            Log::info($e->getMessage());

            return response()->json(['type' => 'success', 'html' => '<option value="0">' . __('messages.not_found_any_groups') . '</option>']);
        }
    }

    public function getCycleNameById($id)
    {
        $cycle = Cycle::where('id', $id)->first();

        if ($cycle) {
            return $cycle->name;
        }
        return '';
    }

    public function getFacultyNameById($id)
    {
        $faculty = Faculty::where('id', $id)->first();

        if ($faculty) {
            return $faculty->name;
        }
        return '';
    }

    public function getSpecialityNameById($id)
    {
        $speciality = Speciality::where('id', $id)->first();

        if ($speciality) {
            return $speciality->name;
        }
        return '';
    }

    public function getGroupNameById($id)
    {
        $group = Group::where('id', $id)->first();

        if ($group) {
            return $group->name;
        }
        return '';
    }

    public function getYearOfStudyNameById($id)
    {
        $yearOfStudy = YearOfStudy::where('id', $id)->first();

        if ($yearOfStudy) {
            return $yearOfStudy->name;
        }
        return '';
    }

    public function getBorderouTypeById($id)
    {
        $borderouType = BorderouType::where('id', $id)->first();

        if ($borderouType) {
            return $borderouType->name;
        }
        return '';
    }

    public function getSemesterNameById($id)
    {
        $semester = Semester::where('id', $id)->first();

        if ($semester) {
            return $semester->name;
        }
        return '';
    }

    public function getCourseNameById($id)
    {
        $course = Course::where('id', $id)->first();

        if ($course) {
            return $course->name;
        }
        return '';
    }

    public function getTeacherNameById($id)
    {
        $teacher = Teacher::where('id', $id)->first();

        if ($teacher) {
            return $teacher->first_name . ' ' . $teacher->last_name . ' (' . $teacher->teacherRank->name . ')';
        }
        return '';
    }

    public function getAllowedFacultiesByModel($modelId)
    {
        $allowedFaculties = AllowedFacultiesByModel::where('model_id', $modelId)->first();

        if($allowedFaculties) {
            return json_decode($allowedFaculties->faculties);
        }

        return [];
    }
}