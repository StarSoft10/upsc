<?php

namespace App\Helpers;

use App\Entities\Database;
use App\Entities\Log;
use Spatie\Permission\Models\Permission;

class AppHelper
{
    public function getAllowedPermissionsByModel($model)
    {
        if ($model == 'admin') {
            return Permission::where([
                ['name', 'NOT LIKE', 'delete_%'],
            ])->get();
        }

        if ($model == 'rector') {
            return Permission::where([
                ['name', 'LIKE', 'view_%']
            ])->get();
        }

        if ($model == 'secretary') {
            return Permission::where('name', 'LIKE', '%\_faculty')
                ->orWhere('name', 'LIKE', '%\_speciality')
                ->orWhere('name', 'LIKE', '%\_group')
                ->orWhere('name', 'LIKE', '%\_course')
                ->get();
        }

        if ($model == 'dean') {
            return Permission::where('name', 'LIKE', '%\_student')
                ->orWhere('name', '=', 'view_borderou')
                ->orWhere('name', '=', 'view_borderou_note')
                ->get();
        }

        if ($model == 'chair') {
            return Permission::where('name', 'LIKE', '%\_student')
                ->orwhere('name', 'LIKE', '%\_borderou')
                ->orWhere('name', 'LIKE', '%\_borderou_note')
                ->get();
        }

        if ($model == 'human-resource') {
            return Permission::where('name', 'LIKE', '%\_student')
                ->orWhere('name', 'LIKE', '%\_teacher')
                ->get();
        }

        if ($model == 'teacher') {
            return Permission::where('name', '=', 'create_borderou')
                ->orWhere('name', '=', 'view_borderou')
                ->orWhere('name', '=', 'edit_borderou')
                ->orWhere('name', '=', 'create_borderou_note')
                ->orWhere('name', '=', 'view_borderou_note')
                ->orWhere('name', '=', 'edit_borderou_note')
                ->get();
        }

        if ($model == 'student') {
            return Permission::where('name', '=', 'view_borderou_note')->get();
        }
    }

    public function addLog($entityRole, $entityId, $actionTable, $logType, $action)
    {
        Log::create([
            'entity_role' => $entityRole,
            'entity_id' => $entityId,
            'action_table' => $actionTable,
            'log_type' => $logType,
            'action' => $action
        ]);
    }

    public function getLogTypes()
    {
        return ['1' => __('pages.create'), '2' => __('pages.edit'), '3' => __('pages.delete'),
            '4' => __('pages.block'), '5' => __('pages.unblock'), '6' => __('pages.confirm')
        ];
    }

    public function getLogTypeById($id)
    {
        $types = ['1' => __('pages.create'), '2' => __('pages.edit'), '3' => __('pages.delete'),
            '4' => __('pages.block'), '5' => __('pages.unblock'), '6' => __('pages.confirm')
        ];

        return $types[$id];
    }

    public static function getStringBetween($str, $from, $to)
    {
        $string = mb_substr($str, mb_strpos($str, $from) + mb_strlen($from));
        if (mb_strstr($string, $to, TRUE) != FALSE) {
            $string = mb_strstr($string, $to, TRUE);
        }

        return $string;
    }

    public static function startsWith($haystack, $needle)
    {
        $length = mb_strlen($needle);
        return (mb_substr($haystack, 0, $length) == $needle);
    }

    public static function endsWith($haystack, $needle)
    {
        $length = mb_strlen($needle);
        if ($length == 0) {
            return true;
        }
        return (mb_substr($haystack, -$length) == $needle);
    }

    public function encrypt($data, $key)
    {
        $encryption_key = base64_decode($key);
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
        return base64_encode($encrypted . '::' . $iv);
    }

    public function decrypt($data, $key)
    {
        $encryption_key = base64_decode($key);
        list($encrypted_data, $iv) = array_pad(explode('::', base64_decode($data), 2), 2, null);
        return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
    }

    public function checkIfSearchMode($request)
    {
        $name = !empty($request->name) ? trim($request->name) : '';
        $idnp = !empty($request->idnp) ? trim($request->idnp) : '';
        $phone = !empty($request->phone) ? trim($request->phone) : '';
        $email = !empty($request->email) ? trim($request->email) : '';
        $city = !empty($request->city) ? trim($request->city) : '';

        $cycleIds = !empty($request->cycleIds) ? $this->removeValueFromArray($request->cycleIds, 0) : [];
        $facultyIds = !empty($request->facultyIds) ? $this->removeValueFromArray($request->facultyIds, 0) : [];
        $specialityIds = !empty($request->specialityIds) ? $this->removeValueFromArray($request->specialityIds, 0) : [];
        $groupIds = !empty($request->groupIds) ? $this->removeValueFromArray($request->groupIds, 0) : [];

        $yearOfStudyIds = !empty($request->yearOfStudyIds) ? $this->removeValueFromArray($request->yearOfStudyIds, 0) : [];
        $budget = isset($request->budget) ? trim($request->budget) : '';
        $expelled = isset($request->expelled) ? trim($request->expelled) : '';
        $study = isset($request->study) ? trim($request->study) : '';

        if ($name !== '' || $idnp !== '' || $phone !== '' || $email !== '' || $city !== '' || count($cycleIds) > 0 ||
            count($facultyIds) > 0 || count($specialityIds) > 0 || count($groupIds) > 0 || count($yearOfStudyIds) > 0 ||
            $budget !== '' || $expelled !== '' || $study !== '') {
            return true;
        }

        return false;
    }

    public function checkIfExistAdmission()
    {
        return Database::where('deleted', 0)->count();
    }

    public function removeValueFromArray($array, $value)
    {
        return array_values(array_diff($array, [$value]));
    }
}