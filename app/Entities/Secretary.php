<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Secretary extends Model
{
    protected $table = 'secretaries';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\Entities\User', 'user_id', 'id');
    }
}
