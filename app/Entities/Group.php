<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';
    protected $guarded = [];

    public function cycle()
    {
        return $this->belongsTo('App\Entities\Cycle', 'cycle_id', 'id');
    }

    public function faculty()
    {
        return $this->belongsTo('App\Entities\Faculty', 'faculty_id', 'id');
    }

    public function speciality()
    {
        return $this->belongsTo('App\Entities\Speciality', 'speciality_id', 'id');
    }

    public function students()
    {
        return $this->hasMany('App\Entities\Student', 'group_id', 'id');
    }

    public function borderous()
    {
        return $this->hasMany('App\Entities\Borderou', 'group_id', 'id');
    }
}
