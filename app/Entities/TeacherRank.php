<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class TeacherRank extends Model
{
    protected $table = 'teacher_ranks';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\Entities\User', 'user_id', 'id');
    }

    public function teachers()
    {
        return $this->hasMany('App\Entities\Teacher', 'teacher_rank_id', 'id');
    }
}
