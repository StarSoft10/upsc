<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class BorderouNote extends Model
{
    protected $table = 'borderou_notes';
    protected $guarded = [];

    public function borderou()
    {
        return $this->belongsTo('App\Entities\Borderou', 'borderou_id', 'id');
    }

    public function student()
    {
        return $this->belongsTo('App\Entities\Student', 'student_id', 'id');
    }
}
