<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Chair extends Model
{
    protected $table = 'chairs';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\Entities\User', 'user_id', 'id');
    }
}
