<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Cycle extends Model
{
    protected $table = 'cycles';
    protected $guarded = [];

    public function faculties()
    {
        return $this->hasMany('App\Entities\Faculty', 'cycle_id', 'id');
    }

    public function specialities()
    {
        return $this->hasMany('App\Entities\Speciality', 'cycle_id', 'id');
    }

    public function groups()
    {
        return $this->hasMany('App\Entities\Group', 'cycle_id', 'id');
    }

    public function students()
    {
        return $this->hasMany('App\Entities\Student', 'cycle_id', 'id');
    }

    public function borderous()
    {
        return $this->hasMany('App\Entities\Borderou', 'cycle_id', 'id');
    }
}
