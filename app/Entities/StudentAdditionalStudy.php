<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class StudentAdditionalStudy extends Model
{
    protected $table = 'students_additional_study';
    protected $guarded = [];
}
