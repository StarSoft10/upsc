<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\Entities\User', 'user_id', 'id');
    }

    public function cycle()
    {
        return $this->belongsTo('App\Entities\Cycle', 'cycle_id', 'id');
    }

    public function faculty()
    {
        return $this->belongsTo('App\Entities\Faculty', 'faculty_id', 'id');
    }

    public function speciality()
    {
        return $this->belongsTo('App\Entities\Speciality', 'speciality_id', 'id');
    }

    public function group()
    {
        return $this->belongsTo('App\Entities\Group', 'group_id', 'id');
    }

    public function yearOfStudy()
    {
        return $this->belongsTo('App\Entities\YearOfStudy', 'year_of_study_id', 'id');
    }

    public function studentChangesInStudies()
    {
        return $this->hasMany('App\Entities\StudentChangesInStudy', 'student_id', 'id');
    }

    public function borderouNotes()
    {
        return $this->hasMany('App\Entities\BorderouNote', 'student_id', 'id');
    }

    public function getAllStudentYearOfStudies()
    {
        $additionalYearOfStudies = StudentAdditionalStudy::select('year_of_study_id')->where('student_id', $this->id)->distinct()->pluck('year_of_study_id')->toArray();
        $additionalYearOfStudies[] = $this->year_of_study_id;

        $yearOfStudies = YearOfStudy::select('name')->whereIn('id', $additionalYearOfStudies)->get()->pluck('name')->toArray();

        $html = '<ul style="list-style: none">';
        foreach ($yearOfStudies as $yearOfStudy){
            $html .= '<li>'. $yearOfStudy .'</li>';
        }
        $html .= '</ul>';

        return $html;

//        return implode(', ', $yearOfStudies);
    }

    public function getAllStudentCycles()
    {
        $additionalCycles = StudentAdditionalStudy::select('cycle_id')->where('student_id', $this->id)->distinct()->pluck('cycle_id')->toArray();
        $additionalCycles[] = $this->cycle_id;

        $cycles = Cycle::select('name')->whereIn('id', $additionalCycles)->get()->pluck('name')->toArray();

        $html = '<ul style="list-style: none">';
        foreach ($cycles as $cycle){
            $html .= '<li>'. $cycle .'</li>';
        }
        $html .= '</ul>';

        return $html;
    }

    public function getAllStudentFaculties()
    {
        $additionalFaculties = StudentAdditionalStudy::select('faculty_id')->where('student_id', $this->id)->distinct()->pluck('faculty_id')->toArray();
        $additionalFaculties[] = $this->faculty_id;

        $faculties = Faculty::select('name')->whereIn('id', $additionalFaculties)->get()->pluck('name')->toArray();

        $html = '<ul style="list-style: none">';
        foreach ($faculties as $faculty){
            $html .= '<li>'. $faculty .'</li>';
        }
        $html .= '</ul>';

        return $html;
    }

    public function getAllStudentSpecialities()
    {
        $additionalSpecialities = StudentAdditionalStudy::select('speciality_id')->where('student_id', $this->id)->distinct()->pluck('speciality_id')->toArray();
        $additionalSpecialities[] = $this->speciality_id;

        $specialities = Speciality::select('name')->whereIn('id', $additionalSpecialities)->get()->pluck('name')->toArray();

        $html = '<ul style="list-style: none">';
        foreach ($specialities as $speciality){
            $html .= '<li>'. $speciality .'</li>';
        }
        $html .= '</ul>';

        return $html;
    }

    public function getAllStudentGroups()
    {
        $additionalGroups = StudentAdditionalStudy::select('group_id')->where('student_id', $this->id)->distinct()->pluck('group_id')->toArray();
        $additionalGroups[] = $this->group_id;

        $groups = Group::select('name')->whereIn('id', $additionalGroups)->get()->pluck('name')->toArray();

        $html = '<ul style="list-style: none">';
        foreach ($groups as $group){
            $html .= '<li>'. $group .'</li>';
        }
        $html .= '</ul>';

        return $html;
    }
}
