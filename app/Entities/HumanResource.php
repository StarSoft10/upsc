<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class HumanResource extends Model
{
    protected $table = 'human_resources';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\Entities\User', 'user_id', 'id');
    }
}
