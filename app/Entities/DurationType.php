<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class DurationType extends Model
{
    protected $table = 'duration_types';
    protected $guarded = [];

    public function durations()
    {
        return $this->hasMany('App\Entities\Duration', 'duration_type_id', 'id');
    }
}
