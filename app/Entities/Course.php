<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';
    protected $guarded = [];

    public function borderous()
    {
        return $this->hasMany('App\Entities\Borderou', 'course_id', 'id');
    }
}
