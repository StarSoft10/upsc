<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class CourseType extends Model
{
    protected $table = 'course_types';
    protected $guarded = [];
}
