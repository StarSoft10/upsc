<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Speciality extends Model
{
    protected $table = 'specialities';
    protected $guarded = [];

    public function cycle()
    {
        return $this->belongsTo('App\Entities\Cycle', 'cycle_id', 'id');
    }

    public function faculty()
    {
        return $this->belongsTo('App\Entities\Faculty', 'faculty_id', 'id');
    }

    public function groups()
    {
        return $this->hasMany('App\Entities\Group', 'speciality_id', 'id');
    }

    public function students()
    {
        return $this->hasMany('App\Entities\Student', 'speciality_id', 'id');
    }

    public function borderous()
    {
        return $this->hasMany('App\Entities\Borderou', 'speciality_id', 'id');
    }
}
