<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Duration extends Model
{
    protected $table = 'durations';
    protected $guarded = [];

    public function durationType()
    {
        return $this->belongsTo('App\Entities\DurationType', 'duration_type_id', 'id');
    }
}
