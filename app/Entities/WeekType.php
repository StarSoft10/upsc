<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class WeekType extends Model
{
    protected $table = 'week_types';
    protected $guarded = [];

    public function weeks()
    {
        return $this->hasMany('App\Entities\Week', 'week_type_id', 'id');
    }
}
