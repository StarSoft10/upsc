<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Rector extends Model
{
    protected $table = 'rectors';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\Entities\User', 'user_id', 'id');
    }
}
