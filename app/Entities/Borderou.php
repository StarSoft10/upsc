<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Borderou extends Model
{
    protected $table = 'borderous';
    protected $guarded = [];

    public function borderouNotes()
    {
        return $this->hasMany('App\Entities\BorderouNote', 'borderou_id', 'id');
    }

    public function borderouType()
    {
        return $this->belongsTo('App\Entities\BorderouType', 'borderou_type_id', 'id');
    }

    public function cycle()
    {
        return $this->belongsTo('App\Entities\Cycle', 'cycle_id', 'id');
    }

    public function faculty()
    {
        return $this->belongsTo('App\Entities\Faculty', 'faculty_id', 'id');
    }

    public function speciality()
    {
        return $this->belongsTo('App\Entities\Speciality', 'speciality_id', 'id');
    }

    public function group()
    {
        return $this->belongsTo('App\Entities\Group', 'group_id', 'id');
    }

    public function yearOfStudy()
    {
        return $this->belongsTo('App\Entities\YearOfStudy', 'year_of_study_id', 'id');
    }

    public function semester()
    {
        return $this->belongsTo('App\Entities\Semester', 'semester_id', 'id');
    }

    public function course()
    {
        return $this->belongsTo('App\Entities\Course', 'course_id', 'id');
    }

    public function teacher()
    {
        return $this->belongsTo('App\Entities\Teacher', 'teacher_id', 'id');
    }
}
