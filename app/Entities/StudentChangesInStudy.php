<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class StudentChangesInStudy extends Model
{
    protected $table = 'student_changes_in_studies';
    protected $guarded = [];

    public function student()
    {
        return $this->belongsTo('App\Entities\Student', 'student_id', 'id');
    }
}
