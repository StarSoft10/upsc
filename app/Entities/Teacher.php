<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $table = 'teachers';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\Entities\User', 'user_id', 'id');
    }

    public function teacherRank()
    {
        return $this->belongsTo('App\Entities\TeacherRank', 'teacher_rank_id', 'id');
    }

    public function borderous()
    {
        return $this->hasMany('App\Entities\Borderou', 'teacher_id', 'id');
    }
}
