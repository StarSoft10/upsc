<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    protected $table = 'faculties';
    protected $guarded = [];

    public function cycle()
    {
        return $this->belongsTo('App\Entities\Cycle', 'cycle_id', 'id');
    }

    public function specialities()
    {
        return $this->hasMany('App\Entities\Speciality', 'speciality_id', 'id');
    }

    public function groups()
    {
        return $this->hasMany('App\Entities\Group', 'faculty_id', 'id');
    }

    public function students()
    {
        return $this->hasMany('App\Entities\Student', 'faculty_id', 'id');
    }

    public function borderous()
    {
        return $this->hasMany('App\Entities\Borderou', 'faculty_id', 'id');
    }
}
