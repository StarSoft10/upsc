<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class BorderouNoteCode extends Model
{
    protected $table = 'borderou_note_codes';
    protected $guarded = [];
}
