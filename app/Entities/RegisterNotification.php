<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class RegisterNotification extends Model
{
    protected $table = 'register_notifications';
    protected $guarded = [];
}
