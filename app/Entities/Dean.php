<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Dean extends Model
{
    protected $table = 'deans';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\Entities\User', 'user_id', 'id');
    }
}
