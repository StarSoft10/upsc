<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function admin()
    {
        return $this->hasOne('App\Entities\Admin');
    }

    public function rector()
    {
        return $this->hasOne('App\Entities\Rector');
    }

    public function secretary()
    {
        return $this->hasOne('App\Entities\Secretary');
    }

    public function dean()
    {
        return $this->hasOne('App\Entities\Dean');
    }

    public function chair()
    {
        return $this->hasOne('App\Entities\Chair');
    }

    public function humanResource()
    {
        return $this->hasOne('App\Entities\HumanResource');
    }

    public function teacher()
    {
        return $this->hasOne('App\Entities\Teacher');
    }

    public function student()
    {
        return $this->hasOne('App\Entities\Student');
    }
}
