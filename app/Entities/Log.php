<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = 'logs';
    protected $guarded = [];

    public function getUserByRoleAndId($role, $id)
    {
        if($role == 'superadmin'){
            return User::where('id', 1)->first()->name;
        }

        if($role == 'admin'){
            $model = Admin::where('id', $id)->first();
            if($model){
                return $model->first_name . ' ' . $model->last_name;
            }
        }

        if($role == 'rector'){
            $model = Rector::where('id', $id)->first();
            if($model){
                return $model->first_name . ' ' . $model->last_name;
            }
        }

        if($role == 'secretary'){
            $model = Secretary::where('id', $id)->first();
            if($model){
                return $model->first_name . ' ' . $model->last_name;
            }
        }

        if($role == 'dean'){
            $model = Dean::where('id', $id)->first();
            if($model){
                return $model->first_name . ' ' . $model->last_name;
            }
        }

        if($role == 'chair'){
            $model = Chair::where('id', $id)->first();
            if($model){
                return $model->first_name . ' ' . $model->last_name;
            }
        }

        if($role == 'human-resource'){
            $model = HumanResource::where('id', $id)->first();
            if($model){
                return $model->first_name . ' ' . $model->last_name;
            }
        }

        if($role == 'teacher'){
            $model = Teacher::where('id', $id)->first();
            if($model){
                return $model->first_name . ' ' . $model->last_name;
            }
        }

        if($role == 'student'){
            $model = Student::where('id', $id)->first();
            if($model){
                return $model->first_name . ' ' . $model->last_name;
            }
        }

        return __('pages.not_found');
    }
}
