<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class AllowedFacultiesByModel extends Model
{
    protected $table = 'allowed_faculties_by_model';
    protected $guarded = [];
}
