<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Database extends Model
{
    protected $table = 'databases';
    protected $guarded = [];
}
