<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class YearOfStudy extends Model
{
    protected $table = 'year_of_studies';
    protected $guarded = [];

    public function students()
    {
        return $this->hasMany('App\Entities\Student', 'year_of_study_id', 'id');
    }

    public function borderous()
    {
        return $this->hasMany('App\Entities\Borderou', 'year_of_study_id', 'id');
    }
}
