<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    protected $table = 'semesters';
    protected $guarded = [];

    public function weeks()
    {
        return $this->hasMany('App\Entities\Week', 'semester_id', 'id');
    }

    public function borderous()
    {
        return $this->hasMany('App\Entities\Borderou', 'semester_id', 'id');
    }
}
