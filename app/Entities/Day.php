<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    protected $table = 'days';
    protected $guarded = [];
}
