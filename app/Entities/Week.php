<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Week extends Model
{
    protected $table = 'weeks';
    protected $guarded = [];

    public function weekType()
    {
        return $this->belongsTo('App\Entities\WeekType', 'week_type_id', 'id');
    }

    public function semester()
    {
        return $this->belongsTo('App\Entities\Semester', 'semester_id', 'id');
    }
}
