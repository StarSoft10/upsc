<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class BorderouType extends Model
{
    protected $table = 'borderou_types';
    protected $guarded = [];

    public function borderous()
    {
        return $this->hasMany('App\Entities\Borderou', 'borderou_type_id', 'id');
    }
}
