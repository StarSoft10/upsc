<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UnconfirmedUser extends Model
{
    protected $table = 'unconfirmed_users';
    protected $guarded = [];
}
