<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->data;

        Log::info('SendMail START');
        Log::info('From: ' . $data['from']);
        Log::info('To: ' . $data['to']);
        Log::info('Subject: ' . $data['subject']);


        $mail = $this
            ->from($data['from'])
            ->to($this->data['to'])
            ->subject($this->data['subject'])
            ->view('mail.mail', compact('data'));

        return $mail;


    }
}
