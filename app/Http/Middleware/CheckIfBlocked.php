<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CheckIfBlocked
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = Auth::user()->getRoleNames()[0];

        if ($role == 'admin') {
            if($this->checkIfUserIsBlockedOrDeleted(Auth::user(), 'admin')){
                return redirect()->route('signInGet')->with('error', __('messages.account_was_blocked_or_deleted'));
            }
        }

        if ($role == 'rector') {
            if($this->checkIfUserIsBlockedOrDeleted(Auth::user(), 'rector')){
                return redirect()->route('signInGet')->with('error', __('messages.account_was_blocked_or_deleted'));
            }
        }

        if ($role == 'secretary') {
            if($this->checkIfUserIsBlockedOrDeleted(Auth::user(), 'secretary')){
                return redirect()->route('signInGet')->with('error', __('messages.account_was_blocked_or_deleted'));
            }
        }

        if ($role == 'dean') {
            if($this->checkIfUserIsBlockedOrDeleted(Auth::user(), 'dean')){
                return redirect()->route('signInGet')->with('error', __('messages.account_was_blocked_or_deleted'));
            }
        }

        if ($role == 'chair') {
            if($this->checkIfUserIsBlockedOrDeleted(Auth::user(), 'chair')){
                return redirect()->route('signInGet')->with('error', __('messages.account_was_blocked_or_deleted'));
            }
        }

        if ($role == 'human-resource') {
            if($this->checkIfUserIsBlockedOrDeleted(Auth::user(), 'humanResource')){
                return redirect()->route('signInGet')->with('error', __('messages.account_was_blocked_or_deleted'));
            }
        }

        if ($role == 'teacher') {
            if($this->checkIfUserIsBlockedOrDeleted(Auth::user(), 'teacher')){
                return redirect()->route('signInGet')->with('error', __('messages.account_was_blocked_or_deleted'));
            }
        }

        if ($role == 'student') {
            if($this->checkIfUserIsBlockedOrDeleted(Auth::user(), 'student')){
                return redirect()->route('signInGet')->with('error', __('messages.account_was_blocked_or_deleted'));
            }
        }

        return $next($request);
    }

    public function checkIfUserIsBlockedOrDeleted($user, $model)
    {
        if ($user->$model->blocked == 1) {
            Auth::logout();
            Log::info('User with id: '. $user->id .' was blocked');

            return true;
        }

        if ($user->$model->deleted == 1) {
            Auth::logout();
            Log::info('User with id: '. $user->id .' was deleted');

            return true;
        }
        return false;
    }
}
