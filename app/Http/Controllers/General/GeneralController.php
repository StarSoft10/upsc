<?php

namespace App\Http\Controllers\General;

use App\Entities\Cycle;
use App\Entities\Database;
use App\Entities\Faculty;
use App\Entities\Group;
use App\Entities\Speciality;
use App\Entities\Student;
use App\Entities\StudentChangesInStudy;
use App\Entities\User;
use App\Entities\YearOfStudy;
use App\Helpers\AppHelper;
use App\Helpers\StudentHelper;
use App\Helpers\StudyHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Routing\Controller;

class GeneralController extends Controller
{
    public $helper;

    public $studyHelper;

    public $studentHelper;

    public function __construct()
    {
        $this->helper = new AppHelper();
        $this->studyHelper = new StudyHelper();
        $this->studentHelper = new StudentHelper();
    }

    public function getFacultiesByCycleForSelectImport(Request $request)
    {
        return $this->studyHelper->getFacultiesByCycleForSelectImport($request);
    }

    public function getSpecialitiesByFacultyForSelectImport(Request $request)
    {
        return $this->studyHelper->getSpecialitiesByFacultyForSelectImport($request);
    }

    public function getGroupsBySpecialityForSelectImport(Request $request)
    {
        return $this->studyHelper->getGroupsBySpecialityForSelectImport($request);
    }

    public function getStudentsForImport(Request $request)
    {
        $databaseId = !empty($request->databaseId) ? trim($request->databaseId) : '';
        $cycleId = !empty($request->cycleId) ? trim($request->cycleId) : '';
        $facultyId = !empty($request->facultyId) ? trim($request->facultyId) : '';
        $specialityId = !empty($request->specialityId) ? trim($request->specialityId) : '';
        $groupId = !empty($request->groupId) ? trim($request->groupId) : '';
        $yearOfStudyId = !empty($request->yearOfStudyId) ? trim($request->yearOfStudyId) : '';

        //For Search
        $cycleIdSearch = !empty($request->cycleIdSearch) ? trim($request->cycleIdSearch) : '';
        $facultyIdSearch = !empty($request->facultyIdSearch) ? trim($request->facultyIdSearch) : '';
        $specialityIdSearch = !empty($request->specialityIdSearch) ? trim($request->specialityIdSearch) : '';
        $fullName = !empty($request->fullName) ? trim($request->fullName) : '';

        if($databaseId == '' || $cycleId == '' || $facultyId == '' || $specialityId == '' || $groupId == '' || $yearOfStudyId == ''){
            return response()->json(['type' => 'warning', 'message' => __('messages.all_fields_is_required')]);
        }

        $database = Database::where('id', $databaseId)->where('deleted', 0)->first();
        if(!$database){
            return response()->json(['type' => 'warning', 'message' => __('messages.not_found_admission_database')]);
        }

        $cycle = Cycle::where('id', $cycleId)->where('deleted', 0)->first();
        if(!$cycle){
            return response()->json(['type' => 'warning', 'message' => __('messages.not_found_cycle')]);
        }

        $faculty = Faculty::where('id', $facultyId)->where('cycle_id', $cycleId)->where('deleted', 0)->first();
        if(!$faculty){
            return response()->json(['type' => 'warning', 'message' => __('messages.not_found_faculty')]);
        }

        $speciality = Speciality::where('id', $specialityId)->where('cycle_id', $cycleId)->where('faculty_id', $facultyId)->where('deleted', 0)->first();
        if(!$speciality){
            return response()->json(['type' => 'warning', 'message' => __('messages.not_found_speciality')]);
        }

        $group = Group::where('id', $groupId)->where('cycle_id', $cycleId)->where('faculty_id', $facultyId)->where('speciality_id', $specialityId)->where('deleted', 0)->first();
        if(!$group){
            return response()->json(['type' => 'warning', 'message' => __('messages.not_found_group')]);
        }

        $yearOfStudy = YearOfStudy::where('id', $yearOfStudyId)->where('deleted', 0)->first();
        if(!$yearOfStudy){
            return response()->json(['type' => 'warning', 'message' => __('messages.not_found_year_of_study')]);
        }

        if(request()->ip() == '127.0.0.1') {
            try {
                DB::connection('mysql_student_local')->getPdo();
                $studentConnection = DB::connection('mysql_student_local');
            } catch (\Exception $e) {
                return response()->json(['type' => 'warning', 'message' => __('messages.please_check_database_configuration')]);
            }
        } else {
            try {
                DB::connection($database->connection_name)->getPdo();
                $studentConnection = DB::connection($database->connection_name);
            } catch (\Exception $e) {
                return response()->json(['type' => 'warning', 'message' => __('messages.please_check_database_configuration')]);
            }
        }

        $faculties = $studentConnection
            ->table('Facultate')->pluck('numeFacultate', 'facultateID');

        if($cycleIdSearch == 1){
            $query = $studentConnection->table('InmatriculareCiclu1 AS im');
//            $query->selectRaw('ab.*, fa.numeFacultate, sp.numeSpecialitateCiclu1 AS numeSpecialitate,
            $query->selectRaw('ab.*, fa.numeFacultate, CONCAT(sp.numeSpecialitateCiclu1, " (", sp.formaDeInstruire, ", ", sp.limbaDeInstruire, ")") AS numeSpecialitate,
                CONCAT(ab.nume, " ", ab.prenume, " ", ab.patronimic) AS student, 
                im.inmatriculatBuget, sp.formaDeInstruire');
            $query->leftJoin('AbiturientCiclu1 AS ab', 'ab.abiturientID', '=', 'im.abiturientID');
            $query->leftJoin('SpecialitateCiclu1 AS sp', 'sp.specialitateCiclu1ID', '=', 'im.specialitateCiclu1ID');
            $query->leftJoin('Facultate AS fa', 'fa.facultateID', '=', 'sp.facultateID');
            $query->whereRaw('(im.inmatriculatBuget = TRUE OR im.inmatriculatContract = TRUE)');

            if($facultyIdSearch !== '' && $facultyIdSearch !== null){
                $query->whereRaw('im.specialitateCiclu1ID IN (SELECT specialitateCiclu1ID FROM SpecialitateCiclu1 WHERE facultateID = '. $facultyIdSearch .')');
            }

            if($specialityIdSearch !== '' && $specialityIdSearch !== null){
                $query->whereRaw('im.specialitateCiclu1ID ='. $specialityIdSearch);
            }

            if($fullName !== '' && $fullName !== null){
                $query->whereRaw( '(ab.nume LIKE "%'. $fullName .'%" OR ab.prenume LIKE "%'. $fullName .'%")');
            }

            $query->orderBy('sp.facultateID', 'ASC');
            $query->orderBy('im.specialitateCiclu1ID', 'ASC');
            $query->orderBy('ab.nume', 'ASC');

            $abiturients = $query->get();

            if($facultyIdSearch !== '' && $facultyIdSearch !== null){
                $specialities = $studentConnection
                    ->table('SpecialitateCiclu1')
                    ->selectRaw('specialitateCiclu1ID, CONCAT(numeSpecialitateCiclu1, " (", formaDeInstruire, ", ", limbaDeInstruire, ")") AS numeSpecialitateCiclu1')
                    ->where('facultateID', $facultyIdSearch)
                    ->pluck('numeSpecialitateCiclu1', 'specialitateCiclu1ID');
            } else {
                $specialities = $studentConnection
                    ->table('SpecialitateCiclu1')
                    ->selectRaw('specialitateCiclu1ID, CONCAT(numeSpecialitateCiclu1, " (", formaDeInstruire, ", ", limbaDeInstruire, ")") AS numeSpecialitateCiclu1')
                    ->pluck('numeSpecialitateCiclu1', 'specialitateCiclu1ID');
            }
        } elseif($cycleIdSearch == 2) {
            $query = $studentConnection->table('AbiturientInmatriculareCiclu2 AS abim');
//            $query->selectRaw('abim.*, fa.numeFacultate, sp.numeSpecialitateCiclu2 AS numeSpecialitate,
            $query->selectRaw('abim.*, fa.numeFacultate, CONCAT(sp.numeSpecialitateCiclu2, " (", sp.limbaDeInstruire, ")") AS numeSpecialitate,
                CONCAT(abim.nume, " ", abim.prenume, " ", abim.patronimic) AS student, 
                abim.inmatriculatBuget, "zi" AS formaDeInstruire, CAST(abim.vizaDeResedinta AS CHAR(255) CHARACTER SET utf8) AS domiciliuAdresa');
            $query->leftJoin('SpecialitateCiclu2 AS sp', 'sp.specialitateCiclu2ID', '=', 'abim.specialitateCiclu2ID');
            $query->leftJoin('Facultate AS fa', 'fa.facultateID', '=', 'sp.facultateID');
            $query->whereRaw('(abim.inmatriculatBuget = TRUE OR abim.inmatriculatContract = TRUE)');

            if($facultyIdSearch !== '' && $facultyIdSearch !== null){
                $query->whereRaw('abim.specialitateCiclu2ID IN (SELECT specialitateCiclu2ID FROM SpecialitateCiclu2 WHERE facultateID = '. $facultyIdSearch .')');
            }

            if($specialityIdSearch !== '' && $specialityIdSearch !== null){
                $query->whereRaw('abim.specialitateCiclu2ID = '. $specialityIdSearch);
            }

            if($fullName !== '' && $fullName !== null){
                $query->whereRaw( '(abim.nume LIKE "%'. $fullName .'%" OR abim.prenume LIKE "%'. $fullName .'%")');
            }

            $query->orderBy('sp.facultateID', 'ASC');
            $query->orderBy('sp.specialitateCiclu2ID', 'ASC');
            $query->orderBy('abim.nume', 'ASC');

            $abiturients = $query->get();

            if($facultyIdSearch !== '' && $facultyIdSearch !== null){
                $specialities = $studentConnection
                    ->table('SpecialitateCiclu2')
                    ->selectRaw('specialitateCiclu2ID, CONCAT(numeSpecialitateCiclu2, " (", limbaDeInstruire, ")") AS numeSpecialitateCiclu2')
                    ->where('facultateID', $facultyIdSearch)
                    ->pluck('numeSpecialitateCiclu2', 'specialitateCiclu2ID');
            } else {
                $specialities = $studentConnection
                    ->table('SpecialitateCiclu2')
                    ->selectRaw('specialitateCiclu2ID, CONCAT(numeSpecialitateCiclu2, " (", limbaDeInstruire, ")") AS numeSpecialitateCiclu2')
                    ->pluck('numeSpecialitateCiclu2', 'specialitateCiclu2ID');
            }
        } else {
            $abiturients = [];
            $specialities = [];
        }

        $selected1 = '';
        $selected2 = '';
        if($cycleIdSearch == 1){
            $selected1 = 'selected';
        }

        if($cycleIdSearch == 2){
            $selected2 = 'selected';
        }


        $html = '<div class="row">
                     <div class="col-md-12 text-center">
                         <h3>'. __('messages.import_abiturients_from') .'</h3>
                     </div>
                 </div>
                 <div class="row">
                     <div class="col-md-2">
                         <div class="form-group">
                             <label for="cycle_id_external" class="control-label">'. __('pages.cycle') .'</label>
                             <select data-placeholder="'. __('pages.chose_cycle') .'" class="standardSelect"
                                 name="cycle_id_external" id="cycle_id_external" required title="'. __('pages.cycle') .'">
                                 <option value="0">'. __('pages.chose_cycle') .'</option>
                                 <option value="1" '. $selected1 .'>'. __('pages.cycle1') .'</option>
                                 <option value="2" '. $selected2 .'>'. __('pages.cycle2') .'</option>
                             </select>
                         </div>
                     </div>
                     <div class="col-md-4">
                         <div class="form-group">
                             <label for="faculty_id_external" class="control-label">'. __('pages.faculty') .'</label>
                             <select data-placeholder="'. __('pages.chose_faculty') .'" class="standardSelect"
                                 name="faculty_id_external" id="faculty_id_external" required title="'. __('pages.faculty') .'">
                                 <option value="0">'. __('pages.chose_faculty') .'</option>';
                                 foreach ($faculties as $key => $faculty){
                                     $selected = '';
                                     if($facultyIdSearch == $key){
                                         $selected = 'selected';
                                     }
                                     $html .= '<option value="'. $key .'" '. $selected .'>'. $faculty .'</option>';
                                 }
                    $html .= '</select>
                         </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="speciality_id_external" class="control-label">'. __('pages.speciality') .'</label>
                            <select data-placeholder="'. __('pages.chose_speciality') .'" class="standardSelect"
                                name="speciality_id_external" id="speciality_id_external" required title="'. __('pages.speciality') .'">
                                <option value="0">'. __('pages.chose_speciality') .'</option>';
                                foreach ($specialities as $key => $speciality){
                                    $selected = '';
                                    if($specialityIdSearch == $key){
                                        $selected = 'selected';
                                    }
                                    $html .= '<option value="'. $key .'" '. $selected .'>'. $speciality .'</option>';
                                }
                    $html .= '</select>
                         </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 70px">
                    <div class="col-md-3">
                        <input class="input-sm form-control-sm form-control" placeholder="'. __('pages.first_name') . ' / ' . __('pages.last_name')  . ' / ' . __('pages.patronymic') .'" 
                            id="name_external" name="name" type="text" value="'. $fullName .'">
                    </div>
                    <div class="col-md-3">
                        <button class="btn btn-primary btn-sm" type="button" id="search_external">'. __('pages.search') .'</button>
                    </div>
                </div>';

        $html .= '<div class="row">
                      <div class="col-md-12">
                          <div class="row" style="margin-bottom: 15px">
                              <div class="col-md-3">
                                  <button class="btn btn-primary btn-lg" type="button" data-toggle="modal" data-target="#confirmImport">
                                      '. __('pages.import') .'
                                  </button>
                              </div>
                              <div class="col-md-6 text-center">
                                  <h5>'. __('pages.total_abiturients') .' : '. count($abiturients) .'</h5>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-12">
                          <table class="table">
                              <thead class="thead-dark">
                                  <tr class="text-center">
                                      <th scope="row">#</th>
                                      <th>'. __('pages.faculty') .'</th>
                                      <th>'. __('pages.speciality') .'</th>
                                      <th>'. __('pages.whole_name') .'</th>
                                      <th>'. __('pages.sex') .'</th>
                                      <th>'. __('pages.birth_date') .'</th>
                                      <th>'. __('pages.address') .'</th>
                                      <th>'. __('pages.idnp') .'</th>
                                  </tr>
                              </thead>
                          <tbody>';
        if(count($abiturients) > 0){
            foreach ($abiturients as $abiturient) {
                if($cycleIdSearch == 1){
                    $abiturientId = $abiturient->abiturientID;
                } else {
                    $abiturientId = $abiturient->masterandID;
                }
                $html .= '<tr>
                              <td class="text-center" style="vertical-align: middle;">
                                  <div class="form-check-inline form-check" style="margin: auto;">
                                      <input type="checkbox" class="form-check-input abiturient" name="abiturient" value="'. $abiturientId .'" style="height: 20px;width: 20px;margin: 0">
                                  </div>
                              </td>
                              <td class="text-center"><span>'. $abiturient->numeFacultate .'</span></td>
                              <td class="text-center"><span>'. $abiturient->numeSpecialitate .'</span></td>
                              <td class="text-center"><span>'. $abiturient->nume .' '. $abiturient->prenume .' '. $abiturient->patronimic .'</span></td>
                              <td class="text-center"><span>'. $abiturient->sex .'</span></td>
                              <td class="text-center"><span>'. Carbon::parse($abiturient->dataNasterii)->format('d/m/Y') .'</span></td>
                              <td class="text-center"><span>'. $abiturient->domiciliuAdresa .'</span></td>
                              <td class="text-center"><span>'. $abiturient->codulPersonal .'</span></td>
                          </tr>';
            }
        } else {
            $html .= '<tr class="text-center">
                          <td colspan="7">'. __('messages.not_found_any_abiturients') .'</td>
                      </tr>';
        }

        $html .= '</tbody></table></div></div>';

        return response()->json(['type' => 'success', 'message' => __('messages.success'), 'html' => $html]);
    }

    public function getExternalSpecialitiesByCycleOrFaculty(Request $request)
    {
        $databaseId = !empty($request->databaseId) ? trim($request->databaseId) : '';
        $cycleIdSearch = !empty($request->cycleIdSearch) ? trim($request->cycleIdSearch) : '';
        $facultyIdSearch = !empty($request->facultyIdSearch) ? trim($request->facultyIdSearch) : '';

        if($databaseId == '' || $cycleIdSearch == ''){
            return response()->json(['type' => 'warning', 'message' => __('messages.all_fields_is_required')]);
        }

        $database = Database::where('id', $databaseId)->where('deleted', 0)->first();
        if(!$database){
            return response()->json(['type' => 'warning', 'message' => __('messages.not_found_admission_database')]);
        }

        if(request()->ip() == '127.0.0.1') {
            try {
                DB::connection('mysql_student_local')->getPdo();
                $studentConnection = DB::connection('mysql_student_local');
            } catch (\Exception $e) {
                return response()->json(['type' => 'warning', 'message' => __('messages.please_check_database_configuration')]);
            }
        } else {
            try {
                DB::connection($database->connection_name)->getPdo();
                $studentConnection = DB::connection($database->connection_name);
            } catch (\Exception $e) {
                return response()->json(['type' => 'warning', 'message' => __('messages.please_check_database_configuration')]);
            }
        }

        $html = '<option value="0">'. __('messages.not_found_any_specialities') .'</option>';

        if($cycleIdSearch == 1){
            if($facultyIdSearch !== '' && $facultyIdSearch !== null){
                $specialities = $studentConnection
                    ->table('SpecialitateCiclu1')
                    ->selectRaw('specialitateCiclu1ID, CONCAT(numeSpecialitateCiclu1, " (", formaDeInstruire, ", ", limbaDeInstruire, ")") AS numeSpecialitateCiclu1')
                    ->where('facultateID', $facultyIdSearch)
                    ->pluck('numeSpecialitateCiclu1', 'specialitateCiclu1ID');
            } else {
                $specialities = $studentConnection
                    ->table('SpecialitateCiclu1')
                    ->selectRaw('specialitateCiclu1ID, CONCAT(numeSpecialitateCiclu1, " (", formaDeInstruire, ", ", limbaDeInstruire, ")") AS numeSpecialitateCiclu1')
                    ->pluck('numeSpecialitateCiclu1', 'specialitateCiclu1ID');
            }
        } elseif ($cycleIdSearch == 2) {
            if($facultyIdSearch !== '' && $facultyIdSearch !== null){
                $specialities = $studentConnection
                    ->table('SpecialitateCiclu2')
                    ->selectRaw('specialitateCiclu2ID, CONCAT(numeSpecialitateCiclu2, " (", limbaDeInstruire, ")") AS numeSpecialitateCiclu2')
                    ->where('facultateID', $facultyIdSearch)
                    ->pluck('numeSpecialitateCiclu2', 'specialitateCiclu2ID');
            } else {
                $specialities = $studentConnection
                    ->table('SpecialitateCiclu2')
                    ->selectRaw('specialitateCiclu2ID, CONCAT(numeSpecialitateCiclu2, " (", limbaDeInstruire, ")") AS numeSpecialitateCiclu2')
                    ->pluck('numeSpecialitateCiclu2', 'specialitateCiclu2ID');
            }
        } else {
            $specialities = [];
        }

        if(count($specialities) > 0) {
            $html = '';
            foreach ($specialities as $key => $specialityName) {
                $html .= '<option value="'. $key .'">'. $specialityName .'</option>';
            }
        }

        return response()->json(['type' => 'success', 'message' => __('messages.success'), 'html' => $html]);
    }

    public function importStudents(Request $request)
    {
        $cycleIdExternal = !empty($request->cycleIdExternal) ? trim($request->cycleIdExternal) : '';
        $abiturientsIds = !empty($request->abiturients) ? $request->abiturients : [];

        $databaseId = !empty($request->databaseId) ? trim($request->databaseId) : '';
        $cycleId = !empty($request->cycleId) ? trim($request->cycleId) : '';
        $facultyId = !empty($request->facultyId) ? trim($request->facultyId) : '';
        $specialityId = !empty($request->specialityId) ? trim($request->specialityId) : '';
        $groupId = !empty($request->groupId) ? trim($request->groupId) : '';
        $yearOfStudyId = !empty($request->yearOfStudyId) ? trim($request->yearOfStudyId) : '';

        if($cycleIdExternal == ''){
            return response()->json(['type' => 'warning', 'message' => __('messages.please_select_cycle')]);
        }

        if(count($abiturientsIds) == 0){
            return response()->json(['type' => 'warning', 'message' => __('messages.please_select_at_least_one_abiturient')]);
        }

        if($databaseId == '' || $cycleId == '' || $facultyId == '' || $specialityId == '' || $groupId == '' || $yearOfStudyId == ''){
            return response()->json(['type' => 'warning', 'message' => __('messages.something_wrong')]);
        }

        $database = Database::where('id', $databaseId)->where('deleted', 0)->first();
        if(!$database){
            return response()->json(['type' => 'warning', 'message' => __('messages.not_found_admission_database')]);
        }

        $cycle = Cycle::where('id', $cycleId)->where('deleted', 0)->first();
        if(!$cycle){
            return response()->json(['type' => 'warning', 'message' => __('messages.not_found_cycle')]);
        }

        $faculty = Faculty::where('id', $facultyId)->where('cycle_id', $cycleId)->where('deleted', 0)->first();
        if(!$faculty){
            return response()->json(['type' => 'warning', 'message' => __('messages.not_found_faculty')]);
        }

        $speciality = Speciality::where('id', $specialityId)->where('cycle_id', $cycleId)->where('faculty_id', $facultyId)->where('deleted', 0)->first();
        if(!$speciality){
            return response()->json(['type' => 'warning', 'message' => __('messages.not_found_speciality')]);
        }

        $group = Group::where('id', $groupId)->where('cycle_id', $cycleId)->where('faculty_id', $facultyId)->where('speciality_id', $specialityId)->where('deleted', 0)->first();
        if(!$group){
            return response()->json(['type' => 'warning', 'message' => __('messages.not_found_group')]);
        }

        $yearOfStudy = YearOfStudy::where('id', $yearOfStudyId)->where('deleted', 0)->first();
        if(!$yearOfStudy){
            return response()->json(['type' => 'warning', 'message' => __('messages.not_found_year_of_study')]);
        }

        if(request()->ip() == '127.0.0.1') {
            try {
                DB::connection('mysql_student_local')->getPdo();
                $studentConnection = DB::connection('mysql_student_local');
            } catch (\Exception $e) {
                return response()->json(['type' => 'warning', 'message' => __('messages.please_check_database_configuration')]);
            }
        } else {
            try {
                DB::connection($database->connection_name)->getPdo();
                $studentConnection = DB::connection($database->connection_name);
            } catch (\Exception $e) {
                return response()->json(['type' => 'warning', 'message' => __('messages.please_check_database_configuration')]);
            }
        }

        if($cycleIdExternal == 1) {
            $query = $studentConnection->table('InmatriculareCiclu1 AS im');
            $query->selectRaw('ab.*, im.inmatriculatBuget');
            $query->leftJoin('AbiturientCiclu1 AS ab', 'ab.abiturientID', '=', 'im.abiturientID');
            $query->whereIn('ab.abiturientID', $abiturientsIds);

        } else {
            $query = $studentConnection->table('AbiturientInmatriculareCiclu2');
            $query->whereIn('masterandID', $abiturientsIds);
        }

        $abiturients = $query->get();

        foreach ($abiturients as $abiturient){
            $nume = preg_replace('/\s+/', '', $abiturient->nume);
            $prenume = preg_replace('/\s+/', '', $abiturient->prenume);
            $replace = ['ă', 'â', 'î', 'ș', 'ț', '-'];
            $replaceWith = ['a', 'a', 'i', 's', 't', '_'];

            $generateEmail = str_replace($replace, $replaceWith, mb_strtolower(trim($nume . '.' . $prenume . '@upsc.md'), 'UTF-8'));
            $studentExist = $this->studentHelper->checkIfStudentExistByEmail($generateEmail);

            if($studentExist){
                if(Student::where('idnp', trim($abiturient->codulPersonal))->exists()){
                    $this->updateStudent($generateEmail, $abiturient, $cycleId, $facultyId, $specialityId, $groupId, $yearOfStudyId);
                } else {
                    $generateEmail = $this->studentHelper->generateStudentEmail($abiturient->nume, $abiturient->prenume);

                    $this->createStudent($generateEmail, $abiturient, $cycleId, $facultyId, $specialityId, $groupId, $yearOfStudyId);
                }
            } else {
                $this->createStudent($generateEmail, $abiturient, $cycleId, $facultyId, $specialityId, $groupId, $yearOfStudyId);
            }
        }

        return response()->json(['type' => 'success', 'message' => __('messages.abiturients_imported_successfully')]);
    }

    public function createStudent($generateEmail, $abiturient, $cycleId, $facultyId, $specialityId, $groupId, $yearOfStudyId)
    {
        $user = User::create([
            'name' => $abiturient->prenume . ' ' . $abiturient->nume,
            'email' => $generateEmail,
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make(config('app.defaultStudentPassword'))
        ]);
        $user->assignRole('student');

        $studentPermissions = $this->helper->getAllowedPermissionsByModel('student');
        foreach ($studentPermissions as $permission) {
            $user->givePermissionTo($permission->name);
        }

        $student = Student::create([
            'user_id' => $user->id,
            'cycle_id' => $cycleId,
            'faculty_id' => $facultyId,
            'speciality_id' => $specialityId,
            'group_id' => $groupId,
            'year_of_study_id' => $yearOfStudyId,
            'first_name' => $abiturient->prenume,
            'last_name' => $abiturient->nume,
            'patronymic' => $abiturient->patronimic,
            'idnp' => $abiturient->codulPersonal,
            'gender' => $abiturient->sex,
            'personal_email' => $abiturient->adresaEmail,
            'mobile' => $abiturient->telefonMobil,
            'birth_date' => Carbon::createFromFormat('Y-m-d', $abiturient->dataNasterii)->format('Y/m/d'),
            'city' => 'NEED TO CHECK',
            'address' => isset($abiturient->domiciliuAdresa) ?: $abiturient->vizaDeResedinta,
            'citizenship' => $abiturient->cetatenia,
            'nationality' => $abiturient->nationalitatea,
            'year_of_admission' => $abiturient->admitereaCurentaID,
            'study' => 1,
            'budget' => $abiturient->inmatriculatBuget,
            'expelled' => 0,
            'additional_info' => ''
        ]);

        StudentChangesInStudy::create([
            'student_id' => $student->id,
            'year_of_admission' => $abiturient->admitereaCurentaID,
            'cycle' => $this->studyHelper->getCycleNameById($cycleId),
            'faculty' => $this->studyHelper->getFacultyNameById($facultyId),
            'speciality' => $this->studyHelper->getSpecialityNameById($specialityId),
            'group' => $this->studyHelper->getGroupNameById($groupId),
            'year_of_study' => $this->studyHelper->getYearOfStudyNameById($yearOfStudyId)
        ]);

        $this->helper->addLog('superadmin', Auth::user()->name, 'students', 1, 'MASSIVE IMPORT DB id: [' . $student->id . ']');
    }

    public function updateStudent($generateEmail, $abiturient, $cycleId, $facultyId, $specialityId, $groupId, $yearOfStudyId)
    {
        User::where('email', $generateEmail)->update([
            'name' => $abiturient->prenume . ' ' . $abiturient->nume,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $existingStudent = Student::where('idnp', trim($abiturient->codulPersonal))->first();

        Student::where('id', $existingStudent->id)->update([
            'cycle_id' => $cycleId,
            'faculty_id' => $facultyId,
            'speciality_id' => $specialityId,
            'group_id' => $groupId,
            'year_of_study_id' => $yearOfStudyId,
            'first_name' => $abiturient->prenume,
            'last_name' => $abiturient->nume,
            'patronymic' => $abiturient->patronimic,
            'idnp' => $abiturient->codulPersonal,
            'gender' => $abiturient->sex,
            'personal_email' => $abiturient->adresaEmail,
            'mobile' => $abiturient->telefonMobil,
            'birth_date' => Carbon::createFromFormat('Y-m-d', $abiturient->dataNasterii)->format('Y/m/d'),
            'city' => 'NEED TO CHECK',
            'address' => isset($abiturient->domiciliuAdresa) ?: $abiturient->vizaDeResedinta,
            'citizenship' => $abiturient->cetatenia,
            'nationality' => $abiturient->nationalitatea,
            'year_of_admission' => $abiturient->admitereaCurentaID,
            'budget' => $abiturient->inmatriculatBuget,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        if ($existingStudent->year_of_admission !== $abiturient->admitereaCurentaID || $existingStudent->cycle_id !== $cycleId ||
            $existingStudent->faculty_id !== $facultyId || $existingStudent->speciality_id !== $specialityId ||
            $existingStudent->group_id !== $groupId || $existingStudent->year_of_study_id !== $yearOfStudyId) {
            StudentChangesInStudy::create([
                'student_id' => $existingStudent->id,
                'year_of_admission' => $abiturient->admitereaCurentaID,
                'cycle' => $this->studyHelper->getCycleNameById($cycleId),
                'faculty' => $this->studyHelper->getFacultyNameById($facultyId),
                'speciality' => $this->studyHelper->getSpecialityNameById($specialityId),
                'group' => $this->studyHelper->getGroupNameById($groupId),
                'year_of_study' => $this->studyHelper->getYearOfStudyNameById($yearOfStudyId)
            ]);
        }

        $this->helper->addLog('superadmin', Auth::user()->name, 'students', 2, 'MASSIVE IMPORT DB id: [' . $existingStudent->id . ']');
    }
}
