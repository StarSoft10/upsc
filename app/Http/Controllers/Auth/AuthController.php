<?php

namespace App\Http\Controllers\Auth;

use App\Entities\RegisterNotification;
use App\Entities\Student;
use App\Entities\Teacher;
use App\Entities\UnconfirmedUser;
use App\Entities\User;
use App\Events\NewRegistrationNotification;
use App\Helpers\AppHelper;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Validator;

class AuthController extends Controller
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    public function signInPost(Request $request)
    {
        $email = !empty($request->email) ? $request->email : '';
        $password = !empty($request->password) ? $request->password : '';

        if($email == '' || $password == ''){
            return back()->with('warning', __('messages.wrong_credentials'));
        }

        if(Auth::attempt(['email' => $email, 'password' => $password])){

            if (Auth::check()){
                $role = Auth::user()->getRoleNames()[0];

                if ($role == 'superadmin'){
                    return redirect()->route('superadmin');
                } elseif ($role == 'admin') {
                    if($this->checkIfUserIsBlockedOrDeleted(Auth::user(), 'admin')){
                        return redirect()->route('signInGet')->with('error', __('messages.account_was_blocked_or_deleted'));
                    }

                    return redirect()->route('admin');
                } elseif ($role == 'rector') {
                    if($this->checkIfUserIsBlockedOrDeleted(Auth::user(), 'rector')){
                        return redirect()->route('signInGet')->with('error', __('messages.account_was_blocked_or_deleted'));
                    }

                    return redirect()->route('rector');
                } elseif ($role == 'secretary') {
                    if($this->checkIfUserIsBlockedOrDeleted(Auth::user(), 'secretary')){
                        return redirect()->route('signInGet')->with('error', __('messages.account_was_blocked_or_deleted'));
                    }

                    return redirect()->route('secretary');
                } elseif ($role == 'dean') {
                    if($this->checkIfUserIsBlockedOrDeleted(Auth::user(), 'dean')){
                        return redirect()->route('signInGet')->with('error', __('messages.account_was_blocked_or_deleted'));
                    }

                    return redirect()->route('dean');
                } elseif ($role == 'chair') {
                    if($this->checkIfUserIsBlockedOrDeleted(Auth::user(), 'chair')){
                        return redirect()->route('signInGet')->with('error', __('messages.account_was_blocked_or_deleted'));
                    }

                    return redirect()->route('chair');
                } elseif ($role == 'human-resource') {
                    if($this->checkIfUserIsBlockedOrDeleted(Auth::user(), 'humanResource')){
                        return redirect()->route('signInGet')->with('error', __('messages.account_was_blocked_or_deleted'));
                    }

                    return redirect()->route('human-resource');
                } elseif ($role == 'teacher') {
                    if($this->checkIfUserIsBlockedOrDeleted(Auth::user(), 'teacher')){
                        return redirect()->route('signInGet')->with('error', __('messages.account_was_blocked_or_deleted'));
                    }

                    return redirect()->route('teacher');
                } elseif ($role == 'student') {
                    if($this->checkIfUserIsBlockedOrDeleted(Auth::user(), 'student')){
                        return redirect()->route('signInGet')->with('error', __('messages.account_was_blocked_or_deleted'));
                    }

                    return redirect()->route('student');
                } else {
                    return redirect()->route('signInGet');
                }
            } else {
                return back()->with('warning', __('messages.wrong_credentials'));
            }
        } else {
            return back()->with('warning', __('messages.wrong_credentials'));
        }
    }

    public function signUpPost(Request $request)
    {
        Log::info('AuthController signUpPost START');

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users|regex:/^[a-z0-9\@\.\_\-]+$/u',
            'password' => 'required|confirmed|min:6'
        ]);

        if ($validator->fails()) {

            Log::info('WARNING');
            Log::info(json_encode($validator->errors()->first()));
            Log::info('AuthController signUpPost STOP');

            return back()->with('error', $validator->errors()->first());
        }

        $firstName = trim($request->first_name);
        $lastName = trim($request->last_name);
        $email = trim($request->email);
        $phone = trim($request->phone);
        $password = trim($request->password);

        Log::info('$firstName: ' . $firstName);
        Log::info('$lastName: ' . $lastName);
        Log::info('$email: ' . $email);
        Log::info('$phone: ' . $phone);
        Log::info('$password: ' . $password);

        $role = $this->getExternalRoleByEmail($request->email);
//        $role = '';

        Log::info('$role: ' . $role);

        if($role == ''){
            $unconfirmedUser = UnconfirmedUser::create([
                'first_name' => $firstName,
                'last_name' => $lastName,
                'email' => mb_strtolower($email, 'UTF-8'),
                'phone' => $phone,
                'plain_password' => $password
            ]);

            $registerNotification = RegisterNotification::create([
                'type' => 'unconfirmed-user',
                'model_id' => $unconfirmedUser->id,
                'model_info' => $unconfirmedUser->first_name .'  '. $unconfirmedUser->last_name,
                'message' => __('messages.new_unconfirmed_user_register'),
                'read' => 0
            ]);

            event(new NewRegistrationNotification('unconfirmed-user', $unconfirmedUser, $registerNotification));

            Log::info('SUCCESS');
            Log::info('Register unconfirmed user');
            Log::info('AuthController signUpPost STOP');

            return back()->with('success', __('messages.please_waiting_confirmation'));
        }

        if($role == 'teacher'){
            $user = User::create([
                'name' => $firstName . ' ' . $lastName,
                'email' => mb_strtolower($email, 'UTF-8'),
                'email_verified_at' => Carbon::now(),
                'password' => Hash::make($password)
            ]);

            $user->assignRole('teacher');

            $permissions = $this->helper->getAllowedPermissionsByModel('teacher');
            foreach ($permissions as $permission) {
                $user->givePermissionTo($permission);
            }

            $teacher = Teacher::create([
                'user_id' => $user->id,
                'first_name' => $firstName,
                'last_name' => $lastName,
                'phone' => $phone
            ]);

            $registerNotification = RegisterNotification::create([
                'type' => 'teacher',
                'model_id' => $teacher->id,
                'model_info' => $teacher->first_name .'  '. $teacher->last_name,
                'message' => __('messages.new_teacher_register'),
                'read' => 0
            ]);

            event(new NewRegistrationNotification('teacher', $teacher, $registerNotification));

            Auth::login($user);

            Log::info('SUCCESS');
            Log::info('Register teacher');
            Log::info('AuthController signUpPost STOP');

            return redirect()->route('teacher');
        }

        if($role == 'student'){
            $user = User::create([
                'name' => $firstName . ' ' . $lastName,
                'email' => mb_strtolower($email, 'UTF-8'),
                'email_verified_at' => Carbon::now(),
                'password' => Hash::make($password)
            ]);

            $user->assignRole('student');

            $permissions = $this->helper->getAllowedPermissionsByModel('student');
            foreach ($permissions as $permission) {
                $user->givePermissionTo($permission);
            }

            $student = Student::create([
                'user_id' => $user->id,
                'first_name' => $firstName,
                'last_name' => $lastName,
                'phone' => $phone
            ]);

            $registerNotification = RegisterNotification::create([
                'type' => 'student',
                'model_id' => $student->id,
                'model_info' => $student->first_name .'  '. $student->last_name,
                'message' => __('messages.new_student_register'),
                'read' => 0
            ]);

            event(new NewRegistrationNotification('student', $student, $registerNotification));

            Auth::login($user);

            Log::info('SUCCESS');
            Log::info('Register student');
            Log::info('AuthController signUpPost STOP');

            return redirect()->route('student');
        }

        Log::info('WARNING');
        Log::info('SOMETHING WRONG');
        Log::info('AuthController signUpPost STOP');

        return back()->with('error', __('messages.something_wrong_try_again_after_time'));
    }

    public function logout()
    {
        if(Auth::user()){
            Auth::logout();
        }

        return redirect()->route('signInGet');
    }

    public function checkIfUserIsBlockedOrDeleted($user, $model)
    {
        if ($user->$model->blocked == 1) {
            Auth::logout();
            Log::info('User with id: '. $user->id .' was blocked');

            return true;
        }

        if ($user->$model->deleted == 1) {
            Auth::logout();
            Log::info('User with id: '. $user->id .' was deleted');

            return true;
        }
        return false;
    }

    public function getExternalRoleByEmail($email)
    {
        if(request()->ip() == '127.0.0.1') {
            $teacherConnection = DB::connection('mysql_teacher_local');
            $studentConnection = DB::connection('mysql_student_local');
        } else {
            $teacherConnection = DB::connection('mysql_teacher');
            $studentConnection = DB::connection('mysql_student');
        }

        $teacher = $teacherConnection
            ->table('employees')
            ->select('spcode')
            ->whereRaw('LOWER(email) LIKE "%' . mb_strtolower($email, 'UTF-8') . '%"')
            ->first();

        if($teacher){
            return 'teacher';
        }

        $student = $studentConnection
            ->table('abiturientciclu1')
            ->select('abiturientID')
            ->whereRaw('LOWER(adresaEmail) LIKE "%' . mb_strtolower($email, 'UTF-8') . '%"')
            ->first();

        if($student){
            return 'student';
        }

        $masterand = $studentConnection
            ->table('abiturientinmatriculareciclu2')
            ->select('masterandID')
            ->whereRaw('LOWER(adresaEmail) LIKE "%' . mb_strtolower($email, 'UTF-8') . '%"')
            ->first();

        if($masterand){
            return 'student';
        }

        return '';
    }
}
