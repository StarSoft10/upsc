<?php

namespace App\Jobs;

use App\Entities\BorderouNoteCode;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class BorderouNoteCodeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 1200;
    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('Update BorderouNoteCode START');
        $delayAt = now()->addDay();
        $code = $this->generateCode();
        $defaultCode = '10101991CM';

        Log::info('$delayAt: ' . $delayAt);
        Log::info('$code: ' . $code);
        Log::info('$defaultCode: ' . $defaultCode);

        BorderouNoteCode::create([
            'code' => $code,
            'default_code' => $defaultCode,
        ]);

        BorderouNoteCodeJob::dispatch()->delay($delayAt);
        Log::info('Update BorderouNoteCode STOP');
    }

    public function failed(\Exception $exception)
    {
        Log::info('FAILED BorderouNoteCode');
        Log::info($exception);
    }

    public function generateCode()
    {
//        return mb_substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'), 0, 6);
        return mb_substr(str_shuffle('0123456789'), 0, 8);
    }
}
