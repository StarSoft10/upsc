<?php

namespace App\Console\Commands;

use App\Jobs\BorderouNoteCodeJob;
use Illuminate\Console\Command;


class UpdateBorderouNoteCode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:code';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start job where Update code for borderou note editing access!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        BorderouNoteCodeJob::dispatch()->delay(now());
        echo "Job start !";
    }
}
