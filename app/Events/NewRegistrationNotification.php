<?php

namespace App\Events;

use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\Log;

class NewRegistrationNotification implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $type;
    public $userObj;
    public $registerNotification;
    public $createdAt;

    /**
     * Create a new event instance.
     *
     * @param $type
     * @param $userObj
     * @param $registerNotification
     */
    public function __construct($type, $userObj, $registerNotification)
    {
        $this->type = $type;
        $this->userObj = $userObj;
        $this->registerNotification = $registerNotification;
//        $this->createdAt = $this->parseDate($userObj->created_at);
        $this->createdAt = $userObj->created_at->diffForHumans();

        Log::info('Event NewRegistrationNotification Start');
        Log::info('$this->type');
        Log::info($this->type);
        Log::info('$this->userObj');
        Log::info($this->userObj);
        Log::info('$this->registerNotification');
        Log::info($this->registerNotification);
        Log::info('Event NewRegistrationNotification Stop');
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['new-registration-notification'];
    }

    public function parseDate($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y H:i:s');
    }
}