<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\Log;

class ReadNewRegistrationNotification implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $registerNotification;

    /**
     * Create a new event instance.
     *
     * @param $registerNotification
     */
    public function __construct($registerNotification)
    {
        $this->registerNotification = $registerNotification;

        Log::info('Event NewRegistrationNotification Start');
        Log::info('$this->registerNotification');
        Log::info($this->registerNotification);
        Log::info('Event NewRegistrationNotification Stop');
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['read-new-registration-notification'];
    }
}