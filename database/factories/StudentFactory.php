<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\Student;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Student::class, function (Faker $faker) {

    static $increment = 1;

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'phone' => $faker->randomNumber(9),
        'mobile' => $faker->randomNumber(9),
        'email' => $faker->email,
        'passport' => $faker->randomNumber(9),
        'birth_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'gender' => $faker->biasedNumberBetween($min = 1, $max = 2, $function = 'sqrt'),
        'status' => $faker->biasedNumberBetween($min = 1, $max = 2, $function = 'sqrt'),
        'unique_code' => str_pad($increment++, 9, '0', STR_PAD_LEFT),
        'from_country' => $faker->country,
        'city' => $faker->city,
        'address' => $faker->address,
        'index' => $faker->randomNumber(5),
        'postal_code' => $faker->postcode,

        'assigned_type' => 1,
        'assigned_to' => 1,

        'place_of_work' => $faker->text,
        'position' => $faker->text,
        'visa_expires_at' => date($format = 'Y-m-d'),
        'insurance_company' => $faker->company,
        'start' => date($format = 'Y-m-d'),
        'end' => date($format = 'Y-m-d'),

        'has_insurance_health' => 0,
        'hospital' => null,
        'hospital_number' => null,
        'open_polisa_at' => null,

        'insurance_agent' => $faker->name(),
        'insurance_agent_number' => $faker->randomNumber(9),
        'insurance_number' => $faker->randomNumber(9),

        'has_relation' => 0
    ];
});
