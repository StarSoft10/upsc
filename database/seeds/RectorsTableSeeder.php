<?php

use App\Entities\Rector;
use App\Entities\User;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class RectorsTableSeeder extends Seeder
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rectorPermissions = $this->helper->getAllowedPermissionsByModel('rector');

        $rector = User::create([
            'name' => 'rector.upsc',
            'email' => 'rectorat@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Rector.2021')
        ]);
        $rector->assignRole('rector');
        foreach ($rectorPermissions as $permission) {
            $rector->givePermissionTo($permission->name);
        }
        Rector::create([
            'user_id' => $rector->id,
            'first_name' => 'Alexandra',
            'last_name' => 'Barbrbăneagră',
            'phone' => '+37368396700'
        ]);
    }
}
