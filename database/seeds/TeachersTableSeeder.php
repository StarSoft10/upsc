<?php

use App\Entities\Teacher;
use App\Entities\User;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class TeachersTableSeeder extends Seeder
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teacherPermissions = $this->helper->getAllowedPermissionsByModel('teacher');

        /*
         * 1
         */
        $teacher = User::create([
            'name' => 'adascalita.victoria',
            'email' => 'adascalita.victoria@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Adascalita.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Viorica',
            'last_name' => 'Adăscăliţa',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
         * 2
         */
        $teacher = User::create([
            'name' => 'adelsgruber.paulus',
            'email' => 'adelsgruber.paulus@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Adelsgruber.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Paulus',
            'last_name' => 'Adelsgruber',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
         * 3
         */
        $teacher = User::create([
            'name' => 'ajder.ecaterina',
            'email' => 'ajder.ecaterina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ajder.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ecaterina',
            'last_name' => 'Ajder',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 4
        */
        $teacher = User::create([
            'name' => 'alavatchi.alexandru',
            'email' => 'alavatchi.alexandru@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Alavatchi.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Alexandru',
            'last_name' => 'Alavaţchi',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 5
       */
        $teacher = User::create([
            'name' => 'albu-oprea.mariana',
            'email' => 'albu-oprea.mariana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Albu-Oprea.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Mariana',
            'last_name' => 'Albu-Oprea',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 6
        */
        $teacher = User::create([
            'name' => 'anghel.alexandru',
            'email' => 'anghel.alexandru@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Anghel.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Alexandru',
            'last_name' => 'Anghel',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 7
        */
        $teacher = User::create([
            'name' => 'antonevici.natalia',
            'email' => 'antonevici.natalia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Antonevici.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Natalia',
            'last_name' => 'Antonevici',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 8
        */
        $teacher = User::create([
            'name' => 'arbuz-spatari.olimpiada',
            'email' => 'arbuz-spatari.olimpiada@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Arbuz-Spatari.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Olimpiada',
            'last_name' => 'Arbuz-Spatari',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
         * 9
         */
        $teacher = User::create([
            'name' => 'auzeac.sergiu',
            'email' => 'auzeac.sergiu@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Auzeac.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Sergiu',
            'last_name' => 'Auzeac',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 10
       */
        $teacher = User::create([
            'name' => 'avornicesa-sofronie.natalia',
            'email' => 'avornicesa-sofronie.natalia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Avornicesa-Sofronie.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Natalia',
            'last_name' => 'Avornicesă-Sofronie',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 11
        */
        $teacher = User::create([
            'name' => 'Babciuc.stanislav',
            'email' => 'Babciuc.stanislav@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Babciuc.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Stanislav',
            'last_name' => 'Babciuc',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
         * 12
         */
        $teacher = User::create([
            'name' => 'babira.eugenia',
            'email' => 'babira.eugenia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Babira.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Eugenia',
            'last_name' => 'Babîră',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
         * 13
         */
        $teacher = User::create([
            'name' => 'balan.constantin',
            'email' => 'balan.constantin@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Balan.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Constantin',
            'last_name' => 'Balan',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 14
        */
        $teacher = User::create([
            'name' => 'balan.aurelia',
            'email' => 'balan.aurelia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Balan.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Aurelia',
            'last_name' => 'Balan',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 15
       */
        $teacher = User::create([
            'name' => 'balmus.nicolae',
            'email' => 'balmus.nicolae@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Balmus.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Nicolae',
            'last_name' => 'Balmuş',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
         * 16
         */
        $teacher = User::create([
            'name' => 'baltag.veronica',
            'email' => 'baltag.veronica@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Baltag.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Veronica',
            'last_name' => 'Baltag',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
         * 17
         */
        $teacher = User::create([
            'name' => 'banu.felicia',
            'email' => 'banu.felicia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Banu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Felicia',
            'last_name' => 'Banu',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
         * 18
         */
        $teacher = User::create([
            'name' => 'baraga.victoria',
            'email' => 'baraga.victoria@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Baraga.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Victoria',
            'last_name' => 'Baraga',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
         * 19

        $teacher = User::create([
            'name' => 'barbaneagra.alexandra',
            'email' => 'barbaneagra.alexandra@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Barbaneagra.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Alexandra',
            'last_name' => 'Barbăneagră',
            'phone' => '0',
            'mobile' => '0'
        ]);
         */

        /*
         * 20
         */
        $teacher = User::create([
            'name' => 'barbaros.nicolae',
            'email' => 'barbaros.nicolae@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Barbaros.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Nicolae',
            'last_name' => 'Barbaros',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
         * 21
         */
        $teacher = User::create([
            'name' => 'begu.adam',
            'email' => 'begu.adam@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Begu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Adam',
            'last_name' => 'Begu',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
         * 22
         */
        $teacher = User::create([
            'name' => 'bejan.iurie',
            'email' => 'bejan.iurie@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Bejan.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Iurie',
            'last_name' => 'Bejan',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
         * 23
         */
        $teacher = User::create([
            'name' => 'bejenaru.ludmila',
            'email' => 'bejenaru.ludmila@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Bejenaru.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ludmila',
            'last_name' => 'Bejenaru',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
         * 24
         */
        $teacher = User::create([
            'name' => 'bencheci.ion',
            'email' => 'bencheci.ion@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Bencheci.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ion',
            'last_name' => 'Bencheci',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
         * 24a
         */
        $teacher = User::create([
            'name' => 'bernic.virginia',
            'email' => 'bernic.virginia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Bernic.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Virginia',
            'last_name' => 'Bernic',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
         * 25
         */
        $teacher = User::create([
            'name' => 'biceva.elena',
            'email' => 'biceva.elena@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Biceva.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Elena',
            'last_name' => 'Bîceva',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
         * 26
         */
        $teacher = User::create([
            'name' => 'bobrova.iuliana',
            'email' => 'bobrova.iuliana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Bobrova.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Iuliana',
            'last_name' => 'Bobrova',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
         * 27
         */
        $teacher = User::create([
            'name' => 'bodareu.galina',
            'email' => 'bodareu.galina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Bodareu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Galina',
            'last_name' => 'Bodareu',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 28
        */
        $teacher = User::create([
            'name' => 'bodorin.cornelia',
            'email' => 'bodorin.cornelia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Bodorin.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Cornelia',
            'last_name' => 'Bodorin',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 29
        */
        $teacher = User::create([
            'name' => 'bolduma.viorel',
            'email' => 'bolduma.viorel@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Bolduma.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Viorel',
            'last_name' => 'Bolduma',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 30
        */
        $teacher = User::create([
            'name' => 'bolduma.veronica',
            'email' => 'bolduma.veronica@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Bolduma.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Veronica',
            'last_name' => 'Bolduma',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 31
        */
        $teacher = User::create([
            'name' => 'bolucencov.anna',
            'email' => 'bolucencov.anna@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Bolucencov.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Anna',
            'last_name' => 'Bolucencov',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 32
        */
        $teacher = User::create([
            'name' => 'bordei.irina',
            'email' => 'bordei.irina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Bordei.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Irina',
            'last_name' => 'Bordei',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 33
        */
        $teacher = User::create([
            'name' => 'bordu.irina',
            'email' => 'bordu.irina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Bordu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Irina',
            'last_name' => 'Bordu',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 34
        */
        $teacher = User::create([
            'name' => 'borozan.maia',
            'email' => 'borozan.maia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Borozan.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Maia',
            'last_name' => 'Borozan',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 35
        */
        $teacher = User::create([
            'name' => 'bostan.marina',
            'email' => 'bostan.marina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Bostan.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Marina',
            'last_name' => 'Bostan',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 36
        */
        $teacher = User::create([
            'name' => 'botezatu.nina',
            'email' => 'botezatu.nina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Botezatu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Nina',
            'last_name' => 'Botezatu',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 37
        */
        $teacher = User::create([
            'name' => 'botnari.dumitru',
            'email' => 'botnari.dumitru@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Botnari.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Dumitru',
            'last_name' => 'Botnari',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 38
        */
        $teacher = User::create([
            'name' => 'boz.olga',
            'email' => 'boz.olga@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Boz.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Olga',
            'last_name' => 'Boz',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 39
        */
        $teacher = User::create([
            'name' => 'brigalda.eleonora',
            'email' => 'brigalda.eleonora@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Brigalda.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Eleonora',
            'last_name' => 'Brigalda',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 40
        */
        $teacher = User::create([
            'name' => 'bucuci.olesea',
            'email' => 'bucuci.olesea@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Bucuci.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Olesea',
            'last_name' => 'Bucuci',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 41
        */
        $teacher = User::create([
            'name' => 'bucun.nicolae',
            'email' => 'bucun.nicolae@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Bucun.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Nicolae',
            'last_name' => 'Bucun',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'budnic.ana',
            'email' => 'budnic.ana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Budnic.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ana',
            'last_name' => 'Budnic',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'bulat.ana',
            'email' => 'bulat.ana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Bulat.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ana',
            'last_name' => 'Bulat',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 43
        */
        $teacher = User::create([
            'name' => 'bulat-guzun.ana',
            'email' => 'bulat-guzun.ana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Bulat-Guzun.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ana',
            'last_name' => 'Bulat-Guzun',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 44
        */
        $teacher = User::create([
            'name' => 'buraciova.elena',
            'email' => 'buraciova.elena@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Buraciova.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Elena',
            'last_name' => 'Buraciova',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 45
        */
        $teacher = User::create([
            'name' => 'burlacu.valentin',
            'email' => 'burlacu.valentin@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Burlacu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Valentin',
            'last_name' => 'Burlacu',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        * 46
        */
        $teacher = User::create([
            'name' => 'burlacu.alexandru',
            'email' => 'burlacu.alexandru@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Burlacu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Alexandru',
            'last_name' => 'Burlacu',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 47
       */
        $teacher = User::create([
            'name' => 'busmachiu.eugenia',
            'email' => 'busmachiu.eugenia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Busmachiu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Eugenia',
            'last_name' => 'Bușmachiu',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 48
       */
        $teacher = User::create([
            'name' => 'butuc.petru',
            'email' => 'butuc.petru@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Butuc.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Petru',
            'last_name' => 'Butuc',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 49
       */
        $teacher = User::create([
            'name' => 'buzinschi.elena',
            'email' => 'buzinschi.elena@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Buzinschi.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Elena',
            'last_name' => 'Buzinschi',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 50
       */
        $teacher = User::create([
            'name' => 'calaras.carolina',
            'email' => 'calaras.carolina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Calaras.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Carolina',
            'last_name' => 'Calaraș',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 51
       */
        $teacher = User::create([
            'name' => 'cantir.ludmila',
            'email' => 'cantir.ludmila@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cantir.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ludmila',
            'last_name' => 'Canțîr',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 52
       */
        $teacher = User::create([
            'name' => 'capatina.cristina',
            'email' => 'capatina.cristina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Capatina.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Cristina',
            'last_name' => 'Capațina',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 53
       */
        $teacher = User::create([
            'name' => 'carabet.natalia',
            'email' => 'carabet.natalia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Carabet.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Natalia',
            'last_name' => 'Carabet',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 54
       */
        $teacher = User::create([
            'name' => 'caracas.olimpiada',
            'email' => 'caracas.olimpiada@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Caracas.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Olimpiada',
            'last_name' => 'Caracaș',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 55
       */
        $teacher = User::create([
            'name' => 'caraman.vlad',
            'email' => 'caraman.vlad@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Caraman.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Vlad',
            'last_name' => 'Caraman',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 56
       */
        $teacher = User::create([
            'name' => 'carata.dumitru',
            'email' => 'carata.dumitru@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Carata.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Dumitru',
            'last_name' => 'Carata',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
      * 56a
      */
        $teacher = User::create([
            'name' => 'carp.ion',
            'email' => 'carp.ion@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Carp.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ion',
            'last_name' => 'Carp',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 57
       */
        $teacher = User::create([
            'name' => 'castravat.Andrei',
            'email' => 'castravat.Andrei@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Castravat.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Andrei',
            'last_name' => 'Castravăț',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 58
       */
        $teacher = User::create([
            'name' => 'cataraga.sergiu',
            'email' => 'cataraga.sergiu@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cataraga.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Sergiu',
            'last_name' => 'Cataraga',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 59
       */
        $teacher = User::create([
            'name' => 'ceban.valeriu',
            'email' => 'ceban.valeriu@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ceban.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Valeriu',
            'last_name' => 'Ceban',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 60
       */
        $teacher = User::create([
            'name' => 'cebanu.lilia',
            'email' => 'cebanu.lilia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cebanu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Lilia',
            'last_name' => 'Cebanu',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 61
       */
        $teacher = User::create([
            'name' => 'celpan-patic.natalia',
            'email' => 'celpan-patic.natalia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Celpan-Patic.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Natalia',
            'last_name' => 'Celpan-Patic',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 62
       */
        $teacher = User::create([
            'name' => 'cepraga.lucia',
            'email' => 'cepraga.lucia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cepraga.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Lucia',
            'last_name' => 'Cepraga',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 63
       */
        $teacher = User::create([
            'name' => 'cerneavschi.viorica',
            'email' => 'cerneavschi.viorica@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cerneavschi.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Viorica',
            'last_name' => 'Cerneavschi',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 64
       */
        $teacher = User::create([
            'name' => 'chelmenciuc.mihaela',
            'email' => 'chelmenciuc.mihaela@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Chelmenciuc.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Mihaela',
            'last_name' => 'Chelmenciuc',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 65
       */
        $teacher = User::create([
            'name' => 'chicu.silvia',
            'email' => 'chicu.silvia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Chicu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Silvia',
            'last_name' => 'Chicu',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 66
       */
        $teacher = User::create([
            'name' => 'chicus.nicolae',
            'email' => 'chicus.nicolae@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Chicus.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Nicolae',
            'last_name' => 'Chicuș',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 67
       */
        $teacher = User::create([
            'name' => 'chirev.larisa',
            'email' => 'chirev.larisa@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Chirev.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Larisa',
            'last_name' => 'Chirev',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 68
       */
        $teacher = User::create([
            'name' => 'chiriac.tatiana',
            'email' => 'chiriac.tatiana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Chiriac.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Tatiana',
            'last_name' => 'Chiriac',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 69
       */
        $teacher = User::create([
            'name' => 'chirilov.vasile',
            'email' => 'chirilov.vasile@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Chirilov.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Vasile',
            'last_name' => 'Chirilov',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 70
       */
        $teacher = User::create([
            'name' => 'chirimbu.sebastian',
            'email' => 'chirimbu.sebastian@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Chirimbu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Sebastian',
            'last_name' => 'Chirimbu',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 71
       */
        $teacher = User::create([
            'name' => 'chirsan.aliona',
            'email' => 'chirsan.aliona@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Chirsan.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Aliona',
            'last_name' => 'Chirsan',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 72
       */
        $teacher = User::create([
            'name' => 'chiseliov.victor',
            'email' => 'chiseliov.victor@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Chiseliov.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Victor',
            'last_name' => 'Chiseliov',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 73
       */
        $teacher = User::create([
            'name' => 'chitoroaga.lucia',
            'email' => 'chitoroaga.lucia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Chitoroaga.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Lucia',
            'last_name' => 'Chitoroagă',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 74
       */
        $teacher = User::create([
            'name' => 'cibotaru.liubovi',
            'email' => 'cibotaru.liubovi@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cibotaru.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Liubovi',
            'last_name' => 'Cibotaru',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 75
       */
        $teacher = User::create([
            'name' => 'cibric.iurie',
            'email' => 'cibric.iurie@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cibric.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Iurie',
            'last_name' => 'Cibric',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 76
       */
        $teacher = User::create([
            'name' => 'ciobanu.iraida',
            'email' => 'ciobanu.iraida@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ciobanu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Iraida',
            'last_name' => 'Ciobanu',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 77
       */
        $teacher = User::create([
            'name' => 'ciobanu.adriana',
            'email' => 'ciobanu.adriana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ciobanu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Adriana',
            'last_name' => 'Ciobanu',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 78
       */
        $teacher = User::create([
            'name' => 'ciobanu.valentina',
            'email' => 'ciobanu.valentina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ciobanu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Valentina',
            'last_name' => 'Ciobanu',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 79
       */
        $teacher = User::create([
            'name' => 'ciobanu.lilia',
            'email' => 'ciobanu.lilia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ciobanu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Lilia',
            'last_name' => 'Ciobanu',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 80
       */
        $teacher = User::create([
            'name' => 'ciorba.constantin',
            'email' => 'ciorba.constantin@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ciorba.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Constantin',
            'last_name' => 'Ciorbă',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 81
       */
        $teacher = User::create([
            'name' => 'cirlan.lilia',
            'email' => 'cirlan.lilia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cirlan.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Lilia',
            'last_name' => 'Cîrlan',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 82
       */
        $teacher = User::create([
            'name' => 'ciubotaru.nicolae',
            'email' => 'ciubotaru.nicolae@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ciubotaru.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Nicolae',
            'last_name' => 'Ciubotaru',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 83
       */
        $teacher = User::create([
            'name' => 'ciubotaru.natalia',
            'email' => 'ciubotaru.natalia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ciubotaru.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Natalia',
            'last_name' => 'Ciubotaru',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 84
       */
        $teacher = User::create([
            'name' => 'cogut.sergiu',
            'email' => 'cogut.sergiu@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cogut.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Sergiu',
            'last_name' => 'Cogut',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 85
       */
        $teacher = User::create([
            'name' => 'cojocari.lidia',
            'email' => 'cojocari.lidia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cojocari.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Lidia',
            'last_name' => 'Cojocari',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 86
       */
        $teacher = User::create([
            'name' => 'cojocaru.elena',
            'email' => 'cojocaru.elena@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cojocaru.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Elena',
            'last_name' => 'Cojocaru',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 87
       */
        $teacher = User::create([
            'name' => 'cojocaru.valentina',
            'email' => 'cojocaru.valentina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cojocaru.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Valentina',
            'last_name' => 'Cojocaru',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 88
       */
        $teacher = User::create([
            'name' => 'cojocaru.vasile',
            'email' => 'cojocaru.vasile@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cojocaru.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Vasile',
            'last_name' => 'Cojocaru',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 90
       */
        $teacher = User::create([
            'name' => 'cojocaru.lidia',
            'email' => 'cojocaru.lidia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cojocaru.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Lidia',
            'last_name' => 'Cojocaru',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 91
       */
        $teacher = User::create([
            'name' => 'colesnic.liubovi',
            'email' => 'colesnic.liubovi@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Colesnic.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Liubovi',
            'last_name' => 'Colesnic',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 92
       */
        $teacher = User::create([
            'name' => 'copacinschi.angela',
            'email' => 'copacinschi.angela@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Copacinschi.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Angela',
            'last_name' => 'Copacinschi',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 93
       */
        $teacher = User::create([
            'name' => 'cosovan.olga',
            'email' => 'cosovan.olga@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cosovan.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Olga',
            'last_name' => 'Cosovan',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 94
       */
        $teacher = User::create([
            'name' => 'covaliova.elena',
            'email' => 'covaliova.elena@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Covaliova.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Elena',
            'last_name' => 'Covaliova',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 95
       */
        $teacher = User::create([
            'name' => 'covaliova.olga',
            'email' => 'covaliova.olga@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Covaliova.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Olga',
            'last_name' => 'Covaliova',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 96
       */
        $teacher = User::create([
            'name' => 'cozman.tatiana',
            'email' => 'cozman.tatiana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cozman.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Tatiana',
            'last_name' => 'Cozman',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 97
       */
        $teacher = User::create([
            'name' => 'crasov.valeria',
            'email' => 'crasov.valeria@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Crasov.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Valeria',
            'last_name' => 'Crasov',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 98
       */
        $teacher = User::create([
            'name' => 'cravcenco.tatiana',
            'email' => 'cravcenco.tatiana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cravcenco.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Tatiana',
            'last_name' => 'Cravcenco',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 99
       */
        $teacher = User::create([
            'name' => 'crivoi.aurelia',
            'email' => 'crivoi.aurelia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Crivoi.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Aurelia',
            'last_name' => 'Crivoi',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 100
       */
        $teacher = User::create([
            'name' => 'croitoru.ion',
            'email' => 'croitoru.ion@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Croitoru.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ion',
            'last_name' => 'Croitoru',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 100
       */
        $teacher = User::create([
            'name' => 'cucereavii.adrian',
            'email' => 'cucereavii.adrian@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cucereavii.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Adrian',
            'last_name' => 'Cucereavîi',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 101
       */
        $teacher = User::create([
            'name' => 'culea.stelian',
            'email' => 'culea.stelian@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Culea.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Stelian',
            'last_name' => 'Culea',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 102
       */
        $teacher = User::create([
            'name' => 'culea.uliana',
            'email' => 'culea.uliana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Culea.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Uliana',
            'last_name' => 'Culea',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 103
       */
        $teacher = User::create([
            'name' => 'curacitchi.angela',
            'email' => 'curacitchi.angela@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Curacitchi.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Angela',
            'last_name' => 'Curacițchi',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 104
       */
        $teacher = User::create([
            'name' => 'Cusca.valentin',
            'email' => 'Cusca.valentin@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cusca.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Valentin',
            'last_name' => 'Cușcă',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 105
       */
        $teacher = User::create([
            'name' => 'cusco.andrei',
            'email' => 'cusco.andrei@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cusco.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Andrei',
            'last_name' => 'Cușco',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 106
       */
        $teacher = User::create([
            'name' => 'cuznetov.larisa',
            'email' => 'cuznetov.larisa@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cuznetov.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Larisa',
            'last_name' => 'Cuznețov',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
       * 107
       */
        $teacher = User::create([
            'name' => 'damian.nadejda',
            'email' => 'damian.nadejda@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Damian.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Nadejda',
            'last_name' => 'Damian',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       * 108
       */
        $teacher = User::create([
            'name' => 'danilescu.tatiana',
            'email' => 'danilescu.tatiana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Danilescu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Tatiana',
            'last_name' => 'Danilescu',
            'phone' => '0',
            'mobile' => '0'
        ]);

        /*
        *
        */
        $teacher = User::create([
            'name' => 'darii.gabriela',
            'email' => 'darii.gabriela@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Darii.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Gabriela',
            'last_name' => 'Darii',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'davidescu.elena',
            'email' => 'davidescu.elena@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Davidescu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Elena',
            'last_name' => 'Davidescu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'denisov.emilia',
            'email' => 'denisov.emilia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Denisov.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Emilia',
            'last_name' => 'Denisov',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'dermenji.svetlana',
            'email' => 'dermenji.svetlana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Dermenji.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Svetlana',
            'last_name' => 'Dermenji',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'dita.marcela',
            'email' => 'dita.marcela@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Dita.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Marcela',
            'last_name' => 'Diţa',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'donic.liubovi',
            'email' => 'donic.liubovi@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Donic.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Liubovi',
            'last_name' => 'Donic',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'donica.ala',
            'email' => 'donica.ala@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Donica.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ala',
            'last_name' => 'Donică',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'donoaga.diana',
            'email' => 'donoaga.diana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Donoaga.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Diana',
            'last_name' => 'Donoagă',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'dorul.violeta',
            'email' => 'dorul.violeta@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Dorul.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Violeta',
            'last_name' => 'Dorul',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'dubineanschi.tatiana',
            'email' => 'dubineanschi.tatiana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Dubineanschi.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Tatiana',
            'last_name' => 'Dubineanschi',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'dumbraveanu.roza',
            'email' => 'dumbraveanu.roza@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Dumbraveanu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Roza',
            'last_name' => 'Dumbraveanu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'dumitru.diana',
            'email' => 'dumitru.diana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Dumitru.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Diana',
            'last_name' => 'Dumitru',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'dvornic.mihaela',
            'email' => 'dvornic.mihaela@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Dvornic.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Mihaela',
            'last_name' => 'Dvornic',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'enachi.valentina',
            'email' => 'enachi.valentina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Enachi.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Valentina',
            'last_name' => 'Enachi',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'ermurache.alexandru',
            'email' => 'ermurache.alexandru@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ermurache.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Alexandru',
            'last_name' => 'Ermurache',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'etco.irina',
            'email' => 'etco.irina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Etco.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Irina',
            'last_name' => 'Ețco',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'fedorcenco.zinaida',
            'email' => 'fedorcenco.zinaida@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Fedorcenco.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Zinaida',
            'last_name' => 'Fedorcenco',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'fisticanu.veaceslav',
            'email' => 'fisticanu.veaceslav@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Fisticanu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Veaceslav',
            'last_name' => 'Fisticanu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'francarli.lorella',
            'email' => 'francarli.lorella@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Francarli.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Lorella',
            'last_name' => 'Francarli',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'frunze.alesea',
            'email' => 'frunze.alesea@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Frunze.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Alesea',
            'last_name' => 'Frunze',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'galusca.Lilia',
            'email' => 'galusca.lilia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Galusca.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Lilia',
            'last_name' => 'Gălușcă',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'gangan.olesea',
            'email' => 'gangan.olesea@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Gangan.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Olesea',
            'last_name' => 'Gangan',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
    $teacher = User::create([
        'name' => 'garstea.nina',
        'email' => 'garstea.nina@upsc.md',
        'email_verified_at' => Carbon::now(),
        'password' => Hash::make('Garstea.2021')
    ]);
    $teacher->assignRole('teacher');
    foreach ($teacherPermissions as $permission) {
        $teacher->givePermissionTo($permission->name);
    }
    Teacher::create([
        'user_id' => $teacher->id,
        'teacher_rank_id' => 1,
        'first_name' => 'Nina',
        'last_name' => 'Garștea',
        'phone' => '0',
        'mobile' => '0'
    ]);
    /*
    *
    */
        $teacher = User::create([
            'name' => 'ghedrovici.olesea',
            'email' => 'ghedrovici.olesea@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ghedrovici.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Olesea',
            'last_name' => 'Ghedrovici',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'gheorghita.cezara',
            'email' => 'gheorghita.cezara@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Gheorghita.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Cezara',
            'last_name' => 'Gheorghiță',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'gherlovan.olga',
            'email' => 'gherlovan.olga@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Gherlovan.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Olga',
            'last_name' => 'Gherlovan',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'gherman.alexei',
            'email' => 'gherman.alexei@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Gherman.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Alexei',
            'last_name' => 'Gherman',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'gherman.oxana',
            'email' => 'gherman.oxana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Gherman.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Oxana',
            'last_name' => 'Gherman',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'ghicov.adrian',
            'email' => 'ghicov.adrian@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ghicov.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Adrian',
            'last_name' => 'Ghicov',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'ghilan.zinaida',
            'email' => 'ghilan.zinaida@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ghilan.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Zinaida',
            'last_name' => 'Ghilan',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'ginju.stela',
            'email' => 'ginju.stela@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ginju.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Stela',
            'last_name' => 'Gînju',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'ginju.gheorghe',
            'email' => 'ginju.gheorghe@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ginju.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Gheorghe',
            'last_name' => 'Gînju',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'glavan.aurelia',
            'email' => 'glavan.aurelia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Glavan.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Aurelia',
            'last_name' => 'Glavan',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'gobjila.ana',
            'email' => 'gobjila.ana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Gobjila.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ana',
            'last_name' => 'Gobjila',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'gogu.tamara',
            'email' => 'gogu.tamara@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Gogu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Tamara',
            'last_name' => 'Gogu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'golubitchi.silvia',
            'email' => 'golubitchi.silvia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Golubițchi.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Silvia',
            'last_name' => 'Golubițchi',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'golubovschi.oxana',
            'email' => 'golubovschi.oxana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Golubovschi.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Oxana',
            'last_name' => 'Golubovschi',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'gonta.victoria',
            'email' => 'gonta.victoria@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Gonta.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Victoria',
            'last_name' => 'Gonța',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'goras.viorica',
            'email' => 'goras.viorica@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Goras.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Viorica',
            'last_name' => 'Goraș',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'gordea.liliana',
            'email' => 'gordea.liliana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Gordea.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Liliana',
            'last_name' => 'Gordea',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'gorscova.marina',
            'email' => 'gorscova.marina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Gorscova.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Marina',
            'last_name' => 'Gorșcova',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'gozun.petru',
            'email' => 'gozun.petru@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Gozun.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Petru',
            'last_name' => 'Gozun',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'gozun.svetlana',
            'email' => 'gozun.svetlana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Gozun.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Svetlana',
            'last_name' => 'Gozun',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'grama.vasile',
            'email' => 'grama.vasile@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Grama.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Vasile',
            'last_name' => 'Grama',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'grati.aliona',
            'email' => 'grati.aliona@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Grati.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Aliona',
            'last_name' => 'Grati',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'grecu.jana',
            'email' => 'grecu.jana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Grecu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Jana',
            'last_name' => 'Grecu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'grosu.mihail',
            'email' => 'grosu.mihail@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Grosu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Mihail',
            'last_name' => 'Grosu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'grosu.olga',
            'email' => 'grosu.olga@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Grosu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Olga',
            'last_name' => 'Grosu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'gudumac.svetlana',
            'email' => 'gudumac.svetlana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Gudumac.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Svetlana',
            'last_name' => 'Gudumac',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'gutu.adela',
            'email' => 'gutu.adela@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Gutu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Adela',
            'last_name' => 'Guțu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'guzun.maria',
            'email' => 'guzun.maria@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Guzun.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Maria',
            'last_name' => 'Guzun',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'haheu.efrosinia',
            'email' => 'haheu.efrosinia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Haheu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Efrosinia',
            'last_name' => 'Haheu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'herta.valeriu',
            'email' => 'herta.valeriu@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Herta.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Valeriu',
            'last_name' => 'Herța',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'herta.lilia',
            'email' => 'herta.lilia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Herta.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Lilia',
            'last_name' => 'Herța',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'ilascu.iurie',
            'email' => 'ilascu.iurie@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ilascu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Iurie',
            'last_name' => 'Ilaşcu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'isac.ina',
            'email' => 'isac.ina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Isac.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ina',
            'last_name' => 'Isac',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'jelescu.petru',
            'email' => 'jelescu.petru@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Jelescu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Petru',
            'last_name' => 'Jelescu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'karachentseva.elena',
            'email' => 'karachentseva.elena@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Karachentseva.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Elena',
            'last_name' => 'Karachentseva',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'karaia.inga',
            'email' => 'karaia.inga@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Karaia.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Inga',
            'last_name' => 'Karaia',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'lagaeva.tatiana',
            'email' => 'lagaeva.tatiana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Lagaeva.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Tatiana',
            'last_name' => 'Lagaeva',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'laposina.emilia',
            'email' => 'laposina.emilia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Laposina.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Emilia',
            'last_name' => 'Lapoşina',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'leahu.alexandru',
            'email' => 'leahu.alexandru@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Leahu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Alexandru',
            'last_name' => 'Leahu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'leu.natalia',
            'email' => 'leu.natalia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Leu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Natalia',
            'last_name' => 'Leu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'lisnic.angela',
            'email' => 'lisnic.angela@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Lisnic.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Angela',
            'last_name' => 'Lisnic',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'livitchi.cristina',
            'email' => 'livitchi.cristina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Livitchi.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Cristina',
            'last_name' => 'Livițchi',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'losii.elena',
            'email' => 'losii.elena@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Losii.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Elena',
            'last_name' => 'Losîi',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'lupascu.lilia',
            'email' => 'lupascu.lilia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Lupascu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Lilia',
            'last_name' => 'Lupașcu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'lupu.ala',
            'email' => 'lupu.ala@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Lupu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ala',
            'last_name' => 'Lupu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'macari.pavel',
            'email' => 'macari.pavel@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Macari.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Pavel',
            'last_name' => 'Macari',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'malcoci.vitalie',
            'email' => 'malcoci.vitalie@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Malcoci.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Vitalie',
            'last_name' => 'Malcoci',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'malicova.valentina',
            'email' => 'malicova.valentina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Malicova.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Valentina',
            'last_name' => 'Malîcova',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'mardari.alina',
            'email' => 'mardari.alina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Mardari.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Alina',
            'last_name' => 'Mardari',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'marin.mariana',
            'email' => 'marin.mariana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Marin.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Mariana',
            'last_name' => 'Marin',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'matiet.violeta',
            'email' => 'matiet.violeta@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Matiet.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Violeta',
            'last_name' => 'Matieț',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'matran.tatiana',
            'email' => 'matran.tatiana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Matran.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Tatiana',
            'last_name' => 'Matran',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'maxim.vasile',
            'email' => 'maxim.vasile@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Maxim.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'vasile',
            'last_name' => 'Maxim',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'maximciuc.victoria',
            'email' => 'maximciuc.victoria@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Maximciuc.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Victoria',
            'last_name' => 'Maximciuc',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'melinte.veronica',
            'email' => 'melinte.veronica@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Melinte.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Veronica',
            'last_name' => 'Melinte',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'melniciuc.radu',
            'email' => 'melniciuc.radu@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Melniciuc.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Radu',
            'last_name' => 'Melniciuc',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'mirza.iulia',
            'email' => 'mirza.iulia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Mirza.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Iulia',
            'last_name' => 'Mîrza',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'mocanu.liuba',
            'email' => 'mocanu.liuba@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Mocanu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Liuba',
            'last_name' => 'Mocanu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'moisei.ludmila',
            'email' => 'moisei.ludmila@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Moisei.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ludmila',
            'last_name' => 'Moisei',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'mokan-vozian.ludmila',
            'email' => 'mokan-vozian.ludmila@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Mokan-vozian.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ludmila',
            'last_name' => 'Mokan-Vozian',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'morarescu.ion',
            'email' => 'morarescu.ion@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Morarescu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ion',
            'last_name' => 'Morărescu ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'munteanu.andrei',
            'email' => 'munteanu.andrei@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Munteanu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Andrei',
            'last_name' => 'Munteanu ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'munteanu.octavian',
            'email' => 'munteanu.octavian@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Munteanu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Octavian',
            'last_name' => 'Munteanu ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */

       $teacher = User::create([
           'name' => 'musinschi.ilie',
           'email' => 'musinschi.ilie@upsc.md',
           'email_verified_at' => Carbon::now(),
           'password' => Hash::make('Musinschi.2021')
       ]);
       $teacher->assignRole('teacher');
       foreach ($teacherPermissions as $permission) {
           $teacher->givePermissionTo($permission->name);
       }
       Teacher::create([
           'user_id' => $teacher->id,
           'teacher_rank_id' => 1,
           'first_name' => 'Ilie',
           'last_name' => 'Mușinschi ',
           'phone' => '0',
           'mobile' => '0'
       ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'musteata.sergiu',
            'email' => 'musteata.sergiu@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Musteata.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Sergiu',
            'last_name' => 'Musteață ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'musteata.dumitru',
            'email' => 'musteata.dumitru@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Musteata.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Dumitru',
            'last_name' => 'Musteață ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'nazaru.cristina',
            'email' => 'nazaru.cristina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Nazaru.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Cristina',
            'last_name' => 'Nazaru ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'neaga.lilia',
            'email' => 'neaga.lilia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Neaga.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Lilia',
            'last_name' => 'Neaga ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'neagu.natalia',
            'email' => 'neagu.natalia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Neagu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Natalia',
            'last_name' => 'Neagu ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'necula.gina',
            'email' => 'necula.gina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Necula.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Gina',
            'last_name' => 'Necula ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'neculagina.aurora',
            'email' => 'neculagina.aurora@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Neculagina.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Aurora',
            'last_name' => 'Neculagina ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'niculita.gheorghe',
            'email' => 'niculita.gheorghe@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Niculita.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Gheorghe',
            'last_name' => 'Niculiță ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'noroc.larisa',
            'email' => 'noroc.larisa@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Noroc.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Larisa',
            'last_name' => 'Noroc ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'oboroceanu.veronica',
            'email' => 'oboroceanu.veronica@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Oboroceanu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Veronica',
            'last_name' => 'Oboroceanu ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'oganisean.diana',
            'email' => 'oganisean.diana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Oganisean.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Diana',
            'last_name' => 'Oganisean ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'ohrimenco.aliona',
            'email' => 'ohrimenco.aliona@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ohrimenco.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Aliona',
            'last_name' => 'Ohrimenco ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'olarescu.valentina',
            'email' => 'olarescu.valentina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Olarescu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Valentina',
            'last_name' => 'Olărescu ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'oloieru.anastasia',
            'email' => 'oloieru.anastasia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Oloieru.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Anastasia',
            'last_name' => 'Oloieru ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'palade.eugen',
            'email' => 'palade.eugen@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Palade.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Eugen',
            'last_name' => 'Palade ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'papuc.ludmila',
            'email' => 'papuc.ludmila@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Papuc.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ludmila',
            'last_name' => 'Papuc ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'patrascu.dumitru',
            'email' => 'patrascu.dumitru@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Patrascu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Dumitru',
            'last_name' => 'Patrașcu ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'pavlenco.mihaela',
            'email' => 'pavlenco.mihaela@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Pavlenco.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Mihaela',
            'last_name' => 'Pavlenco ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'perjan.carolina',
            'email' => 'perjan.carolina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Perjan.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Carolina',
            'last_name' => 'Perjan ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'petrenco.liuba',
            'email' => 'petrenco.liuba@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Petrenco.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Liuba',
            'last_name' => 'Petrenco ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'petrencu.anatol',
            'email' => 'petrencu.anatol@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Petrencu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Anatol',
            'last_name' => 'Petrencu ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'petriciuc.lilia',
            'email' => 'petriciuc.lilia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Petriciuc.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Lilia',
            'last_name' => 'Petriciuc ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'pirvan.mariana',
            'email' => 'pirvan.mariana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Pirvan.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Mariana',
            'last_name' => 'Pîrvan ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'pislari.stela',
            'email' => 'pislari.stela@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Pislari.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Stela',
            'last_name' => 'Pîslari ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'plamadeala.victoria',
            'email' => 'plamadeala.victoria@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Plamadeala.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Victoria',
            'last_name' => 'Plămădeală ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'platita.mihaela',
            'email' => 'platita.mihaela@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Platita.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Mihaela',
            'last_name' => 'Platița ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'plesca.maria',
            'email' => 'plesca.maria@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Plesca.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Maria',
            'last_name' => 'Pleşca ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'plescenco.galina',
            'email' => 'plescenco.galina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Plescenco.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Galina',
            'last_name' => 'Pleșcenco ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'plosnita.elena',
            'email' => 'plosnita.elena@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Plosnita.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Elena',
            'last_name' => 'Ploșniță ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'ponomari.dorina',
            'email' => 'ponomari.dorina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ponomari.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Dorina',
            'last_name' => 'Ponomari ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'popa.pavel',
            'email' => 'popa.pavel@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Popa.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Pavel',
            'last_name' => 'Popa ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'popa.natalia',
            'email' => 'popa.natalia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Popa.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Natalia',
            'last_name' => 'Popa ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'popa.stefan',
            'email' => 'popa.stefan@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Popa.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ștefan',
            'last_name' => 'Popa ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'popescu.maria',
            'email' => 'popescu.maria@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Popescu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Maria',
            'last_name' => 'Popescu ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'popescu.cristina',
            'email' => 'popescu.cristina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Popescu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Cristina',
            'last_name' => 'Popescu ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'port.sergiu',
            'email' => 'port.sergiu@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Port.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Sergiu',
            'last_name' => 'Port ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'postolache.ana-maria',
            'email' => 'postolache.ana-maria@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Postolache.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ana-Maria',
            'last_name' => 'Postolache ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'prangache.liuba',
            'email' => 'prangache.liuba@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Prangache.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Liuba',
            'last_name' => 'Prangache ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'pricop.victor',
            'email' => 'pricop.victor@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Pricop.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Victor',
            'last_name' => 'Pricop ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'procopii.liuba',
            'email' => 'procopii.liuba@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Procopii.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Liuba',
            'last_name' => 'Procopii ',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'prosii.irina',
            'email' => 'prosii.irina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Prosii.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Irina',
            'last_name' => 'Prosii',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'prus.elena',
            'email' => 'prus.elena@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Prus.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Elena',
            'last_name' => 'Prus',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'puica.elizaveta',
            'email' => 'puica.elizaveta@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Puica.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Elizaveta',
            'last_name' => 'Puică',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'puscasu.ana',
            'email' => 'puscasu.ana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Puscasu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ana',
            'last_name' => 'Pușcașu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'puzdrovschi.iurie',
            'email' => 'puzdrovschi.iurie@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Puzdrovschi.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Iurie',
            'last_name' => 'Puzdrovschi',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'racu.igor',
            'email' => 'racu.igor@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Racu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Igor',
            'last_name' => 'Racu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'racu.iulia',
            'email' => 'racu.iulia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Racu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Iulia',
            'last_name' => 'Racu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'racu.jana',
            'email' => 'racu.jana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Racu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Jana',
            'last_name' => 'Racu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'racu.aurelia',
            'email' => 'racu.aurelia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Racu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Aurelia',
            'last_name' => 'Racu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'raileanu.olga',
            'email' => 'raileanu.olga@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Raileanu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Olga',
            'last_name' => 'Răileanu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'raileanu.veronica',
            'email' => 'raileanu.veronica@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Raileanu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Veronica',
            'last_name' => 'Răileanu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'reabenchi.eugen',
            'email' => 'reabenchi.eugen@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Reabenchi.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Eugen',
            'last_name' => 'Reabenchi',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'rosa.madalena',
            'email' => 'rosa.madalena@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Rosa.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Madalena',
            'last_name' => 'Rosa',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'rosca.ruslan',
            'email' => 'rosca.ruslan@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Rosca.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ruslan',
            'last_name' => 'Roșca',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'rosca.timofei',
            'email' => 'rosca.timofei@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Rosca.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Timofei',
            'last_name' => 'Roșca',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'rosca.daniela',
            'email' => 'rosca.daniela@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Rosca.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Daniela',
            'last_name' => 'Roșca',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'rotaru.maria',
            'email' => 'rotaru.maria@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Rotaru.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Maria',
            'last_name' => 'Rotaru',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'rotaru.elena',
            'email' => 'rotaru.elena@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Rotaru.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Elena',
            'last_name' => 'Rotaru',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'rusu.valentin',
            'email' => 'rusu.valentin@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Rusu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Valentin',
            'last_name' => 'Rusu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'rusu.vasilisa',
            'email' => 'rusu.vasilisa@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Rusu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Vasilisa',
            'last_name' => 'Rusu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'sadovei.larisa',
            'email' => 'sadovei.larisa@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Sadovei.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Larisa',
            'last_name' => 'Sadovei',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'sagoian.eraneac',
            'email' => 'sagoian.eraneac@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Sagoian.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Eraneac',
            'last_name' => 'Sagoian',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'sajin.natalia',
            'email' => 'sajin.natalia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Sajin.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Natalia',
            'last_name' => 'Sajin',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'sallanz.josef',
            'email' => 'sallanz.josef@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Sallanz.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Josef',
            'last_name' => 'Sallanz',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'samburic.elena',
            'email' => 'samburic.elena@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Samburic.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Elena',
            'last_name' => 'Samburic',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'sanduleac.sergiu',
            'email' => 'sanduleac.sergiu@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Sanduleac.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Sergiu',
            'last_name' => 'Sanduleac',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'sava.anastasia',
            'email' => 'sava.anastasia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Sava.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Anastasia',
            'last_name' => 'Sava',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'sava.igor',
            'email' => 'sava.igor@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Sava.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Igor',
            'last_name' => 'Sava',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'sava.lucia',
            'email' => 'sava.lucia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Sava.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Lucia',
            'last_name' => 'Sava',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'savitchi.corina',
            'email' => 'savitchi.corina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Savitchi.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Corina',
            'last_name' => 'Savițchi',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'schiopu.lucia',
            'email' => 'schiopu.lucia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Schiopu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Lucia',
            'last_name' => 'Șchiopu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'schiopu.constantin',
            'email' => 'schiopu.constantin@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Schiopu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Constantin',
            'last_name' => 'Șchiopu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'schoberl.kathrin',
            'email' => 'schoberl.kathrin@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Schoberl.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Kathrin',
            'last_name' => 'Schoberl',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'scolnii.olga',
            'email' => 'scolnii.olga@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Scolnii.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Olga',
            'last_name' => 'Școlnîi',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'scoric.olga',
            'email' => 'scoric.olga@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Scoric.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Olga',
            'last_name' => 'Scoric',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'simac.ana',
            'email' => 'simac.ana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Simac.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ana',
            'last_name' => 'Simac',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'simcenco.irina',
            'email' => 'simcenco.irina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Simcenco.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Irina',
            'last_name' => 'Simcenco',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'sinitaru.larisa',
            'email' => 'sinitaru.larisa@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Sinitaru.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Larisa',
            'last_name' => 'Sinițaru',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'sirghi.olesea',
            'email' => 'sirghi.olesea@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('sirghi.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Olesea',
            'last_name' => 'Sîrghi',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'smochin.olga',
            'email' => 'smochin.olga@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Smochin.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Olga',
            'last_name' => 'Smochin',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'smolnitchi.dumitrita',
            'email' => 'smolnitchi.dumitrita@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Smolnitchi.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Dumitrița',
            'last_name' => 'Smolnițchi',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'socolova.natalia',
            'email' => 'socolova.natalia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Socolova.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Natalia',
            'last_name' => 'Socolova',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'solcan.angela',
            'email' => 'solcan.angela@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Solcan.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Angela',
            'last_name' => 'Solcan',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'spataru.rodica',
            'email' => 'spataru.rodica@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Spataru.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Rodica',
            'last_name' => 'Spataru',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'stefanita.ion',
            'email' => 'stefanita.ion@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Stefanita.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ion',
            'last_name' => 'Ștefăniță',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'stempovschi.elena',
            'email' => 'stempovschi.elena@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Stempovschi.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Elena',
            'last_name' => 'Stempovschi',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'stirbu.corina',
            'email' => 'stirbu.corina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Stirbu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Corina',
            'last_name' => 'Știrbu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'stratan.valentina',
            'email' => 'stratan.valentina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Stratan.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Valentina',
            'last_name' => 'Stratan',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'sugjda.svetlana',
            'email' => 'sugjda.svetlana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Sugjda.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Svetlana',
            'last_name' => 'Șugjda',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'surugiu.dorina',
            'email' => 'surugiu.dorina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Surugiu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Dorina',
            'last_name' => 'Surugiu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'tabac.sergiu',
            'email' => 'tabac.sergiu@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Tabac.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Sergiu',
            'last_name' => 'Tabac',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'talpa.svetlana',
            'email' => 'talpa.svetlana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Talpa.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Svetlana',
            'last_name' => 'Talpă',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'tap.elena',
            'email' => 'tap.elena@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Tap.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Elena',
            'last_name' => 'Țap',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'tapu.ion',
            'email' => 'tapu.ion@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Tapu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ion',
            'last_name' => 'Țapu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'taralunga.boris',
            'email' => 'taralunga.boris@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Taralunga.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Boris',
            'last_name' => 'Țarălungă',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'tarita.victor',
            'email' => 'tarita.victor@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Tarita.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Victor',
            'last_name' => 'Tărîţă',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'tarna.ecaterina',
            'email' => 'tarna.ecaterina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Tarna.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ecaterina',
            'last_name' => 'Țărnă',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'tataru.nina',
            'email' => 'tataru.nina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Tataru.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Nina',
            'last_name' => 'Tataru',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'tataru.angela',
            'email' => 'tataru.angela@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Tataru.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Angela',
            'last_name' => 'Tataru',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'teleman.angela',
            'email' => 'teleman.angela@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Teleman.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Angela',
            'last_name' => 'Teleman',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'ticu.lucia',
            'email' => 'ticu.lucia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ticu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Lucia',
            'last_name' => 'Țîcu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'timbaliuc.Elena',
            'email' => 'timbaliuc.elena@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Timbaliuc.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Elena',
            'last_name' => 'Țîmbaliuc',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'timus.olga',
            'email' => 'timus.olga@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Timus.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Olga',
            'last_name' => 'Timuș',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'tiosa.iuliana',
            'email' => 'tiosa.iuliana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Tiosa.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Iuliana',
            'last_name' => 'Tiosa',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'tiron.olga',
            'email' => 'tiron.olga@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Tiron.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Olga',
            'last_name' => 'Tiron',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'topor.gabriella',
            'email' => 'topor.gabriella@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Topor.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Gabriella',
            'last_name' => 'Topor',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'trifan.veronica',
            'email' => 'trifan.veronica@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Trifan.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Veronica',
            'last_name' => 'Trifan',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'trifan.anastasia',
            'email' => 'trifan.anastasia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Trifan.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Anastasia',
            'last_name' => 'Trifan',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'tulea.alexei',
            'email' => 'tulea.alexei@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Tulea.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Alexei',
            'last_name' => 'Țulea',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'turcan.tosia',
            'email' => 'turcan.tosia@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Turcan.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Tosia',
            'last_name' => 'Turcan',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'turcanu.ludmila',
            'email' => 'turcanu.ludmila@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Turcanu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ludmila',
            'last_name' => 'Țurcanu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'tverdohleb.igor',
            'email' => 'tverdohleb.igor@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Tverdohleb.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Igor',
            'last_name' => 'Tverdohleb',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'tvic.irina',
            'email' => 'tvic.irina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Tvic.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Irina',
            'last_name' => 'Țvic',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'untila.tatiana',
            'email' => 'untila.tatiana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Untila.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Tatiana',
            'last_name' => 'Untilă',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'untila.victor',
            'email' => 'untila.victor@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Untila.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Victor',
            'last_name' => 'Untilă',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'ursachi.rodica',
            'email' => 'ursachi.rodica@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ursachi.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Rodica',
            'last_name' => 'Ursachi',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'ursu.valentina',
            'email' => 'ursu.valentina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ursu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Valentina',
            'last_name' => 'Ursu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'ursu.ludmila',
            'email' => 'ursu.ludmila@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Ursu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ludmila',
            'last_name' => 'Ursu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'us.vladimir',
            'email' => 'us.vladimir@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Us.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Vladimir',
            'last_name' => 'Us',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'usaci.doina',
            'email' => 'usaci.doina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Usaci.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Doina',
            'last_name' => 'Usaci',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'usatii.larisa',
            'email' => 'usatii.larisa@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Usatii.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Larisa',
            'last_name' => 'Usatîi',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'vacarciuc.mariana',
            'email' => 'vacarciuc.mariana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Vacarciuc.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Mariana',
            'last_name' => 'Vacarciuc',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'vasilache.ana',
            'email' => 'vasilache.ana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Vasilache.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ana',
            'last_name' => 'Vasilache',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'vasilache.andrei',
            'email' => 'vasilache.andrei@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Vasilache.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Andrei',
            'last_name' => 'Vasilache',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'vatavu.alexandru',
            'email' => 'vatavu.alexandru@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Vatavu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Alexandru',
            'last_name' => 'Vatavu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'versteac.vasile',
            'email' => 'versteac.vasile@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Versteac.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Vasile',
            'last_name' => 'Versteac',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'vilcu.marcela',
            'email' => 'vilcu.marcela@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Vilcu.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Marcela',
            'last_name' => 'Vîlcu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'virlan.maria',
            'email' => 'virlan.maria@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Virlan.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Maria',
            'last_name' => 'Vîrlan',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'vitcovschi.ala',
            'email' => 'vitcovschi.ala@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Vitcovschi.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ala',
            'last_name' => 'Vitcovschi',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'vozian.vasile',
            'email' => 'vozian.vasile@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Vozian.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Vasile',
            'last_name' => 'Vozian',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'yavuz.tatiana',
            'email' => 'yavuz.tatiana@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Yavuz.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Tatiana',
            'last_name' => 'Yavuz',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'zagaievschi.corina',
            'email' => 'zagaievschi.corina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Zagaievschi.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Corina',
            'last_name' => 'Zagaievschi',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'zaharia.viorica',
            'email' => 'zaharia.viorica@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Zaharia.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Viorica',
            'last_name' => 'Zaharia',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'zamsa.simion',
            'email' => 'zamsa.simion@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Zamsa.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Simion',
            'last_name' => 'Zamșa',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
       *
       */
        $teacher = User::create([
            'name' => 'zderciuc.ion',
            'email' => 'zderciuc.ion@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Zderciuc.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ion',
            'last_name' => 'Zderciuc',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
        *
        */
        $teacher = User::create([
            'name' => 'zgardan.aliona',
            'email' => 'zgardan.aliona@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Zgardan.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Aliona',
            'last_name' => 'Zgardan-Crudu',
            'phone' => '0',
            'mobile' => '0'
        ]);
        /*
         *
         */
        $teacher = User::create([
            'name' => 'zubenschi.ecaterina',
            'email' => 'zubenschi.ecaterina@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Zubenschi.2021')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Ecaterina',
            'last_name' => 'Zubenschi',
            'phone' => '0',
            'mobile' => '0'
        ]);
    }

}
