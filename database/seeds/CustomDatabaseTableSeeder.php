<?php

use App\Entities\Database;
use Illuminate\Database\Seeder;

class CustomDatabaseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Database::create([
            'name' => 'Admiterea 2018',
            'year' => '2019',
            'server_ip' => '81.180.65.149',
            'database_name' => 'upscmd_admitereadb2018',
            'connection_name' => 'admiterea_2018',
            'username' => '************',
            'password' => '************',
            'port' => '3306'
        ]);

        Database::create([
            'name' => 'Admiterea 2019',
            'year' => '2019',
            'server_ip' => '81.180.65.149',
            'database_name' => 'upscmd_admitereadb2019',
            'connection_name' => 'admiterea_2019',
            'username' => '************',
            'password' => '************',
            'port' => '3306'
        ]);

        Database::create([
            'name' => 'Admiterea 2020',
            'year' => '2020',
            'server_ip' => '81.180.65.149',
            'database_name' => 'upscmd_admitereadb2020',
            'connection_name' => 'admiterea_2020',
            'username' => '************',
            'password' => '************',
            'port' => '3306'
        ]);

        Database::create([
            'name' => 'Admiterea 2021',
            'year' => '2021',
            'server_ip' => '81.180.65.149',
            'database_name' => 'upscmd_admitereadb2021',
            'connection_name' => 'admiterea_2021',
            'username' => '************',
            'password' => '************',
            'port' => '3306'
        ]);
    }
}
