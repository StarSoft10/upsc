<?php

use App\Entities\Dean;
use App\Entities\User;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DeansTableSeeder extends Seeder
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $deanPermissions = $this->helper->getAllowedPermissionsByModel('dean');
        /*
        * First
        */
        $dean1 = User::create([
            'name' => 'decan.fapd',
            'email' => 'arte_design@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Decan.2021')
        ]);
        $dean1->assignRole('dean');
        foreach ($deanPermissions as $permission) {
            $dean1->givePermissionTo($permission->name);
        }
        Dean::create([
            'user_id' => $dean1->id,
            'first_name' => 'Ana',
            'last_name' => 'Simac',
            'phone' => '+37369189900'
        ]);
        /*
         * Second
         */
        $dean2 = User::create([
            'name' => 'decan.ffi',
            'email' => 'ffi@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Decan.2021')
        ]);
        $dean2->assignRole('dean');
        foreach ($deanPermissions as $permission) {
            $dean2->givePermissionTo($permission->name);
        }
        Dean::create([
            'user_id' => $dean2->id,
            'first_name' => 'Gabriella',
            'last_name' => 'Topor',
            'phone' => '+37379568313'
        ]);
        /*
         * Third
         */
        $dean3 = User::create([
            'name' => 'decan.flls',
            'email' => 'decan.flls@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Decan.2021')
        ]);
        $dean3->assignRole('dean');
        foreach ($deanPermissions as $permission) {
            $dean3->givePermissionTo($permission->name);
        }
        Dean::create([
            'user_id' => $dean3->id,
            'first_name' => 'Ana',
            'last_name' => 'Budnic',
            'phone' => '+37369795824'
        ]);
        /*
         * Fourth
         */
        $dean4 = User::create([
            'name' => 'decan.fpps',
            'email' => 'decan.fpps@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Decan.2021')
        ]);
        $dean4->assignRole('dean');
        foreach ($deanPermissions as $permission) {
            $dean4->givePermissionTo($permission->name);
        }
        Dean::create([
            'user_id' => $dean4->id,
            'first_name' => 'Maria',
            'last_name' => 'Vîrlan',
            'phone' => '+37368659666'
        ]);

        /*
         * Fifth
         */
        $dean5 = User::create([
            'name' => 'decan.fsei',
            'email' => 'decan.fsei@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Decan.2021')
        ]);
        $dean5->assignRole('dean');
        foreach ($deanPermissions as $permission) {
            $dean5->givePermissionTo($permission->name);
        }
        Dean::create([
            'user_id' => $dean5->id,
            'first_name' => 'Nina',
            'last_name' => 'Garștea',
            'phone' => '+37369555317'
        ]);
    }
}
