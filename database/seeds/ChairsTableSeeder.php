<?php

use App\Entities\Chair;
use App\Entities\User;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class ChairsTableSeeder extends Seeder
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $chairPermissions = $this->helper->getAllowedPermissionsByModel('chair');

        /*
        * First
        */
        $chair = User::create([
            'name' => 'sef-catedra1',
            'email' => '6@mail.ru',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('123456')
        ]);
        $chair->assignRole('chair');
        foreach ($chairPermissions as $permission) {
            $chair->givePermissionTo($permission->name);
        }
        Chair::create([
            'user_id' => $chair->id,
            'first_name' => 'Șef catedră 1',
            'last_name' => 'Șef catedră 1',
            'phone' => '123123'
        ]);


        /*
         * Second
         */
    }
}
