<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'superadmin']);
        Role::create(['name' => 'admin']);
        Role::create(['name' => 'rector']);
        Role::create(['name' => 'secretary']);
        Role::create(['name' => 'dean']);
        Role::create(['name' => 'chair']);
        Role::create(['name' => 'human-resource']);
        Role::create(['name' => 'teacher']);
        Role::create(['name' => 'student']);
    }
}
