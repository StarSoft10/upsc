<?php

use App\Entities\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class SuperAdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $allPermissions = Permission::all();

        /*
         * First
         */
        $superAdmin = User::create([
            'name' => 'ILUSHKA',
            'email' => '1@mail.ru',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('123456')
        ]);
        $superAdmin->assignRole('superadmin');
        foreach ($allPermissions as $permission) {
            $superAdmin->givePermissionTo($permission->name);
        }
        /*
         * Second
         */
        $superAdmin2 = User::create([
            'name' => 'bulicanu.ion',
            'email' => 'bulicanu.ion@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Bulicanu.2021')
        ]);
        $superAdmin2->assignRole('superadmin');
        foreach ($allPermissions as $permission) {
            $superAdmin2->givePermissionTo($permission->name);
        }
    }
}
