<?php

use App\Entities\Chair;
use App\Entities\Dean;
use App\Entities\HumanResource;
use App\Entities\Rector;
use App\Entities\Secretary;
use App\Entities\Student;
use App\Entities\Teacher;
use App\Entities\User;
use App\Entities\Admin;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

class UsersTableSeeder extends Seeder
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $allPermissions = Permission::all();
        $adminPermissions = $this->helper->getAllowedPermissionsByModel('admin');
        $rectorPermissions = $this->helper->getAllowedPermissionsByModel('rector');
        $secretaryPermissions = $this->helper->getAllowedPermissionsByModel('secretary');
        $deanPermissions = $this->helper->getAllowedPermissionsByModel('dean');
        $chairPermissions = $this->helper->getAllowedPermissionsByModel('chair');
        $humanResourcePermissions = $this->helper->getAllowedPermissionsByModel('human-resource');
        $teacherPermissions = $this->helper->getAllowedPermissionsByModel('teacher');
        $studentPermissions = $this->helper->getAllowedPermissionsByModel('student');

        //Super Admin
        $superAdmin = User::create([
            'name' => 'ILUSHKA',
            'email' => '1@mail.ru',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('123456')
        ]);
        $superAdmin->assignRole('superadmin');
        foreach ($allPermissions as $permission) {
            $superAdmin->givePermissionTo($permission->name);
        }


        //Admin
        $admin = User::create([
            'name' => 'Admin 1',
            'email' => '2@mail.ru',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('123456')
        ]);
        $admin->assignRole('admin');
        foreach ($adminPermissions as $permission) {
            $admin->givePermissionTo($permission->name);
        }
        Admin::create([
            'user_id' => $admin->id,
            'first_name' => 'Admin nr.1',
            'last_name' => 'Admin nr.1',
            'phone' => '123123'
        ]);


        //Rector
        $rector = User::create([
            'name' => 'Rector 1',
            'email' => '3@mail.ru',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('123456')
        ]);
        $rector->assignRole('rector');
        foreach ($rectorPermissions as $permission) {
            $rector->givePermissionTo($permission->name);
        }
        Rector::create([
            'user_id' => $rector->id,
            'first_name' => 'Rector nr.1',
            'last_name' => 'Rector nr.1',
            'phone' => '123123'
        ]);


        //Secretary
        $secretary = User::create([
            'name' => 'Secretary nr.1',
            'email' => '4@mail.ru',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('123456')
        ]);
        $secretary->assignRole('secretary');
        foreach ($secretaryPermissions as $permission) {
            $secretary->givePermissionTo($permission->name);
        }
        Secretary::create([
            'user_id' => $secretary->id,
            'first_name' => 'Secretary nr.1',
            'last_name' => 'Secretary nr.1',
            'phone' => '123123'
        ]);


        //Dean
        $dean = User::create([
            'name' => 'Dean nr.1',
            'email' => '5@mail.ru',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('123456')
        ]);
        $dean->assignRole('dean');
        foreach ($deanPermissions as $permission) {
            $dean->givePermissionTo($permission->name);
        }
        Dean::create([
            'user_id' => $dean->id,
            'first_name' => 'Dean nr.1',
            'last_name' => 'Dean nr.1',
            'phone' => '123123'
        ]);


        //Chair
        $chair = User::create([
            'name' => 'Chair nr.1',
            'email' => '6@mail.ru',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('123456')
        ]);
        $chair->assignRole('chair');
        foreach ($chairPermissions as $permission) {
            $chair->givePermissionTo($permission->name);
        }
        Chair::create([
            'user_id' => $chair->id,
            'first_name' => 'Chair nr.1',
            'last_name' => 'Chair nr.1',
            'phone' => '123123'
        ]);


        //Human Resource
        $humanResource = User::create([
            'name' => 'HR nr.1',
            'email' => '7@mail.ru',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('123456')
        ]);
        $humanResource->assignRole('human-resource');
        foreach ($humanResourcePermissions as $permission) {
            $humanResource->givePermissionTo($permission->name);
        }
        HumanResource::create([
            'user_id' => $humanResource->id,
            'first_name' => 'HR nr.1',
            'last_name' => 'HR nr.1',
            'phone' => '123123'
        ]);


        //Teacher
        $teacher = User::create([
            'name' => 'Teacher nr.1',
            'email' => '8@mail.ru',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('123456')
        ]);
        $teacher->assignRole('teacher');
        foreach ($teacherPermissions as $permission) {
            $teacher->givePermissionTo($permission->name);
        }
        Teacher::create([
            'user_id' => $teacher->id,
            'teacher_rank_id' => 1,
            'first_name' => 'Teacher nr.1',
            'last_name' => 'Teacher nr.1',
            'phone' => '123123',
            'mobile' => '987654321'
        ]);


        //Student
        $student = User::create([
            'name' => 'Cebotari Mihai',
            'email' => '9@mail.ru',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make(123456)
        ]);
        $student->assignRole('student');
        foreach ($studentPermissions as $permission) {
            $student->givePermissionTo($permission->name);
        }

        Student::create([
            'user_id' => $student->id,
            'cycle_id' => 1,
            'faculty_id' => 1,
            'speciality_id' => 1,
            'group_id' => 1,
            'year_of_study_id' => 3,
            'first_name' => 'Mihai',
            'last_name' => 'Cebotari',
            'patronymic' => 'Igor',
            'idnp' => '12313',
            'gender' => 'M',
            'phone' => '12313',
            'mobile' => '12313',
            'birth_date' => '10/10/1991',
            'city' => 'Chisinau',
            'address' => 'address',
            'district_code' => '123',
            'insurance_policy' => 1,
            'citizenship' => 'MD',
            'nationality' => 'MD',
            'year_of_admission' => '2021',
            'stayed' => 1,
            'carnet_number' => '12313123',
            'graduate_institution' => 'Liceu',
            'act_seriously_study' => 'AB',
            'act_number_study' => '321654',
            'act_graduation_year' => '2020',
            'study' => 1,
            'budget' => 0,
            'expelled' => 0
        ]);
    }
}
