<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissionsForUsers = [
            'create_rector',
            'edit_rector',
            'delete_rector',
            'view_rector',

            'create_secretary',
            'edit_secretary',
            'delete_secretary',
            'view_secretary',

            'create_dean',
            'edit_dean',
            'delete_dean',
            'view_dean',

            'create_chair',
            'edit_chair',
            'delete_chair',
            'view_chair',

            'create_human_resource',
            'edit_human_resource',
            'delete_human_resource',
            'view_human_resource',

            'create_teacher',
            'edit_teacher',
            'delete_teacher',
            'view_teacher',

            'create_student',
            'edit_student',
            'delete_student',
            'view_student',
            'export_student',

            'confirm_unconfirmed_users',
            'edit_unconfirmed_users',
            'delete_unconfirmed_users',
            'view_unconfirmed_users',
        ];

        $permissionsForStudies = [
            'create_year_of_study',
            'edit_year_of_study',
            'delete_year_of_study',
            'view_year_of_study',

            'create_cycle',
            'edit_cycle',
            'delete_cycle',
            'view_cycle',

            'create_faculty',
            'edit_faculty',
            'delete_faculty',
            'view_faculty',

            'create_speciality',
            'edit_speciality',
            'delete_speciality',
            'view_speciality',

            'create_group',
            'edit_group',
            'delete_group',
            'view_group',

            'create_course',
            'edit_course',
            'delete_course',
            'view_course',

            'create_course_type',
            'edit_course_type',
            'delete_course_type',
            'view_course_type',

            'create_day',
            'edit_day',
            'delete_day',
            'view_day',

            'create_duration_type',
            'edit_duration_type',
            'delete_duration_type',
            'view_duration_type',

            'create_duration',
            'edit_duration',
            'delete_duration',
            'view_duration',

            'create_semester',
            'edit_semester',
            'delete_semester',
            'view_semester',

            'create_week_type',
            'edit_week_type',
            'delete_week_type',
            'view_week_type',

            'create_week',
            'edit_week',
            'delete_week',
            'view_week',

            'create_teacher_rank',
            'edit_teacher_rank',
            'delete_teacher_rank',
            'view_teacher_rank',

            'create_borderou',
            'edit_borderou',
            'delete_borderou',
            'view_borderou',

            'create_borderou_note',
            'edit_borderou_note',
            'delete_borderou_note',
            'view_borderou_note',
            'export_borderou_note',

        ];

        foreach ($permissionsForUsers as $permission) {
            Permission::create(['name' => $permission]);
        }

        foreach ($permissionsForStudies as $permission) {
            Permission::create(['name' => $permission]);
        }
    }
}