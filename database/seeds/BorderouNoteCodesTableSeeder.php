<?php

use App\Entities\BorderouNoteCode;
use Illuminate\Database\Seeder;

class BorderouNoteCodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BorderouNoteCode::create([
            'code' => mb_substr(str_shuffle('0123456789'), 0, 8),
            'default_code' => '10101991CM',
        ]);
    }
}
