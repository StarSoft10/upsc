<?php

use Database\Seeders\PermissionTableSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(CustomDatabaseTableSeeder::class);
         $this->call(PermissionTableSeeder::class);
         $this->call(DaysTableSeeder::class);
         $this->call(StudiesTableSeeder::class);
         $this->call(TeacherRanksTableSeeder::class);
         $this->call(RolesTableSeeder::class);
//         $this->call(UsersTableSeeder::class);
         $this->call(SuperAdminsTableSeeder::class);
         $this->call(AdminsTableSeeder::class);
         $this->call(RectorsTableSeeder::class);
         $this->call(SecretariesTableSeeder::class);
         $this->call(DeansTableSeeder::class);
         $this->call(ChairsTableSeeder::class);
         $this->call(HumanResourcesTableSeeder::class);
         $this->call(TeachersTableSeeder::class);
         $this->call(StudentsTableSeeder::class);
         $this->call(BorderouNoteCodesTableSeeder::class);
    }
}
