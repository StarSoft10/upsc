<?php

use App\Entities\Day;
use Illuminate\Database\Seeder;

class DaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $days = ['1' => 'pages.monday', '2' => 'pages.tuesday', '3' => 'pages.wednesday', '4' => 'pages.thursday',
            '5' => 'pages.friday', '6' => 'pages.saturday', '7' => 'pages.sunday'];

        foreach ($days as $order => $day){
            Day::create(['name' => $day, 'order' => $order]);
        }
    }
}
