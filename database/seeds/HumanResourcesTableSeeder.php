<?php

use App\Entities\HumanResource;
use App\Entities\User;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class HumanResourcesTableSeeder extends Seeder
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $humanResourcePermissions = $this->helper->getAllowedPermissionsByModel('human-resource');

        /*
        * First
        */
        $humanResource = User::create([
            'name' => 'ru1',
            'email' => '7@mail.ru',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('123456')
        ]);
        $humanResource->assignRole('human-resource');
        foreach ($humanResourcePermissions as $permission) {
            $humanResource->givePermissionTo($permission->name);
        }
        HumanResource::create([
            'user_id' => $humanResource->id,
            'first_name' => 'HR nr.1',
            'last_name' => 'HR nr.1',
            'phone' => '123123'
        ]);


        /*
         * Second
         */
    }
}
