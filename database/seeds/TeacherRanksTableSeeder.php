<?php

use App\Entities\TeacherRank;
use Illuminate\Database\Seeder;

class TeacherRanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TeacherRank::create([
            'name' => 'Asistent Universitar',
            'short_name' => 'AU'
        ]);

        TeacherRank::create([
            'name' => 'Doctor',
            'short_name' => 'D'
        ]);

        TeacherRank::create([
            'name' => 'Doctor, Conferențiar Universitar',
            'short_name' => 'DCU'
        ]);

        TeacherRank::create([
            'name' => 'Doctor Habilitat',
            'short_name' => 'DH'
        ]);

        TeacherRank::create([
            'name' => 'Lector',
            'short_name' => 'L'
        ]);

        TeacherRank::create([
            'name' => 'Profesor',
            'short_name' => 'P'
        ]);

        TeacherRank::create([
            'name' => 'Lector universitar',
            'short_name' => 'LU'
        ]);
    }
}
