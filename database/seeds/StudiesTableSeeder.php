<?php

use App\Entities\Course;
use App\Entities\Cycle;
use App\Entities\Faculty;
use App\Entities\Semester;
use App\Entities\Speciality;
use App\Entities\Group;
use App\Entities\TeacherRank;
use App\Entities\YearOfStudy;
use Illuminate\Database\Seeder;

class StudiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 5; $i++){
            YearOfStudy::create(['name' => 'Anul ' . $i]);
        }

        Semester::create(['name' => 'Semestrul 1', 'order' => 1]);
        Semester::create(['name' => 'Semestrul 2', 'order' => 2]);
        Semester::create(['name' => 'Semestrul 3', 'order' => 3]);
        Semester::create(['name' => 'Semestrul 4', 'order' => 4]);
        Semester::create(['name' => 'Semestrul 5', 'order' => 5]);

        Course::create([
            'name' => 'Arte plastice',
            'short_name' => 'AP',
            'code' => 'M001'
        ]);

        $cycle1 = Cycle::create(['name' => 'Ciclul 1', 'short_name' => 'C1']);
        $cycle2 = Cycle::create(['name' => 'Ciclul 2', 'short_name' => 'C2']);

        $faculty1 = Faculty::create(['cycle_id' => $cycle1->id, 'name' => 'Arte Plastice și Design', 'short_name' => 'APD']);
        $faculty11 = Faculty::create(['cycle_id' => $cycle1->id, 'name' => 'Arte Plastice și Design', 'short_name' => 'APD']);

        $faculty2 = Faculty::create(['cycle_id' => $cycle2->id, 'name' => 'Filologie și Istorie', 'short_name' => 'FI']);
        $faculty22 = Faculty::create(['cycle_id' => $cycle2->id, 'name' => 'Filologie și Istorie', 'short_name' => 'FI']);

        $faculty3 = Faculty::create(['cycle_id' => $cycle1->id, 'name' => 'Limbi și Literaturi Străine', 'short_name' => 'LLS']);
        $faculty33 = Faculty::create(['cycle_id' => $cycle1->id, 'name' => 'Limbi și Literaturi Străine', 'short_name' => 'LLS']);

        $faculty4 = Faculty::create(['cycle_id' => $cycle1->id, 'name' => 'Psihologie și psihopedagogie specială', 'short_name' => 'PPS']);
        $faculty44 = Faculty::create(['cycle_id' => $cycle1->id, 'name' => 'Psihologie și psihopedagogie specială', 'short_name' => 'PPS']);

        $faculty5 = Faculty::create(['cycle_id' => $cycle1->id, 'name' => 'Științe ale educației și informatică', 'short_name' => 'SEI']);
        $faculty55 = Faculty::create(['cycle_id' => $cycle1->id, 'name' => 'Științe ale educației și informatică', 'short_name' => 'SEI']);

        $speciality1 = Speciality::create(['cycle_id' => $cycle1->id, 'faculty_id' => $faculty1->id, 'name' => 'Informatica', 'short_name' => 'I']);
        $speciality11 = Speciality::create(['cycle_id' => $cycle1->id, 'faculty_id' => $faculty11->id, 'name' => 'Informatica', 'short_name' => 'F']);

        $speciality2 = Speciality::create(['cycle_id' => $cycle2->id, 'faculty_id' => $faculty2->id, 'name' => 'Limba Română', 'short_name' => 'LR']);
        $speciality22 = Speciality::create(['cycle_id' => $cycle2->id, 'faculty_id' => $faculty22->id, 'name' => 'Arte Plastice', 'short_name' => 'AP']);


        $group1 = Group::create([
            'cycle_id' => $cycle1->id,
            'faculty_id' => $faculty1->id,
            'speciality_id' => $speciality1->id,
            'name' => 'Group nr.1',
            'short_name' => 'G1'
        ]);
        $group11 = Group::create([
            'cycle_id' => $cycle1->id,
            'faculty_id' => $faculty1->id,
            'speciality_id' => $speciality1->id,
            'name' => 'Group nr.11',
            'short_name' => 'G11'
        ]);
        $group111 = Group::create([
            'cycle_id' => $cycle1->id,
            'faculty_id' => $faculty11->id,
            'speciality_id' => $speciality11->id,
            'name' => 'Group nr.111',
            'short_name' => 'G111'
        ]);
        $group1111 = Group::create([
            'cycle_id' => $cycle1->id,
            'faculty_id' => $faculty11->id,
            'speciality_id' => $speciality11->id,
            'name' => 'Group nr.1111',
            'short_name' => 'G1111'
        ]);

        $group2 = Group::create([
            'cycle_id' => $cycle2->id,
            'faculty_id' => $faculty2->id,
            'speciality_id' => $speciality2->id,
            'name' => 'Group nr.2',
            'short_name' => 'G2'
        ]);
        $group22 = Group::create([
            'cycle_id' => $cycle2->id,
            'faculty_id' => $faculty2->id,
            'speciality_id' => $speciality2->id,
            'name' => 'Group nr.22',
            'short_name' => 'G22'
        ]);
        $group222 = Group::create([
            'cycle_id' => $cycle2->id,
            'faculty_id' => $faculty22->id,
            'speciality_id' => $speciality22->id,
            'name' => 'Group nr.222',
            'short_name' => 'G222'
        ]);
        $group2222 = Group::create([
            'cycle_id' => $cycle2->id,
            'faculty_id' => $faculty22->id,
            'speciality_id' => $speciality22->id,
            'name' => 'Group nr.2222',
            'short_name' => 'G2222'
        ]);
    }
}
