<?php

use App\Entities\Secretary;
use App\Entities\User;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class SecretariesTableSeeder extends Seeder
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $secretaryPermissions = $this->helper->getAllowedPermissionsByModel('secretary');

        /*
        * First
        */
        $secretary = User::create([
            'name' => 'secretar1',
            'email' => '4@mail.ru',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('123456')
        ]);
        $secretary->assignRole('secretary');
        foreach ($secretaryPermissions as $permission) {
            $secretary->givePermissionTo($permission->name);
        }
        Secretary::create([
            'user_id' => $secretary->id,
            'first_name' => 'Secretar 1',
            'last_name' => 'Secretar 1',
            'phone' => '123123'
        ]);


        /*
         * Second
         */
    }
}
