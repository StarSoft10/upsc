<?php

use App\Entities\Student;
use App\Entities\User;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class StudentsTableSeeder extends Seeder
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $studentPermissions = $this->helper->getAllowedPermissionsByModel('student');

        $user = User::create([
            'name' => 'Cebotari Mihai',
            'email' => '9@mail.ru',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make(123456)
        ]);
        $user->assignRole('student');
        foreach ($studentPermissions as $permission) {
            $user->givePermissionTo($permission->name);
        }

        Student::create([
            'user_id' => $user->id,
            'cycle_id' => 1,
            'faculty_id' => 1,
            'speciality_id' => 1,
            'group_id' => 1,
            'year_of_study_id' => 3,
            'first_name' => 'Mihai',
            'last_name' => 'Cebotari',
            'patronymic' => 'Igor',
            'idnp' => '12313',
            'gender' => 'M',
            'personal_email' => 'AAAAAA@mail.com',
            'mobile' => '12313',
            'birth_date' => '10/10/1991',
            'city' => 'Chisinau',
            'address' => 'address',
            'citizenship' => 'MD',
            'nationality' => 'MD',
            'year_of_admission' => '2021',
            'study' => 1,
            'budget' => 0,
            'expelled' => 0,
            'additional_info' => 'GOOD'
        ]);


        $user2 = User::create([
            'name' => 'Cebotari Ilie',
            'email' => '10@mail.ru',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make(123456)
        ]);
        $user2->assignRole('student');
        foreach ($studentPermissions as $permission) {
            $user2->givePermissionTo($permission->name);
        }

        Student::create([
            'user_id' => $user2->id,
            'cycle_id' => 1,
            'faculty_id' => 1,
            'speciality_id' => 1,
            'group_id' => 1,
            'year_of_study_id' => 3,
            'first_name' => 'Ilie',
            'last_name' => 'Cebotari',
            'patronymic' => 'Igor',
            'idnp' => '12313',
            'gender' => 'M',
            'personal_email' => 'BBBB@mail.com',
            'mobile' => '12313',
            'birth_date' => '10/10/1991',
            'city' => 'Chisinau',
            'address' => 'address',
            'citizenship' => 'MD',
            'nationality' => 'MD',
            'year_of_admission' => '2021',
            'study' => 1,
            'budget' => 1,
            'expelled' => 0,
            'additional_info' => 'GOOD'
        ]);

//        factory(Student::class, 101)->create();
    }
}