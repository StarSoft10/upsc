<?php

use App\Entities\User;
use App\Entities\Admin;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminsTableSeeder extends Seeder
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminPermissions = $this->helper->getAllowedPermissionsByModel('admin');

        /*
        *
        */
        $admin1 = User::create([
            'name' => 'benzari.mihai',
            'email' => 'mihai.benzari@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Benzari.2021')
        ]);
        $admin1->assignRole('admin');
        foreach ($adminPermissions as $permission) {
            $admin1->givePermissionTo($permission->name);
        }
        Admin::create([
            'user_id' => $admin1->id,
            'first_name' => 'Mihai',
            'last_name' => 'Benzari',
            'phone' => '+37368613980'
        ]);
        /*
        *
        */
        $admin2 = User::create([
            'name' => 'cecan.roman',
            'email' => 'cecan.roman@upsc.md',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('Cecan.2021')
        ]);
        $admin2->assignRole('admin');
        foreach ($adminPermissions as $permission) {
            $admin2->givePermissionTo($permission->name);
        }
        Admin::create([
            'user_id' => $admin2->id,
            'first_name' => 'Roman',
            'last_name' => 'Cecan',
            'phone' => '+37379033966'
        ]);
        /*
        *
        */



    }
}
