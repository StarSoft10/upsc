<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('cycle_id')->nullable();
            $table->unsignedBigInteger('faculty_id')->nullable();
            $table->unsignedBigInteger('speciality_id')->nullable();
            $table->unsignedBigInteger('group_id')->nullable();
            $table->unsignedBigInteger('year_of_study_id')->nullable();
            $table->text('first_name')->nullable();
            $table->text('last_name')->nullable();
            $table->text('patronymic')->nullable();
            $table->text('idnp')->nullable();
            $table->text('gender')->nullable();
            $table->text('phone')->nullable();
            $table->text('mobile')->nullable();
            $table->text('personal_email')->nullable();
            $table->text('birth_date')->nullable();
            $table->text('city')->nullable();
            $table->text('address')->nullable();
            $table->text('district_code')->nullable();
            $table->text('insurance_policy')->nullable();
            $table->text('citizenship')->nullable();
            $table->text('nationality')->nullable();
            $table->text('year_of_admission')->nullable();
            $table->text('stayed')->nullable();
            $table->text('carnet_number')->nullable();
            $table->text('graduate_institution')->nullable();
            $table->text('act_seriously_study')->nullable();
            $table->text('act_number_study')->nullable();
            $table->text('act_graduation_year')->nullable();
            $table->text('study')->nullable();
            $table->text('budget')->nullable();
            $table->text('expelled')->nullable();
            $table->text('additional_info')->nullable();
            $table->integer('blocked')->nullable()->default(0);
            $table->integer('deleted')->nullable()->default(0);
            $table->integer('password_changed')->nullable()->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('cycle_id')->references('id')->on('cycles')->onDelete('cascade');
            $table->foreign('faculty_id')->references('id')->on('faculties')->onDelete('cascade');
            $table->foreign('speciality_id')->references('id')->on('specialities')->onDelete('cascade');
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
            $table->foreign('year_of_study_id')->references('id')->on('year_of_studies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
