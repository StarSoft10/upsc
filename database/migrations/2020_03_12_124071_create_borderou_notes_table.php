<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBorderouNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borderou_notes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('borderou_id')->nullable();
            $table->unsignedBigInteger('student_id')->nullable();
            $table->text('semester_note')->nullable(); //++ manual
            $table->text('semester_score')->nullable(); //formula  semester_note*0.6
            $table->text('exam_note')->nullable(); //++ manual
            $table->text('exam_score')->nullable(); //exam_note*0.4
            $table->text('final_note')->nullable(); ///formula  semester_score+exam_score=
            $table->text('ects')->nullable();
            $table->timestamps();

            $table->foreign('borderou_id')->references('id')->on('borderous')->onDelete('cascade');
            $table->foreign('student_id')->references('id')->on('students')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('borderou_notes');
    }
}
