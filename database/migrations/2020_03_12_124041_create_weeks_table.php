<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeeksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weeks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('week_type_id')->nullable();
            $table->unsignedBigInteger('semester_id')->nullable();
            $table->text('year')->nullable();
            $table->text('start')->nullable();
            $table->text('end')->nullable();
            $table->text('order')->nullable();
            $table->integer('deleted')->nullable()->default(0);
            $table->timestamps();

            $table->foreign('week_type_id')->references('id')->on('week_types')->onDelete('cascade');
            $table->foreign('semester_id')->references('id')->on('semesters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weeks');
    }
}
