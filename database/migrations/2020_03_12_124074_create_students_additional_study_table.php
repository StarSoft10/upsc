<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsAdditionalStudyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students_additional_study', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('student_id')->nullable();
            $table->integer('cycle_id')->nullable();
            $table->integer('faculty_id')->nullable();
            $table->integer('speciality_id')->nullable();
            $table->integer('group_id')->nullable();
            $table->integer('year_of_study_id')->nullable();
            $table->text('year_of_admission')->nullable();
            $table->text('study')->nullable();
            $table->text('budget')->nullable();
            $table->text('expelled')->nullable();
            $table->text('additional_info')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students_additional_study');
    }
}
