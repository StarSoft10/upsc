<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('durations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('duration_type_id')->nullable();
            $table->text('name')->nullable();
            $table->text('start')->nullable();
            $table->text('end')->nullable();
            $table->text('order')->nullable();
            $table->integer('deleted')->nullable()->default(0);
            $table->timestamps();

            $table->foreign('duration_type_id')->references('id')->on('duration_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('durations');
    }
}
