<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('entity_role')->nullable();
            $table->text('entity_id')->nullable();
            $table->text('action_table')->nullable();
            $table->text('log_type')->nullable(); // 1 => Create, 2 => Edit, 3 => Delete, 4 => Block, 5 => Unblock, 6 => Confirm Unconfirmed User
            $table->text('action')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
